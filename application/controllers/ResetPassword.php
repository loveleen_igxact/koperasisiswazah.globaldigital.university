<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResetPassword extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');	// loads admin model

    }
    public function index(){

    	$this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('resetpasswordv');

        $this->load->view('layout/footer');
    	
    	 if (isset($_POST['submit'])) {
    	 	 $whereuserId=$_SESSION['loggedInData']['id'];
    	 	 $password=md5($_POST['password']);
    	 	// $where = array('id' =>$whereuserId,'password' => md5($_POST['password']);

$result = $this->AdminModel->getdatauser($whereuserId,$password);

if (!empty($result)) {
	$where = array('id' =>$whereuserId);
 $datas = array('password' =>md5($_POST['NewPassword']));
 $updatepasword = $this->AdminModel->updateData($where,$datas,'users');
 $to_email=$_SESSION['loggedInData']['email'];
	   	$from_email='adminksbb@lmsmalaysia.com';
	   	$subject='Password changed';
$message='Your password has been reset.';
$this->load->library('email');
            $this->email->from($from_email, 'Password changed');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);
if ($this->email->send()) {
                $this->session->set_flashdata("email_sent", "Email sent successfully.");


               // echo "Message has been sent successfully";
              echo "<script>
              alert('Password changed and Message has been sent successfully');
              window.location.href = 'http://lmsmalaysia.com/';
              </script>";



                // return true;
                 // redirect(base_url('login')); 

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                 echo "failure";
                return false;
            }	

}
else{
	echo "<script>alert('Oops! Password does not match.');</script>";
 
}

    	 	// print_r($_POST['NewPassword']);
    	 }
	 	
	}

}    