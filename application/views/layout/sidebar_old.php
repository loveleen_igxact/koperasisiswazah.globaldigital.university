<?php 
$url =  $this->router->fetch_method(); 
$session_data = $this->session->userdata('loggedInData');
?>
      <div class="left side-menu">
            <div class="slimscroll-menu" id="remove-scroll">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu" id="side-menu">
                        <li class="menu-title">Main</li>
                     <!--   <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span>Dashboard</span></a></li>  -->
                        <?php if ($session_data['user_type'] == '1') { ?>
                         <!--   <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Reports</span></a></li>  -->
                            <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i><span> Marketing <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                                <ul class="submenu">
                                   
                            <li class=""><a class="" href="<?php echo base_url(); ?>application/prospects">Prospects</a></li>
                            <li><a href="<?php echo base_url(); ?>application/application"> Applications </a></li>
                            <li><a href="<?php echo base_url(); ?>application/follw">Follow-up History</a></li>
                            <li><a href="<?php echo base_url(); ?>application/agencies">Agencies</a></li>
                            <li><a href="<?php echo base_url(); ?>application/assign_prospects">Assign Prospects</a></li>
                            <li><a href="<?php echo base_url(); ?>application/agent">Agents</a></li>
                            <li><a href="<?php echo base_url(); ?>application/materials">Materials</a></li>
                            <li><a href="<?php echo base_url(); ?>application/import_csv">Import Prospects </a></li>
                                   
                                </ul>
                            </li>
                        <?php } ?>
                        <!-- <li class="menu-title">Components</li> -->
                          <?php if ($session_data['user_type'] == '1') { ?>
                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-human-handsup"></i> <span> Admissions<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                          
                                <ul class="submenu">
                                    <li><a href="<?php echo base_url(); ?>application/application">Application</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/registration">Registrations</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/intake">Intake</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/admission_session">Academic Sessions</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/interview">Interview Schedules</a></li>
                                    <li><a href="javascript:void(0)">Bulk Status Update</a></li>
                                </ul>
                                 <?php } ?>
                           <!--  
                              <ul class="submenu">
                                    <li><a href="<?php echo base_url(); ?>addmision/registration">Semester Registrations</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/doc_checklist">Document Checklist</a></li>
                                    <li><a href="<?php echo base_url(); ?>addmision/admission_session">Application</a></li>
                                </ul>
                             -->
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-weight"></i><span> Finance <span class="badge badge-pill badge-success float-right">6</span></span></a>
                            <?php if ($session_data['user_type'] == '1') { ?>
                                <ul class="submenu">
                                    <li><a href="<?php echo base_url(); ?>Finance/bills">Bills</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/collection">Collections</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/refunds">Refunds</a></li>
                                    <li><a href="javascript:void(0);">Approvals</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/upload_collection">Group Receipt</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/transactions">Transactions</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/group_billing">Group Billing</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/invoice_reminder">Invoice Reminders</a></li>
                                </ul>
                                <?php } else { ?>
                                <ul class="submenu">
                                    <li><a href="<?php echo base_url(); ?>Finance/statement_of_account">Statement Of Account</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/payment">Payment</a></li>
                                    <li><a href="<?php echo base_url(); ?>Finance/ledger">Ledger</a></li>
                                </ul>
                                <?php } ?>
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Academic<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <?php if ($session_data['user_type'] == '1') { ?>
                           <ul class="submenu">
                                <li><a href="<?php echo base_url(); ?>Acadmic/course_offerd">Offered Course</a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/classes">Classes</a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/attendance">Attendance </a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/marks">Marks</a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/assignments">Assignments</a></li>
                            </ul>
                            <?php } else { ?>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url(); ?>Acadmic/course_offerd">Course List </a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/classes">Add Drop</a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/attendance">Attendance & Coursework Status </a></li>
                                <li><a href="<?php echo base_url(); ?>Acadmic/assignments">Assignments</a></li>
                            </ul>
                            <?php } ?>
                        </li>
                        <!--   <?php if ($session_data['user_type'] == '1') { ?>
                     <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-format-list-bulleted-type"></i><span> Exam <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                           
                            <ul class="submenu">
                                <li><a href="javascript:void(0);">Page1</a></li>
                                <li><a href="javascript:void(0);">Page2</a></li>
                                <li><a href="javascript:void(0);">Page3</a></li>
                                <li><a href="javascript:void(0);">Page4</a></li>
                                <li><a href="javascript:void(0);">Page5</a></li>
                            </ul>
                         
                            <ul class="submenu">
                                <li><a href="javascript:void(0);">Exam Slip</a></li>
                                <li><a href="javascript:void(0);">Exam Result</a></li>
                            </ul>
                            <?php } ?>
                        </li>    -->
                     
                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-album"></i> <span>Annoucements <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url(); ?>Announcement">View All</a></li>
                                   <?php if ($session_data['user_type'] == '1') { ?>
                                 <li><a href="<?php echo base_url(); ?>announcement/create_announcement">Add New </a></li>
                                   <?php } ?> 
                            </ul>
                        </li>
                       <!-- <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-maps"></i><span> Settings <span class="badge badge-pill badge-danger float-right">2</span></span></a>
                            <ul class="submenu">
                                <li><a href="javascript:void(0);">Page1</a></li>
                                <li><a href="javascript:void(0);">Page2</a></li>
                                <li><a href="javascript:void(0);">Page3</a></li>
                                <li><a href="javascript:void(0);">Page4</a></li>
                                <li><a href="javascript:void(0);">Page5</a></li>
                            </ul>
                        </li>  -->
                      

                      <!--  <?php if ($session_data['user_type'] == '3') { ?>
                            <li><a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Timetables</span></a>
                            <ul class="submenu">
                                <li><a href="javascript:void(0);">View</a></li>
                            </ul>
                        </li>
                        <?php } ?>  -->
                         <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-album"></i> <span>Teachers <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url(); ?>Teacher">View All</a></li>
                                <?php if ($session_data['user_type'] == '1') { ?>
                                <li><a href="<?php echo base_url(); ?>Teacher">Add New</a></li>
                            <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- Sidebar -->
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -left -->
        </div>
     
 
