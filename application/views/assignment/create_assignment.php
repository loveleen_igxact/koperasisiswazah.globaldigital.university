    <?php $session_data = $this->session->userdata['loggedInData']; ?>
    <style type="text/css">
        .image-upload img {
        cursor: pointer;
    }
        .image-upload >input {
        display: none;
    }
        .image-upload p {
        position: absolute;
        top: 33px;
        left: 69px;
        font-size: 14px;
    }

    .image-upload {
        background-color: #f9f9fb;
        padding: 8px;
        border: 1px solid #cccccc;
        position: relative;
    }
    .left_right_content p {
        font-size: 15px;
    }
    .uploading_images {
        background-color: #f9f9fb;
        margin: 10px auto;
        padding: 7px;
        border: 1px solid #cccc;
    }
    .no_margin_bottom {
        margin-bottom: 4px;
    }
    .uploading_images {
        background-color: #f9f9fb;
        margin: 10px auto;
        padding: 7px;
        overflow-y: scroll;
        border: 1px solid #cccc;
        overflow-x: hidden;
        height: 223px;
    }
    .outer_formsts{
        background-color: #f4f4f4;
        padding: 6px;
    }
    .outer_select {
            border: 1px solid #d0cfcf;
            height: 135px;
            overflow: scroll;
            position: relative;
            padding: 0px 33px;
            /*overflow-x: hidden;*/
        }
        .bottom_up {
            margin: 8px auto;
        }
        .padding_bottom {
             padding-bottom: 10px;
        }
        .dxf {
    position: relative;
}
.calendar {
    position: absolute;
    bottom: 6px;
    right: 4px;
}
    </style>
    <link href="<?php echo  CSS_PATH; ?>css/bootstrap_multiselect.css" rel="stylesheet" type="text/css">
    <div class="content-page">
        <div class="content">
             <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                           <!--  <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>UPLOAD DOCUMENTS</h4>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                <li class="breadcrumb-item active">Upload Documents</li>
                            </ol> -->
                        </div>
                    </div>
                </div>
            </div> 
        <div class="card m-b-30">
            <div class="card-body">
                <form action="<?php echo base_url(); ?>teacher/upload_documents" class="" enctype="multipart/form-data" method="post">
                <h4 class="mt-0 header-title">Create Assignment</h4>
                <div class="m-b-30">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left_right_content pt-3">
                                <p class="float-left" >Upload Files</p>
                                <p class="float-right">Size Limit 1 GB</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="image-upload">
                                <label for="file-input">
                                    <img src="http://lmsmalaysia.com/public/images/up_folder.png">
                                    <p><span style="color: #2091de;">Browse</span>Your files</p>
                                </label>
                                <input type="file" id="file-input" name="fileToUpload[]" required="" multiple=""><span></span>
                            </div>
                            <!-- <div class="outer_uploading">
                                <h6>Uploading Files</h6>
                                <div class="uploading_images">
                                    <div class="row">
                                        <div class="col-md-1 col-sm-2 col-xs-12">
                                            <img src="http://lmsmalaysia.com/public/images/jpg_icon.png">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-12 ">
                                            <p class="no_margin_bottom " >Image_1225.jpg<br>27 MB of 4.1 MB </p>
                                            <div class="progress">
                                            <div class="progress-bar jpg_bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-sm-2 col-xs-12 pt-3">
                                            <img src="http://lmsmalaysia.com/public/images/png_icon.png">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-12 pt-3 ">
                                            <p class="no_margin_bottom " >Image_1225.jpg<br>27 MB of 4.1 MB </p>
                                            <div class="progress">
                                            <div class="progress-bar png_bar"   role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-sm-2 col-xs-12 pt-3">
                                            <img src="http://lmsmalaysia.com/public/images/jpg_icon.png">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-12 pt-3 ">
                                            <p class="no_margin_bottom " >Image_1225.jpg<br>27 MB of 4.1 MB </p>
                                            <div class="progress">
                                            <div class="progress-bar jpg_bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                         <div class="col-md-1 col-sm-2 col-xs-12 pt-3">
                                            <img src="http://lmsmalaysia.com/public/images/jpg_icon.png">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-12 pt-3 ">
                                            <p class="no_margin_bottom " >Image_1225.jpg<br>27 MB of 4.1 MB </p>
                                            <div class="progress">
                                            <div class="progress-bar jpg_bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                         <div class="col-md-1 col-sm-2 col-xs-12 pt-3">
                                            <img src="http://lmsmalaysia.com/public/images/jpg_icon.png">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-12 pt-3 ">
                                            <p class="no_margin_bottom " >Image_1225.jpg<br>27 MB of 4.1 MB </p>
                                            <div class="progress">
                                            <div class="progress-bar jpg_bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-12 pt-2">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control"  placeholder="" name=""> 
                                    </div>
                                </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea type="text" class="form-control" rows="4"  placeholder="" name=""></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group dxf ">
                                    <label>Date</label>
                                    <input type="text" class="form-control " placeholder="" name="">
                                    <span class="calendar"><img src="http://lmsmalaysia.com/public/images/calendar.png"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group dxf">
                                    <label>Time</label>
                                    <input type="text" class="form-control" placeholder="" name="">
                                    <span class="calendar"><img src="http://lmsmalaysia.com/public/images/calendar.png"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-check-label padding_bottom ">Select Student</label>
                                <div class="outer_select">
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 1
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 2
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 3
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 4
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 5
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 6
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 7
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> Option 8
                                        </label>
                                    </p>
                                </div>
                            </div>
                
                            <div class="col-md-2">
                                <label class="form-check-label padding_bottom">Assign to Class</label>
                                <div class="outer_select">
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 4th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 5th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 6th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 7th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 8th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 9th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 10th
                                        </label>
                                    </p>
                                    <p class="bottom_up">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value=""> 11th
                                        </label>
                                    </p>
                                </div>
                            </div>
                        <div class="col-md-5">
                            <label class="form-check-label padding_bottom">Assign to Group</label>
                            <div class="outer_select">
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 1
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 2
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 3
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 4
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 5
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 6
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 7
                                    </label>
                                </p>
                                <p class="bottom_up">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" value=""> Option 8
                                    </label>
                                </p>
                            </div>
                        </div>

                        </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-4">
                                <div class="outer_formsts">
                                    <h6><strong> Supported file formats</strong></h6>
                                    <p><strong>Image: jpg - png - gif</strong></p>
                                    <p>Document: ppt - pptx - xls - doc<br>
                                        docx - rtf - pdf</p>
                                    <p>Video: wav - wma - mp3 - mov - avi<br>
                                        mpeg - wmv mp4 - flv</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-left m-t-15">
                    <button type="submit" name="submit" class="btn color_another btn-lg waves-effect waves-light">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>

    <script type="text/javascript">
      $(function () {
            $('#lstFruits').multiselect({
                includeSelectAllOption: true
            });
            $('#btnSelected').click(function () {
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                });
            });
        });
    </script>