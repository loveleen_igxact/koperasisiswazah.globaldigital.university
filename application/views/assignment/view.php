<style type="text/css">
    ul.assig_list li::before {
    content: '';
    height: 6px;
    width: 6px;
    background: #606cb3;
    display: block;
    position: absolute;
    transform: rotate(45deg);
    top: 6px;
    left: 0;
}

ul.assig_list li {
    margin: 0;
    margin-bottom: 1em;
    padding-left: 1.5em;
    list-style: none;
    position: relative;
}
ul.assig_list {
    padding: 0px;
}
.font-size {
    font-size: 14px;
    letter-spacing: 2px;
}
.td_img img {
    border: 1px solid #e7eaed;
    width: 100%;
    padding: 7px;
}
.icon_size i {
    font-size: 18px;
    margin-top: 6px;
}
.li_bg {
    background-color: #f3f3f3;
    border-right: 1px solid #e4e4e4;
}
.li_bg a {
    font-size: 14px;
}
.nav-pills .nav-link, .nav-tabs .nav-link {
    color: #606cad;
    font-weight: 500;
}
#profile {
    height: 513px;
    overflow-y: scroll;
}
.add_more {
    font-size: 15px;
    width: 180px;
}
.icons_all {
    padding: 0px;
    width: 100%;
}
</style>
 <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Application</h4>-->
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active">New Application</li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- end row -->
                        
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="add_more">
                                        <ul class="icons_all">
                                            <li>
                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="">
                                                    <img src="http://lmsmalaysia.com/public/images/edit_edit.png"> Edit 
                                            </a>
                                            </li>
                                            <li>
                                                <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="">
                                                    <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"> Delete
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                        <div class="row">
                                            <div class="card-body">
                                            <h4 class="mt-0" style="text-transform: uppercase; letter-spacing: 2px;">Nam eget laoreet sem Nullam rhoncus sed metus</h4>
                                            <p class="font-size"><span><img src="http://lmsmalaysia.com/public/images/calander.png">&nbsp 10/05/2018 To 11/02/2019, 03:00PM</span>
                                               <p><strong>Status : <span style="color: #febb14"> Pending</span></strong></p>
                                            <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item li_bg">
                                                <a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true">OVERVIEW</a>
                                            </li>
                                            <li class="nav-item li_bg">
                                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false">ATTACHMENTS</a>
                                            </li>
                                            <li class="nav-item li_bg">
                                                <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-selected="false">SUBMISSION</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane p-3 active show" id="home" role="tabpanel">
                                               <div class="list_task">
                                                    <h5>Lorem Ipsum is simply dummy text </h5>
                                                    <ul class="assig_list">
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                    </ul>
                                                </div>
                                                <div class="pera_graph">
                                                    <h5>Lorem Ipsum is simply dummy text </h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                </div>
                                                <div class="list_task">
                                                    <h5>Lorem Ipsum is simply dummy text </h5>
                                                    <ul class="assig_list">
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                         <tr>
                                                          <td style="width: 7%" class="td_img">
                                                            <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                          </td>
                                                          <td class="text-left">
                                                              <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                              <p>Uploaded 18/08/219 <br>29GB</p>
                                                          </td>
                                                          <td class="text-right">
                                                            <ul class="icons_all">
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                          </td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                            </div>
                                            <div class="tab-pane p-3" id="messages" role="tabpanel">
                                                <p class="mb-0">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                               <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img (32).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                               <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img (32).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                   <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(8).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                   <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img (32).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="media m-b-30">
                                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img (20).jpg" alt="Generic placeholder image" height="80" width="80">
                                                                <div class="media-body">
                                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp 10 Jan 2019</span>
                                                                        <br>
                                                                        <span class="icon_size"><i class="far fa-file-image"></i>&nbsp Files: 13</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>