 <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Finance / Account Statement </h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Account Statement</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Account Statement </h4>
                                        </div>
                                        
                                     
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>Bill No</th>
                                                    <th class="th-sm">Date
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Doc No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Description
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Amount(RM)
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Paid(RM)
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>

                                                    <th class="th-sm">Payment Method
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Balance (RM)
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Payment Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php 
                                                        $i = 1;
                                    foreach($bill_data as $det):
                                  
                                            ?>
                                                    <tr scope="row">
                                                    <td><?php echo $det['bill_id']; ?></td>  
                                                    <td><?php echo $det['created_at']; ?></td>
                                                    <td><?php echo $det['doc_no']; ?></td>
                                                    <td><?php echo $det['description']; ?></td>
                                                    <td><?php echo $det['amount']; ?></td>
                                                    <td><?php echo $det['paid']; ?></td>
                                                    <td><?php 
                                                    foreach ($payment_methods as $key => $value) {
                                                       
                                                        if ($value['id']==$det['method_id']) {
                                                            echo $value['name'];
                                                           
                                                        }
                                                    }

                                                    ?></td>
                                                    
                                                    <td><?php echo $det['balance']; ?></td>
                                                    <td>Done</td>
                                                        </tr>
                                                      <?php endforeach; ?>
                                                    
                                                     
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>