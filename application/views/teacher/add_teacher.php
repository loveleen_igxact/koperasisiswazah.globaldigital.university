<div class="content-page">
    <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add Teacher</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                        <li class="breadcrumb-item active">Add New Teacher</li>
                    </ol>
                </div>
            </div>
        </div>
        <form action="<?php echo base_url(); ?>Teacher/add_teacher" method="POST">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div style="clear:both"></div>
                            <div class="row">
                                <div class="personal col-12">
                                    <h4>Personal Information</h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name<span style="color: red;">*</span></label>
                                        <input type="text" class="form-control " required="" placeholder="" name="name"> 
                                     </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email<span style="color: red;">*</span></label>
                                        <input name="email" type="Email"  class="form-control " required="" placeholder=""> 
                                     </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone<span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" name="phone" required="" placeholder=""> 
                                     </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><strong>Select Department<span style="color: red;">*</span></strong></label>
                                        
                                            <select class="form-control" tabindex="-1" aria-hidden="true" required name="department">
                                            <?php if (!empty($departments)) {
                                                foreach ($departments as $key => $val) { ?>
                                                    <option value="<?php echo $val['id']; ?>"><?php echo $val['name']; ?></option>
                                            <?php } } ?>
                                            </select>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="text-center">
                                            <input type="submit" name="submit" value="submit" class="btn btn-lg btn-primary">
                                             <a href="#" class="btn btn-lg btn-primary">Cancel</a>
                                        </div>
                                    </div>
                                </div>