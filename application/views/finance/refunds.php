
        <div class="modal fade" id="adjustment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">Add New Collection</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Doc No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                     <th class="th-sm"> Item
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Description
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Max Amount
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Full
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Amount
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>IVX02675</td>
                                                    <td>YURAN PENGAJIAN (UKM)</td>
                                                    <td>YURAN PENGAJIAN (UKM) - YURAN SEMESTER 1</td>
                                                    <td>6800</td>
                                                    <td>
                                                        <input type="checkbox" name="">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="form-group">

                        <label class="control-label"><strong> Credit Note Type</strong></label>

                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                <option>Please Select</option>

                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->

                                    <option value="AK">option</option>

                                    <option value="HI">option</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            <label><strong>Date Issue</strong></label>
                            <div>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                    <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                                <!-- input-group -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            <label><strong>Total</strong></label>
                           <input type="text" class="form-control" name="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-group">
                            <label><strong>Remark</strong></label>
                           <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-lg">Save</button>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Begin page -->
    <div id="wrapper">
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Finance / Refunds</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Refunds</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="search ">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" class="form-control serch" required="" placeholder="Search by Student Name or Document No..."> 
                                                        <span class="serch_icon"><button class="btn btn-dark" type="button">Search</button></span>
                                                     </div>
                                                </div>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Refunds More Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Account</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">option</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                            <label><strong> Date Issued</strong></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="start" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="end" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Status</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                    <option value="AK">option</option>
                                                    <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 pt-2">
                                        <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>
                                    </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>     
                            </div>
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <!-- <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Bills</h4>
                                        </div> -->
                                            <!-- <div class="add_more">
                                                <a href="create_bill.html">
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Add New</button>
                                                </a>
                                                <a href="group_billing.html">
                                                    <button type="button" class="btn btn-dark waves-effect waves-light">Add Group Biling</button>
                                                </a> 
                                            </div> -->
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Date Issued
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                     <th class="th-sm">Document
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Student Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">ID No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Description
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Amount
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                    <td>-----</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>      
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
