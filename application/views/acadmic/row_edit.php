  <link href="<?php echo PLUG_PATH; ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
<form action="<?php echo  base_url('Acadmic/add_classes/'. @$edit_data[0]['id']);?> " method="POST" >
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">New Class</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Academic Session<span style=" color: red;">*</span></strong></label>
                            <select name="academic_session" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option value="">Select</option>
                                   <option value="1" <?php echo  (@$edit_data[0]['academic_session']  == '1') ?'selected' :'' ?> >2017/03 - MAC 2017</option>
									<option value="2" <?php echo  (@$edit_data[0]['academic_session']  == '2') ?'selected' :'' ?> >2017/03 - MEI - JUN 2017</option>
									<option value="3" <?php echo  (@$edit_data[0]['academic_session']  == '3') ?'selected' :'' ?> >2018/03 - MAC 2018</option>
									<option value="4" <?php echo  (@$edit_data[0]['academic_session']  == '4') ?'selected' :'' ?> >2018/03 - MARCH - JULY 2018</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Faculty<span style=" color: red;">*</span></strong></label>
                            <select name="faculty" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option value="">Select</option>
							  <option value="1" <?php echo  (@$edit_data[0]['faculty']  == '1') ?'selected' :'' ?>>GG - FAKULTI PENDIDIKAN</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Course<span style=" color: red;">*</span></strong></label>
                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="course" required="" >
                               <option value="">Select</option>
                               <option value="1" <?php echo  (@$edit_data[0]['course']  == '1') ?'selected' :'' ?>>GGGA6213 - TEORI DAN AMALAN KURIKULUM</option>
							   <option value="2" <?php echo  (@$edit_data[0]['course']  == '2') ?'selected' :'' ?>>GGGA6243 - PERANCANGAN KURIKULUM</option>
							   <option value="3" <?php echo  (@$edit_data[0]['course']  == '3') ?'selected' :'' ?>>GGGB6012 - PENULISAN AKADEMIK 1</option>
							   <option value="4" <?php echo  (@$edit_data[0]['course']  == '4') ?'selected' :'' ?>>GGGB6013 - KAEDAH PENYELIDIKAN 1</option>
							   <option value="5" <?php echo  (@$edit_data[0]['course']  == '5') ?'selected' :'' ?>>GGGB6022 - PENULISAN AKADEMIK2</option>
							   <option value="6" <?php echo  (@$edit_data[0]['course']  == '6') ?'selected' :'' ?>>GGGB6023 - KAEDAH PENYELIDIKAN 2</option>
							   <option value="7" <?php echo  (@$edit_data[0]['course']  == '7') ?'selected' :'' ?>>GGGB6133 - TEORI-TEORI KESIHATAN MENTTAL</option>
							   <option value="8" <?php echo  (@$edit_data[0]['course']  == '8') ?'selected' :'' ?>>GGGB6213 - PSIKOLOGI PENGAJARAN DAN PEMBELAJARAN LANJUTAN</option>
							   <option value="9" <?php echo  (@$edit_data[0]['course']  == '9') ?'selected' :'' ?>>GGGB6223 - PSIKOLOGI PERKEMBANGAN LANJUTAN</option>
							   <option value="10" <?php echo  (@$edit_data[0]['course']  == '10') ?'selected' :'' ?>>GGGB6243 - DINAMIKA KUMPULAN DAN PERFUNGSIAN MENTAL</option>
							   <option value="11" <?php echo  (@$edit_data[0]['course']  == '11') ?'selected' :'' ?>>GGGB6253 - TEORI DAN AMALAN KAUNSELING</option>
							   <option value="12" <?php echo  (@$edit_data[0]['course']  == '12') ?'selected' :'' ?>>GGGB6263 - ISU-ISU DALAM PENDIDIKAN KHAS</option>
							   <option value="13" <?php echo  (@$edit_data[0]['course']  == '13') ?'selected' :'' ?>>GGGB6273 - TEORI PERKEMBANGAN KANAK-KANAK</option>
							   <option value="14" <?php echo  (@$edit_data[0]['course']  == '14') ?'selected' :'' ?>>GGGB6293 - TEORI DAN PROSES PERKEMBANGAN KANAK-KANAK DENGAN KEPERLUAN KHAS</option>
							   <option value="15" <?php echo  (@$edit_data[0]['course']  == '15') ?'selected' :'' ?>>GGGB6333 - TEORI DALAM PENGUKURAN DAN PENILAIAN</option>
							   <option value="16" <?php echo  (@$edit_data[0]['course']  == '16') ?'selected' :'' ?>>GGGB6383 - PERKEMBANGAN KERJAYA</option>
							   <option value="17" <?php echo  (@$edit_data[0]['course']  == '17') ?'selected' :'' ?>>GGGB6413 - PSIKOLOGI KEMAHIRAN BERFIKIR</option>
							   <option value="18" <?php echo  (@$edit_data[0]['course']  == '18') ?'selected' :'' ?>>GGGB6423 - PSIKOLOGI SILANG BUDAYA</option>
							
							   
					</select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Class<span style="color: red;">*</span></strong></label>
                       <input type="text" name="class" class="form-control serch" required="" placeholder="Example:ABC 123-1" value="<?php echo  @$edit_data[0]['class'];   ?>">
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Lecturer</strong> </label>
                        <input type="text" name="lecturer" class="form-control serch"  placeholder="Please Select" value="<?php echo  @$edit_data[0]['lecturer'];   ?>">
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Total Student</strong></label>
                        <input type="number" name="total_student" class="form-control serch"  placeholder="" value="<?php echo  @$edit_data[0]['total_student'];   ?>">
                     </div>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong> Duplicate</strong></label><br>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">

                        <label class="btn btn-full active">
                        <input type="radio" name="duplicate" id="option2"  value="1"> Yes </label>
                        <label class="btn btn-full ">
                        <input type="radio" name="duplicate" id="option3" value="0"> No</label>
                    </div>
                </div>
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            
          </div>
        </div>
      </div>
      
      </form>
    <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>
    <script src="<?php echo JS_PATH;?>plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH; ?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo CSS_PATH;?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH;?>js/app.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.js"></script>
