<!--<div class="static_mneu_bar">
    <div class="slimscroll-menu" id="remove-scrolls">
        <div id="sidebar-menu_2">
            <ul class="metismenu" id="">
                <li class="upper_heading" ><strong>General</strong></li>
                <!-- <li class="menu-title">Main</li> 
                <li><a  href="system_preference.html" class="waves-effect"><span class="badge badge-primary badge-pill float-right"></span> <span> System Preference</span></a></li>
                <li><a href="organization.html" class="waves-effect"><span> Organization Information</span></a></li>
                <li><a href="department.html" class="waves-effect"><span> Department</span></a></li>
                <li><a class="active" href="staff.html" class="waves-effect"><span> Staff</span></a></li>
                <li><a href="user.html" class="waves-effect"><span> User</span></a></li>
                <li><a href="access_level.html" class="waves-effect"><span> Access Level</span></a></li>
                <li><a href="phrase.html" class="waves-effect"><span> Phrase</span></a></li>
                <li><a href="references.html" class="waves-effect"><span> References</span></a></li>
                <li><a href="barring.html" class="waves-effect"><span> Barring</span></a></li>
                <li><a href="Campus.html" class="waves-effect"><span> Campus</span></a></li>
            </ul>
        </div>
    </div>
</div>-->
        <div class="content-page">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Edit Staff </h4>
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active"> New Staff </li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    
                    <!-- end row -->
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="col-md-12 hearding pt-3">
                                <h4 class="mt-0 header-title left_side">Edit Staff Form</h4>
                            </div>
                            <hr>
                                <div class="card-body">
                                    <form method="post">
                                    <div class="row">
                                        <div id="prospect"></div>
                                        <div style="clear:both"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Name<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="Example : Azmirul Ahmad" name="name" value="<?php echo $staff['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Staff No<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="" name="staff_no" value="<?php echo $staff['staff_no']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Identity Card<span style="color: red;">*</span></label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="id_type">
                                                    <option> Please Select</option>
                                                    <?php if (!empty($id_types)) { 
                                                        foreach ($id_types as $key => $values) { ?>
                                                    <option <?php if ($staff['identification_type_id'] == $values['id']) { ?> selected
                                                    <?php } ?> value="<?php echo $values['id']; ?>"><?php echo $values['name']; ?> </option>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ID No<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="" name="id_no" value="<?php echo $staff['id_no']; ?>">
                                                <!-- <span>This will be used as a password for first time creating a staff.</span> -->
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email<span style="color: red;">*</span></label>
                                                <input type="email" name="email" class="form-control " required="" placeholder="" value="<?php echo $staff['email']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone<span style="color: red;">*</span></label>
                                                <input type="text" name="phone" class="form-control " required="" placeholder="" value="<?php echo $staff['phone']; ?>">
                                            </div>
                                        </div>
                                       <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Campus<span style="color: red;">*</span></label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="campus">
                                                    <option > Please Select</option>
                                                    <?php if (!empty($campuses)) { 
                                                        foreach ($campuses as $key => $campus) { ?>
                                                        <option <?php if ($staff['campus_id'] == $campus['id']) { ?> selected
                                                    <?php } ?> value="<?php echo $campus['id']; ?>"><?php echo $campus['name']; ?> </option>
                                                    <?php }
                                                    } ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Faculty</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="faculty">
                                                    <option> Please Select</option>
                                                    <?php if (!empty($faculty)) { 
                                                        foreach ($faculty as $key => $value) { ?>
                                                    <option <?php if ($staff['faculty_id'] == $value['id']) { ?> selected
                                                    <?php } ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?> </option>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="department_id">
                                                <option> Please Select</option>
                                                <?php if (!empty($departments)) {
                                                    foreach ($departments as $key => $val) { ?>
                                                        <option <?php if ($staff['department_id'] == $val['id']) { ?> selected
                                                    <?php } ?> value="<?php echo $val['id']; ?>"><?php echo $val['name']; ?></option>
                                                <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="position">
                                                    <option> Please Select</option>
                                                     <?php if (!empty($positions)) {
                                                    foreach ($positions as $key => $vals) { ?>
                                                        <option <?php if ($staff['position_id'] == $vals['id']) { ?> selected
                                                    <?php } ?> value="<?php echo $vals['id']; ?>"><?php echo $vals['name']; ?></option>
                                                <?php } } ?>
                                                </select>
                                            </div>
                                        </div> -->
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Class</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="class">
                                                    <option> Please Select</option>
                                               
                                                        <option <?php if ($staff['class_id'] == 1) { ?> selected
                                                    <?php } ?> value="1">first</option>
                                                    <option <?php if ($staff['class_id'] == 2) { ?> selected
                                                    <?php } ?> value="2">second</option>
                                                         <option <?php if ($staff['class_id'] == 3) { ?> selected
                                                    <?php } ?> value="2">third</option>
                                              
                                                </select>
                                            </div>
                                        </div>
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="subject">
                                                    <option> Please Select</option>
                                               
                                                        <option <?php if ($staff['subject_id'] == 1) { ?> selected
                                                    <?php } ?> value="1">java</option>
                                                    <option <?php if ($staff['subject_id'] == 2) { ?> selected
                                                    <?php } ?> value="2">php</option>
                                                         <option <?php if ($staff['subject_id'] == 3) { ?> selected
                                                    <?php } ?> value="3">android</option>
                                              
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 pt-3 text-center">
                                            <button class="btn color_another btn-lg" type="submit" name="update">Update</button>
                                            <button class="btn btn-secondary btn-lg" type="submit">Cancel</button>
                                        </div>
                                    </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>