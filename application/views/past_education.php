<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
    <link rel="shortcut icon" href="assets/images/fav_lms.png">
     <link href="<?php echo PLUG_PATH;?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH;?>/select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH;?>/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/style1.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH;?>/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="modal fade" id="new_log" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">New Past Education</h5>
            </div>
           
          </div>
          <hr>
          <div class="modal-body">
		  <form action="<?php echo base_url('application/add_education/'.$id.'/nostep/add');?>" method="post">
           <div class="container">
            <div class="row">
                <hr>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Qualification<span id="" style="color: red;">*</span> : <span id="post_job_qua">Diploma</span></strong></label>
                    </div>
                </div>
				<input type="hidden" name="education_id" id="education_id" value="">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Institute<span style="color: red;">*</span></strong></label>
                            <select name="institute" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Select</option>
                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                           <option value=""></option>
									<option value="22">INSTITUT PENDIDIKAN GURU (IPG)</option>
									<option value="24">OTHERS</option>
									<option value="8">UNIVERSITI ISLAM ANTARABANGSA MALAYSIA (UIAM)</option>
									<option value="5">UNIVERSITI KEBANGSAAN MALAYSIA (UKM)</option>
									<option value="3">UNIVERSITI MALAYA (UM)</option>
									<option value="20">UNIVERSITI MALAYSIA KELANTAN (UMK)</option>
									<option value="17">UNIVERSITI MALAYSIA PAHANG (UMP)</option>
									<option value="18">UNIVERSITI MALAYSIA PERLIS (UNIMAP)</option>
									<option value="10">UNIVERSITI MALAYSIA SABAH (UMS)</option>
									<option value="2">UNIVERSITI MALAYSIA SARAWAK (UNIMAS)</option>
									<option value="14">UNIVERSITI MALAYSIA TERENGGANU (UMT)</option>
									<option value="11">UNIVERSITI PENDIDIKAN SULTAN IDRIS (UPSI)</option>
									<option value="21">UNIVERSITI PERTAHANAN NASIONAL MALAYSIA (UPNM)</option>
									<option value="6">UNIVERSITI PUTRA MALAYSIA (UPM)</option>
									<option value="12">UNIVERSITI SAINS ISLAM ANTARABANGSA (USIM)</option>
									<option value="4">UNIVERSITI SAINS MALAYSIA (USM)</option>
									<option value="23">UNIVERSITI SULTAN AZLAN SHAH (USAS)</option>
									<option value="19">UNIVERSITI SULTAN ZAINAL ABIDIN (UNISZA)</option>
									<option value="16">UNIVERSITI TEKNIKAL MALAYSIA MELAKA (UTEM)</option>
									<option value="7">UNIVERSITI TEKNOLOGI MALAYSIA</option>
									<option value="13">UNIVERSITI TEKNOLOGI MARA (UITM)</option>
									<option value="15">UNIVERSITI TUN HUSSEIN ONN (UTHM)</option>
									<option value="9">UNIVERSITI UTARA MALAYSIA (UUM)</option>
									<option value="-1">Others</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Program</strong></label>
                        <input type="text" name="program" class="form-control " required="" placeholder="Diploma In Multimedia"> 
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Grade/CGPA</strong></label>
                        <input type="text" name="grade"  class="form-control " required="" placeholder="A / 3.00"> 
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Completion Year</strong></label>
                        <input type="text" name="completion_year" class="form-control " required="" placeholder="2000"> 
                     </div>
                </div> 
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Remarks</label>
                        <input type="text" class="form-control " name="remark" required="" placeholder="Remarks"> 
                     </div> 
                </div> 
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-lg btn-primary">Save</button>
            <button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">Cancel</button>
            
          </div>
        </div>
		
	</form>
      </div>
    </div>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>

                        </form>
                    </li>
                    <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary"> Advance Search</button>
                            </a>
                        </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <strong class="admin_name">Admin</strong> <img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a><a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
        
        <?php 
        $this->load->view('layout/sidebar'); 
        $this->load->view('layout/stickmenu'); 
        
        ?>
         <div class="content-page" style="margin-left: 460px">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Past Education</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active"> Past Education</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="main-img-preview">
                                                <img class="thumbnail img-preview" src="assets/images/small/img-7.jpg" title="Preview Logo" style="width: 390px;">
                                            </div>
                                            <div class="input-group">
                                                <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->
                                                <div class="input-group-btn" style="width: 100%;">
                                                    <div class="fileUpload btn btn-danger fake-shadow">
                                                        <span ><i class="mdi mdi-camera"  style="font-size: 20px"></i></span>
                                                        <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p><strong>NORULAIN BINTI ABDULLAL</strong></p>
                                            <P>Identification : 9874561230</P>
                                            <P>Certificate :</P>
                                             <!--<P>Student No : GP12425</P>
                                            <P class="sas" style="color: red;" >Student No : MARCH - JULY 2018(2018/3)</P>
                                            <P>Student No : Active</P> -->
                                        </div>
                                        <!-- <div class="col-md-4" style="padding-top: 20px">
                                            <P>Faculty : xyz</P>
                                            <P>Program : xyz</P>
                                            <P>Intake : 2018/03</P>
                                            <P>Semester : 1</P>  
                                        </div> -->
                                    </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                            </div>
                        </div>
                            <!---card 2 table-->
								<div class="card m-b-30">
									<div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Past Education </h4>
                                        </div>
                                       <div class="add_more">
                                             <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            
                                                <button type="button" id="add_eduction"  class="btn btn-dark waves-effect waves-light" data-toggle="modal" data-target="#new_log">Add New</button>
                                            
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="ds">
                                            <button type="button" class="btn btn-info waves-effect waves-light main" onclick="selectAll()" value="Select All">Select All</button>
                                            <a href="#">
                                                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="UnSelectAll()" value="Unselect All">Clear</button>
                                            </a>
                                        </div> 
                                        <hr>
                                        <div class="clearfix"></div> 
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                  <label class="control-label"><strong>Eduction Level <span style="color: red;">*</span></strong></label>
                                                <select id="educatiion_level" onChange="past_education(this.value, this.text);" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option value="">Select</option>
                                                  <option value="8">Degree</option>
                                                <option value="7">Diploma</option>
                                                <option value="9">Masters</option>
                                                    
                                                </select>
                                            </div>
                                        </div>  
                                         <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                    <th></th>
                                                    <th>No</th>
                                                    <th class="th-sm">Institute
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Qualification
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Grade/GPA
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Year
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="materialUnchecked" name="acs" value="Mobile">
                                                    </div>
                                                    </td>
                                                    <th scope="row">1</th>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td> <!-- <a href="update_marketing.html" style="margin: 10px auto;">
                                                        <button data-toggle="tooltip" class="btn" data-placement="top"  title="" data-original-title="Update"><i class="mdi mdi-table-edit" style="font-size:25px;"></i>
                                                        </button>
                                                    </a> -->
                                                    <!-- <a href="follow_up.html"> 
                                                        <button data-toggle="tooltip" class="btn btn-primary btn-lg " data-placement="top" title="" data-original-title="">Follow Up</button>
                                                    </a> -->
                                                    </td>
                                                </tr>
                                            
                                            </tbody>
                                        </table>
									</div>
								</div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    <!-- jQuery  -->
       <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH;?>/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH;?>/select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>/bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo CSS_PATH;?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH;?>js/app.js"></script>
    <script src="<?php echo JS_PATH;?>js/sasa.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH;?>/dropzone/dist/dropzone.js"></script>
       <script type="text/javascript">
        $(document).ready(function() {
		$('#educatiion_level').on('change', function(){ alert('jagdish');
				$('#education_id').val($(this).val());
				$('#add_eduction').prop('disabled', false);
		});
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
    </script>
       
 <script type="text/javascript">
	 
	 
	function past_education(obj) {

	if(obj == '8') {
		var ed = 'Degree';
	} 
	if( obj == '7') {
		var ed = 'Diploma';
	} if( obj == '9') {
		var ed = 'Masters';
		
	}
	$('#add_eduction').prop('disabled', false);
	$('#education_id').val(obj);
	$('#post_job_qua').text(ed);
}
            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>
