<div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Update Application</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Update Application</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                        <div class="row">
                                            <div class="card-body">
                                            <h4 class="mt-0 header-title">Application Form</h4>
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                            <li class="nav-item">
                                            <a class="nav-link active show" data-toggle="tab" href="#home1" role="tab" aria-selected="false">Personal</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile1" role="tab" aria-selected="false">Past Eduction</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#messages1" role="tab" aria-selected="false">Contact</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#settings1" role="tab" aria-selected="true">Program</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#settings2" role="tab" aria-selected="true">Documents</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#settings3" role="tab" aria-selected="true">Upload Photos</a>
                                            </li>
                                            <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#settings4" role="tab" aria-selected="true">Review & Submit</a>
                                            </li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                            <div class="tab-pane active show p-3" id="home1" role="tabpanel">
										<form action="javascript:submit_form();" id="personalformData" name="personal" method="post">
                                            <div class="row">
                                                <div class="personal col-12">
                                                    <h4>Personal Information</h4>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>International</label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input type="radio" name="options" name="international" id="option2"> No </label>
                                                        <label class="btn btn-full ">
                                                        <input type="radio" name="options" name="international" id="option3"> Yes</label>
                                                    </div>
                                                </div>
                                        <div class="col-md-8">
                                        <div class="form-group">
                                        <label><strong>Gender</strong></label><br>
                                       
                                            
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline3" name="gender" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline3">Male</label>
                                            </div>
                                           
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline4" name="gender" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline4">Female</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>ID Type <span style="color: red;">*</span></strong></label>
                                                <select name="id-type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">option</option>
                                                        <option value="HI">option</option>
                                                    
                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ID No<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control " name="id_no" required="" placeholder="eg: 123456789"> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="name" required="" placeholder=""> 
                                         </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Salution<span style="color: red;">*</span></strong></label>
                                                <select name="salution" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">option</option>
                                                        <option value="HI">option</option>
                                                    
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Place of Birth<span style="color: red;">*</span></label>
                                            <input type="text" name="place_of_birth" class="form-control " required="" placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email<span style="color: red;">*</span></label>
                                            <input type="Email" name="email" class="form-control " required="" placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Secondary Email<span style="color: red;">*</span></label>
                                            <input type="Email" name="secondary_email" class="form-control " required="" placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Phone<span style="color: red;">*</span></label>
                                            <input type="text" name="phone" class="form-control " required="" placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Nationality</strong></label>
                                                <select name="nationality" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">option</option>
                                                        <option value="HI">option</option>
                                                    
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Race</strong></label>
                                                <select name="race" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Religion</strong></label>
                                                <select name="religion" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Marital</strong></label>
                                                <select name="marital" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                                    <label>Disability</label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input type="radio" name="disability" id="option2"> No </label>
                                                        <label class="btn btn-full ">
                                                        <input type="radio" name="disability" id="option3"> Yes</label>
                                                    </div>
                                                </div>

                                    <div class="col-md-6 personal">
                                        <h4>Address</h4>
                                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>Address Line 1</strong></label>
                                            <input type="text" name="address_line1" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong> Address Line 2</strong></label>
                                            <input type="text" name="address_line2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>City</strong></label>
                                            <input type="text" name="city" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Country</strong></label>
                                                <select name="country" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>State</strong></label>
                                            <input type="text" name="state" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong> Postcode</strong></label>
                                            <input type="text" name="postcode" class="form-control" placeholder=""> 
                                         </div>
                                    </div>
                                </div>
                                </div>
                                   <div class="col-md-6 personal">
                                        <h4>Secondary Address</h4>
                                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>Address Line 1</strong></label>
                                            <input type="text" name="address_line11" class="form-control " placeholder=""> 
                                         </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong> Address Line 2</strong></label>
                                            <input type="text" name="address_line21" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>City</strong></label>
                                            <input type="text" name="city2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Country</strong></label>
                                                <select name="country2" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>State</strong></label>
                                            <input type="text" name="state2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong> Postcode</strong></label>
                                            <input type="text" name="postcode2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                </div>
                                </div>
                               <div class="col-md-6 personal">
                                            <h4>Recruited by<!-- <span style="color: red;">*</span> --></h4>
                                            <div class="row">
                                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Marketing Source</strong></label>
                                                <select name="marketing_source" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Marketing Staff</strong></label>
                                                <select name="marketing_staff" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Agent</strong></label>
                                                <select name="agent" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label"><strong>Student Get Student</strong></label>
                                                <select name="student_get_student" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label"><strong>Staff Get Student</strong></label>
                                                <select name="staff_get_student" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                             <label class="control-label"><strong>Other</strong></label>
                                           <div><textarea name="other" required="" class="form-control" placeholder="Other Recruiter information" rows="5"></textarea></div>
                                        </div>   
                                </div>

                                <div class="col-md-6 personal">
                                            <h4>Other</h4>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Sponsor</strong></label>
                                                <select name="sponsor" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                                    <label><strong>Hostal Required</strong></label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input type="radio" name="hostal_required"  id="option2"> No </label>
                                                        <label class="btn btn-full ">
                                                        <input type="radio" name="hostal_required"  id="option3"> Yes</label>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Room Type</strong></label>
                                                <select name="room_type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Information Source</strong></label>
                                                <select name="information_source" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Remarks</strong></label>
                                           <div><textarea name="remarks"  class="form-control" rows="5"></textarea></div>
                                        </div>   
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><strong>How Do You Know About Us?</strong></label><br>
                                         <label><strong>Tag<span style="color: red;">*</span><!--</strong--></strong></label><strong><br>
                                        <!-- Radio button-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline1" name="tag" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline1">All</label>
                                            </div>
                                            <!-- Radio button-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline2" name="tag" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline2">Hot</label>
                                            </div>
                                            <!-- Radio button-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline3" name="tag" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline3">Warm</label>
                                            </div>
                                             <!-- Radio button-->
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline4" name="tag" mdbinputdirective="">
                                              <label class="custom-control-label" for="defaultInline4">Cold</label>
                                            </div>

                                        </div>
                                        <div class="text-center">
                                            <button type="submit" id="personal-form" class="btn btn-lg btn-primary waves-effect">Save & Continue</button>
                                        </div>

                                </div>
                                

                                 
                                    </strong>
                                </div>
                                            </div>
                                            <div class="tab-pane p-3" id="profile1" role="tabpanel">
                                            <h4>Past Eduction</h4>
                                            <div class="col-md-4 ml-1" style="padding: 0px;" >
                                            <div class="form-group">
                                            <label class="control-label"><strong>Eduction Level <span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">option</option>
                                                        <option value="HI">option</option>
                                                    
                                                </select>
                                            </div>
                                            </div>
                                            <div class="text-right pb-2">
                                                <button type="button" class="btn btn-dark waves-effect">Add New</button>
                                            </div>
                                            <table class="table table-bordered">
                                                <thead class="black white-text">
                                                    <tr>
                                                        <th scope="col">No</th>
                                                        <th scope="col">Institute</th>
                                                        <th scope="col">Qualification</th>
                                                        <th scope="col">Grade/CGPA</th>
                                                        <th scope="col">Year</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                    <!--  <tbody>
                                                    <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                    </tr>
                                                    </tbody> -->
                                            </table>
                                            <div class="float-left">
                                                <button type="button" class="btn btn-secondary waves-effect btn-lg">Previous</button>
                                            </div>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-lg waves-effect btn-primary">Save & Continue</button>
                                            </div>
                                            <div class="clearfix"></div>
                                            </div>
                                            <div class="tab-pane p-3" id="messages1" role="tabpanel">
                                            <h4>Contact</h4>
                                            <div class="col-md-4 ml-1" style="padding: 0px;" >
                                            <div class="form-group">
                                            <label class="control-label"><strong>Eduction Level <span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">option</option>
                                                        <option value="HI">option</option>
                                                    
                                                </select>
                                            </div>
                                            </div>
                                            <div class="text-right pb-2">
                                                <button type="button" class="btn btn-dark waves-effect">Add New</button>
                                            </div>
                                            <table class="table table-bordered">
                                                <thead class="black white-text">
                                                    <tr>
                                                        <th scope="col">No</th>
                                                        <th scope="col">Relationship</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Contact</th>
                                                        <th scope="col">Action</th>
                                                       
                                                    </tr>
                                                </thead>
                                                    <!--  <tbody>
                                                    <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                    </tr>
                                                    </tbody> -->
                                            </table>
                                            <div class="float-left">
                                                <button type="button" class="btn btn-secondary waves-effect btn-lg">Previous</button>
                                            </div>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-lg waves-effect btn-primary">Save & Continue</button>
                                            </div>
                                            </div>
                                            <div class="tab-pane p-3 " id="settings1" role="tabpanel">
                                                <div class="row">
                                                <div class="col-md-4 personal">
                                                    <h4>Program <span style="color: red;">*</span></h4>
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Intake<span style="color: red;">*</span></strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Select</option>
                                                                <option value="AK">option</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>

                                                </div>
                                                <div class="col-md-4 personal">
                                                    <h4>Program 1 <span style="color: red;">*</span></h4>
                                                    <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Study Mode</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                                </div>
                                                <div class="col-md-4 personal">
                                                    <h4>Program 2 <span style="color: red;">*</span></h4>
                                                     <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Study Mode</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                                </div>
                                                 <div class="col-md-4 personal">
                                                    <h4>Program 3 <span style="color: red;">*</span></h4>
                                                    <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Study Mode</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                                </div>
                                                <div class="col-md-12">
                                                <div class="float-left">
                                                <button type="button" class="btn btn-secondary waves-effect btn-lg">Previous</button>
                                            </div>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-lg waves-effect btn-primary">Save & Continue</button>
                                            </div>
                                        </div>
                                            </div>
                                            </div>
                                            <div class="tab-pane p-3 " id="settings2" role="tabpanel">
                                                <div class="row">
                                                    <h4>Documents </h4>
                                                     <table class="table table-bordered">
                                                <thead class="black white-text">
                                                    <tr>
                                                        <th scope="col">No</th>
                                                        <th scope="col">File Type</th>
                                                        <th scope="col">Name</th>
                                                    </tr>
                                                </thead>
                                                    <!--  <tbody>
                                                    <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                    </tr>
                                                    </tbody> -->
                                            </table>

                                            <div class="col-md-12">
                                                <div class="float-left">
                                                <button type="button" class="btn btn-secondary waves-effect btn-lg">Previous</button>
                                            </div>
                                            <div class="float-right">
                                                <button type="button" class="btn btn-lg waves-effect btn-primary">Save & Continue</button>
                                            </div>
                                        </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane p-3 " id="settings3" role="tabpanel">
                                            <div class="row">
                                                <h4>Upload Photo </h4>
                                                <div class="col-md-3 offset-md-2">
                                                    <div class="form-group"> 
                                                        <div class="text-center">
                                                            <h4>Preview</h4>
                                                        </div>
                                                        <div class="main-img-preview">
                                                        <img class="thumbnail img-preview" src="assets/images/small/img-4.jpg" title="Preview Logo">
                                                        </div>
                                                        <div class="input-group">
                                                        <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->
                                                        <div class="input-group-btn text-center" style="width: 100%;">
                                                        <div class="fileUpload btn btn-warning fake-shadow">
                                                        <span ><i class="glyphicon glyphicon-upload"></i> Upload Photo</span>
                                                        <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                                        </div>
                                                        </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                                  <div class="col-md-12">
                                                <div class="float-left">
                                                <button type="button" class="btn btn-secondary waves-effect btn-lg">Previous</button>
                                            </div>
                                            <div class="float-right">
                                                <button type="button"  class="btn btn-lg waves-effect btn-primary">Save & Continue</button>
                                            </div>
                                        </div>
                                            </div>
                                            </div>
                                            </div>

                                            </div>
                                        </form>
                                </div>
                                
                                   
                                    
                                    </div> 
                                  
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	
});

function submit_form(){
	var data_to_send = $('#personalformData').serialize();
	
	 $.ajax({
				 url: "<?php echo base_url();?>addmision/adminssionData",  
				 type : 'POST',                 
				 data : data_to_send,				 
				 datatype : 'html',
                 success : function(data)
                 {
                     
                 }
				});
}

</script>

