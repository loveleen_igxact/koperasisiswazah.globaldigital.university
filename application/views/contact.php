﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
    <link rel="shortcut icon" href="assets/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH ?>css/style1.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>

<body>
     <div class="modal fade" id="new_log" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form action="javascript:contact_submit_form();" id="contact_formData" name="personal" method="post">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">New Document</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Relationship<span style="color: red;">*</span></strong></label>
                            <select name="relationship" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option value="">Select</option>
                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                            
                                    <option value="2">Mother</option>
                                    <option value="3">Brother</option>
                                    <option value="1">Father</option>
                                    <option value="4">Sister</option>
                                    <option value="5">Uncle</option>
                                    <option value="6">Aunt</option>
                                    <option value="7">Grandfather</option>
                                    <option value="8">Grandmother</option>
                                    <option value="9">Guardian</option>
                                    <option value="10">Others</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Name<span style="color: red;">*</span></strong></label>
                     <input type="text" name="name" class="form-control " required="" placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>ID Type<span style="color: red;">*</span></strong></label>
                            <select name="id_type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option>Select</option>
                            <option value="1">Identification Certificate</option>
                            <option value="2">Passport</option>
                            </select>
                    </div>
                </div>
                  <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>ID No<span style="color: red;">*</span></strong></label>
                     <input type="text" class="form-control " name="id_no" required="" placeholder="e.g:1234569785">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>Race</strong></label>
                            <select name="race" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option>Select</option>
                              
                                 <option value="1">Malay</option>
                                <option value="2">Chinese</option>
                                <option value="3">Indian</option>
                                <option value="10">Others</option>
                                <option value="12">Bumiputra Sabah</option>
                                <option value="13">Bumiputra Sarawak</option>
                                <option value="15">Bajau</option>
                                <option value="16">Bidayuh</option>
                                <option value="17">Bisaya</option>
                                <option value="18">Brunei</option>
                                <option value="19">Bugis</option>
                                <option value="20">Dusun</option>
                                <option value="21">Iban</option>
                                <option value="22">India Muslim</option>
                                <option value="23">Kadazan</option>
                                <option value="24">Lun Bawang</option>
                                <option value="25">Lundayeh</option>
                                <option value="26">Melanau</option>
                                <option value="27">Melayu (Chaniago)</option>
                                <option value="28">Melayu Brunei</option>
                                <option value="29">Murut</option>
                                <option value="30">Punjabi</option>
                                <option value="31">Serani</option>
                                <option value="32">Siam</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Occupation</strong></label>
                     <input type="text" name="occupation" class="form-control " required="" placeholder="e.g:1234569785">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Phone</strong></label>
                     <input type="text" class="form-control " name="phone" placeholder="">   
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Secondary Phone</strong></label>
                     <input type="text" class="form-control " name="secondary_phone"  placeholder="">   
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Email</strong></label>
                     <input type="text" class="form-control " name="email"  placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Anual Income</strong></label>
                     <input type="text" class="form-control " name="anual_income"  placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Liability</strong></label>
                     <input type="text" class="form-control " name="liability"  placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-check">

                        <input type="checkbox" name="copy_address" bclass="form-check-input" id="materialUnchecked" name="acs" value="Mobile">
                        Copy Address from Student
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Address Line 1<span style="color: red;">*</span></strong></label>
                     <input type="text" class="form-control " name="address_1"
                     required="" placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Address Line 2 </strong></label>
                     <input type="text" class="form-control " name="address_2" placeholder="">   
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>City<span style="color: red;">*</span></strong></label>
                     <input type="text" class="form-control " name="city" required="" placeholder="">   
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>Country<span style="color: red;">*</span></strong></label>
                            <select name="country" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option>Select</option>
                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                    <option value="AK">option</option>
                                    <option value="HI">option</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>State<span style="color: red;">*</span></strong></label>
                            <select name="state" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option>Select</option>
                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                    <option value="AK">option</option>
                                    <option value="HI">option</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label"><strong>Postcode<span style="color: red;">*</span></strong></label>
                     <input type="text" name="postcode" class="form-control " required="" placeholder="">   
                    </div>
                </div>
            </div>
           </div>
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-lg btn-primary" id="contact-form" >Save</button>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            
          </div>
        </div>
      </div>
  </form>
    </div>

    <!--end upload-->
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>

                        </form>
                    </li>
                    <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary ">Advance Search</button>
                            </a>
                        </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <strong class="admin_name">Admin</strong> <img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a><a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
      <?php $this->load->view('layout/sidebar'); ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->

         <?php 

          $this->load->view('layout/stickmenu'); ?>
        <div class="content-page" style="margin-left: 460px">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Profile / Contact </h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active"> Profile / Contact </li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="main-img-preview">
                                                <img class="thumbnail img-preview" src="http://phpslick.com/lms/uploads/<?php echo @$prospects_data[0]['profile_pic']; ?>" title="Preview Logo" style="width: 390px;">
                                            </div>
                                            <div class="input-group">
                                                <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->
                                                <div class="input-group-btn" style="width: 100%;">
                                                    <div class="fileUpload btn btn-danger fake-shadow">
                                                        <span ><i class="mdi mdi-camera"  style="font-size: 20px"></i></span>
                                                        <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p><strong><?php echo $prospects_data[0]['name']; ?></strong></p>
                                            <P>Identification : <?php echo $prospects_data[0]['id_no']; ?></P>
                                            <P>Certificate :</P>
                                             <!--<P>Student No : GP12425</P>
                                            <P class="sas" style="color: red;" >Student No : MARCH - JULY 2018(2018/3)</P>
                                            <P>Student No : Active</P> -->
                                        </div>
                                        <!-- <div class="col-md-4" style="padding-top: 20px">
                                            <P>Faculty : xyz</P>
                                            <P>Program : xyz</P>
                                            <P>Intake : 2018/03</P>
                                            <P>Semester : 1</P>  
                                        </div> -->
                                    </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                            </div>
                        </div>
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Contact List</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="add_more">
                                            <a href="#">
                                             <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                              <button type="button" class="btn btn-dark waves-effect waves-light" data-target="#new_log" data-toggle="modal">Add New</button>
                                         </a>
                                        </div>
                                        <div class="ds">
                                            <button type="button" class="btn btn-info waves-effect waves-light main" onclick="selectAll()" value="Select All">Select All</button>
                                            <a href="#">
                                                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="UnSelectAll()" value="Unselect All">Clear</button>
                                            </a>
                                        </div> 
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                    <th>No</th>
                                                    <th class="th-sm">Relationship
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Contact
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $i =1;

                                              $relationship =   array("2" =>  "Mother",
                                                        "3" =>"Brother",
                                                        "1" =>"Father",
                                                        "4" =>"Sister",
                                                        "5" =>"Uncle",
                                                        "6" =>"Aunt",
                                                        "7" =>"Grandfather",
                                                        "8" =>"Grandmother",
                                                        "9" =>"Guardian",
                                                        "10" =>"Others");

                                                foreach($contact_data as $key => $values) { ?>
                                                <tr>
                                                 <th scope="row"><?php echo $i; ?></th>
                                                 <td><?php echo  @$relationship[$values['relationship']]; ?></td>
                                                 <td><?php echo  $values['name']; ?></td>
                                              <td><?php echo  $values['phone']; ?></td>
                                                 <td>-</td>  

                                                   
                                                </tr>
                                            <?php   $i++;} ?>
                                            </tbody>
                                        </table>
                                     
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    <!-- jQuery  -->
    <script src="<?php echo JS_PATH ?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH ?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH ?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH ?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH ?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH ?>jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH ?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo JS_PATH ?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH ?>js/app.js"></script>
    <script src="<?php echo JS_PATH ?>js/sasa.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.js"></script>
       <script type="text/javascript">
        $(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
    </script>
       
 <script type="text/javascript">

    function contact_submit_form(){ 
    var data_to_send = $('#contact_formData').serialize();
    alert(<?php echo $id; ?>);
     $.ajax({
                 url: "<?php echo base_url();?>application/add_contact/<?php echo $id;?>",  
                 type : 'POST',                 
                 data : data_to_send,                
                 datatype : 'html',
                 success : function(data)
                 {
                   location.reload();
                 }
                });
}

            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>