<div class="content-page">
<!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Group Billing</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active">Group Billing</li>
                        </ol>
                    </div>
                    </div>
                </div>
                <!-- end row -->
                <div class="col-12">
                <div class="card m-b-30">
                <div class="card-body">
                <div id="prospect"></div>
                <div style="clear:both"></div>
                <form method="post">
                <div class="row">
                <div class="col-md-4">
                <div class="form-group">
                <label><strong>Bill Type</strong></label><br>


                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" checked="checked" class="custom-control-input" id="defaultInline3" name="inlineDefaultRadiosExample" mdbinputdirective="">
                <label class="custom-control-label" for="defaultInline3">Group Billing</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="defaultInline4" name="inlineDefaultRadiosExample" mdbinputdirective="">
                <label class="custom-control-label" for="defaultInline4">Manual Billing</label>
                </div>
                </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                <label><strong>Student Type</strong></label><br>
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" checked="checked" id="defaultInline7" name="inlineDefaultRadios" mdbinputdirective="">
                <label class="custom-control-label" for="defaultInline7">All</label>
                </div>    
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="defaultInline5" name="inlineDefaultRadios" mdbinputdirective="">
                <label class="custom-control-label" for="defaultInline5">Local</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="defaultInline6" name="inlineDefaultRadios" mdbinputdirective="">
                <label class="custom-control-label" for="defaultInline6">International</label>
                </div>
                </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>Faculty<span style="color: red;">*</span></strong></label>
                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="faculty">
                            <option value="Select">Please Select </option>
                            <?php if (!empty($faculty)) {
                                foreach ($faculty as $key => $val) { ?>
                                <option value="<?php echo $val['name'] ?>"><?php echo $val['name'] ?></option>
                            <?php } } ?>
                        </select>
                    </div>
                    <span id="facultyErr" style="color: red;"></span>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>Program<span style="color: red;">*</span></strong></label>
                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="program">
                        <option value="Select">Select</option>
                        <?php if (!empty($programs)) {
                            foreach ($programs as $key => $value) { ?>
                            <option value="<?php echo $value['name'] ?>"><?php echo $value['name'] ?></option>
                        <?php } } ?>
                        </select>
                    </div>
                    <span id="programErr" style="color: red;"></span>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong>Intake<span style="color: red;">*</span></strong></label>
                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" id="intake">
                        <option value="Select">Select</option>
                        <?php if (!empty($intake)) {
                            foreach ($intake as $key => $vals) { ?>
                            <option value="<?php echo $vals['intake']."/".$vals['month']; ?>"><?php echo $vals['intake']."/".$vals['month']; ?></option>
                        <?php } } ?>
                        </select>
                    </div>
                    <span id="intakeErr" style="color: red;"></span>
                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-lg btn-primary waves-effect" name="search" id="search">Search</button>
                </div>
            </div>
        </form>
                <!-- / Collapse buttons -->  
                </div>
            
        </div>
        <div id="result" style="display: none;">
            <div class="panel-heading">
               <h4>Group Billing</h4>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <div id="groupbill-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="top col-md-6"><div class="dt-buttons"><a class="dt-button" tabindex="0" aria-controls="groupbill-table" href="#"><span>Select All</span></a><a class="dt-button" tabindex="0" aria-controls="groupbill-table" href="#"><span>Clear</span></a></div></div><div class=" col-md-12"><div id="groupbill-table_processing" class="dataTables_processing" style="display: none;"><div class="bg-white">Loading... <i class="fa fa-spin fa-spinner"></i></div></div></div>
                <table id="groupbill-table" class="table b-t b-b no-footer dataTable" aria-describedby="groupbill-table_info" role="grid" style="width: 1059px;">

                <thead>
                    <tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 22px;">No.</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 476px;">Student Name</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 222px;">IC/Passport</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 195px;">Student ID</th></tr>
                </thead>
                <tbody>
                    <?php if (!empty($users)) {
                        $i = 1;
                        foreach ($users as $key => $value) { ?>
                        <tr class="odd">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo $value['id_no']; ?></td>
                            <td><?php echo $value['id_no']; ?></td>
                        </tr>
                    <?php $i++; } } ?>
                </tbody>
            </table>
            <div class="bottom m-t-xs col-md-6"></div>
            <div class="bottom col-md-6"></div></div>
              </div>
              <div class="form-group">
                <div class="col-lg-2 pull-left">
                  <button type="submit" class="btn m-b-xs btn-success btn-addon text-capitalize  submit " disabled="disabled">Submit</button>
                </div>
              </div>
            </div>
        </div>
    </div>      
</div>
</div>
    </div>
    </div>
</div>
    </div>
</body>
</html>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    $("#search").click(function(){
        faculty = $("#faculty").val();
        program = $("#program").val();
        intake = $("#intake").val();
        if (faculty == 'Select') {
            $("#facultyErr").html('This field is required');
            return false;
        } else if (program == 'Select') {
            $("#programErr").html('This field is required');
            return false;
        } else if (intake == 'Select') {
            $("#intakeErr").html('This field is required');
            return false;
        } else {
            $("#result").show();
            return false;
        }
    });

</script>