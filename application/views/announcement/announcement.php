<?php $session_data = $this->session->userdata['loggedInData']; ?>

<style type="text/css">

    .icons_all li {

    /*display: inline-block;*/

    padding: 0px 17px;

    /*position: relative;*/

}

.dataTables_wrapper .dataTables_length {

    float: left;

    float: right !important;

    position: absolute;

    right: 0px;

    top: 17px;

}

.table td {

    width: 16.6%;

}

</style>

<div class="content-page">

    <!-- Start content -->

    <div class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-sm-12">

                    <div class="page-title-box">

                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>  Announcement</h4> -->

                        <!--<ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>

                            <li class="breadcrumb-item active"> Announcement</li>

                        </ol>-->

                    </div>

                </div>

            </div>

                    <!-- end row -->

                        <div class="col-12">

                            <?php echo $this->session->flashdata('msg'); ?>

                            <!--<div class="card m-b-30">

                                <div class="card-body">

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                     <div class="search col-md-12" style="padding: 0px;">

                                                    <div class="form-group">

                                                        <label>Search</label>

                                                        <input type="text" class="form-control s" required="" placeholder="Search Subject..."> 

                                                        <span class="serch_icon"><button class="btn btn-dark" type="button">Search</button></span>

                                                     </div>

                                                </div>

                                        <div class="button">

                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                                              Announcement More Filter

                                            </a>

                                        </div>



                                    <div class="row" style="padding-top: 10px;">

                                        <div class="collapse col-md-12" id="collapseExample">

                                            <div class="row">

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                            <label>Publish Date</label>

                                                        <div>

                                                            <div class="input-daterange input-group" id="date-range">

                                                                <input type="text" class="form-control" name="start" placeholder="From">

                                                                <input type="text" class="form-control" name="end" placeholder="To">

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                 <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label class="control-label"> <strong>Posted By</strong></label>

                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                                <option>Select</option>

                                                                 <option value="AK">option</option>

                                                                 <option value="HI">option</option>

                                                            </select>

                                                    </div>

                                                </div>

                                                <div class="col-md-4">

                                                    <label class="control-label"><strong> Published To</strong> </label><br>

                                                    <div class="custom-control custom-checkbox custom-control-inline">

                                                        <input type="checkbox" class="custom-control-input" id="defaultInline1">

                                                        <label class="custom-control-label" for="defaultInline1">Staff</label>

                                                    </div>



                                                    <div class="custom-control custom-checkbox custom-control-inline">

                                                        <input type="checkbox" class="custom-control-input" id="defaultInline2">

                                                        <label class="custom-control-label" for="defaultInline2">Student</label>

                                                    </div>

                                                </div>

                                                    

                                                </div>

                                                 <div class="button col-md-12 text-center">

                                                        <a class="btn btn-primary" gradient="peach type=" button"="" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">

                                                         Search

                                                        </a>

                                                </div> 

                                            </div>

                                        </div> 

                                    </div>

                                </div>

                            </div>

                        </div>-->

                    </div>

                </div>

                            

                <div class="card m-b-30">

                    <div class="card-body">

                        <div class="hearding">

                            <h4 class="mt-0 header-title left_side">Announcements  </h4>

                        </div>

                        <?php if($session_data['user_type'] != "3") { ?>

                        <div class="add_more">

                            <!-- <button type="button" class="btn btn-info waves-effect waves-light">Export</button>  -->

                            <a class="btn color_another waves-effect waves-light btn-addon" href="<?php echo base_url('announcement/create_announcement'); ?>">

                                <i class="fa fa-plus"></i>Add New

                            </a>

                        </div> 

                        <?php } ?>

                        <div class="clearfix"></div>

                        <!-- <hr> -->

                        <table id="dtBasicExample" class="table table-hover pt-5 " cellspacing="0" width="100%">

                            <thead class="black white-text">

                                <tr>

                                <!-- <th class="no_sort">No</th> -->
                                    <th class="th-sm">File type

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>
                                    <th class="th-sm">Subject

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <th class="th-sm">From

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <th class="th-sm">To

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <th class="th-sm">Posted by

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <th class="th-sm">Created at

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <?php if ($session_data['user_type'] != '3') { ?>

                                    <th class="th-sm">Action

                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                    </th>

                                    <?php } ?>

                                </tr>

                            </thead>

                            <tbody>

                                <?php if (!empty($announcement_data)) {

                                    $i = 1;

                                    foreach ($announcement_data as $key => $value) { 
                                        $url = "http://lmsmalaysia.com/uploads/documents/".$value['file1'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); 
                                ?>

                                <tr id="current_row<?php echo $value['id']; ?>">
                                    <td>
                                <?php 
                                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="60" width="50" src="http://lmsmalaysia.com/uploads/documents/<?php echo $value['file1'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>
                            </td>
                                    <!-- <th scope="row"><?php echo $i; ?></th> -->

                                    <td><?php echo $value['subject'] ?></td>
                                    <td>
                                        <?php if ($value['from_date'] !='0000-00-00 00:00:00') {
                                            echo date('d/m/Y', strtotime($value['from_date']));
                                            
                                        }else{
                                            echo " ";
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <?php if ($value['to_date'] !='0000-00-00 00:00:00') {
                                            echo date('d/m/Y', strtotime($value['to_date']));
                                            
                                        }else{
                                            echo " ";
                                        }

                                        ?>
                                    </td>
                                   <!--  <td><?php echo ($value['from_date'] !='0000-00-00')? date('d/m/Y', strtotime($value['from_date'])) :'--';  ?></td> -->

                                    <!-- <td><?php echo ($value['to_date'] !='0000-00-00')? date('d/m/Y', strtotime($value['to_date'])) :'--';  ?></td> -->

                                    <td><?php if ($value['created_by']=='1') {
                                        echo "Admin";
                                        
                                    }else{
                                        echo "Teacher";
                                    } ?></td>

                                    <td><?php echo date('d/m/Y', strtotime($value['created'])); ?></td>

                                  

                                    <td>

                                        <ul class="icons_all">

                                             <li>

                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="view" aria-describedby="tooltip652805" href="<?php echo base_url('announcement/view_announcement?id='.$value['id']); ?>">

                                                <i class="fa fa-eye" aria-hidden="true"></i>

                                                </a>

                                            </li>

                                            <li>
                                             <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                           <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="<?php echo base_url('uploads/documents/'.$value["file1"]) ?>"><i class="fas fa-download"></i></a>
                                <?php } else { ?>
                               
                                <?php } ?>
                                            </li>
                                            
                                              <?php if ($session_data['user_type'] != '3') { ?> 
                                            <li>

                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805" href="<?php echo base_url('announcement/edit_announcement?id='.$value['id']); ?>">

                                                <img src="http://lmsmalaysia.com/public/images/edit_edit.png">

                                                </a>

                                            </li>

                                            <li>

                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'announcements')">

                                                    <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png">

                                                </a>

                                            </li>
                                            

                                        </ul>

                                    </td>

                                    <?php } ?>

                                </tr>

                                <?php $i++; } } ?>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    </div>

        </div>

    </div>

</body>

</html>