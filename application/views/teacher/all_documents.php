<style type="text/css">
	ul.icons_all li {
    display: inline-block;
    padding: 0px 7px;
    position: relative;
}

ul.icons_all {
    padding: 0px;
}
ul.icons_all li::before {
    content: '';
    border-right: 2px solid #d5d4d4;
    position: absolute;
    height: 20px;
    right: 1px;
}
.dataTables_wrapper .dataTables_length {
    float: right !important;
    position: absolute;
    right: 0px;
    /*bottom: 174px;*/
    top: -39px;
}
.nav-tabs-custom>li>a::after {
    content: "";
    background: #4856a3;
    height: 2px;
    position: absolute;
    width: 100%;
    left: 0;
    bottom: -1px;
    -webkit-transition: all 250ms ease 0s;
    transition: all 250ms ease 0s;
    -webkit-transform: scale(0);
    transform: scale(0);
}
.sasa img {
    width: 15px;
}

.sasa p {
    position: absolute;
    right: 160px;
    top: 75px;
    font-size: 13px;
    font-weight: 500;
}
.nav-tabs-custom>li>a.active {
    color: #4856a3!important;
}
.sasa p:after {
    content: '';
    border-right: 1px solid #d5d4d4;
    position: relative;
    left: 8px;
}
.black, .picker__list-item:hover {
    background-color: #e2e2e2 !important;
    color: #000;
}
</style>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
               <!--  <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>  Documents</h4>
                        -<ol class="breadcrumb">-->
                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                        <!--    <li class="breadcrumb-item active"> Documents</li>-->
                        <!--</ol>--
                    </div>
                </div> -->
            </div>
        <!-- end row -->
        <div class="col-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
        </div>                            
        <div class="card m-b-30 m-t-30"">
            <div class="card-body">
                <div class="hearding">
                    <h4 class="mt-0 header-title left_side">DOCUMENTS</h4>
                </div>
                <div class="add_more">
                    <a href="<?php echo base_url('teacher/upload_documents'); ?>">
                    <button type="button" class="btn color_another waves-effect waves-light pb-2">Upload</button>
                </a>
                </div> 
                <div class="clearfix"></div>
                <ul class="nav nav-tabs pt-3 nav-tabs-custom" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">My Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Shared Documents</a>
                    </li>
                </ul>
                <div class="tab-content">
                	<div class="sasa">
					   	 <p><a href="#"><img src="http://lmsmalaysia.com/public/images/download_icon.png">&nbsp Download</a></p>
					</div>
                    <div class="tab-pane pt-2 active show" id="home1" role="tabpanel">
                        <table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">
                    <thead class="black white-text">
                        <tr>
                        	<th>
                        		
								    <input type="checkbox" class="" id="defaultUnchecked">
								
                        	</th>
                            <!-- <th class="no_sort" >No</th> -->
                            <th class="th-sm">File Type
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                             <th class="th-sm">Title
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                             <th class="th-sm">Description
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Uploaded on
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm no_sort">Action
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($my_documents)) {
                    $i = 1;
                    foreach ($my_documents as $key => $value) { 
                        $url = "http://lmsmalaysia.com/uploads/documents/".$value['file'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); 
                    ?>
                        <tr id="current_row<?php echo $value['id']; ?>">
                        	<td>
                        		
								    &nbsp&nbsp<input type="checkbox" class="" id="defaultUnchecked">
								
                        	</td>
                            <!-- <th scope="row"><?php echo $i; ?></th> -->
                            <td>
                                <?php 
                                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="60" width="50" src="http://lmsmalaysia.com/uploads/documents/<?php echo $value['file'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>
                            </td>
                              <td><?php echo $value['title']; ?></td>
                            <td><?php echo $value['description']; ?> </td>
                            <td><?php echo date('d/m/Y', strtotime($value['created'])); ?></td>

                            <td>
                            	<ul class="icons_all">
                                	<li><a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805" href="<?php echo base_url('teacher/edit_documents?id='.$value['id']); ?>">
                                    	<img src="http://lmsmalaysia.com/public/images/edit_edit.png">
                               			</a>
                               		</li>
                                	<li><a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'user_documents')"><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></a>
                                	</li>
                               		 <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                   	<li> <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="<?php echo base_url("teacher/download?file=".$value['file']) ?>"><img 	src="http://lmsmalaysia.com/public/images/download_icon.png"></a>
                                   	</li>
                                <?php } else { ?>
                                	<li><a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/documents/<?php echo $value['file']; ?>"><img src="http://lmsmalaysia.com/public/images/download_icon.png"></a>
                                	</li>
                                	<li>
                                		<a href="#">
                                			<img src="http://lmsmalaysia.com/public/images/share_icon.png">
                                		</a>
                                	</li>
                                <?php } ?>
                                </ul>
                            </td>
                        </tr>
                    <?php $i++; } } ?>
                    </tbody>
                </table>
                    </div>
                    <div class="tab-pane pt-2" id="profile1" role="tabpanel">
                        <table id="dtBasicExample" class="table table-hover " cellspacing="0" width="100%">
                    <thead class="black white-text">
                        <tr>
                            <th>
                            	<input type="checkbox" class="" id="defaultUnchecked">
                            </th>
                            <th class="th-sm">File Type
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Title
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Description
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Posted Date
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm no_sort">Action
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($shared_documents)) {
                    $i = 1;
                    foreach ($shared_documents as $key => $val) { 
                        $url = "http://lmsmalaysia.com/uploads/documents/".$val['file'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); 
                    ?>
                        <tr id="current_row<?php echo $val['id']; ?>">
                            <!-- <th scope="row"><?php echo $i; ?></th> -->
                            <td>
                        		
								    &nbsp&nbsp<input type="checkbox" class="" id="defaultUnchecked">
								
                        	</td>
                            <td>
                                <?php 
                                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="60" width="50" src="http://lmsmalaysia.com/uploads/documents/<?php echo $val['file'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>
                            </td>
                             <td>Pellentesque interdum interdum lacus</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Aenean accumsan ultrices nisi quis consequat. </td>
                            <td><?php echo date('m/d/Y', strtotime($val['created'])); ?></td>
                            <td>
                                <!-- <a href="<?php echo base_url('teacher/edit_documents?id='.$val['id']); ?>">
                                    <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                </a>
                                <a href="javascript:void(0)" onclick="deleteRow(<?php echo $val['id']; ?>, 'user_documents')"> <button data-toggle="tooltip" data-placement="top" title="Void" ><i class="mdi mdi-block-helper" style="font-size:25px; color: #4856a4; cursor: pointer;"></i></button></a> -->
                                <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <a class="first_icon"  data-toggle="tooltip" data-placement="top" title="Download file" href="<?php echo base_url("teacher/download?file=".$val['file']) ?>"><img src="http://lmsmalaysia.com/public/images/download_icon.png"></a>
                                <?php } else { ?>
                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/documents/<?php echo $val['file']; ?>"><img src="http://lmsmalaysia.com/public/images/download_icon.png"></a>
                                <?php } ?>
                                
                            </td>
                        </tr>
                    <?php $i++; } } ?>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</body>
</html>