<?php

$intke = array('1' => '2016' , '2' =>' 2017','3' => '2018', '4' =>'2019','5'=>'2020','6'=>'2021');

?>  

<style type="text/css">

.dataTables_wrapper .dataTables_length {

    float: left;

    float: right !important;

    position: absolute;

    right: 0px;

    top: -40px;

}

.hidden{

    display: none;

}

.serch_icon {

    position: absolute;

    bottom: 0px;

    right: 0px;

}  

.table td {

    width: 23%;

}

table.dataTable tbody th, table.dataTable tbody td {

    padding: 8px 18px !important;

}

</style>

  <div class="content-page">

            <!-- Start content -->

            <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="page-title-box">

                                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Application</h4> -->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active">Application</li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                    <!-- end row -->

                        <div class="col-12">

                            <div class="card m-b-30 hidden">

                                <div class="card-body">

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                        <div class="search " id="search_btn">

                                                   <form action="<?php echo base_url('application/application') ?>" method="post">

                                                    <div class="form-group">

                                                        <label>Search</label>

                                                        <input type="text" class="form-control serch" value ="<?php echo  @$search; ?>" name="search" placeholder="Search by Name , ID No.."> 

                                                        <span class="serch_icon"><button class="btn btn-dark" type="submit">Search</button></span>

                                                     </div>

                                            </form>     

                                        </div>

                                    <!-- <h6>Filter by Prospect Name</h6> -->

                                        <!-- Collapse buttons

                                        <div class="button">

                                            <a class="btn btn-primary "  id="more_filter_pos" gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                                            More Application Filter

                                            </a>

                                        </div>  -->

                                        <!-- / Collapse buttons -->



                                        <!-- Collapsible element -->

                                        <div class="row" style="padding-top: 10px;">

                                    <form action="<?php echo base_url('application/application') ?>" method="post">

                                        <div class="collapse col-md-12" id="collapseExample">

                                            <div class="row">

                                               

                                  

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"> <strong>Application Status</strong></label>

                                                <select name="app_status" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option value="">Select</option>

                   

                                                <option value="8" <?php echo (@$p1_program == 8)?'selected':''; ?>>Accepted</option>

                                                <option value="9" <?php echo (@$p1_program == 9)?'selected':''; ?>>Decline</option>

                                                <option value="6" <?php echo (@$p1_program == 6)?'selected':''; ?>>Incomplete</option>

                                                <option value="10" <?php echo (@$p1_program == 10)?'selected':''; ?>>No Response</option>

                                                <option value="4" <?php echo (@$p1_program == 4)?'selected':''; ?>>Offered</option>

                                                <option value="3" <?php echo (@$p1_program == 3)?'selected':''; ?>>Processed</option>

                                                <option value="2" <?php echo (@$p1_program == 2)?'selected':''; ?>>Received</option>

                                                <option value="1" <?php echo (@$p1_program == 1)?'selected':''; ?>>Registered</option>

                                                <option value="5" <?php echo (@$p1_program == 5)?'selected':''; ?>>Rejected</option>

                                                <option value="7" <?php echo (@$p1_program == 7)?'selected':''; ?>>  Reply Offer</option>



                                                </select>

                                        </div>

                                    </div>

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Program</strong></label>

                                                <select name="p1_program" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option VALUE="">Select</option>

                                                    <option value="10" <?php echo (@$p1_program == 10)?'selected':''; ?>>SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)</option>

                                                    <option value="13" <?php echo (@$p1_program == 13)?'selected':''; ?>>SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)</option>

                                                    <option value="14" <?php echo (@$p1_program == 14)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)</option>

                                                    <option value="17" <?php echo (@$p1_program == 17)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)</option>

                                                    <option value="6" <?php echo (@$p1_program == 6)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)</option>

                                                    <option value="12" <?php echo (@$p1_program == 12)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN KHAS)</option>

                                                    <option value="22" <?php echo (@$p1_program == 22)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)</option>

                                                    <option value="3" <?php echo (@$p1_program == 3)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)</option>

                                                    <option value="2" <?php echo (@$p1_program == 2)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)</option>

                                                    <option value="5" <?php echo (@$p1_program == 5)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)</option>

                                                    <option value="8" <?php echo (@$p1_program == 8)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SAINS)</option>

                                                    <option value="23" <?php echo (@$p1_program == 23)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)</option>

                                                    <option value="11" <?php echo (@$p1_program == 11)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)</option>

                                                    <option value="9" <?php echo (@$p1_program == 9)?'selected':''; ?>>SARJANA PENDIDIKAN (PENGURUSAN SUKAN)</option>

                                                    <option value="18" <?php echo (@$p1_program == 18)?'selected':''; ?>>SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)</option>

                                                    <option value="1" <?php echo (@$p1_program == 1)?'selected':''; ?>>SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)</option>

                                                    <option value="7" <?php echo (@$p1_program == 7)?'selected':''; ?>>SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)</option>

                                                    <option value="4" <?php echo (@$p1_program == 4)?'selected':''; ?>>SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)</option>

                                                    <option value="15" <?php echo (@$p1_program == 15)?'selected':''; ?>>SARJANA PENDIDIKAN (TESL)</option>

                                                    <option value="20" <?php echo (@$p1_program == 20)?'selected':''; ?>>SARJANA PENDIDIKAN BAHASA ARAB</option>

                                                    <option value="19" <?php echo (@$p1_program == 19)?'selected':''; ?>>SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT</option>

                                                </select>

                                        </div>

                                    </div>

                                     <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Campus</strong></label>

                                                <select name="p1_campus" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option VALUE="">Select</option>

                                                    <option value="3" <?php echo (@$p1_campus == 3)?'selected':''; ?>>IPGKB</option>

                                                    <option value="2" <?php echo (@$p1_campus == 2)?'selected':''; ?>>IPGKTR</option>

                                                    <option value="4" <?php echo (@$p1_campus == 4)?'selected':''; ?>>KOTA KINABALU</option>

                                                    <option value="1" <?php echo (@$p1_campus == 1)?'selected':''; ?>>UKM BANGI</option>

                                                

                                                </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>intake</strong></label>

                                                <select name="Intake" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                 <option value="">Select</option>   

                                                <option value="1" <?php echo (@$Intake == '1')?'selected':''; ?>>2016/02</option>

                                                <option value="2" <?php echo (@$Intake == '2')?'selected':''; ?>>2017/02</option>

                                                <option value="3" <?php echo (@$Intake == '3')?'selected':''; ?>>2018/03</option>

                                                <option value="4" <?php echo (@$Intake == '4')?'selected':''; ?>>2019/02</option>

                                                <option value="4" <?php echo (@$Intake == '5')?'selected':''; ?>>2019/01</option>

                                                </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Applicant Type</strong></label>

                                                <select name="applicant_type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option VALUE="">Select</option>

                                                     <option value="1" <?php echo (@$applicant_type ==1)?'selected':''; ?>>local</option>

                                                     <option value="2" <?php echo (@$applicant_type ==2)?'selected':''; ?>>International</option>

                                                </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                           <div class="form-group">

                                                <label><strong>Application Date</strong></label>

                                                <div>

                                                    <div class="input-group">

                                                        <input type="text"  value =<?php echo @$created_at ?> name="created_at" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">

                                                    <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>

                                                    </div>

                                                    </div>

                                                    <!-- input-group -->

                                                </div>

                                                </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12 text-center">

                                        <input type="submit" class="btn btn-dark waves-effect waves-light" name="s_search" value="search">

                                    </div>



                                    </div>

                                    <div class="button" style="padding-left:10px; ">

                                            <a class="btn btn-danger" gradient="peach type=" button"="" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">

                                            Close Filter

                                            </a>

                                        </div>



                                        </div>

                                    </form>

                                    </div>

                                        <!-- / Collapsible element -->  

                                </div>

                                

                            </div>

                        </div>

                    </div>

                            <!---card 2 table-->

                        

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Application List</h4>

                                        </div>

                                            <div class="add_more">

                                                <!-- <a href="#"><button type="button" id="export_csv" class="btn btn-info waves-effect waves-light export">Export</button></a> -->

                                                <!--<a class="btn color_another btn-addon waves-effect waves-light" href="<?php echo base_url('application/add_app');?>">-->

                                                <!-- <i class="fa fa-plus"></i>   Add New Application-->

                                                <!--</a>-->

                                           </div>

                                        <div class="clearfix"></div>

                                      <!--  <hr>

                                         <div class="ds">

                                            <button type="button" id="select_all" class="btn btn-info waves-effect waves-light main" onclick='selectAll()' value="Select All">Select All</button>

                                            <a href="#">

                                                <button type="button" class="btn btn-dark waves-effect waves-light" onclick='UnSelectAll()' value="Unselect All">Clear</button>

                                            </a>

                                        </div>  -->

                                        <!-- <hr> -->

                                        <div id="dvData">

                                            <div class="search " id="search_btn">

                                                <form action="<?php echo base_url('application/application') ?>" method="post">

                                                    <div class="form-group col-md-4 p-0 pt-3 ">

                                                        <!--<label>Search</label>-->

                                                        <input type="text" class="form-control serch" value ="<?php echo  @$search; ?>" name="search" placeholder="Search by Name , ID No.."> 

                                                        <span class="serch_icon"><button class="btn color_another" type="submit">Search</button></span>

                                                     </div>

                                                </form>     

                                            </div>

                                        <table id="dtBasicExample" class="table table-hover " cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                <!-- <th class="no_sort">No</th> -->

                                                    <th class="th-sm">Name

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Programs

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Intake

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Status

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm no_sort">Action

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php 

                                                //echo '<pre>'; print_r($program); die;

                                                foreach($application_data  as $value) {

                                                        //$program_datas =  @$program[$value['id']]['intake'];

                                                        $get_student_program = $this->CI->get_data_by_id('student_programs','id',$value['student_id']);

                                                        if ($get_student_program == true) {

                                                          $get_student_program_name = $this->CI->get_data_by_id('programs','id',$get_student_program['program_id']);

                                                          if ($get_student_program_name == true) {

                                                            $program1 = $get_student_program_name['name'];

                                                          } else {

                                                            $program1 = "";

                                                          }

                                                        } else {

                                                            $program1 = "";

                                                        }

                                                        $get_program1 = $this->CI->get_data_by_id('programs','id',$value['p1_program']);

                                                        $get_program2 = $this->CI->get_data_by_id('programs','id',$value['p2_program']);

                                                        $get_program3 = $this->CI->get_data_by_id('programs','id',$value['p3_program']);

                                                ?>

                                                <tr>

                                                    <!-- <th scope="row"><?php echo $value['id'];  ?></th> -->

                                                    <td>

                                                        <span style="font-weight: 500; color:#1414da; text-transform:uppercase; "><a style="color:#1414da;" href="<?php echo base_url('application/profile/'.$value['id']); ?>"><?php echo $value['name'];  ?> </a></span><br>Identification certificate: <span ><?php echo $value['id_no'];  ?> <br>Phone no: <?php echo $value['phone'];  ?>

                                                    </td>

                                                    <td>

                                                        <?php if($value['p1_program'] != '' ) { echo @$get_program1['name'] .'<br />' .@$get_program2['name'] .  '<br/>' . @$get_program3['name'];} else  { echo 'Not Availble'; } ?></td>

                                                        <!-- <td><?php print_r($intakedata[$value['intake']]['intake']) ;?> </td> -->
                                                        <td><?php foreach ($intakedata as $key => $value11) {
                                                        if ($value11['id']==$value['intake']) {
                                                            print_r($value11['intake']);
                                                        }
                                                       
                                                    } ?> </td>



                                                    <!-- <td><?php echo $value['intake']; ?></td> -->

                                                    <td style="color: #e86100; font-weight: 500;"><?php echo  ($value['eligible'] == '1')?'Accepted': 'Application Pending';?> </td>

                                                    <td>

                                                        <ul class="icons_all" >

                                                    <?php if($value['eligible'] != 1) { ?>

                                                        <li><a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="<?php echo base_url('application/add_app/'.$value['id'].'/step1/edit') ?> "><img src="http://lmsmalaysia.com/public/images/edit_edit.png"></a></li>

                                                   <?php  } elseif($value['eligible'] = 1){?>
                                                    <li><a class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Process for Registration"  onclick="return confirm('Are you sure want to proceess this user for registration ?');" href="<?php echo base_url('application/prospect_status1/'.$value['id']) ?> "><img src="http://lmsmalaysia.com/public/images/move_icon.png"></a><a id="demo"></a></li>

                                                   

                                                  <!--  <li><a class="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Move" href="<?php echo base_url('application/prospect_status1/'.$value['id']) ?> "><img src="http://lmsmalaysia.com/public/images/move_icon.png"></a></li> -->

                                                   <?php }  ?>



                                                  <!--  <a href=""> 

                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-delete-forever" style="font-size:25px; color: #eb2129;"></i></button>

                                                    </a>

-->

                                                    

                                                    <li> <a class="first_icon" href="<?php echo base_url('application/view/'.$value['id']); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><img src="http://lmsmalaysia.com/public/images/eye_icon.png">

                                                    </a></li>

                                                    </ul>



                                                    </td>

                                                </tr>



                                            <?php  } ?>

                                            

                                            </tbody>

                                        </table>

                                    </div>

                                    </div>

                                </div>



                        </div>

                    </div>

                </div>

                <!-- container-fluid -->

            </div>

            <!-- content -->

             <!-- <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer> -->

        

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    

 <script> 

    function selectAll() {

        $('.select_check').prop('checked', true);

    }

    function UnSelectAll() {

        $('.select_check').prop('checked', false);

    }

    $(document).ready(function() {

        $('#more_filter_pos').click(function () {

            $('#search_btn').hide();    

            

        }) 

        

        $('#close_filter').click(function () {

            $('#search_btn').show();    

            

        });

        

        

        $('#export_csv').click(function () {

            var exportarr = []; 

            $("input:checkbox[name=type]:checked").each(function(){

                exportarr.push($(this).val());

            });

            

            

                

            $.ajax({

                type: "POST",

                url: "<?php echo base_url('application/export_csv') ?>",

                data: {'id' : exportarr },

                success:  function(data){

                    window.open("export_csv", '_blank');

                   // window.open('data:application/csv;charset=utf-8,' + encodeURIComponent(data)); 

                    // var url = "https://www.w3schools.com";

                    // window.open(url, '_blank');

                    //javascript:void(window.open("https://www.w3schools.com",  '_blank'));

                    

                }

            });

            

        }); 

     

    })



 </script>

