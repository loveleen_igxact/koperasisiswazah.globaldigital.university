

        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Transaction</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Transaction</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                      <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Invoice Reminder</h4>
                                        </div>
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                        <div class="row" style="padding-top: 10px;">
                                        <div class=" col-md-12" id="collapseExample">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Campus</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Faculty</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Program</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Intake</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Status</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Outstanding</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>&nbsp</strong></label>
                                                           <input type="text" class="form-control" name="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 pt-2">
                                                    <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>
                                                </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                              <note>Table Show after Search</note>
                                          </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"><strong>Use Template<span style="color: red;">*</span></strong></label>
                                                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                            <option>Please Select</option>
                                                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                            <option value="AK">All</option>
                                                            <option value="HI">option</option>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                   <div class="form-group">
                                                        <label><strong>Expiry Date<span style="color: red;">*</span></strong></label>
                                                        <div>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                            <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                            </div>
                                                            </div>
                                                            <!-- input-group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="ds">
                                            <button type="button" class="btn btn-info waves-effect waves-light main" onclick="selectAll()" value="Select All">Select All</button>
                                            <a href="#">
                                                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="UnSelectAll()" value="Unselect All">Clear</button>
                                            </a>
                                        </div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th> 
                                                    <th class="th-sm">Student Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">IC/Passport
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Program
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Outstanding
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Total Reminder
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>OR00041</td>
                                                    <td>02 May 2017</td>
                                                    <td>ROZITA BINTI GHAZALI</td>
                                                    <td>874593210</td>
                                                    <td>Lorem Ipsum is simply dummy text of the printing</td> 
                                                    <td><a href="">1500</a>
                                                    </td>
                                                    <td>
                                                        <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="materialUnchecked" name="acs" value="Mobile">
                                                    
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>      
                        </div>
                    </div>
                </div>
            </div>
    
 <script type="text/javascript">
            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

</body>
</html>