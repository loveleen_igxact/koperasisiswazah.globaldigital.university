<?php 

$documents = array("1" => "Salinan Kad Pengenalan",

    "2"                       => "Salinan Sijil Degree",

    "3"                       => "Gambar (1 copies)",

    "4"                       => "Salinan Transkrip Degree",

    "5"                       => "Salinan Sijil Diploma",

    "6"                       => "Salinan Transkrip Diploma",

    "7"                       => "Surat Pengesahan Jawatan",

    "8"                       => "Slip Bayaran Yuran Proses",

    

);



$relation_arr = array("1" => "Father",

    "2"                       => "Mother",

    "3"                       => "Brother",

    "4"                       => "Sister",

    "5"                       => "Uncle",

    "6"                       => "Aunt",

    "7"                       => "Grandfather",

    "8"                       => "Grandmother",

    "9"                       => "Guardian",

);





 $intake = array('1' => '2018/02' , '2' =>' 2019/02','3' => '2018/03');



$programs  =  array(



   '10'  => 'SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)' ,

   '13' => 'SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)',

   

   '14' => 'SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)',

   "17" =>'SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)',

   '6' =>'SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)',

   

   '12' => 'SARJANA PENDIDIKAN (PENDIDIKAN KHAS)',

    '22' => 'SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)',

    '3' => 'SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)', 

    '2' => ' SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)',

    '5'=> 'SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)',

    '8'=>'SARJANA PENDIDIKAN (PENDIDIKAN SAINS)',

    '23'=>'SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)',

    '11'=>'SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)',

    '9' => 'SARJANA PENDIDIKAN (PENGURUSAN SUKAN)',

    "18" =>'SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)',

    "1" => 'SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)',

    "7" => 'SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)',

    "4" => 'SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)',

    "15" =>'SARJANA PENDIDIKAN (TESL)',

    "20" => 'SARJANA PENDIDIKAN BAHASA ARAB',

    "19" => 'SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT'

);





?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">

    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>

    <meta content="Admin Dashboard" name="description">

    <link rel="shortcut icon" href="assets/images/fav_lms.png">

    <link href="<?php echo PLUG_PATH ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="<?php echo PLUG_PATH ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo PLUG_PATH ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo CSS_PATH;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH ?>css/metismenu.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH ?>css/icons.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH;?>css/style1.css" rel="stylesheet" type="text/css">

    <!-- Dropzone css -->

    <link href="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">

</head>



<body>

    <!-- Begin page -->

    <div id="wrapper">

        <!-- Top Bar Start -->

         <?php $this->load->view('layout/header'); ?>

        <!-- Top Bar End -->

        <!-- ========== Left Sidebar Start ========== -->

       <?php $this->load->view('layout/sidebar'); ?>

        <!-- Left Sidebar End -->



        <!-- ============================================================== -->

        <!-- Start right Content here -->

        <!-- ============================================================== -->

        

          <div class="content-page" >

            <!-- Start content -->

           

           <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="page-title-box">

                                <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Profile </h4>-->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active"> Profile </li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                    <!-- end row -->

                        <div class="col-12">

                            <div class="card m-b-30">

                                <div class="card-body">

                                    <div class="hearding">

                <h4 class="mt-0 header-titl left_side">PROFILE</h4>

            </div>

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-4">

                                           <div class="main-img-preview">

                                                <?php if(empty( $prospects_data[0]['profile_pic'])) { 

                                                     $image = 'http://lmsmalaysia.com/uploads/avtr.png';



                                                } else  {



                                                    $image = 'http://lmsmalaysia.com/uploads/'.$prospects_data[0]['profile_pic'];



                                                } ?>

                                                <img class="thumbnail img-preview" src="<?php echo @$image; ?>" title="Preview Logo">

                                            </div>

                                            <div class="input-group">

                                                <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->

                                              <!--  <div class="input-group-btn" style="width: 100%;">

                                                    <div class="fileUpload btn btn-danger fake-shadow">

                                                        <span ><i class="mdi mdi-camera"  style="font-size: 20px"></i></span>

                                                        <input id="logo-id" name="logo" type="file" class="attachment_upload">

                                                    </div>

                                                </div>  -->

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <p><strong>Name</strong> : <?php echo $prospects_data[0]['name']; ?><!-- </p><?php echo "<pre>"; print_r($prospects_data); ?>
 -->
                                            <P><strong>Identification Number </strong>: <?php echo $prospects_data[0]['id_no']; ?></P>

                                           

                                             <P><strong> Salutation </strong> : <?php 
                                             if($prospects_data[0]['gender']=='f'){
                                              if($prospects_data[0]['marital'] == 2){
                                              echo "Mrs";
                                            }else{echo "Miss";} }
                                            else{
                                              echo "Mr";
                                            }
                                               ?></P>

                                            <P class="sas" > <strong>Phone Number </strong>: <?php echo $prospects_data[0]['phone']  ?></P><!-- <?php echo  print_r(@$prospects_data); ?> -->

                                            <P><strong> Email</strong> : <?php echo $prospects_data[0]['email']  ?></P>

                                            <P><strong>Gender </strong> : <?php if ($prospects_data[0]['gender'] == "m" || $prospects_data[0]['gender'] =="male") {

                                              echo "Male"; } elseif ($prospects_data[0]['gender'] == "f" || $prospects_data[0]['gender'] =="female") {echo "Female";}  ?></P>

                                        </div>

                                         <div class="col-md-4">

                                            

                                            <P><strong> Nationality </strong> : <?php echo $nationalities[0]['Nationality']  ?></P>

                                            <P><strong> Religion </strong> : <?php echo $religion[0]['name']  ?></P>

                                            <P><strong> Race </strong> : <?php echo $race[0]['name']  ?></P> 
                                            <!-- <?php echo "<pre>";print_r($prospects_data);print_r($prospects_data[0]['marital']); echo "string11111111"; ?> -->

                                            <p><strong>Marital Status</strong> : <?php 
                                            if ($prospects_data[0]['marital']== "") {
                                              echo " ";
                                            }else{
                                              if($prospects_data[0]['marital'] == 2) {
                                                echo "Married";
                                              }else  {
                                                echo "Single";
                                              }

                                            } ?>

                                            </p>

                                            <p><strong> Place of Birth</strong> : <?php echo $prospects_data[0]['place_of_birth']  ?></p>

                                            <p><strong> Date of Birth</strong> : <?php echo $prospects_data[0]['date_Of_birth']  ?></p>

                                        </div>

                                    </div>

                                    </div>

                                        <!-- / Collapsible element -->  

                                </div>

                            </div>

                        </div>

                            <!---card 2 table-->

                                <!--<div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side"> About </h4>

                                        </div>

                                        <div class="add_more " style="padding-bottom: 10px;">

                                           <!--  <button type="button" class="btn btn-success waves-effect waves-light"><i class="ion-android-call"></i> Call</button>

                                            <a href="student_update.html">



                                                <button type="button" class="btn btn-dark waves-effect waves-light"><i class="mdi mdi-lead-pencilssssss"></i> Update</button>

                                            </a> 

                                        </div> 

                                        <div class="clearfix"></div>

                                         

                                       <table class="table table-hovered m-b-none">

                                        <tbody>

                                          <tr>

                                            <td>Name</td>

                                            <td><?php echo $prospects_data[0]['name']  ?></td>

                                          </tr>

                                          <tr>

                                            <td>Salutation</td>

                                            <td><?php echo $prospects_data[0]['salutation']  ?></td>

                                          </tr>

                                          <tr>

                                            <td>Identification Certificate No</td>

                                            <td><?php echo $prospects_data[0]['id_no']  ?></td>

                                          </tr>

                                          <tr>

                                            <td>Phone Number</td>

                                            <td>

                                              <?php echo $prospects_data[0]['phone']  ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Email</td>

                                            <td>

                                              <?php echo $prospects_data[0]['email']  ?>

                                            </td>

                                          </tr>

                                              

                                          <tr>

                                            <td>Gender</td>

                                            <td>

                                             <?php echo $prospects_data[0]['gender']  ?>

                                            </td>

                                          </tr>

                                        

                                          <tr>

                                            <td>Nationality</td>

                                            <td>

                                              <?php echo $nationalities[0]['Nationality']  ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Religion</td>

                                            <td>

                                             <?php echo $religion[0]['name']  ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Race</td>

                                            <td>

                                               <?php echo $race[0]['name']  ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Marital Status</td>

                                            <td>

                                              <?php echo ($prospects_data[0]['marital'] == 1) ? 'Married' : 'Single'; ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Place of Birth</td>

                                            <td>

                                             <?php echo $prospects_data[0]['place_of_birth']  ?>

                                            </td>

                                          </tr>

                                          <tr>

                                            <td>Date of Birth</td>

                                            <td>

                                               <?php echo $prospects_data[0]['date_Of_birth']  ?>

                                            </td>

                                          </tr>

                                        </tbody>

                                      </table>                 

                                    </div>

                                </div>-->

                                <!-- registration -->



                                <?php 

                                    $get_program1 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p1_program']);

                                    $get_program2 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p2_program']);

                                    $get_program3 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p3_program']); 

                                  if($prospects_data[0]['application_status'] == 1) {?>

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Registration</h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-hover " cellspacing="0" width="100%">

                                      <thead class="black white-text">

                                        <tr>

                                          <th class="no_sort" >No</th>

                                          <th class="th-sm">Program

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                          </th>

                                          <th class="th-sm">Intake

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                          </th>

                                          <th class="th-sm">Application Date

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                          </th>

                                        </tr>

                                      </thead>

                                      <tbody>

                                        <tr>

                                          <th scope="row">1</th>

                                            <td><?php foreach($total_p as $pro) { ?> 

                                                  <?php echo $pro->name ?><br>
                                                <?php } ?>

                                          <!-- <td><?php echo $get_program1.'<br/>'.$get_program2.'<br/>'.$get_program3 ?></td> -->
                                            
                                          <td><?php
                                          foreach ($intakeA as $key => $value) {
                                            if($value['id']==$prospects_data[0]['intake']){
                                              echo $value['intake'];
                                            }
                                            
                                          }?></td>

                                            

                                          <td><?php echo $prospects_data[0]['created_at']  ?></td>

                                        </tr>

                                      </tbody>

                                    </table>

                                </div>

                                <!--end registration table-->

                                <!-- document -->

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Documents</h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                    <th class="no_sort">No</th>

                                                    <th class="th-sm">Name

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">View

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">type

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Updated At

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php foreach($document as $key => $doc) { ?>

                                                <tr>

                                                    <th scope="row"><?php echo ++$key?></th>

                                                    <td><?php echo $doc['name'] ?></td>

                                                    <td><a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/<?php echo $doc['name']; ?>"><img src="http://lmsmalaysia.com/public/images/download_icon.png"></a>
                                                    </td>
                                  

                                                   <!--  <td><?php echo @$programs[$program[0]['program1_program']].'<br/>'.@$programs[$program[0]['program2_program']].'<br/>'. @$programs[$program[0]['program3_program']] ?></td> -->

                                                    <td><?php echo @$documents[$doc['type']] ?></td>

                                                    <td><?php echo $doc['created_at'] ?></td>

                                                   

                                                </tr>

                                            <?php } ?>

                                            </tbody>

                                        </table>

                                </div>

                                <!--end document table-->



                                <!-- Contact -->

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Contact List</h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                    <th>No</th>

                                                    <th class="th-sm">Relationship  

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Name

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Contact

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                   

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php foreach($contact as $key => $con) {?>

                                                <tr>

                                                    <th scope="row"><?php echo ++$key ?></th>

                                                    <td><?php echo @$relation_arr[$con['relationship']] ?></td>

                                                    <td><?php echo $con['name'] ?></td>

                                                    <td><?php echo $con['phone'] ?></td>

                                                   

                                                </tr>

                                            <?php } ?>

                                            </tbody>

                                        </table>

                                </div>



                              <?php } ?>

                                <!--end contact table-->



                                <!-- Marketing -->

                              <!--  <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side"> Marketing Application </h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                    <th>No</th>

                                                    <th class="th-sm">Program   

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Intake

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Stages

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <th scope="row">1</th>

                                                    <td><?php //echo @$programs[$program[0]['program1_program']].'<br/>'.@$programs[$program[0]['program2_program']].'<br/>'. @$programs[$program[0]['program3_program']] ?></td>

                                                    <td><?php //echo @$intake[$program[0]['intake']] ?></td>

                                                    <td>Lead</td>

                                                    

                                                </tr>

                                            

                                            </tbody>

                                        </table>

                                </div>  -->

                                <!--end Marketing table-->  



                                <!-- Marketing -->

                              <!--  <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side"> Application </h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                    <th>No</th>

                                                    <th class="th-sm">Program   

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Intake

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Status

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                   

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <th scope="row">1</th>

                                                    <td><?php //echo @$programs[$program[0]['program1_program']].'<br/>'.@$programs[$program[0]['program2_program']].'<br/>'. @$programs[$program[0]['program3_program']] ?></td>

                                                    <td><?php //echo @$intake[$program[0]['intake']] ?></td>

                                                    <td><?php //echo $prospects_data[0]['application_status'] == 1 ? "accepted":"pending" ?></td>

                                                   

                                                </tr>

                                            </tbody>

                                        </table>

                                </div>  -->

                                <!--end Marketing table-->



                                <!-- Marketing -->

                             <!--   <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side"> General </h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>

                                    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                            <tbody>

                                          <tr>

                                            <td>Outstanding Balance</td>

                                            <td> </td>

                                          </tr>

                                          <tr>

                                            <td>Overpayment</td>

                                            <td></td>

                                          </tr>

                                          <tr>

                                            <td>Bank</td>

                                            <td></td>

                                          </tr>

                                          <tr>

                                            <td>Deposit</td>

                                            <td></td>

                                          </tr>

                                          <tr>

                                            <td>Sponsor</td>

                                            <td></td>

                                          </tr>

                                        </tbody>

                                    </table>

                                </div>  -->

                                <!--end Marketing table-->



                                <!-- Programs -->

                          <!--       <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side"> Programs </h4>

                                        </div> 

                                        <div class="clearfix"></div>



                                    </div>



                                   <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                    <th>No</th>

                                                    <th class="th-sm">Faculty

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Program

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Intake

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Academic Session

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Charges Start Date

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Status

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Primary

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                   

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <th scope="row"></th>

                                                    <td></td>

                                                    <td></td>

                                                    <td></td>

                                                    <td></td>

                                                    <td></td>

                                                    <td></td>

                                                    <td></td>

                                                   

                                                </tr>

                                            </tbody>

                                        </table>  -->

                                   

                                </div>

                                <!--end Programs table-->



                        </div>

                        </div>

                    </div>

                <!-- container-fluid -->

            </div>

            <!-- content -->

             <footer class="footer">Copyright© 2019 LMS</footer>

        </div>

        <!-- ============================================================== -->

        <!-- End Right content here -->

        <!-- ============================================================== -->

    </div>

    <!-- END wrapper -->

    <!-- jQuery  -->

    <script src="<?php echo JS_PATH ?>js/jquery.min.js"></script>

    <script src="<?php echo JS_PATH ?>js/bootstrap.bundle.min.js"></script>

    <script src="<?php echo JS_PATH ?>js/metisMenu.min.js"></script>

    <script src="<?php echo JS_PATH ?>js/jquery.slimscroll.js"></script>

    <script src="<?php echo JS_PATH ?>js/waves.min.js"></script>

    <script src="<?php echo PLUG_PATH ?>jquery-sparkline/jquery.sparkline.min.js"></script>

    <!-- Plugins js -->

    <script src="<?php echo PLUG_PATH ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

    <script src="<?php echo PLUG_PATH ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script src="<?php echo PLUG_PATH ?>select2/js/select2.min.js"></script>

    <script src="<?php echo PLUG_PATH ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>

    <script src="<?php echo PLUG_PATH ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>

    <script src="<?php echo PLUG_PATH ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Plugins Init js -->

    <script src="<?php echo JS_PATH ?>pages/form-advanced.js"></script>

    <!-- App js -->

    <script src="<?php echo JS_PATH ?>js/app.js"></script>

    <script src="<?php echo JS_PATH ?>js/sasa.js"></script>

    <!-- Dropzone js -->

    <script src="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.js"></script>

       <script type="text/javascript">

        $(document).ready(function() {

    var brand = document.getElementById('logo-id');

    brand.className = 'attachment_upload';

    brand.onchange = function() {

        document.getElementById('fakeUploadLogo').value = this.value.substring(12);

    };



    // Source: http://stackoverflow.com/a/4459419/6396981

    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            

            reader.onload = function(e) {

                $('.img-preview').attr('src', e.target.result);

            };

            reader.readAsDataURL(input.files[0]);

        }

    }

    $("#logo-id").change(function() {

        readURL(this);

    });

});

    </script>

       

 <script type="text/javascript">

            function selectAll(){

                var items=document.getElementsByName('acs');

                for(var i=0; i<items.length; i++){

                    if(items[i].type=='checkbox')

                        items[i].checked=true;

                }

            }

            

            function UnSelectAll(){

                var items=document.getElementsByName('acs');

                for(var i=0; i<items.length; i++){

                    if(items[i].type=='checkbox')

                        items[i].checked=false;

                }

            }           

        </script>



<style type="text/css">

.form-control, .thumbnail {

    border-radius: 2px;

}

.btn-danger {

    background-color: #B73333;

}



/* File Upload */

.fake-shadow {

    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);

}

.fileUpload {

    bottom: 43px;

    position: relative;

    overflow: hidden;

    left: 17px;

}

.fileUpload #logo-id {

    position: absolute;

    top: 0;

    right: 0;

    margin: 0;

    padding: 0;

    font-size: 33px;

    cursor: pointer;

    opacity: 0;

    filter: alpha(opacity=0);

}

.img-preview {

    max-width: 100%;

    width: 100%;

}

.thumbnail {

    display: block;

    padding: 4px;

    margin-bottom: 20px;

    line-height: 1.428571429;

    background-color: #fff;

    border: 1px solid #ddd;

    border-radius: 4px;

    -webkit-transition: all .2s ease-in-out;

    transition: all .2s ease-in-out;

}

.main-img-preview img {

    height: 280px;

    width: 280px;

    border-radius: 50%;

}

    </style>

</body>

</html>

