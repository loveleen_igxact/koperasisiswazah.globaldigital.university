<?php //echo "<pre>"; print_r($prospect_data); die; ?><?php  $intke = array('1' => '2016/02' , '2' =>' 2017/02','3' => '2018/03', '4' =>'2019/02'); ?>

 <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Registration</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Registration</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="search ">
                                                  
												  	<form action="<?php echo base_url('addmision/registration') ?>" method="post">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" class="form-control serch" value ="<?php echo  @$search; ?>" name="search" placeholder="Search by Name , ID No.."> 
                                                        <span class="serch_icon"><button class="btn btn-dark" type="submit">Search</button></span>
                                                     </div>
											</form>
                                                </div>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Registration Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
										<form action ="<?php echo base_url('addmision/registration')?>" method="POST">
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                               <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="p1_campus" >
                                                    <option value="">Select</option>
                                                     <option value="1" <?php echo (@$p1_campus == 1)?'selected':''; ?>>UKM BANGI</option>
                                                    <option value="2" <?php echo (@$p1_campus == 2)?'selected':''; ?>>IPGKTR</option>
                                                    <option value="3" <?php echo (@$p1_campus == 3)?'selected':''; ?>>IPGKB</option>
                                                    <option value="4" <?php echo (@$p1_campus == 4)?'selected':''; ?>>KOTA KINABALU</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>intake</strong></label>
                                                <select name="Intake" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                               <option value=""> Select</option>
                                                 <option value="1" <?php echo (@$Intake == '1')?'selected':''; ?>>2016/02</option>
                                                <option value="2" <?php echo (@$Intake == '2')?'selected':''; ?>>2017/02</option>
                                                <option value="3" <?php echo (@$Intake == '3')?'selected':''; ?>>2018/03</option>
                                                <option value="4" <?php echo (@$Intake == '4')?'selected':''; ?>>2019/02</option>

                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Registration Day</strong></label>
                                                <input type="text" class="form-control"  placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select name="s_program" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                  <option value=""> Select</option>
												<option value="10" <?php echo (@$p1_program == 10)?'selected':''; ?>>SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)</option>
                                                <option value="13" <?php echo (@$p1_program == 13)?'selected':''; ?>>SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)</option>
                                                <option value="14" <?php echo (@$p1_program == 14)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)</option>
                                                <option value="17" <?php echo (@$p1_program == 17)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)</option>
                                                <option value="6" <?php echo (@$p1_program == 6)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)</option>
                                                <option value="12" <?php echo (@$p1_program == 12)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN KHAS)</option>
                                                <option value="22" <?php echo (@$p1_program == 22)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)</option>
                                                <option value="3" <?php echo (@$p1_program == 3)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)</option>
                                                <option value="2" <?php echo (@$p1_program == 2)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)</option>
                                                <option value="5" <?php echo (@$p1_program == 5)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)</option>
                                                <option value="8" <?php echo (@$p1_program == 8)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SAINS)</option>
                                                <option value="23" <?php echo (@$p1_program == 23)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)</option>
                                                <option value="11" <?php echo (@$p1_program == 11)?'selected':''; ?>>SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)</option>
                                                <option value="9" <?php echo (@$p1_program == 9)?'selected':''; ?>>SARJANA PENDIDIKAN (PENGURUSAN SUKAN)</option>
                                                <option value="18" <?php echo (@$p1_program == 18)?'selected':''; ?>>SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)</option>
                                                <option value="1" <?php echo (@$p1_program == 1)?'selected':''; ?>>SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)</option>
                                                <option value="7" <?php echo (@$p1_program == 7)?'selected':''; ?>>SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)</option>
                                                <option value="4" <?php echo (@$p1_program == 4)?'selected':''; ?>>SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)</option>
                                                <option value="15" <?php echo (@$p1_program == 15)?'selected':''; ?>>SARJANA PENDIDIKAN (TESL)</option>
                                                <option value="20" <?php echo (@$p1_program == 20)?'selected':''; ?>>SARJANA PENDIDIKAN BAHASA ARAB</option>
                                                <option value="19" <?php echo (@$p1_program == 19)?'selected':''; ?>>SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Status</strong></label>
											<select name="status" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
												<option value=""> Select</option>
												<option value="8" <?php echo (@$status == 8)?'selected':''; ?>>Accepted</option>
                                                <option value="9"<?php echo (@$status == 9)?'selected':''; ?> >Decline</option>
                                                <option value="6" <?php echo (@$status == 6)?'selected':''; ?>>Incomplete</option>
                                                <option value="10" <?php echo (@$status == 10)?'selected':''; ?>>No Response</option>
												 <option value="4" <?php echo (@$status == 4)?'selected':''; ?>>Offered</option>
                                                <option value="3" <?php echo (@$status == 3)?'selected':''; ?>>Processed</option>
                                                <option value="2" <?php echo (@$status == 2)?'selected':''; ?>>Received</option>
												 <option value="1" <?php echo (@$status == 1)?'selected':''; ?>>Registered</option>
                                                <option value="5" <?php echo (@$status == 5)?'selected':''; ?>>Rejected</option>
                                                <option value="7" <?php echo (@$status == 7)?'selected':''; ?>>Reply Offer</option>
                       
											</select>
											
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-dark waves-effect waves-light" name="s_search" value="Search">
                                    </div>
                                    </div>
                                        </div>
                                    </div>
									</form>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Application List</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                           <!--  <a href="addmission_add_application.html">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
                                            </a> -->
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Programs
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											<?php foreach($prospect_data as $key=> $value) { ?>
                                                <tr>
                                                    <th scope="row"><?php echo $value['id'];  ?> </th>
                                                    <td><a href="<?php echo base_url('application/profile/'.$value['id'] ); ?>" ></a><span style="font-weight: 600; color: #eb2129;"><?php echo $value['name'];  ?></span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;"><?php  echo $value['id_no']; ?> <br>Phone no: 123456789</td>
                                                    <td><?php echo @get_program($value['p1_program']);?><br> 
                                                        Campus : <?php echo @get_campus($value['p1_campus']);?>
                                                        Status: <?php echo ($value['app_status'] == 1) ?'Accepted' : 'Pening';?>
                                                    </td>
                                                    <td><?php echo @$intake[$value['Intake']];  ?></td>
                                                    <td><a href="<?php echo base_url('application/profile/'.$value['id']); ?>"><button><i class="ion-forward" style="font-size:29px;" ></i></button></a>
                                                   <!--  <a href=""> <span><i class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span></a> -->
                                                    </td>
                                                </tr>
											<?php  } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            
