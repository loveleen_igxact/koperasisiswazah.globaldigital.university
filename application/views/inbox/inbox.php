<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Inbox</h4> -->
                            <!--<ol class="breadcrumb">-->
                            <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                            <!--    <li class="breadcrumb-item active">Inbox</li>-->
                            <!--</ol>-->
                        </div>
                    </div>
                </div>
            </div>
                    <!-- end row -->
                    <!-- Left sidebar -->
                    <!-- <div class="email-leftbar card"><a href="email-compose.html" class="btn btn-danger btn-rounded btn-custom btn-block waves-effect waves-light">Compose</a>
                        <div class="mail-list m-t-20"><a href="inbox.html" class="active">Inbox <span class="ml-1">(1)</span></a><a href="#">Draft</a> <a href="#">Sent Mail</a></div>
                    </div> -->
                <!-- End Left sidebar -->
                <!-- Right Sidebar -->
                <div class="mb-3">
                    <div class="card">
                        <div class="btn-toolbar p-3" role="toolbar">
                            <form method="post">
                            <div class="btn-group mo-mb-2">
                                <?php $session_data = $this->session->userdata['loggedInData']; 
                                    if (!empty($messages)) { 
                                        // foreach ($messages as $key => $value) { 
                                        //     if ($session_data['user_type'] == '3') {
                                        //         $get_count = "1";
                                        //     } else {
                                        //         $get_count = $this->CI->getCount('message_reply','msg_id',$value['id']);
                                        //     }
                                        // }
                                        //     if ($get_count>0) {
                                ?>
                                <button type="submit" name="submit" class="btn btn-primary waves-light waves-effect" id="delete"><i class="far fa-trash-alt"></i></button>
                                <?php //} else { ?>
                                <!-- <h5>No Message Found.</h5> -->
                                <?php // } 
                            } else { ?>
                                <h5>No Message Found.</h5>
                                <?php } ?>
                            </div>   
                        </div>
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Inbox</h4>
                        <ul class="message-list">
                            <?php if (!empty($messages)) {
                                foreach ($messages as $key => $value) { 
                                    if ($session_data['user_type'] == '3') {
                                        $get_count = "1";
                                        $read_status = $value['t_read_status'];
                                    } else {
                                        $get_count = $this->CI->getCount('message_reply','msg_id',$value['id']);
                                        $read_status = $value['f_read_status'];
                                    }
                                    if ($get_count>0) {
                                    if ($read_status == '1') {?>
                                        <li>
                                <?php } else { ?>
                                    <li class="unread">
                                <?php } ?>
                                <a href="<?php echo base_url('inbox/read_email?id='.$value['id'])?>">
                                    <div class="col-mail col-mail-1">
                                                <!-- <div class="checkbox-wrapper-mail"> -->
                                        <input type="checkbox" id="chk19" name="checkbox[]" value="<?php echo $value['id']; ?>">
                                        <label for="chk19" class="toggle"></label>
                                    <!-- </div> -->
                                        <p class="title"><?php $get_Data = $this->CI->get_data_by_id('users','id',$value['from_id']); if ($session_data['user_type'] == '3') { echo $get_Data['name']; } else {echo "me"; } ?></p>
                                    </div>
                                    <div class="col-mail col-mail-2">
                                        <?php 
                                        $string = character_limiter($value['message'], 40); ?>
                                        <div class="subject pt-3"><span class="teaser image_class"><?php echo $string; ?></span></div>
                                        <div class="date"><?php echo date('d F,Y',strtotime($value['created'])); ?></div>
                                    </div>
                                </a>
                            </li>
                            <?php } ?>
                        <?php } } ?>
                    </ul>
                </form>
            </div>
        </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $("#delete").click(function(){
        if($("#chk19"). prop("checked") == false){
            alert('Select a message');
            return false;
        }
    });
$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
</script>
       
<script type="text/javascript">
    function selectAll(){
        var items=document.getElementsByName('acs');
        for(var i=0; i<items.length; i++){
            if(items[i].type=='checkbox')
                items[i].checked=true;
        }
    }
    
    function UnSelectAll(){
        var items=document.getElementsByName('acs');
        for(var i=0; i<items.length; i++){
            if(items[i].type=='checkbox')
                items[i].checked=false;
        }
    }           
</script>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>