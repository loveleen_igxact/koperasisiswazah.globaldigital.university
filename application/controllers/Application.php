<?php

defined('BASEPATH') or exit('No direct script access allowed');

error_reporting(E_ALL & ~E_NOTICE);

class Application extends CI_Controller

{



    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     *      http://example.com/index.php/welcome

     *  - or -

     *      http://example.com/index.php/welcome/index

     *  - or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */



    public function __construct()

    {

        parent::__construct();

        $this->load->library(array('form_validation'));

        $this->load->helper(array('form', 'url'));

        $this->load->model('AdminModel'); // loads admin model

        $this->load->helper('new_helper');

        $this->CI = & get_instance();

    }

    public function index()

    {

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('welcome_message');

        $this->load->view('layout/footer');

    }



    public function prospects($id = null)

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $where  = array();

        $search = array();



        if (isset($_POST['s_submit'])) {

            //echo '<pre>'; print_r($_REQUEST);die;

            if (isset($_POST['s_tag']) && $_POST['s_tag'] != '') {

                $search['tag'] = $_POST['s_tag'];

                $data['tag']   = $_POST['s_tag'];

            }if (isset($_POST['s_validity']) && $_POST['s_validity'] != '') {

                $search['validity'] = $_POST['s_validity'];

                $data['validity']   = $_POST['s_validity'];

            }if (isset($_POST['s_stage']) && $_POST['s_stage'] != '') {

                $search['stage'] = $_POST['s_stage'];

                $data['stage']   = $_POST['s_stage'];

            }if (isset($_POST['s_program']) && $_POST['s_program'] != '') {

                $search['p1_program'] = $_POST['s_program'];

                $data['p1_program']   = $_POST['s_program'];

            }if (isset($_POST['s_intake']) && $_POST['s_intake'] != '') {

                $search['Intake'] = $_POST['s_intake'];

                $data['Intake']   = $_POST['s_intake'];

            }if (isset($_POST['s_date_added']) && $_POST['s_date_added'] != '') {

                $search['created_at'] = date('Y-m-d', strtotime($_POST['s_date_added']));

            }if (isset($_POST['s_marketing_source']) && $_POST['s_marketing_source'] != '') {

                $search['marketing_source'] = $_POST['s_marketing_source'];

                $data['marketing_source']   = $_POST['s_marketing_source'];



            }if (isset($_POST['s_agency']) && $_POST['s_agency'] != '') {

                $search['agency'] = $_POST['s_agency'];

                $data['agency']   = $_POST['s_agency'];

            }if (isset($_POST['s_agent']) && $_POST['s_agent'] != '') {

                $search['agent'] = $_POST['s_agent_'];

                $data['agent']   = $_POST['s_agent_'];



            }if (isset($_POST['s_created_by']) && $_POST['s_created_by'] != '') {

                $search['created_by'] = $_POST['s_created_by'];



                $data['created_by'] = $_POST['s_created_by'];

            } elseif (isset($_POST['s_nationality']) && $_POST['s_nationality'] != '') {

                $search['nationality'] = $_POST['s_nationality'];

                $data['nationality']   = $_POST['s_nationality'];



            }

            $where = array('prospect_status' => 1);

            $result = $this->AdminModel->getData('prospect', $where, $search);

        } else {

            if (isset($_POST['search'])) {

                $data['search'] = $_POST['search'];

                $search = array('name' => trim($_POST['search']));

                $where = array('prospect_status' => 1);

                $result = $this->AdminModel->getData('prospect', $where, $search);

            } else {
                
                if($id == null){

                $where = array('prospect_status' => 1);
                
                }else{
                    
                     $where = array('prospect_status' => 1,'id' => $id);
                }

                $result = $this->AdminModel->getData('prospect', $where);

            }

        }

        $data['prospect_data'] = $result;
        $data['intakedata']=$this->AdminModel->getData('intake');

        //echo '<pre>';print_r($data['prospect_data']); die;

        $data['agencies_data'] = $this->AdminModel->getData('agency', '', '');

        $this->load->view('prospects', $data);

        $this->load->view('layout/footer');

    }



    public function add_prospects()

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');



        $data['country'] = $this->AdminModel->getData('countries');

        $data['campus_program'] = $this->AdminModel->getData('programs');

        $data['nationalities'] = $this->AdminModel->getData('nationalities');

        $data['race'] = $this->AdminModel->getData('race');

        $data['religion'] = $this->AdminModel->getData('religion');

        $data['class'] = $this->AdminModel->getData('class');

        $data['subject'] = $this->AdminModel->getData('subjects');
        $condition= array('status' => 1);

         $data['intakeP']   = $this->AdminModel->getData('intake',$condition);

        if (isset($_POST['submit'])) {

            
unset($_REQUEST['/Application/add_prospects']);
unset($_REQUEST['timezone']);
unset($_REQUEST['Horde']);
unset($_REQUEST['horde_secret_key']);

            

            //  echo '<pre>'; print_r($_REQUEST['email']);die();

            

             $check_email = $this->AdminModel->checkEmailprospect($_REQUEST['email']);

             $checkIdNo = $this->AdminModel->checkIdNo($_REQUEST['id_no']);

            if($check_email == true) {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger" style=" color:#000 !important;">Email Already Exist</div>');

                redirect(base_url('application/add_prospects'));

            } else if($checkIdNo == true) {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger" style=" color:#000 !important;">ID No Already Exist</div>');

               redirect(base_url('application/add_prospects'));

            } else{

            unset($_REQUEST['submit']);

            $_REQUEST['created_by'] = "admin";

            $id = $this->AdminModel->saveData($_REQUEST, 'prospect');

           $id = $this->db->insert_id() ;

            //$idd = $this->AdminModel->saveData($_REQUEST, 'students');

            

            $followup = array('next_follow_up' => 'new', 'description' => 'add prospects', 'medium' => '9', 'logged_by' => $_SESSION['loggedInData']['name']);

            $this->AdminModel->saveData($followup, 'follow_up');

            //$this->db->insert('prospect',$_REQUEST);

            $name  = $_REQUEST['name'];

            $email  = $_REQUEST['email'];

            

            $phone = $_REQUEST['phone'];



            $from_email = "adminksbb@lmsmalaysia.com";



            $this->load->library('email');



            $this->email->from($from_email, 'Student Details');

            $this->email->to($email);

            $this->email->subject('Welcome to UKM Master of Education Executive Programme');

            $this->email->message('Dear Student,

             

Thank you for submitting your prospect. Your application is currently under review. A representative from Koperasi Siswazah Bangi Berhad will contact you soon.



Thank You !

Koperasi Siswazah Bangi Berhad');



            //Send mail

            if ($this->email->send()) {

                $this->session->set_flashdata("email_sent", "Email sent successfully.");

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                //  $this->load->view('email_form');

            }



            redirect(base_url() . 'application/prospects/'.$id);

            return;

        }  } else {

            $this->load->view('add_prospects', $data);



        }

        $this->load->view('layout/footer');

    }



    public function follw()

    {

        $where = array();



        if (isset($_POST['s_search'])) {



            if (isset($_POST['status']) && $_POST['status'] != '') {

                $search['status'] = $_POST['status'];

                $data['status']   = $_POST['status'];

            }if (isset($_POST['medium']) && $_POST['status'] != '') {

                $search['medium'] = $_POST['medium'];

                $search['medium'] = $_POST['medium'];

            }if (isset($_POST['created_at']) && $_POST['created_at'] != '') {

                $search['created_at'] = $_POST['created_at'];

                $search['created_at'] = $_POST['created_at'];

            }

            $result = $this->AdminModel->getData('follow_up', $where, $search);



        } else {

            if (isset($_REQUEST['search'])) {

                $data['search'] = $_REQUEST['search'];

                $search         = array('logged_by' => $_REQUEST['search']);

                $result         = $this->AdminModel->getData('follow_up', $where, $search);



            } else {

                $result = $this->AdminModel->getData('follow_up', $where);



            }

        }

        $data['follw_data'] = $result;



        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('follow_up', $data);

        $this->load->view('layout/footer');

        



    }

    public function agencies()

    {



        //~ if(isset($_POST['search_btn'])) {



        //~ }

        $where  = array();

        $search = array();

        if (isset($_POST['s_search'])) {



            if (isset($_POST['status']) && $_POST['status'] != '') {

                $search['status'] = $_POST['status'];

                $data['status']   = $_POST['status'];

            }

            $result = $this->AdminModel->getData('agency', $where, $search);

        } else {



            if (isset($_POST['search_btn'])) {

                if ($_POST['agency_name'] != '') {

                    $search['name'] = $_POST['agency_name'];

                }

                $data['agency_name'] = $_POST['agency_name'];

                $result              = $this->AdminModel->getData('agency', $where, $search);



            } else {

                $result = $this->AdminModel->getData('agency', $where, $search);

            }



        }

        $data['agencies_data'] = $result;

        // $data['agencies_data'] = $this->AdminModel->getData('agency');

        $agency = array();

        foreach ($data['agencies_data'] as $records) {

            $agency[$records['id']] = $records;



        }



        $data['agency'] = $agency;



        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('agency', $data);

        $this->load->view('layout/footer');



    }

    public function add_agencies()

    {

        //~ echo '<pre>'; print_r($_POST); die;

        if (isset($_POST['registration_no'])) {

            $this->db->insert('agency', $_REQUEST);

            $this->agencies();

            return;

        }

    }



    public function edit_agencies()

    {



        if (isset($_POST['registration_no'])) {



            $table = 'agency';

            $where = array('id' => $_REQUEST['id']);



            unset($_REQUEST['id']);



            $data = $_REQUEST;

            $this->AdminModel->updateData($where, $data, $table);

            $this->agencies();

            return;

        }

    }



    public function update_agency()

    {



        $userrow           = urldecode($_REQUEST['obj']);

        $data['json_data'] = json_decode($userrow);



        $abc = $this->load->view('update', $data);



    }



    public function deleteRow()

    {

        $session_data = $this->session->userdata['loggedInData'];

        if($_REQUEST['table'] == "inbox") {

            if ($session_data['user_type'] == '3') {

                $datas = array('to_status' => '0');

            } else {

                $datas = array('from_status' => '0');

            }

            $where = array('id'=>$_REQUEST['id']);

            $this->AdminModel->updateData($where,$datas,'inbox');

        } else {

             $this->AdminModel->row_delete($_REQUEST['id'], $_REQUEST['table']);

        }

       

    }



    public function agent()

    {

        $agent    = array();

        $where    = array();

        $where_ag = array();

        if (isset($_POST['search_btn'])) {



            if ($_POST['agent_name'] != '') {

                $where['name'] = $_POST['agent_name'];

            }

            $data['agent_name'] = $_POST['agent_name'];

        }



        if (isset($_POST['search'])) {



            if ($_POST['agency_name'] != '') {

                $where['agency_id'] = $_POST['agency_name'];

            }

            $data['agency_name'] = $_POST['agency_name'];

        }



        $data['agencies_data'] = $this->AdminModel->getData('agency');



        $data['agent_data'] = $this->AdminModel->getData('agent', $where);



        //~ $data['agencies_data'] = $this->AdminModel->getData('agency');



        //~ $data['agent_data'] = $this->AdminModel->getData('agent');



        foreach ($data['agent_data'] as $agents) {



            $agent[$agents['id']] = $agents;

        }

        $data['agents'] = $agent;

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('agent', $data);

        $this->load->view('layout/footer');

    }



    public function add_agent()

    {



        if (isset($_POST['agency_id'])) {

            $this->db->insert('agent', $_REQUEST);

            $this->agent();

            return;

        }

    }



    public function update_agent()

    {



        $userrow           = urldecode($_REQUEST['obj']);

        $data['json_data'] = json_decode($userrow);



        $abc = $this->load->view('agent_update', $data);



    }



    public function edit_agent()

    {



        if (isset($_REQUEST['email'])) {



            $table = 'agent';

            $where = array('id' => $_REQUEST['id']);



            unset($_REQUEST['id']);



            $data = $_REQUEST;



            $this->AdminModel->updateData($where, $data, $table);

            redirect(base_url() . '/application/agent');

            $this->agent();

            return;

        }

    }



    public function application($id=null)

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        $data   = array();

        $where  = array();

        $search = array();

        //echo '<pre>'; print_r($_REQUEST);

        if (isset($_REQUEST['s_search'])) {

            if (isset($_REQUEST['app_status']) && $_REQUEST['app_status'] != '') {

                $search['app_status'] = trim($_REQUEST['app_status']);

                $data['app_status']   = $_REQUEST['app_status'];

            } else if (isset($_REQUEST['p1_program']) && $_REQUEST['p1_program'] != '') {



                $search['p1_program'] = trim($_REQUEST['p1_program']);

                $data['p1_program']   = $_REQUEST['p1_program'];



            } else if (isset($_REQUEST['p1_campus']) && $_REQUEST['p1_campus'] != '') {

                $search['p1_campus'] = trim($_REQUEST['p1_campus']);

                $data['p1_campus']   = $_REQUEST['p1_campus'];

            } else if (isset($_REQUEST['Intake']) && $_REQUEST['Intake'] != '') {

                $search['Intake'] = trim($_REQUEST['Intake']);

                $data['Intake']   = $_REQUEST['Intake'];

            } else if (isset($_REQUEST['applicant_type']) && $_REQUEST['applicant_type'] != '') {

                $search['applicant_type'] = trim($_REQUEST['applicant_type']);

                $data['applicant_type']   = $_REQUEST['applicant_type'];

            } else if (isset($_REQUEST['created_at']) && $_REQUEST['created_at'] != '') {

                $search['created_at'] = date('Y-m-d', strtotime($_REQUEST['created_at']));

                $data['created_at']   = date('Y-m-d', strtotime($_REQUEST['created_at']));

            }

       

            $where = array('prospect_status' => 2);

            $result = $this->AdminModel->getData('prospect', $where, $search);



        } else {

            if (isset($_REQUEST['search'])) {

                $data['search'] = trim($_REQUEST['search']);

                $search         = array('name' => $_REQUEST['search']);

                $where = array('prospect_status' => 2);

                 $result = $this->AdminModel->getData('prospect', $where, $search);

                  //  print_r($result);die();



            } else {
                  if($id == null){

                $where = array('prospect_status' => 2);
                
                }else{
                    
                     $where = array('prospect_status' => 2,'id' => $id);
                }

                //  $where = array('prospect_status' => 2);

                 $result = $this->AdminModel->getData('prospect', $where);



            }

        }

        // $prorgram    = $this->AdminModel->getData('programs', array());

        // $program_arr = array();

        // foreach ($prorgram as $key => $values) {

        //     $program_arr[$values['prospects_id']]['intake']  = $values['intake'];

        //     $program_arr[$values['prospects_id']]['program'] = $values['program1_program'];

        // }



        // $data['program'] = $program_arr;
         $data['intakedata']=$this->AdminModel->getData('intake');

        $data['application_data'] = $result;

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('application', $data);

        $this->load->view('layout/footer');

    }



    public function add_app1($id = null, $step = null, $action = null)

    {
   
        if (!empty($id) && $action == 'edit') {

            $where    = array('id' => $id);
unset($_REQUEST['/application/add_app1/' . $id . '/step1/' . $action]);
unset($_REQUEST['timezone']);
            $prospect = $this->AdminModel->updateData($where, $_REQUEST, 'prospect');

            redirect(base_url('application/add_app/' . $id . '/step2/' . $action));

        } else {


unset($_REQUEST['/application/add_app1/' . $id . '/step1/' . $action]);
unset($_REQUEST['timezone']);
            $last_id = $this->AdminModel->saveData($_REQUEST, 'prospect');



            redirect(base_url('application/add_app/' . $last_id . '/step2/' . $action));

        }

    }



    public function add_document($id, $step)

    {



        $sourcePath = $_FILES['userImage']['tmp_name']; // Storing source path of the file in a variable

        $targetPath = "uploads/" . $_FILES['userImage']['name']; // Target path where file is to be stored

        move_uploaded_file($sourcePath, $targetPath);
        date_default_timezone_set("Asia/Kuala_Lumpur");

        $data = array(

            'prospectes_id' => $id,

            'name'          => $_FILES['userImage']['name'],

            'type'          => $_POST['document_type'],
            'created_at'=>date('Y-m-d H:i:s'),


        );
    
        $prospect = $this->AdminModel->saveData($data, 'document');
echo $name = "http://lmsmalaysia.com/uploads/".$data['name'];

//          sleep(20);
// echo "string111";
        // echo $_FILES['userImage']['name'];

        // redirect (base_url('application/add_app/'.$id.'/step5'));



    }
     public function get_document($id, $step)

    {
        $condition                  = array('prospectes_id' => $id);
        $data['documentUploadData']  = $this->AdminModel->getData('document',$condition);
        echo "string2222222";

    }



    public function add_profile($id, $step)

    {



        $sourcePath = $_FILES['logo']['tmp_name']; // Storing source path of the file in a variable

        $targetPath = "uploads/" . $_FILES['logo']['name']; // Target path where file is to be stored

        move_uploaded_file($sourcePath, $targetPath);



        $data = array(

            'profile_pic' => $_FILES['logo']['name'],



        );

        $where    = array('id' => $id);

        $prospect = $this->AdminModel->updateData($where, $data, 'prospect');

        echo $this->db->last_query();die;



    }



    public function add_app($id = null, $setup = null, $action = null)

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        $data                   = array();

        $data['contact_data']   = array();

        $data['prospects_data'] = array();

        $data['id']             = $id;

        $data['setup']          = $setup;

        $data['action']         = $action;

        $data['country']        = $this->AdminModel->getData('countries');

        $data['marketing_source'] = $this->AdminModel->getData('marketing_source');

        $data['states'] = $this->AdminModel->getData('states');

        $data['race'] = $this->AdminModel->getData('race');

        $data['institutions'] = $this->AdminModel->getData('institutions');

        $data['classes'] = $this->AdminModel->getData('class');

        $data['subjects'] = $this->AdminModel->getData('subjects');

        $data['study_mode'] = $this->AdminModel->getData('study_mode');

        $data['campuses'] = $this->AdminModel->getData('campus');

        $data['campus_program'] = $this->AdminModel->getData('programs');

        if (!empty($id)) {

            $where                  = array('prospects_id' => $id);

            $dwhere                 = array('prospectes_id' => $id);

            $pwhere                 = array('id' => $id);

            $pewhere                = array('prospects_id' => $id);
            // echo $pwhere;
            // print_r($pwhere);
            // echo "string222222";

            $data['race']  = $this->AdminModel->getData('race');

            $data['religion']  = $this->AdminModel->getData('religion');

            $data['nationalities']  = $this->AdminModel->getData('nationalities');

            $data['contact_data']   = $this->AdminModel->getData('contact', $where);

            $data['document']       = $this->AdminModel->getData('document', $dwhere);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $pwhere);

           //  print_r($data['prospects_data']);die;
                
            

           $race_id = array('id' => $data['prospects_data'][0]['race']);



           $data['race_id']   = $this->AdminModel->getData('race', $race_id);

           $religion_id = array('id' => $data['prospects_data'][0]['religion']);



            $data['religion_id']   = $this->AdminModel->getData('religion', $religion_id);

            $nation_id = array('NationalityID' => $data['prospects_data'][0]['nationality']);



            $data['nation_id']   = $this->AdminModel->getData('nationalities', $nation_id);

            $state_id = array('id' => $data['prospects_data'][0]['state']);



            $data['state_id']   = $this->AdminModel->getData('states', $state_id);

            $country_id = array('id' => $data['prospects_data'][0]['country']);

              

            $data['country_id']   = $this->AdminModel->getData('countries', $country_id);

               // print_r($where);
                // die;

            $data['program']   = $this->AdminModel->getData('prospect', $pwhere);
            // print_r($pwhere);
             // print_r($data['program']);
             // die;

            $p1 = $data['program'][0]['p1_program'];

            $p2 = $data['program'][0]['p2_program'];

            $p3 = $data['program'][0]['p3_program'];

            // $p1 = $data['prospects_data'][0]['p1_program'];

            // $p2 = $data['prospects_data'][0]['p2_program'];

            // $p3 = $data['prospects_data'][0]['p3_program'];

            $total_p  = array($p3,$p2,$p1);

            $data['total_p']   = $this->AdminModel->getproData('programs', $total_p);
            $data['intakeP']   = $this->AdminModel->getData('intake');

        // print_r($data['total_p']);die();

         

          //  print_r($data['prospects_data'][0]['p1_program']);die();



            $data['program_data']   = $this->AdminModel->getData('prospect', $pwhere);

            $data['past_education'] = $this->AdminModel->getData('past_education', $where);

            $data['doc_data']       = $this->AdminModel->doc_ckecklist();

            // $data['prospects_data'] = $this->AdminModel->getData('prospect',$pwhere);
            //     echo "<pre>";
            // print_r($data);
            //     die();


        }

        $data['race']  = $this->AdminModel->getData('race');

        $data['religion']  = $this->AdminModel->getData('religion');

        $data['nationalities']  = $this->AdminModel->getData('nationalities');
        $condition                  = array('prospectes_id' => $id);
        $data['documentUploadData']  = $this->AdminModel->getData('document',$condition);

        $this->load->view('add_application', $data);

    }



    public function marketing($id)

    {

        $data['id'] = $id;

        $data       = array();

        if (!empty($id)) {



            $where                      = array('id' => $id);

            $data['prospects_data']     = $this->AdminModel->getData('prospect', $where);

            $data['id']                 = $id;

            $data['marketing_prospect'] = $this->AdminModel->getData('marketing_prospect');

            $this->load->view('marketing', $data);

        }



    }



    public function register($id)

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        

        

        $data['id'] = $id;

        $data       = array();

        if (!empty($id)) {



            $where                  = array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);



            $data['id'] = $id;



            $this->load->view('register_profile', $data);

        }



    }



    public function update_record($id)

    {

        $data['id'] = $id;

        $data       = array();

        if (!empty($id)) {



            $where                  = array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

            $data['id']             = $id;



            $this->load->view('studentupdate', $data);

        }



    }



    public function update_student($id)

    {

        $where    = array('id' => $id);

        $prospect = $this->AdminModel->updateData($where, $_REQUEST, 'prospect');

        redirect(base_url('application/profile/' . $id));

    }



    public function update_profile($id)

    {

        $data = array();

        $this->load->view('update_profile', $data);



    }



    public function materials()

    {

        $data = array();



        $data['materials_data'] = $this->AdminModel->getData('materials');

        $this->load->view('upload', $data);

    }



    public function add_uploads()

    {

        $this->db->insert('materials', $_REQUEST);

        $this->materials();

    }



    public function profile($id = null)

    {

        $data['id'] = $id;

        $data = array();

        if (!empty($id)) {

            $where = array('id' => $id);
            // echo "string3333";
            // echo $where;
            // print_r($where);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

            $nation_id = array('NationalityID' => $data['prospects_data'][0]['nationality']);

            $data['nationalities'] = $this->AdminModel->getData('nationalities', $nation_id);

            $religion_id = array('id' => $data['prospects_data'][0]['religion']);

            $data['religion'] = $this->AdminModel->getData('religion', $religion_id);

            $race_id = array('id' => $data['prospects_data'][0]['race']);

            $data['race'] = $this->AdminModel->getData('race', $race_id);

             //print_r($data['nationalities'][0]['Nationality']) ;die();

            $data['id'] = $id;

            $where_doc = array('prospectes_id' => $id);
            
            $data['intakeA'] = $this->AdminModel->getData('intake');

            $data['document'] = $this->AdminModel->getData('document', $where_doc);

            $where_contact = array('prospects_id' => $id);

            $data['contact'] = $this->AdminModel->getData('contact', $where_contact);
            $pwhere= array('id' => $id);
            $data['program']   = $this->AdminModel->getData('prospect', $pwhere);
            $p1 = $data['program'][0]['p1_program'];
            $p2 = $data['program'][0]['p2_program'];
            $p3 = $data['program'][0]['p3_program'];
            $total_p  = array($p3,$p2,$p1);
            $eligible_program=$data['program'][0]['eligible_program'];

            $data['total_p']   = $this->AdminModel->getproData('programs', $eligible_program);

            // $data['program'] = $this->AdminModel->getData('programs', $where_contact);

            // $where_intake = array('id' => $data['program'][0]['intake']);

            // $data['intake'] = $this->AdminModel->getData('intake', $where_pro);

            $this->load->view('profile', $data);

        }

    }



    public function contact($id = null)

    {



        $data       = array();

        $data['id'] = $id;

        if (!empty($id)) {

            $where                  = array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

            $where                  = array('prospects_id' => $id);

            $data['contact_data']   = $this->AdminModel->getData('contact', $where);

            $data['id']             = $id;

            $data['abcdata']        = $id;

            //echo '<pre>'; print_r($data['prospects_data']); die;

            $this->load->view('contact', $data);

        }



    }



    public function add_contact($id = null, $step = null, $action = null)

    {

        if (isset($_POST['relationship']) && $action == 'edit') {

            $_REQUEST['prospects_id'] = $id;

            $where                    = array('prospects_id' => $id);
            
unset($_REQUEST['/application/add_contact/' . $id . '/step3/' . $action]);
unset($_REQUEST['timezone']);


            // echo '<pre>';print_r($_REQUEST); die;

            $this->AdminModel->saveData($_REQUEST, 'contact');

            redirect(base_url('application/add_app/' . $id . '/step3/' . $action));



        } else {

            $_REQUEST['prospects_id'] = $id;

            $this->AdminModel->saveData($_REQUEST, 'contact');

            redirect(base_url('application/add_app/' . $id . '/step3/' . $action));

        }



    }



    public function add_program($id = null, $step = null, $action = null)

    {

          unset($_REQUEST['prospects_id']);
              unset($_REQUEST['/application/add_program/' . $id . '/step4/' . $action]);
              unset($_REQUEST['timezone']);

      //  print_r($_REQUEST);

     //   echo $id;die();

     

        if (isset($_POST['intake']) && $action != 'edit') {

           // $_REQUEST['prospects_id'] = $id;

             $where  = array('id' => $id);

      

           $this->AdminModel->updateData($where, $_REQUEST, 'prospect');

            redirect(base_url('application/add_app/' . $id . '/step5/' . $action));

        } else {

           // $_REQUEST['prospects_id'] = $id;

            $where  = array('id' => $id);

            // $prospect = $this->AdminModel->updateData($where,$_REQUEST,'programs');



            $this->AdminModel->updateData($where, $_REQUEST, 'prospect');

            redirect(base_url('application/add_app/' . $id . '/step5/' . $action));



        }



    }



    public function assign_prospects()

    {

        $where                  = array();

        $data                   = array();

        $search                 = array();

        $data['prospects_data'] = $this->AdminModel->getData('prospect', $where, $search);

        $this->load->view('assign_prospects', $data);

    }



    public function document($id = null)

    {

        $data       = array();

        $data['id'] = $id;

        if (!empty($id)) {

            $where                  = array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

            $where                  = array('prospectes_id' => $id);

            $data['contact_data']   = $this->AdminModel->getData('document', $where);

            $data['id']             = $id;

            $this->load->view('document', $data);

        }



    }



    public function email($id = null)

    {

        $data                   = array();

        $data['id']             = $id;

        $where                  = array('id' => $id);

        $where1                 = array('prospects_id' => $id);

        $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

        $data['email_data']     = $this->AdminModel->getData('prospects_email', $where1);

        $this->load->view('email', $data);



    }



    public function letter($id = null)

    {

        $data = array();



        $data['id']             = $id;

        $where                  = array('id' => $id);

        $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

        $this->load->view('letter', $data);



    }



    public function fine($id = null)

    {

        $data                   = array();

        $data['id']             = $id;

        $where                  = array('id' => $id);

        $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);

        $this->load->view('letter', $data);



    }



        public function view($id)
        {
            $data = array();
            $data['id'] = $id;
            $where = array('id' => $id);
            $wherep = array('prospects_id' => $id);
            $data['prospects_data'] = $this->AdminModel->getData('prospect', $where);
            $state_id = array('id' => $data['prospects_data'][0]['state']);
            $data['state_id']   = $this->AdminModel->getData('states', $state_id);
            $country_id = array('id' => $data['prospects_data'][0]['country']);

            $data['studyMode'] = $this->AdminModel->getData('study_mode');
            $data['nationality'] = $this->AdminModel->getData('nationalities');

            $data['country_id']   = $this->AdminModel->getData('countries', $country_id);
            $p1 = $data['prospects_data'][0]['p1_program'];
            $p2 = $data['prospects_data'][0]['p2_program'];
            $p3 = $data['prospects_data'][0]['p3_program'];
            $total_p  = array($p1,$p2,$p3);
            $data['intakedata']   = $this->AdminModel->getData('intake');
            $data['total_p']   = $this->AdminModel->getproData('programs', $total_p);
            $this->load->view('view_application', $data);
        }

        public function add_education($id, $step, $action = '')
        {
            if ($action == 'edit') {
            $_REQUEST['prospects_id'] = $id;
            $where = array('prospects_id' => $id);
            unset($_REQUEST['/application/add_education/' . $id . '/step2/' . $action]);
            unset($_REQUEST['timezone']);
            //$_REQUEST['education_id'] = 7;
            $this->AdminModel->saveData($_REQUEST, 'past_education');
            redirect(base_url('application/add_app/' . $id . '/step2/' . $action));
            } else {
                $_REQUEST['prospects_id'] = $id;
                $this->AdminModel->saveData($_REQUEST, 'past_education');
                redirect(base_url('application/add_app/' . $id . '/step2/' . $action));
            }
        }



    public function review_submit($id = null, $step2 = null, $action = null)

    {

        

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        

        //echo uniqid();die();

      //  print_r($_REQUEST);die();

        $where                       = array('id' => $id);

        $data['application_status '] = '1';
        $data['eligible']=$_POST['eligible'];
        $data['eligible_program']=$_POST['eligible_program'];

        $this->AdminModel->updateData($where, $data, 'prospect');

        $sql     = $this->db->query('SELECT name, email , id_no,  id,profile_pic FROM  prospect where id = ' . $id);

        $results = $sql->result_array();



        // print_r($results['name']);die();



   //     $userdata['name']        = $results[0]['name'];

   //     $userdata['username']        = $results[0]['name'];

   //     $userdata['email']       = $results[0]['email'];

   //     $userdata['id_no']       = $results[0]['id_no'];

   //     $userdata['prospect_id'] = $results[0]['id'];

    //    $userdata['password']    = md5($userdata['email']);

   //     $userdata['profile_pic'] = $results[0]['profile_pic'];

    //    $userdata['user_type']   = 3;

   //     $this->AdminModel->saveData($userdata, 'users');



       // redirect(base_url('application/profile/' . $id));

       redirect(base_url('application/application'));

    }

    

    

     public function prospect_status($id = null)

    {

        //echo uniqid();die();

      //  print_r($_REQUEST);die();

        $where                       = array('id' => $id);

        $data['prospect_status '] = '2';

        $this->AdminModel->updateData($where, $data, 'prospect');


       

       redirect(base_url() . 'application/application/'.$id);

        // redirect(base_url('application/application'));

    }

    

    public function prospect_status1($id = null)

    {

        $where                       = array('id' => $id);

        $data['prospect_status '] = '3';

        $this->AdminModel->updateData($where, $data, 'prospect');





        redirect(base_url('addmision/registration/'.$id));

    }



    public function send_email($id)

    {

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

        $where = array('id' => $id);

        $data['application_status '] = '2';

        $this->AdminModel->updateData($where, $data, 'prospect');

        $sql = $this->db->query('SELECT * FROM  prospect where id = ' . $id);

        $results = $sql->result_array();



        $userdata['name']        = $results[0]['name'];

        $userdata['username']        = $results[0]['name'];

        $userdata['email']       = $results[0]['email'];

        $userdata['prospect_id'] = $results[0]['id'];

        $userdata['password']    = md5($userdata['email']);

        $userdata['profile_pic'] = $results[0]['profile_pic'];

        $userdata['user_type']   = 3;

        $inserted_id = $this->AdminModel->saveData($userdata, 'users');
       
        unset($userdata['username']);

        unset($userdata['prospect_id']);

        unset($userdata['password']);

        unset($userdata['user_type']);

        unset($userdata['profile_pic']);

        $userdata['user_id '] =  $inserted_id;

        $userdata['secondary_name'] = $results[0]['secondary_name'];

        $userdata['secondary_email'] = $results[0]['secondary_email'];

        $userdata['international'] = $results[0]['international'];

        $userdata['emergency_contact'] = $results[0]['emergency_contact'];
         $userdata['image_path'] = $results[0]['profile_pic'];

        $userdata['gender'] = $results[0]['gender'];
        $userdata['nationality'] = $results[0]['nationality'];
        $userdata['p1_program'] = $results[0]['p1_program'];

        $userdata['p2_program'] = $results[0]['p2_program'];

        $userdata['p1_study_mode'] = $results[0]['p1_study_mode'];

        $userdata['p1_campus'] = $results[0]['p1_campus'];

        $userdata['address_line1'] = $results[0]['address_line1'];

        $userdata['address_line2'] = $results[0]['address_line2'];

        $userdata['intake'] = $results[0]['intake'];
        
        $userdata['eligible_program'] = $results[0]['eligible_program'];

        $userdata['city'] = $results[0]['city'];

        $userdata['state'] = $results[0]['state'];

        $userdata['country'] = $results[0]['country'];

        $userdata['postcode'] = $results[0]['postcode'];

        // $userdata['class_id'] = $results[0]['class_id'];

        // $userdata['subject_id'] = $results[0]['subject_id'];

        $userdata['race'] = $results[0]['race'];

        $userdata['marital_id'] = $results[0]['marital']; 

        $userdata['place_of_birth'] = $results[0]['place_of_birth'];

        $userdata['date_of_birth'] = $results[0]['date_Of_birth'];

        $userdata['salutation_id'] = $results[0]['salutation'];
        $userdata['religion'] = $results[0]['religion'];
        $userdata['phone'] = $results[0]['phone'];

        $userdata['type'] = "1";
        $userdata['created_at'] = $results[0]['created_at'];
        $userdata['created_by'] = $results[0]['created_by'];
        

        $userdata['idtype'] = $results[0]['idtype'];
        $userdata['id_no'] = $results[0]['id_no'];
        
        $inserted_students = $this->AdminModel->saveData($userdata, 'students');

        // $this->AdminModel->updateData($wherep, $dataa, 'students');

        $arr = array(

            'program_id'=>$results[0]['eligible_program'],

            'campus_id'=>$results[0]['p1_campus'],

            'intake_id'=>$results[0]['intake'],

            'student_id'=>$inserted_students

        );

        $student_programs = $this->AdminModel->saveData($arr, 'student_programs');

        $name  = $results[0]['name'];

        $email = $results[0]['email'];

        $from_email = "adminksbb@lmsmalaysia.com";

        $this->load->library('email');

        $this->email->from($from_email, 'Login Details');

        $this->email->to($email);

        $this->email->subject('Your Account Login Details');

        $this->email->message('Dear student,Your registration is successful.

Please use the below login details to access your dashboard.

User Name :'.$email.'

Password :'.$email.' 



Dashboard Link : 

http://lmsmalaysia.com/



Thank You !

Koperasi Siswazah Bangi Berhad');



            //Send mail

        if ($this->email->send()) {

            $this->session->set_flashdata("email_sent", "Email sent successfully.");

        } else {

            $this->session->set_flashdata("email_sent", "Error in sending Email.");

            //  $this->load->view('email_form');

        }

        redirect(base_url('addmision/registration'));

    }



    public function uploadData()

    {

        $count = 0;

        $fp    = fopen($_FILES['userfile']['tmp_name'], 'r') or die("can't open file");

        $i     = 1;

        $datac = array();

        while ($csv_line = fgetcsv($fp, 1024)) {



            //echo '<pre>'; print_r($csv_line);



            $count++;

            if ($count == 1) {

                $datac = $csv_line;



                continue;



            } //keep this if condition if you want to remove the first row



            $data_n = array();

            for ($i = 1; $i < count($csv_line); $i++) {

                for ($j = 0; $j < count($datac); $j++) {



                    $data_n[$datac[$j]] = $csv_line[$j];



                }

            }



            $i++;

            if ($_REQUEST['tabname'] == 'Prospects') {



                $table = 'prospect';



            } else if ($_REQUEST['tabname'] == 'Applications') {

                $table = 'prospect';



            } else if ($_REQUEST['tabname'] == 'Agencies') {

                $table = 'agency';

            } else if ($_REQUEST['tabname'] == 'Agents') {

                $table = 'agent';

            } else if ($_REQUEST['tabname'] == 'Materials') {

                $table = 'materials';

            }



            $this->AdminModel->saveData($data_n, $table);



        }

        fclose($fp) or die("can't close file");

        $data['success'] = "success";

        return $data;

    }



    public function import_csv()

    {

        $data = array();

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('importcsv', $data);

        $this->load->view('layout/footer');



    }

    public function prop_email($id)

    {

        $_REQUEST['prospects_id'] = $id;

        $this->AdminModel->saveData($_REQUEST, 'prospects_email');



        $sql     = $this->db->query('SELECT name, email FROM  prospect where id = ' . $id);

        $results = $sql->result_array();



        $name  = @$results[0]['subject'];

        $email = @$results[0]['massage'];



        $to = $results[0]['email'];

        $to = 'User  Email ';

        $message .= 'Here is the account detils';

        $message .= '<tabel>';

        $message .= '<tr><td>Subject </td><td>' . @$name . '</td></tr>';

        $message .= '<tr><td>Message </td><td>' . @$email . '</td></tr>';

        $message .= '</tabel>';



        $headers = "MIME-Version: 1.0" . "\r\n";

        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers

        $headers .= 'From: <noreply@insonix.com>' . "\r\n";

        @mail($to, $subject, $message, $headers);



        redirect(base_url('application/email/' . $id));

    }



    public function add_past($id)

    {

        $data       = array();

        $data['id'] = $id;

        $this->load->view('past_education', $data);

    }

    public function register_profile($id)

    {

        $data       = array();

        $data['id'] = $id;

        $this->load->view('register_profile_tab', $data);

    }

    public function register_sem($id)

    {

        $data       = array();

        $data['id'] = $id;

        $this->load->view('semester_register', $data);

    }

    public function timeline($id)

    {

        $data       = array();

        $data['id'] = $id;

        $this->load->view('timeline', $data);

    }

    public function addm_app($id)

    {

        $data       = array();

        $data['id'] = $id;

        $this->load->view('application_profile1', $data);

    }



    public function export_csv()

    {



        $filename = 'application_' . date('Ymd') . '.csv';

        header("Content-Description: File Transfer");

        header("Content-Disposition: attachment; filename=$filename");

        header("Content-Type: application/csv; ");



        // get data

        $where = array();



        $ids = array('96', '97');

        //$sql  =  $this->db->query('SELECT *  FROM  prospect where id  in ('.  implode(" , ",$_REQUEST['id']) .')' );

        $sql     = $this->db->query('SELECT *  FROM  prospect where id = 96');

        $results = $sql->result_array();



        // $usersData = $this->AdminModel->getData('prospect',$where);;



        // file creation

        $file = fopen('php://output', 'w');



        $header = array_keys($results[0]);

        fputcsv($file, $header);

        foreach ($results as $key => $line) {



            fputcsv($file, $line);

            foreach ($line as $key => $value) {



                //     fputcsv($file,$line);

            }

            //

        }

        fclose($file);

        exit;



    }



    public function get_state()

    {

        $id    = $_REQUEST['id'];

        $where = array('country_id' => $id);

        $state = $this->AdminModel->getData('states', $where);

        $html  = '<option> Select  </option>';



        foreach ($state as $name) {

            $html .= '<option value="' . $name['id'] . '"> ' . $name['name'] . '</option>';



        }



        echo $html;

    }



    public function updat_markeitng($id, $tab, $marketing_prospect_id, $action)

    {



        $data                       = array();

        $data['id']                 = $id;

        $where                      = array('id' => $marketing_prospect_id);

        $data['marketing_prospect'] = $this->AdminModel->getData('marketing_prospect', $where);



        //echo  '<pre>'; print_r($data['marketing_prospect']); die;

        $data['campus_program'] = $this->AdminModel->getData('campus_program');

        $data['campus']         = $this->AdminModel->getData('campus');

        $data['study_mode']     = $this->AdminModel->getData('study_mode');

        $data['intake']         = $this->AdminModel->getData('add_intake');

        $this->load->view('update_marketing', $data);



    }



    public function marketing_add($id, $tab, $action)

    {



        if (!empty($_REQUEST['marketing_source'])) {



            unset($_REQUEST['submit']);

            $_REQUEST['prospects_id'] = trim($id);

            $this->AdminModel->saveData($_REQUEST, 'marketing_prospect');

            redirect(base_url('marketing/' . $id));



        }

        $data['campus_program'] = $this->AdminModel->getData('campus_program');

        $data['campus']         = $this->AdminModel->getData('campus');

        $data['study_mode']     = $this->AdminModel->getData('study_mode');

        $data['intake']         = $this->AdminModel->getData('add_intake');



        $data['id'] = $id;

        $this->load->view('create_marketing', $data);



    }



    public function marketing_follwup($id, $tab, $prospect_id, $acticon)

    {

        $data['id'] = $id;

        $this->load->view('create_marketing', $data);



    }



    public function sendMailAdmin($from_email="adminksbb@lmsmalaysia.com",$to_email="adminksbb@lmsmalaysia.com",$subject="adminTestSubject",$message="test"){

        



            $this->load->library('email');



            $this->email->from($from_email, 'Student Details');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);

             if ($this->email->send()) {

                

                $this->session->set_flashdata("email_sent", "Email sent successfully.");

                //echo "success";

                return true;

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                //echo "failure";

                return false;

                //  $this->load->view('email_form');

            }



    }



    public function registerform()

    {

        $data['country'] = $this->AdminModel->getData('countries');

        $data['all_programs'] = $this->AdminModel->getData('programs');

        $data['nationalities'] = $this->AdminModel->getData('nationalities');

        $data['study_mode'] = $this->AdminModel->getData('study_mode');

        $data['race'] = $this->AdminModel->getData('race');

        $data['religion'] = $this->AdminModel->getData('religion');

        $data['campus'] = $this->AdminModel->getData('campus');

        $data['class'] = $this->AdminModel->getData('class');

        $data['subject'] = $this->AdminModel->getData('subjects');
        $condition= array('status' => 1);
        $data['intakeP']   = $this->AdminModel->getData('intake',$condition);

        if (isset($_POST['submit'])) {
            
           // print_r($_REQUEST);die;
            
             unset($_REQUEST['/Application/registerform']);
              unset($_REQUEST['timezone']);

             $check_email = $this->AdminModel->checkEmailprospect($_REQUEST['email']);

             $checkIdNo = $this->AdminModel->checkIdNo($_REQUEST['id_no']);

            if($check_email == true) {

                 $this->session->set_flashdata('msg', '<div class="alert alert-danger" style=" color:#000 !important;">Email Already Exist</div>');

                              

                redirect(base_url('application/registerform'));

            } else if($checkIdNo == true) {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger" style=" color:#000 !important;">ID No Already Exist</div>');

                redirect(base_url('application/registerform'));

            } else{

            unset($_REQUEST['submit']);

            $_REQUEST['created_by'] = "student";

            // unset($_REQUEST['city']);

            // unset($_REQUEST['country']);

            // unset($_REQUEST['state']);

            // unset($_REQUEST['postcode']);

            $id = $this->AdminModel->saveData($_REQUEST, 'prospect');
          //  print_r($id);
          //  die;

            

                    // unset($_REQUEST['idtype']);

                    // unset($_REQUEST['secondary_name']);

                    // unset($_REQUEST['emergency_contact']);

                    // unset($_REQUEST['address_line1']);

                    // unset($_REQUEST['address_line2']);

                    // unset($_REQUEST['Intake']);

                    // unset($_REQUEST['p1_campus']);

                    // unset($_REQUEST['p1_program']);

                    // unset($_REQUEST['p1_study_mode']);

                    // unset($_REQUEST['p2_campus']);

                    // unset($_REQUEST['p2_program']);

                    // unset($_REQUEST['p2_study_mode']);

                    // unset($_REQUEST['created_by']);

                   //  unset($_REQUEST['class_id']);

                    //  unset($_REQUEST['subject_id']);

               //$idd = $this->AdminModel->saveData($_REQUEST, 'students');

            $name  = $_REQUEST['name'];

            

            $email = $_REQUEST['email'];

            $email22="adminksbb@lmsmalaysia.com";

            $phone = $_REQUEST['phone'];



            $from_email = "adminksbb@lmsmalaysia.com";
             $adminemail = "financeksbb@lmsmalaysia.com";


            $this->load->library('email');



            $userSubject = 'Welcome to UKM Master of Education Executive Programme';

             $adminSubject = 'New Prospect Notification';

             

             // $userSubject = 'Welcome To Koperasisiswazah University';

             // $adminSubject = 'New Prospect Notification';

             

             $userMessage='Dear Student,



Thank you for submitting your prospect. Your application is currently under review. A representative from Koperasi Siswazah Bangi Berhad will contact you soon.



Thank You !

Koperasi Siswazah Bangi Berhad';                     

             

            $adminMessage='Hi Admin,

A new prospect is added in the system.

Please review the prospect and process for the student registration.

http://lmsmalaysia.com/application/prospects

Here are the Student Detail : 

Name :'.$name.'

Email :'.$email.'

Thank You';



            



             //send mail to user

          $userMailResponse = $this->sendMailAdmin($from_email, $email, $userSubject, $userMessage);

           // send mail to admin

          $adminMailResponse = $this->sendMailAdmin($from_email, $email22, $adminSubject,$adminMessage);



            // $this->email->from($from_email, 'Student Details');

            // $this->email->to($email);

            // $this->email->subject('Here is the account details');

            // $this->email->message($name);



            //Send mail

            // if ($this->email->send()) {

            //     $this->session->set_flashdata("email_sent", "Email sent successfully.");

            // } else {

            //     $this->session->set_flashdata("email_sent", "Error in sending Email.");

            //     //  $this->load->view('email_form');

            // }



            redirect(base_url() . 'Application/thankyou');

            return;

        }   } else {

            $this->load->view('registerform', $data);



        }

        $this->load->view('layout/footer');

    }



    public function submit()

    {



        $this->load->view('submit');



    }

    public function thankyou()

    {



        $this->load->view('thankyou');



    }

    public function welcome()

    {

        $data['get_employee_info'] = $this->AdminModel->get_data_by_id('users','id',1);

        //$this->load->view('layout/header');

        $this->load->view('welcome_lms',$data);

    }

    public function get_data_by_id($table,$field,$id)

    {

        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);

        return $get_data_by_id;

    }

}