<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Acadmic extends CI_Controller {

   
    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');   // loads admin model
         if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login')); 
        }

    }
    public function index()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('welcome_message');
        $this->load->view('layout/footer');
        

    }

     //classess
	public function classes()
    {  
		$where = array();
        $data = array();
		 
		//echo '<pre>'; print_r($data); die;
		if(isset($_POST['faculty']) && isset($_POST['academic_session']) ) {
				$where['faculty'] =  $_POST['faculty'];
				$where['academic_session'] =  $_POST['academic_session'];
				if($_POST['course'] != '') {
					$where['course'] =  $_POST['course'];
				}
				
				
			$data['class_list'] = $this->AdminModel->getData('classes', $where);
			$row_data  = array();
			foreach($data['class_list'] as $key => $value) {
			 $row_data[$value['id']] =  $value;
			}
			$data['row_data'] =  $row_data; 
				
		}
		
		
		$data['faculty_data'] = $this->AdminModel->getData('faculty', '');
		
		$data['academic_session'] = $this->AdminModel->getData('academic_session', '');
		$data['course_data'] = $this->AdminModel->getData('course', '');
		//echo '<pre>'; print_r($data); die;
       
        $this->load->view('acadmic/classes', $data);
    
        

    }

	public function student($id){
		$data = array();
		$sql = 'SELECT c.name as course_name,  f.name as faculty_name, acs.name as acadmy_name,  cc.class, cc.lecturer,cc.total_student from classes AS cc  LEFT JOIN course c ON cc.course =  c.id  LEFT JOIN faculty as f ON  cc.course = f.id LEFT JOIN academic_session acs on cc.academic_session = acs.id  where cc.id = '.$id;
		$query =  $this->db->query($sql);
		$data['student_data'] =  $query->result_array();
		
		$this->load->view('acadmic/student', $data);	
	}
    
    public function add_classes($id = '') {
		if(empty($id)) {
			if(isset($_POST['academic_session']) && isset($_POST['class']) )  {
		 	
				$savedata =  $this->AdminModel->saveData($_POST,'classes' ); 
				redirect(base_url('Acadmic/classes'));
			}
		} else {
			$where =  array('id' => $id); 
			
			$this->AdminModel->updateData($where,$_POST,'classes');
			redirect(base_url('Acadmic/classes'));
		}
		
	}
	
	public function mark($id) {
		$data = array();
		$sql = 'SELECT c.name as course_name,  f.name as faculty_name, acs.name as acadmy_name,  cc.class, cc.lecturer,cc.total_student from classes AS cc  LEFT JOIN course c ON cc.course =  c.id  LEFT JOIN faculty as f ON  cc.course = f.id LEFT JOIN academic_session acs on cc.academic_session = acs.id  where cc.id = '.$id;
		$query =  $this->db->query($sql);
		$data['student_data'] =  $query->result_array();
		
		$this->load->view('acadmic/mark', $data);	
	}
	
	public function attendances($id) {
		$data = array();
		$sql = 'SELECT c.name as course_name,  f.name as faculty_name, acs.name as acadmy_name,  cc.class, cc.lecturer,cc.total_student from classes AS cc  LEFT JOIN course c ON cc.course =  c.id  LEFT JOIN faculty as f ON  cc.course = f.id LEFT JOIN academic_session acs on cc.academic_session = acs.id  where cc.id = '.$id;
		$query =  $this->db->query($sql);
		$data['student_data'] =  $query->result_array();
		
		$this->load->view('acadmic/attendance_edit', $data);	
	}
	
	public function update_status($id) {
		$data = array();
		$sql = 'SELECT c.name as course_name,  f.name as faculty_name, acs.name as acadmy_name,  cc.class, cc.lecturer,cc.total_student from classes AS cc  LEFT JOIN course c ON cc.course =  c.id  LEFT JOIN faculty as f ON  cc.course = f.id LEFT JOIN academic_session acs on cc.academic_session = acs.id  where cc.id = '.$id;
		$query =  $this->db->query($sql);
		$data['student_data'] =  $query->result_array();
		
		$this->load->view('acadmic/student_course_status', $data);	
	}
     //classess
	public function course_offerd()
    {
		//$data['course_offerd'] = $this->AdminModel->getData('classes', $where);
        
        $this->load->view('acadmic/course_offerd');
       
        

    }
    public function marks()
    {
        $where = array();
        $data = array();
		
		//echo '<pre>'; print_r($data); die;
		if(isset($_POST['faculty']) && isset($_POST['academic_session']) ) {
				$where['faculty'] =  $_POST['faculty'];
				$data['faculty'] =  $_POST['faculty'];
				$where['academic_session'] =  $_POST['academic_session'];
				$data['academic_session'] =  $_POST['academic_session'];
				
				if($_POST['course'] != '') {
					
					$where['course'] =  $_POST['course'];
					$data['course'] =  $_POST['course'];
				}
				
				
				$data['class_list'] = $this->AdminModel->getData('classes', $where);
				
		}
		$data['faculty_data'] = $this->AdminModel->getData('faculty', '');
		
		$data['academic_session'] = $this->AdminModel->getData('academic_session', '');
		$data['course_data'] = $this->AdminModel->getData('course', '');
        
        $this->load->view('acadmic/mark_main', $data);

    }

    public function attendance()
    {
		$where = array();
        $data = array();
		if(isset($_POST['faculty']) && isset($_POST['academic_session']) ) {
				$where['faculty'] =  $_POST['faculty'];
				$where['academic_session'] =  $_POST['academic_session'];
				if($_POST['course'] != '') {
					$where['course'] =  $_POST['course'];
				}
				
				
				$data['class_list'] = $this->AdminModel->getData('classes', $where);
				
		}
		$data['faculty_data'] = $this->AdminModel->getData('faculty', '');
		
		$data['academic_session'] = $this->AdminModel->getData('academic_session', '');
		$data['course_data'] = $this->AdminModel->getData('course', '');
        $this->load->view('acadmic/attendance', $data);

    }
    public function assignments()
    {
        $where = array();
		$data['assignment_list'] = $this->AdminModel->getData('assignment', $where);
		$this->load->view('acadmic/assignment', $data);
    }
     //classess
	public function create_assignment($id='')
    {   $data = array();
		if($id !='') {
			$data['id']= $id;
			$where = array('id' => $id);
			$data['assignment'] =  $this->AdminModel->getData('assignment', $where);
		}
        $this->load->view('acadmic/create_assignment', $data);
    }
    public function addassignment($id='')
    {
		if(!empty($id)) {
			$where = array('id' => $id);
			$data =  $_REQUEST;
        if(!empty($_FILES['attachment'])) {
			
			$sourcePath = $_FILES['attachment']['tmp_name'];       // Storing source path of the file in a variable
			$targetPath = "uploads/assignment/".$_FILES['attachment']['name']; // Target path where file is to be stored
			move_uploaded_file($sourcePath,$targetPath) ; 
			$data['attachment'] = $_FILES['attachment']['name'];
			
		}
		
		$prospect = $this->AdminModel->updateData($where,$data,'assignment');
		} else {
		$data =  $_REQUEST;
        if(isset($_FILES['attachment'])) {
			
			$sourcePath = $_FILES['attachment']['tmp_name'];       // Storing source path of the file in a variable
			$targetPath = "uploads/assignment/".$_FILES['attachment']['name']; // Target path where file is to be stored
			move_uploaded_file($sourcePath,$targetPath) ; 
			$data['attachment'] = $_FILES['attachment']['name'];
			
		}
		
		$savedata =  $this->AdminModel->saveData($data,'assignment');
		}
		redirect(base_url('acadmic/assignments'));
    }  
        

   

    public function row_edit() {
    	$data = array();
    	$id= $_REQUEST['id'];
    	$where = array('id' => $id);
    	 $data['campus_data'] = $this->AdminModel->getData('campus_program', '');
    	 $data['edit_data'] = $this->AdminModel->getData('classes', $where);
    	echo $this->load->view('acadmic/row_edit', $data,true);
    	
    }

	

}
