<?php $session_data = $this->session->userdata['loggedInData']; ?>
<link href="<?php echo  CSS_PATH; ?>css/bootstrap_multiselect.css" rel="stylesheet" type="text/css">
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Edit Documents</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                        <!--    <li class="breadcrumb-item active">Upload Documents</li>-->
                        <!--</ol>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card m-b-30">
        <div class="card-body">
            <form class="" enctype="multipart/form-data" method="post">
                <div class="form-group"> 
                    <!-- <?php echo "<pre>";
                print_r($get_document_info);  ?>  -->
                            <label>Title</label>
                            <input type="text" class="form-control" required="" placeholder="" name="title" value="<?php echo $get_document_info['title']; ?>"> 
                            <!-- <input type="text" class="form-control"  placeholder="Enter Title" name="title">  -->
                </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" class="form-control" rows="4" placeholder="Enter Description" name="description"  id="message" style="width:100%; height:100px!important; resize:none;" required /><?php echo $get_document_info['description']; ?></textarea>
                           <!--  <textarea type="text" class="form-control" rows="4"  placeholder="Enter Description" name="description"></textarea> -->
                        </div>

            <h4 class="mt-0 header-title">Upload Documents</h4>
            <div class="m-b-30">
                <?php if($get_document_info['file'] !='') { ?>
                <div class="col-md-6">
                <div class="form-group"><input type="text" name="fileToUpload[]" value="<?php echo $get_document_info['file']; ?>" class="form-control"><a href="<?php echo base_url('document/delete_file?id='.$get_document_info['id']) ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                </div>
                <?php } else { ?>
                <div><input type="file" name="fileToUpload[]" required="" value="<?php echo $get_document_info['file']; ?>"><span></span></div>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php if ($session_data['user_type'] == '3') { ?>
                    <label for="exampleInput"><strong>Select Teacher</strong></label><br>
                    <select id="lstFruits" class="form-control" multiple="multiple" name= "students[]">
                        <?php if(!empty($teachers_data)) { 
                          foreach ($teachers_data as $value) { 
                              $existing_employee_ids = explode(',',$get_document_info['student_id']);
                          ?>
                        <option <?php if(in_array($value['id'], $existing_employee_ids)) {  ?> selected <?php } ?> value="<?php echo $value['user_id']; ?>"><?php echo $value['name']; ?></option>
                        <?php } } ?>
                    </select>
                    <?php } else { ?>
                    <label for="exampleInput"><strong>Select Student</strong></label><br>
                    <select id="lstFruits" class="form-control" multiple="multiple" name= "students[]">
                        <?php if(!empty($students_data)) { 
                          foreach ($students_data as $value) { 
                            $existing_employee_ids = explode(',',$get_document_info['student_id']);
                            ?>
                        <option <?php if(in_array($value['id'], $existing_employee_ids)) {  ?> selected <?php } ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                        <?php } } ?>
                    </select>
                    <?php } ?>
                </div>
            </div>
            <div class="text-center m-t-15">
                <button type="submit" name="update" class="btn btn-dark btn-lg waves-effect waves-light">Process File</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>

<script type="text/javascript">
  $(function () {
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
        $('#btnSelected').click(function () {
            var selected = $("#lstFruits option:selected");
            var message = "";
            selected.each(function () {
            });
        });
    });
</script>