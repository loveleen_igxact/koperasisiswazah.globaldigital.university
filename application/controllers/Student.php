<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Student extends CI_Controller {



    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     *      http://example.com/index.php/welcome

     *  - or -

     *      http://example.com/index.php/welcome/index

     *  - or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */

    function __construct() {

        parent::__construct();

        $this->load->library(array('form_validation','session'));

        $this->load->helper(array('form','url'));

        $this->load->model('AdminModel');   // loads admin model

        $this->CI = & get_instance();

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

    }

    // public function index()

    // {
    //      $session_data = $this->session->userdata['loggedInData'];
    //      if($session_data['user_type'] == '3') {
    //      $where = array('user_id'=> $session_data['id']);
    //      $students = $this->AdminModel->getData('students',$where);
    //       $wherep = array('class_id'=> $students[0]['class_id'] , 'subject_id'=>$students[0]['subject_id']);
    //      $data['teachers_data'] = $this->AdminModel->getData('staffs',$wherep);
    //    }else{
    //     $data['teachers_data'] = $this->AdminModel->getData('staffs');
    //    }

    //     $this->load->view('layout/header');

    //     $this->load->view('layout/sidebar');

    //     $this->load->view('teacher/teacher',$data);

    //     $this->load->view('layout/footer');

    // }

    public function get_data_by_id($table,$field,$id)

    {

        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);

        return $get_data_by_id;

    }
    public function students_list()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $whereStudent = array('type'=> '1');

        if($session_data['user_type'] == '4') {

           

         $where = array('user_id'=> $session_data['id']);

         $teacher = $this->AdminModel->getData('staffs',$where);

          $wherep = array('class_id'=> $teacher[0]['class_id'] , 'subject_id'=>$teacher[0]['subject_id'],'type'=>'1');

  

         $data['students_data'] = $this->AdminModel->getData('students',$wherep);

        

        } else{

            if (isset($_POST['search']) ) {

                if (!empty($_POST)) {

                    $data['students_data'] = $this->AdminModel->searchStudent($_POST);

                } else {

                    $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

                }

            } else if (isset($_POST['search_button']) ) {

                if (!empty($_POST)) {

                    $data['students_data'] = $this->AdminModel->searchStudent($_POST);

                } else {

                    $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

                }

            } else{

                $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

            }

        }  

        //print_r($data); die;

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('student/students_list',$data);

        $this->load->view('layout/footer');

    }

      public function profile($id = null)

    {

        $data['id'] = $id;

        $data = array();

        if (!empty($id)) {

            $where = array('user_id' => $id);

            $data['user_info'] = $this->AdminModel->get_data_by_id('students','user_id',$id);

            

            // echo "<pre>"; print_r($data); die;

            $nation_id = array('NationalityID' => $data['user_info']['nationality']);

            $data['nationalities'] = $this->AdminModel->getData('nationalities', $nation_id);

            // if ($data['prospects_data'][0]['religion'] == "0") {

            //     $religion_id = array('id' => $data['prospects_data'][0]['religion']);

            //     $data['religion'] = $this->AdminModel->getData('religion', $religion_id);

            // } else {

            //     $data['religion'] = "";

            // }

            // $race_id = array('id' => $data['prospects_data'][0]['race']);

            // $data['race'] = $this->AdminModel->getData('race', $race_id);

            // $data['id'] = $id;

            $where_doc = array('prospectes_id' => $id);

            $data['religion']  = $this->AdminModel->getData('religion');

            $data['race']  = $this->AdminModel->getData('race');
            $data['intakeA']  = $this->AdminModel->getData('intake');
            $data['programsfinal']  = $this->AdminModel->getData('programs');

            $data['document'] = $this->AdminModel->getData('document', $where_doc);

            $where_contact = array('prospects_id' => $id);

            $data['contact'] = $this->AdminModel->getData('contact', $where_contact);

            // $data['program'] = $this->AdminModel->getData('programs', $where_contact);

            // $where_intake = array('id' => $data['program'][0]['intake']);

            //$data['intake'] = $this->AdminModel->getData('intake', $where_pro);

            $this->load->view('student/profile', $data);

        }

    }



    public function edit_std($id = null)

    {

        

       // echo "edit";die();

        $id = $_GET['id'];

    // echo $_GET['id'];

        // echo $id;

        // die();

        // $data = array();

        // echo $data; die;

        // $where = array('user_id'=> $_GET['id']);

        // $where = array('user_id' => $id);

            $where = array('user_id' =>  $_GET['id']);

            $data['user_info'] = $this->AdminModel->get_data_by_id('students','user_id',$id);

        // echo "<pre>";

        // print_r($data);die;

            $data['race']  = $this->AdminModel->getData('race');

            $data['religion']  = $this->AdminModel->getData('religion');

            $data['nationalities']  = $this->AdminModel->getData('nationalities');

            $pwhere= array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $pwhere);



       

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('student/edit_std',$data);

        $this->load->view('layout/footer');

        if (isset($_POST['update'])) {
            
            if (empty($_FILES['fileToUpload']['name'])) {
               $name=$data['user_info']['image_path'];
            }
            else{

            $uploads_dir = 'uploads/';
            $name = $_FILES['fileToUpload']['name'];
            if (is_uploaded_file($_FILES['fileToUpload']['tmp_name']))
            {       
                //in case you want to move  the file in uploads directory
                 move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploads_dir.$name);
                 
            }
              }

            $arr = array(

                'name'=>$_POST['name'],

                // 'username'=>$_POST['name'],

                'email'=>$_POST['email'],

                'gender'=>$_POST['gender'],

                // 'salutation_id'=>$_POST['salutation_id'],
                'student_no'=>$_POST['studentNo'],

                'phone'=>$_POST['phone'],

                'nationality'=>$_POST['nationality'],

                'religion'=>$_POST['religion'],

                'race'=>$_POST['race'],
                'image_path'=>$name,

                'place_of_birth'=>$_POST['place_of_birth'],

                'date_of_birth'=>$_POST['date_of_birth'],

                'marital_id'=>$_POST['marital_status'],

            );

            $where = array('id'=>$_POST['student_number']);

           

            $this->AdminModel->updateData($where,$arr,'students');

            

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Record Updated Successfully.</div>');

            redirect(base_url('student/students_list'));

            

        }
        if (isset($_POST['cancel'])) {
            redirect(base_url('student/students_list'));
        }
        

    }

    }