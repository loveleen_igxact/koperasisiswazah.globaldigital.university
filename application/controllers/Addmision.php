<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addmision extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');	// loads admin model
        $this->load->helper('new_helper');
        $this->CI = & get_instance();
        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login')); 
        }
    }
	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('welcome_message');
		$this->load->view('layout/footer');
	}

    public function prospects() {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');

        $data['prospect_data'] = $this->AdminModel->getData('prospect');
        //echo '<pre>';print_r($data['prospect_data']); die;
        $this->load->view('prospects' , $data);
        $this->load->view('layout/footer');
    }


    public function add_prospects() {

         //echo '<pre>'; print_r($_REQUEST);
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        if(isset($_POST['submit'])) {
            unset($_REQUEST['submit']);
            $this->db->insert('prospect',$_REQUEST);
             $this->prospects();
            return;
        } else {
            $this->load->view('add_prospects');

        }
        $this->load->view('layout/footer');
    }


	public function follw() {
      $data['follw_data'] = $this->AdminModel->getData('follow_up');

        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('follow_up', $data);
        $this->load->view('layout/footer');

    }
    public function agencies() {

        if(isset($_POST['search_btn'])) {

        }

        $data['agencies_data'] = $this->AdminModel->getData('agency');
        $agency =  array();
        foreach ($data['agencies_data'] as  $records) {
            $agency[$records['id']] = $records;

        }
        $data['agency'] =  $agency;
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('agency',$data);    
        $this->load->view('layout/footer');


    }
    public function add_agencies() {
       if(isset($_POST['registration_no'])) { 
             $this->db->insert('agency',$_REQUEST);
            $this->agencies();
            return;
        }
    }

    public function edit_agencies() {


        if(isset($_POST['registration_no'])) {

            $table = 'agency';
            $where =  array('id'=> $_REQUEST['id']);

            unset($_REQUEST['id']);

            $data =  $_REQUEST;
            $this->AdminModel->updateData($where,$data,$table);
            $this->agencies();
            return;
        }
    }


    public function  update_agency(){

        $userrow =  urldecode($_REQUEST['obj']);
        $data['json_data'] =  json_decode($userrow);

        $abc =   $this->load->view('update', $data);

    }


    public function deleteRow() {

        $this->AdminModel->row_delete($_REQUEST['id'], $_REQUEST['table']);
    }

    public function agent() {
        $agent  =  array();
        $data['agencies_data'] = $this->AdminModel->getData('agency');

        $data['agent_data'] = $this->AdminModel->getData('agent');

        foreach ($data['agent_data'] as $agents) {

            $agent[$agents['id']] =   $agents;
        }
        $data['agents'] =$agent;
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('agent',$data);    
        $this->load->view('layout/footer');
    }

    public function add_agent()  {

        if(isset($_POST['agency_id'])) {
             $this->db->insert('agent',$_REQUEST);
            $this->agent();
            return;
        } 
    }


    public  function  update_agent() {


        $userrow =  urldecode($_REQUEST['obj']);
        $data['json_data'] =  json_decode($userrow);

        $abc =   $this->load->view('agent_update', $data);

    }
	 public function intake() { 
		 $data = array();
		 $where= array();
		 $data['Status'] =1;
		 
		 
		 if(isset($_POST['s_search'])) {
			 
			if(isset($_POST['Status'])) {
				if($_POST['Status'] !=''){
					$where['status'] =  trim($_POST['Status']);
				}
				$data['Status'] = $_POST['Status'];
			}
			 
		 } else {
			 
			 if(isset($_POST['search'])) {
				if($_POST['stu_name'] !=''){
					$where['intake'] =  trim($_POST['stu_name']);
				}
				$data['stu_name'] = $_POST['stu_name'];
			}
		 }
		 
		
		
		
		 $data['intake_data'] = $this->AdminModel->getData('add_intake', $where);
		 //echo  $this->db->last_query();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('addmision/intake',$data);    
		$this->load->view('layout/footer');
	 }
	 
	 public function addintake() {


		if(isset($_POST['submit'])) {
               $_POST['online_application_session'] = $_POST['start'].'_'.$_POST['end'] ;
                unset($_POST['start']);
                unset($_POST['end']);
                unset($_POST['submit']);
               $this->AdminModel->saveData($_POST,'add_intake' ); 
               redirect(base_url('/addmision/intake'));
		}
        $data = array();
		$data['intake_data'] = $this->AdminModel->getData('intake');

			$data['campus_data'] = $this->AdminModel->getData('campus_program');
        $this->load->view('addmision/addintake',$data);    
        //$this->load->view('layout/footer');
	 }


	  public function edit_intake($id) {

	  	$where =   array('id' => $id );
		if(isset($_POST['submit'])) {
               $_POST['online_application_session'] = $_POST['start'].'_'.$_POST['end'] ;
                unset($_POST['start']);
                unset($_POST['end']);
                unset($_POST['submit']);
               
               $this->AdminModel->updateData($where,$_POST,'add_intake');
                
               redirect(base_url('/addmision/intake'));
		}
        $data = array();
        $data['id'] = $id;
        $data['intake_result'] = $this->AdminModel->getData('add_intake',$where);
		$data['intake_data'] = $this->AdminModel->getData('intake');
		$data['campus_data'] = $this->AdminModel->getData('campus_program');
        $this->load->view('addmision/edit_intake',$data);    
        //$this->load->view('layout/footer');
	 }
	 
	 
	  public function admission_session() {
		// echo  '<pre>'; print_r($_POST); die;
		$where =  array();
		$data['status'] = '1';
		$search =  array();
		if(isset($_POST['search'])) {
			if($_POST['code_search'] !=''){
				$where['code'] =  $_POST['code_search'];
				
				}
			$data['code_search'] = $_POST['code_search'];
		}
		
		
		if(isset($_POST['s_search'])) {
			
			
			if(isset($_POST['academic_session']) &&  $_POST['academic_session'] !=''){
				$search['academic_session'] =  $_POST['academic_session'];
				$data['academic_session'] = $_POST['academic_session'];
			} 
			
			if(isset($_POST['status']) &&  $_POST['status'] !=''){
				$search['status'] =  $_POST['status'];
				$data['status'] = $_POST['status'];
			}
			
		}
		
	
		$data['session'] = $this->AdminModel->getData('session', $where,$search);
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/session',$data);    
        $this->load->view('layout/footer');
	 }
	  public function add_session() {

        if(isset($_POST['submit'])) {
               $data['academic_session'] = $_POST['astart'].'_'.$_POST['aend'] ;
               $data['registration_session'] = $_POST['regstart'].'_'.$_POST['regend'] ;
               $data['subject_add_drop_session'] = $_POST['sadstart'].'_'.$_POST['sadend'] ;
               $data['subject_withdraw'] = $_POST['swstart'].'_'.$_POST['swend'] ;
               $data['acknowlegment_session'] = $_POST['adkstart'].'_'.$_POST['adkend'] ;
               $data['attendance_entry'] = $_POST['adestart'].'_'.$_POST['adeend'] ;
               $data['coursework_mark_entry'] = $_POST['cmestart'].'_'.$_POST['cmeend'] ;
               $data['final_exam'] = $_POST['femstart'].'_'.$_POST['femend'] ;
               $data['code'] = $_POST['code'];
               $data['month'] = $_POST['month'];
               $data['name'] = $_POST['name'];
               $data['program'] = $_POST['program'];
               $data['description'] = $_POST['description'];
               $this->AdminModel->saveData($data,'session' ); 
               redirect(base_url('/addmision/admission_session'));
        }


		$data = array();
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/add_session',$data);    
        $this->load->view('layout/footer');
	 }



	 public function edit_session($id) {

        if(isset($_POST['submit'])) {
               $data['academic_session'] = $_POST['astart'].'_'.$_POST['aend'] ;
               $data['registration_session'] = $_POST['regstart'].'_'.$_POST['regend'] ;
               $data['subject_add_drop_session'] = $_POST['sadstart'].'_'.$_POST['sadend'] ;
               $data['subject_withdraw'] = $_POST['swstart'].'_'.$_POST['swend'] ;
               $data['acknowlegment_session'] = $_POST['adkstart'].'_'.$_POST['adkend'] ;
               $data['attendance_entry'] = $_POST['adestart'].'_'.$_POST['adeend'] ;
               $data['coursework_mark_entry'] = $_POST['cmestart'].'_'.$_POST['cmeend'] ;
               $data['final_exam'] = $_POST['femstart'].'_'.$_POST['femend'] ;
               $data['code'] = $_POST['code'];
               $data['month'] = $_POST['month'];
               $data['name'] = $_POST['name'];
               $data['program'] = $_POST['program'];
               $data['description'] = $_POST['description'];
               $where = array('id' => $id );
               $this->AdminModel->updateData($where,$data,'session');
               //$this->AdminModel->saveData($data,'session' ); 
               redirect(base_url('/addmision/admission_session'));
        }


		$data = array();
		$where =  array('id' => $id );
		$data['session_data'] = $this->AdminModel->getData('session', $where);
		$data['campus_data'] =$this->AdminModel->getData('campus_program');
		$data['id'] = $id;
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/edit_session',$data);    
        $this->load->view('layout/footer');
	 }

	 public function interview() {
		 
		 $where =  array();
		 	 $data['campus'] = $this->AdminModel->getData('campus',  '');
			 $data['program_data'] = $this->AdminModel->getData('campus_program',  '');
		$sql =  'SELECT i.* ,  pc.name as program_name,c.name as campus_name,  ik.intake,ik.month FROM interview as i LEFT JOIN campus as c ON i.campus = c.id LEFT JOIN  campus_program as pc ON i.program = pc.id LEFT JOIN add_intake as ik ON i.intake = ik.intake where i.status = 1';
		
		if(isset($_POST['search'])) {
			if($_POST['search_campus'] !=''){
				$sql .= ' and  i.campus = '.$_POST['search_campus'];
				
				}
			$data['search_campus'] = $_POST['search_campus'];
		}
		 
		$result =$this->db->query($sql); 
		$result_set  = $result->result_array();

		 //$data = array();
		
		 $data['interview_data'] = $result_set;



		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/interview',$data);    
        $this->load->view('layout/footer');
		 
	 }
	 
	 
	  public function add_interview() {
		 
		 $data = array();
		 $data['campus'] = $this->AdminModel->getData('campus',  '');
		 $data['campus_program'] = $this->AdminModel->getData('campus_program',  '');
		 $data['intake'] = $this->AdminModel->getData('add_intake',  '');
		if(isset($_POST['submit'])) {
               $_POST['time'] = $_POST['time1'].'_'.$_POST['time2'] ;
                unset($_POST['time1']);
                unset($_POST['time2']);
        
                unset($_POST['submit']);
                $_POST['date'] =  date('Y-m-d', strtotime($_POST['date']));
                //  echo  '<pre>'; print_r($_POST);die;
               $this->AdminModel->saveData($_POST,'interview' ); 
               redirect(base_url('/addmision/interview'));
        }
        $this->load->view('addmision/add_interview',$data);    
        //$this->load->view('layout/footer');
		 
	 }


	  public function edit_interview($id) {
		 
		 $data = array();
		 $data['campus'] = $this->AdminModel->getData('campus',  '');
		 $data['campus_program'] = $this->AdminModel->getData('campus_program',  '');
		 $data['intake'] = $this->AdminModel->getData('add_intake',  '');
		 $data['interview_data'] = $this->AdminModel->getData('interview', array('id' =>  $id));
		if(isset($_POST['submit'])) {
               $_POST['time'] = $_POST['time1'].'_'.$_POST['time2'] ;
               //echo '<pre>'; print_r($_REQUEST) ; DIE; 
                unset($_POST['time1']);
                unset($_POST['time2']);
        
                unset($_POST['submit']);
                $_POST['date'] =  date('Y-m-d', strtotime($_POST['date']));
                 //echo  '<pre>'; print_r($_POST);die;
                $where = array('id' =>$id);
                $this->AdminModel->updateData($where,$_POST,'interview');
               //$this->AdminModel->saveData($_POST,'interview' ); 
               redirect(base_url('/addmision/interview'));
        }
        $data['id']= $id;
        $this->load->view('addmision/edit_interview',$data);    
        //$this->load->view('layout/footer');
		 
	 }
	 
	 
	 public function registration($id=null) {
		$data = array();
		$where = array();
		$search = array();
		 
		 
		 
		if(isset($_POST['s_search'])) {
			
			if(isset($_POST['p1_campus']) && $_POST['p1_campus'] !='') {
				$search['p1_campus'] = trim($_POST['p1_campus']); 
				$data['p1_campus'] = $_POST['p1_campus']; 
			}  if(isset($_POST['Intake']) && $_POST['Intake'] !='') {
				$search['Intake'] = trim($_POST['Intake']);
				$data['Intake'] = $_POST['Intake'];
			} if(isset($_POST['s_program']) && $_POST['s_program'] !='') {
				$search['p1_program'] = trim($_POST['s_program']);
				$data['p1_program'] = $_POST['s_program'];
			} if(isset($_POST['status']) && $_POST['status'] !='') {
				$search['status'] = trim($_POST['status']);
				$data['status'] = $_POST['status'];
			}
			  $where = array('prospect_status' => 3);
			$result =  $this->AdminModel->getData('prospect', $where,$search);
				
		} else {
			if(isset($_REQUEST['search'])) {
				$data['search'] =  trim($_REQUEST['search']); 
				$search =  array('name'=> trim($_REQUEST['search']));
				  $where = array('prospect_status' => 3);
				$result =  $this->AdminModel->getData('prospect', $where,$search);
				
			} else {
				if($id == null){

                $where = array('prospect_status' => 3);
                
                }else{
                    
                     $where = array('prospect_status' => 3,'id' => $id);
                }
			      // $where = array('prospect_status' => 3);
				$result =  $this->AdminModel->getData('prospect', $where);
			}
		}
		$data['intakedata']=$this->AdminModel->getData('intake');
		$data['agencies_data'] = $this->AdminModel->getData('agency');
		$data['prospect_data'] = $result;
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('registration',$data);    
        $this->load->view('layout/footer');
		 
	 }
	 
	 
	 public function application() {
		$data = array();
		$data['agencies_data'] = $this->AdminModel->getData('agency');
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/application',$data);    
        $this->load->view('layout/footer');
		 
		 
	 }
	 
	 public function addapplication() {
		 
		 $data = array();
		 
		 //~ if(isset($_POST)){
				//~ echo '<pre>';
				//~ print_r($_POST);
		 //~ }
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/add_application',$data);    
        $this->load->view('layout/footer');
		 
		 
	 }
	 
	 
	 function adminssionData(){
		
		
		//echo '<pre>';
		//print_r($_POST);die;
		//echo $_POST["name"];
		//print_r(json_decode($_POST["formData"],1));
		
		$this->AdminModel->saveData($_POST,'admission_application' );
		echo 'true';
		
		die;
		
	
	 }
	 
	 
	 

    public function edit_agent() {

       // echo '<pre>'; print_r($_REQUEST); die;
        if(isset($_POST['registration_no'])) {

            $table = 'agent';
            $where =  array('id'=> $_REQUEST['id']);

            unset($_REQUEST['id']);

            $data =  $_REQUEST;
            $this->AdminModel->updateData($where,$data,$table);
            $this->agencies();
            return;
        }
    }

    public function update_registration_day() {
		 
		 $data = array();
		 
		 //~ if(isset($_POST)){
				//~ echo '<pre>';
				//~ print_r($_POST);
		 //~ }
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/update_registration_day',$data);    
        $this->load->view('layout/footer');
		 
		 
	 }
	  public function program_academic_session() {
		 
		 $data = array();
		 
		 //~ if(isset($_POST)){
				//~ echo '<pre>';
				//~ print_r($_POST);
		 //~ }
		$this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('addmision/program_academic_session',$data);    
        $this->load->view('layout/footer');
		 
	 }
	public function doc_checklist() { 
 		$data['doc_data'] = $this->AdminModel->doc_ckecklist();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('addmision/doc_checklist',$data);    
		$this->load->view('layout/footer');
	}
	public function get_data_by_id($table,$field,$id)
    {
        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);
        return $get_data_by_id;
    }
}