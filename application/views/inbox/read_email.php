<div class="content-page" >
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Inbox</h4> -->
                        <!-- <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active">Inbox</li>
                        </ol> -->
                    </div>
                </div>
            </div>
        </div>
                <div class="mb-3">
                    <div class="card">
                        <div class="btn-toolbar p-3" role="toolbar">
                            <div class="btn-group mo-mb-2">
                                <!-- <button type="button" class="btn btn-primary waves-light waves-effect"><i class="fa fa-inbox"></i></button> -->
                                <a href="javascript:void(0)" onclick="deleteRow(<?php echo $messages['id']; ?>, 'inbox')">
                                    <button type="button" class="btn btn-primary waves-light waves-effect"><i class="far fa-trash-alt"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="media m-b-30">
                                <div class="media-body">
                                    <h4 class="font-14 m-0"><?php echo $from_data['name']; ?></h4><small class="text-muted"><?php echo $from_data['email']; ?></small></div>
                            </div>
                            <p><?php echo date('F d, h:i A', strtotime($messages['created'])); ?></p>
                            <h4 class="mt-0 font-16"><?php echo $messages['subject']; ?></h4>
                            <!-- <p>Dear <?php echo $to_data['name']; ?>,</p> -->
                            <p><?php echo $messages['message']; ?></p>
                            <!-- <p>Sincerly,<br></p>
                            <p><?php echo $from_data['name']; ?></p><br> -->
                            <hr>
                            <?php if (!empty($message_reply)) { ?>
                            <!-- <h4>Reply</h4> -->
                                <?php foreach ($message_reply as $key => $val) { 
                                    $get_data = $this->CI->get_data_by_id('users','id',$val['sender_id']);
                                ?>
                                <p><strong>From:</strong> <?php echo $get_data['name']; ?></p>
                                <p><?php echo $val['reply']; ?></p>
                                <!-- <p>Sincerly,
                                <?php  echo $get_data['name']; ?></p> -->
                                <?php } } ?>
                            <hr>
                            <a href="<?php echo base_url('inbox/reply?id='.$messages['id']); ?>" class="btn btn-secondary waves-effect m-t-30"><i class="mdi mdi-reply"></i> Reply</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };
    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
    </script>
       
 <script type="text/javascript">
            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>