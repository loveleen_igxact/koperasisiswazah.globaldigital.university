

        <div class="content-page" >

            <!-- Start content -->

            <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="page-title-box">

                                <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Staff </h4>-->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active"> New Staff </li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                </div>

                    <!-- end row -->                        

                    <div class="card m-b-30">

                        <div class="col-md-12 hearding pt-3">

                            <h4 class="mt-0 header-title left_side">Add New Staff</h4>

                        </div>

                            <!-- <hr> -->

                                <div class="card-body">

                                    <form method="post">

                                    <div class="row">

                                        <div id="prospect"></div>

                                        <div style="clear:both"></div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Name<span style="color: red;">*</span></label>

                                                <input type="text" class="form-control " required="" placeholder="" name="name">

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Staff No<span style="color: red;">*</span></label>

                                                <input type="text" class="form-control " required="" placeholder="" name="staff_no">

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>ID Type<span style="color: red;">*</span></label>

                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="id_type">

                                                    <option> Please Select</option>

                                                    <?php if (!empty($id_types)) { 

                                                        foreach ($id_types as $key => $values) { ?>

                                                    <option value="<?php echo $values['id']; ?>"><?php echo $values['name']; ?> </option>

                                                    <?php }

                                                    } ?>

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>ID No<span style="color: red;">*</span></label>

                                                <input type="text" class="form-control" required="" placeholder="" name="id_no">

                                               <!--  <span>This will be used as a password for first time creating a staff.</span> -->

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Email<span style="color: red;">*</span></label>

                                                <input type="email" name="email" class="form-control " required="" placeholder="">

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Phone<span style="color: red;">*</span></label>

                                                <input type="text" name="phone" class="form-control " required="" placeholder="">

                                            </div>

                                        </div>

                                        <!--<div class="col-md-4">

                                            <div class="form-group">

                                                <label>Campus<span style="color: red;">*</span></label>

                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="campus">

                                                    <option> Please Select</option>

                                                    <?php if (!empty($campuses)) { 

                                                        foreach ($campuses as $key => $campus) { ?>

                                                        <option value="<?php echo $campus['id']; ?>"><?php echo $campus['name']; ?> </option>

                                                    <?php }

                                                    } ?>

                                                    

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Faculty</label>

                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="faculty">

                                                    <option> Please Select</option>

                                                    <?php if (!empty($faculty)) { 

                                                        foreach ($faculty as $key => $value) { ?>

                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?> </option>

                                                    <?php }

                                                    } ?>

                                                </select>

                                            </div>

                                        </div>-->

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Department<span style="color: red;">*</span></label>

                                                <select class="form-control" tabindex="-1" required="" aria-hidden="true" name="department_id">

                                                <option> Please Select</option>

                                                <?php if (!empty($departments)) {

                                                    foreach ($departments as $key => $val) { ?>

                                                        <option value="<?php echo $val['id']; ?>"><?php echo $val['name']; ?></option>

                                                <?php } } ?>

                                                </select>

                                            </div>

                                        </div>

                                        <!-- <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Subject</label>

                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="position">

                                                    <option> Please Select</option>

                                                     <?php if (!empty($positions)) {

                                                    foreach ($positions as $key => $vals) { ?>

                                                        <option value="<?php echo $vals['id']; ?>"><?php echo $vals['name']; ?></option>

                                                <?php } } ?>

                                                </select>

                                            </div>

                                        </div> -->

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Class<span style="color: red;">*</span></label>

                                                <select class="form-control" tabindex="-1" required="" aria-hidden="true" name="class">

                                                    <option> Please Select</option>

                                                     <?php if (!empty($class)) {

                                                    foreach ($class as $key => $vals) { ?>

                                                        <option value="<?php echo $vals['id']; ?>"><?php echo $vals['class_name']; ?></option>

                                                <?php } } ?>

                                                </select>

                                            </div>

                                        </div>

                                         <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Subject<span style="color: red;">*</span></label>

                                                <select class="form-control" tabindex="-1" required="" aria-hidden="true" name="subject">

                                                    <option> Please Select</option>

                                                     <?php if (!empty($subject)) {

                                                    foreach ($subject as $key => $vals) { ?>

                                                        <option value="<?php echo $vals['id']; ?>"><?php echo $vals['subject_name']; ?></option>

                                                <?php } } ?>

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-md-12 pt-3">

                                            <button class="btn color_another  btn-lg" type="submit" name="submit">Save</button>

                                            <button class="btn btn-secondary more_border btn-lg" type="submit">Cancel</button>

                                        </div>

                                    </div>

                                    </form> 

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <style type="text/css">

        .form-control, .thumbnail {

            border-radius: 2px;

        }

        .btn-danger {

            background-color: #B73333;

        }

        

        /* File Upload */

        .fake-shadow {

            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);

        }

        .fileUpload {

            bottom: 43px;

            position: relative;

            overflow: hidden;

            left: 17px;

        }

        .fileUpload #logo-id {

            position: absolute;

            top: 0;

            right: 0;

            margin: 0;

            padding: 0;

            font-size: 33px;

            cursor: pointer;

            opacity: 0;

            filter: alpha(opacity=0);

        }

        .img-preview {

            max-width: 100%;

            width: 100%;

        }

        .thumbnail {

            display: block;

            padding: 4px;

            margin-bottom: 20px;

            line-height: 1.428571429;

            background-color: #fff;

            border: 1px solid #ddd;

            border-radius: 4px;

            -webkit-transition: all .2s ease-in-out;

            transition: all .2s ease-in-out;

        }

    </style>

</body>

</html>