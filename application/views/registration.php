<?php  $intake = array('1' => '2016/02' , '2' =>' 2017/02','3' => '2018/03', '4' =>'2019/02','5'=>'2019/01','6'=>'2018/04');

?>



 <div class="content-page">

            <!-- Start content -->

            <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="page-title-box">

                                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Registration</h4> -->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active">Registration</li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                    <!-- end row -->

                        <div class="col-12" style="display: none;">

                            <div class="card m-b-30">

                                <div class="card-body" >

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                        <div class="search ">

                                            <form action="<?php echo base_url('addmision/registration') ?>" method="post">

                                                <div class="form-group">

                                                    <label>Search</label>

                                                    <input type="text" class="form-control serch" value ="<?php echo  @$search; ?>" name="search" placeholder="Search by Name , ID No.."> 

                                                    <span class="serch_icon"><button class="btn btn-success" type="submit">Search</button></span>

                                                </div>

                                            </form>

                                        </div>

                                    </div>

                                </div>

                                    <!-- <h6>Filter by Prospect Name</h6> -->

                                        <!-- Collapse buttons -->

                                     <!--   <div class="button">

                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                                            Registration Filter

                                            </a>

                                        </div>  -->

                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->

                                        <form action ="<?php echo base_url('application/registration')?>" method="POST">

                                        <div class="row" style="padding-top: 10px;">

                                        <div class="collapse col-md-12" id="collapseExample">

                                            <div class="row">

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label class="control-label"><strong>Campus</strong></label>

                                                   <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="p1_campus" required="">

                                                        <option value="">Select</option>

                                                         <option value="1">UKM BANGI</option>

                                                        <option value="2">IPGKTR</option>

                                                        <option value="3">IPGKB</option>

                                                        <option value="4">KOTA KINABALU</option>

                                                    </select>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label class="control-label"><strong>intake</strong></label>

                                                <select name="Intake" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                <option value="1">2016/02</option>

                                                <option value="2">2017/02</option>

                                                <option value="3">2018/03</option>

                                                <option value="4">2019/02</option>

                                                </select>

                                            </div>

                                        </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Registration Day</strong></label>

                                                <input type="text" class="form-control" required="" placeholder="">

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Program</strong></label>

                                                <select name="s_program" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                  <option value=""> Select</option>

                                                <option value="10">SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)</option>

                                                <option value="13">SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)</option>

                                                <option value="14">SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)</option>

                                                <option value="17">SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)</option>

                                                <option value="6">SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)</option>

                                                <option value="12">SARJANA PENDIDIKAN (PENDIDIKAN KHAS)</option>

                                                <option value="22">SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)</option>

                                                <option value="3">SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)</option>

                                                <option value="2">SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)</option>

                                                <option value="5">SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)</option>

                                                <option value="8">SARJANA PENDIDIKAN (PENDIDIKAN SAINS)</option>

                                                <option value="23">SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)</option>

                                                <option value="11">SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)</option>

                                                <option value="9">SARJANA PENDIDIKAN (PENGURUSAN SUKAN)</option>

                                                <option value="18">SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)</option>

                                                <option value="1">SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)</option>

                                                <option value="7">SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)</option>

                                                <option value="4">SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)</option>

                                                <option value="15">SARJANA PENDIDIKAN (TESL)</option>

                                                <option value="20">SARJANA PENDIDIKAN BAHASA ARAB</option>

                                                <option value="19">SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT</option>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Status</strong></label>

                                            <select name="status" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                <option value="8">Accepted</option>

                                                <option value="9">Decline</option>

                                                <option value="6">Incomplete</option>

                                                <option value="10">No Response</option>

                                                <option value="4">Offered</option>

                                                <option value="3">Processed</option>

                                                <option value="2">Received</option>

                                                <option value="1">Registered</option>

                                                <option value="5">Rejected</option>

                                                <option value="7">Reply Offer</option>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <input type="submit" class="btn btn-dark waves-effect waves-light" name="s_search" value="Search">

                                    </div>

                                    </div>

                                        </div>

                                    </div>

                                    </form>

                                        <!-- / Collapsible element -->  

                                </div>

                            </div>

                            <!---card 2 table-->

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Registrations</h4>

                                        </div>

                                           <div class="add_more">

                                          <!--  <button type="button" class="btn btn-info waves-effect waves-light">Export</button>  -->

                                           <!--  <a href="addmission_add_application.html">

                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>

                                            </a> -->

                                        </div>

                                        <div class="clearfix"></div>

                                       <!--  <hr> -->

                                        <div class="search ">

                                            <form action="<?php echo base_url('addmision/registration') ?>" method="post">

                                                <div class="col-md-4 p-0 pt-3  form-group">

                                                    <label>Search</label>

                                                    <input type="text" class="form-control serch" value ="<?php echo  @$search; ?>" name="search" placeholder="Search by Name , ID No.."> 

                                                    <span class="serch_icon"><button class="btn color_another" type="submit">Search</button></span>

                                                </div>

                                            </form>

                                        </div>

                                        <table id="dtBasicExample" class="table table-hover dataTable " cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                <!-- <th class="no_sort">No</th> -->

                                                    <th class="th-sm">Name

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Programs

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Intake

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm no_sort">Action

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>
                                                      <th class="th-sm no_sort">Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                            

                                            <?php foreach($prospect_data as $key=> $value) { 

                                                $get_program1 = $this->CI->get_data_by_id('programs','id',$value['p1_program']);

                                                $get_program2 = $this->CI->get_data_by_id('programs','id',$value['p2_program']);

                                                $get_program3 = $this->CI->get_data_by_id('programs','id',$value['p3_program']);?>

                                                <tr>

                                                    <!-- <th scope="row"><?php echo $value['id'];  ?> </th> -->

                                                    <td><a href="<?php echo base_url('application/profile/'.$value['id']); ?>"><span style="font-weight: 500; color: #1414da; text-transform:uppercase;"><?php echo $value['name'];  ?></span></a><br>Identification certificate: <span> <?php echo $value['id_no'] ?><br>Phone no: <?php echo $value['phone'];  ?></td>

                                                    <td><?php echo $get_program1['name']; ?><br><?php echo $get_program2['name']; ?><br><?php echo $get_program3['name']; ?><br><?php echo $value['id_no'] ?><br><?php echo $value['application_status'] == 2 ? "Accepted" : "Registration Pending" ?>

                                                    </td>

                                                    <!-- <td><?php echo $value['intake'];  ?></td> -->
                                                    <!-- <td><?php print_r(@$intakedata[$value['intake']]['intake']) ;?> </td> -->
                                                    <td><?php foreach ($intakedata as $key => $value11) {
                                                        if ($value11['id']==$value['intake']) {
                                                            print_r($value11['intake']);
                                                        }
                                                       
                                                    } ?> </td>


                                                    <td>

                                                        <ul class="icons_all">

                                                        <li>

                                                            <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile" href="<?php echo base_url('application/profile/'.$value['id']); ?>"><!--<i class="fas fa-share" style="color:blue; "></i>--><img src="http://lmsmalaysia.com/public/images/admin_icon.png">

                                                            </a>

                                                        </li>

                                                        <?php if($value['application_status'] == 1){  ?>  

                                                        <li>
                                                            <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Process for Student" onclick="return confirm('Are you sure want to register this user as student ?');" href="<?php echo base_url('application/send_email/'.$value['id']); ?>"><img src="http://lmsmalaysia.com/public/images/mark.png">

                                                                   <!-- <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Register" href="<?php echo base_url('application/send_email/'.$value['id']); ?>">
                                                                    <i class="fas fa-share" style="color:blue; "></i><img src="http://lmsmalaysia.com/public/images/mark.png"> -->

                                                         

                                                            </a> 

                                                        </li>

                                                        <?php } ?>

                                                    </ul>

                                                        <!--  <a href=""> <span><i class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span></a> -->

                                                         </td>
                                                          <td>
                                                            <ul class="icons_all">
                                                            <?php if($value['application_status'] == 1){ 
                                                                echo "Unregistered ";
                                                             } 
                                                             else{ echo "Registered";
                                                             } ?>
                                                             </ul>
                                                       
                                                         </td>

                                                    <!-- <td>

                                                        <ul class="icons_all">

                                                            <li>

                                                                <a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile">

                                                                    <span><img src="http://lmsmalaysia.com/public/images/admin_icon.png"></span>

                                                                </a>

                                                            </li>

                                                            <li>

                                                                <a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile">

                                                                    <span><img src="http://lmsmalaysia.com/public/images/mark.png"></span>

                                                                </a>

                                                            </li>

                                                        </ul>

                                                    </td> -->

                                                </tr>

                                            <?php  } ?>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            

                        </div>

                    </div>

                </div>

                <!-- container-fluid -->

            </div>

            

            <style type="text/css">

                .serch_icon {

                position: absolute;

                bottom: 0px;

                right: 0px;

                /*top: 2px;*/

            }

            .dataTables_wrapper .dataTables_length {

                float: left;

                float: right !important;

                position: absolute;

                right: 0px;

                top: -39px;

            }

            table.dataTable tbody th, table.dataTable tbody td {

             padding: 8px 17px !important;

            }

            </style>

           

            