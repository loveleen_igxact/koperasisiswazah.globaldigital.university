<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Teacher extends CI_Controller {



    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     *      http://example.com/index.php/welcome

     *  - or -

     *      http://example.com/index.php/welcome/index

     *  - or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */

    function __construct() {

        parent::__construct();

        $this->load->library(array('form_validation','session'));

        $this->load->helper(array('form','url'));

        $this->load->model('AdminModel');   // loads admin model

        $this->CI = & get_instance();

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

    }

    public function index()

    {

        

         $session_data = $this->session->userdata['loggedInData'];

         if($session_data['user_type'] == '3') {
         $where = array('user_id'=> $session_data['id']);

         $students = $this->AdminModel->getData('students',$where);
        // echo "<pre>";print_r($students);die();

          $wherep = array('class_id'=> $students[0]['class_id'] , 'subject_id'=>$students[0]['subject_id'],'status'=>'1');

  

         $data['teachers_data'] = $this->AdminModel->getData('staffs',$wherep);

        

      //  echo "<pre>";print_r($data);die();

        

       }else{

       // $data['teachers_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs');

       }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/teacher',$data);

        $this->load->view('layout/footer');

    }

    public function get_data_by_id($table,$field,$id)

    {

        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);

        return $get_data_by_id;

    }

    public function create()

    {

        $data['departments'] = $this->AdminModel->getData('departments');

        $data['faculty'] = $this->AdminModel->getData('faculty');

        $data['positions'] = $this->AdminModel->getData('positions');

        $data['id_types'] = $this->AdminModel->getData('id_types');

        $data['campuses'] = $this->AdminModel->getData('campus');

        $data['class'] = $this->AdminModel->getData('class');

        $data['subject'] = $this->AdminModel->getData('subjects');

        if (isset($_POST['submit'])) {

          //  print_r($_REQUEST);die();

            $chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";

            $length = 6;

            $password = substr( str_shuffle( $chars ), 0, $length );

            $check_email = $this->AdminModel->checkEmail($_POST['email']);

            if($check_email == true) {

                $this->session->set_flashdata('msg', '<div class="alert alert-danger" style=" color:#000 !important;">Email Already Exist</div>');

                redirect(base_url('teacher'));

            } else {

                $arr = array(

                    'name'=>$_POST['name'],

                    'email'=>$_POST['email'],

                    'password'=>md5($password),

                    //'staff_no'=>$_POST['staff_no'],

                    //'id_no'=>$_POST['id_no'],

                    'email'=>$_POST['email'],

                    //'phone'=>$_POST['phone'],

                    //'department_id'=>$_POST['department_id'],

                    'user_type'=>'4'

                );

                $arr['username'] = $_POST['name'];

                $insert_id = $this->AdminModel->saveData($arr,'users');

                unset($arr['username']);

                unset($arr['password']);

                unset($arr['user_type']);

                $arr['user_id'] = $insert_id;

                $arr['identification_type_id'] = $_POST['id_type'];

                //$arr['campus_id'] = $_POST['campus'];

                //$arr['faculty_id'] = $_POST['faculty'];

                // $arr['position_id'] = $_POST['position'];

                $arr['class_id'] = $_POST['class'];

                $arr['subject_id'] = $_POST['subject'];

                $arr['staff_no'] = $_POST['staff_no'];

                $arr['id_no'] = $_POST['id_no'];

                $arr['phone'] = $_POST['phone'];

                $arr['department_id'] = $_POST['department_id'];

                $insert_data = $this->AdminModel->saveData($arr,'staffs');

                $arr1 = array(

                    'staff_id'=>$insert_id,

                    //'campus'=>$_POST['campus'],

                    'id_type'=>$_POST['id_type'],

                    //'faculty'=>$_POST['faculty'],

                    //'position'=>$_POST['position'],

                    'created'=>date('Y-m-d'),

                );

                $insert_data = $this->AdminModel->saveData($arr1,'staff_info');

                

            



            $this->load->library('email');

                $subject = 'Regarding Teacher Registration';

                $message = "Hi ".$_POST['name'].",
                Your Login credentials are
                Username: ".$_POST['email']."
                Password: ".$password."
                Thanks";
                  $email=$_POST['email'];
                  $from_email = "adminksbb@lmsmalaysia.com";
                  $email22="adminksbb@lmsmalaysia.com";

                  $adminMessage= "Hi Admin,
                  New teacher ".$_POST['name']." has been added successfully.
                   Username: ".$_POST['email']."
                   Password: ".$password."
                   Thanks";

                    //send mail to user

                $userMailResponse = $this->sendMailAdmin($from_email, $email, $subject, $message);

                   // send mail to admin

                $adminMailResponse = $this->sendMailAdmin($from_email, $email22, $subject,$adminMessage);

                // $this->send_email('loveleen.igxact@gmail.com',$message,$subject);

                $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Teacher Added Successfully.</div>');

                redirect(base_url('teacher'));

            }

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/create',$data);

        $this->load->view('layout/footer');

    }

      public function sendMailAdmin($from_email="adminksbb@lmsmalaysia.com",$to_email="adminksbb@lmsmalaysia.com",$subject="adminTestSubject",$message="test"){


            $this->load->library('email');

            $this->email->from($from_email, 'Teacher Details');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);

             if ($this->email->send()) {

                

                $this->session->set_flashdata("email_sent", "Email sent successfully.");

                //echo "success";

                return true;

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                //echo "failure";

                return false;

                //  $this->load->view('email_form');

            }



    }

    public function edit()

    {

        

       // echo "edit";die();

        $id = $_GET['id'];

        $data['departments'] = $this->AdminModel->getData('departments');

        $data['faculty'] = $this->AdminModel->getData('faculty');

        $data['positions'] = $this->AdminModel->getData('positions');

        $data['id_types'] = $this->AdminModel->getData('id_types');

        $data['campuses'] = $this->AdminModel->getData('campus');

        $data['staff'] = $this->AdminModel->get_data_by_id('staffs','user_id',$id);

        if (isset($_POST['update'])) {

            $arr = array(

                'name'=>$_POST['name'],

                'username'=>$_POST['name'],

                'email'=>$_POST['email'],

                //'password'=>$password,

                //'staff_no'=>$_POST['staff_no'],

                //'id_no'=>$_POST['id_no'],

                //'phone'=>$_POST['phone'],

                //'department_id'=>$_POST['department_id'],

                // 'user_type'=>'4'

            );

            $where = array('id'=>$id);

            $this->AdminModel->updateData($where,$arr,'users');

            unset($arr['username']);

                $arr['identification_type_id'] = $_POST['id_type'];

                //$arr['campus_id'] = $_POST['campus'];

                //$arr['faculty_id'] = $_POST['faculty'];

                //$arr['position_id'] = $_POST['position'];

                $arr['class_id'] = $_POST['class'];

                $arr['subject_id'] = $_POST['subject'];

                $arr['staff_no'] = $_POST['staff_no'];

                $arr['id_no'] = $_POST['id_no'];

                $arr['phone'] = $_POST['phone'];

                $arr['department_id'] = $_POST['department_id'];

                $wheres = array('user_id'=>$id);

                $insert_data = $this->AdminModel->updateData($wheres,$arr,'staffs');;

            $arr1 = array(

                //'staff_id'=>$insert_id,

                //'campus'=>$_POST['campus'],

                'id_type'=>$_POST['id_type'],

                //'faculty'=>$_POST['faculty'],

                //'position'=>$_POST['position'],

                'created'=>date('Y-m-d'),

            );

            $insert_data = $this->AdminModel->saveData($arr1,'staff_info');

            $where1 = array('staff_id'=>$id);

            $this->AdminModel->updateData($where1,$arr1,'staff_info');

            

            // $subject = 'Regarding Teacher Registeration';

            // $message = "Hii,".$_POST['name']."<br>Your Login credentials are <br> Email: ".$_POST['email']."<br> Password: ".$password."";

            // $this->send_email('bhawna.webfreaksolution@gmail.com',$message,$subject);

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Record Updated Successfully.</div>');

            redirect(base_url('teacher'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/edit_teacher',$data);

        $this->load->view('layout/footer');

    }

    public function students_list()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $whereStudent = array('type'=> '1');

        if($session_data['user_type'] == '4') {

           

         $where = array('user_id'=> $session_data['id']);

         $teacher = $this->AdminModel->getData('staffs',$where);

          $wherep = array('class_id'=> $teacher[0]['class_id'] , 'subject_id'=>$teacher[0]['subject_id'],'type'=>'1');

  

         $data['students_data'] = $this->AdminModel->getData('students',$wherep);

        

        } else{

            if (isset($_POST['search']) ) {

                if (!empty($_POST)) {

                    $data['students_data'] = $this->AdminModel->searchStudent($_POST);

                } else {

                    $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

                }

            } else if (isset($_POST['search_button']) ) {

                if (!empty($_POST)) {

                    $data['students_data'] = $this->AdminModel->searchStudent($_POST);

                } else {

                    $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

                }

            } else{

                $data['students_data'] = $this->AdminModel->getData('students',$whereStudent);

            }

        }  

        //print_r($data); die;

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/students_list',$data);

        $this->load->view('layout/footer');

    }

    public function edit_std($id = null)

    {

        

       // echo "edit";die();

        $id = $_GET['id'];

    // echo $_GET['id'];

        // echo $id;

        // die();

        // $data = array();

        // echo $data; die;

        // $where = array('user_id'=> $_GET['id']);

        // $where = array('user_id' => $id);

            $where = array('user_id' =>  $_GET['id']);

            $data['user_info'] = $this->AdminModel->get_data_by_id('students','user_id',$id);

        // echo "<pre>";

        // print_r($data);die;

            $data['race']  = $this->AdminModel->getData('race');

            $data['religion']  = $this->AdminModel->getData('religion');

            $data['nationalities']  = $this->AdminModel->getData('nationalities');

            $pwhere= array('id' => $id);

            $data['prospects_data'] = $this->AdminModel->getData('prospect', $pwhere);



       

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/edit_std',$data);

        $this->load->view('layout/footer');

        if (isset($_POST['update'])) {

            $arr = array(

                'name'=>$_POST['name'],

                // 'username'=>$_POST['name'],

                'email'=>$_POST['email'],

                'gender'=>$_POST['gender'],

                // 'salutation_id'=>$_POST['salutation_id'],

                'phone'=>$_POST['phone'],

                'nationality'=>$_POST['nationality'],

                'religion'=>$_POST['religion'],

                'race'=>$_POST['race'],

                'place_of_birth'=>$_POST['place_of_birth'],

                'date_of_birth'=>$_POST['date_of_birth'],

                'marital_id'=>$_POST['marital_status'],

            );

            $where = array('id'=>$_POST['student_number']);

           

            $this->AdminModel->updateData($where,$arr,'students');

            

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Record Updated Successfully.</div>');

            redirect(base_url('teacher/students_list'));

            

        }
        if (isset($_POST['cancel'])) {
            redirect(base_url('teacher/students_list'));
        }
        

    }



    public function documents()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $where = array('user_id'=>$session_data['id']);

        if ($session_data['user_type'] == '3') {

            $type = "student";

        } else {

            $type = "teacher";

        }

        $data['my_documents'] = $this->AdminModel->getMyDocuments($session_data['id'],$type);

        $data['shared_documents'] = $this->AdminModel->getSharedDocuments($session_data['id'],$type);

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/all_documents',$data);

        $this->load->view('layout/footer');

    }

    public function all_documents()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $where = array('user_id'=>$session_data['id']);

        if ($session_data['user_type'] == '3') {

            $type = "student";

        } else {

            $type = "teacher";

        }

        $data['my_documents'] = $this->AdminModel->getMyDocuments($session_data['id'],$type);

        $data['shared_documents'] = $this->AdminModel->getSharedDocuments($session_data['id'],$type);

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/documents',$data);

        $this->load->view('layout/footer');

    }

    public function upload_documents_new()

    {

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        // $wheres = array('user_type'=>'4');

        $data['students_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);

        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['submit'])) {

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                if (!empty($_POST['students'])) {

                    $arr['student_id'] = implode(',', $_POST['students']);

                } else {

                    $arr['student_id'] = "0";

                }

                if ($session_data['user_type'] == '3') {

                    $type = "student";

                } else {

                    $type = "teacher";

                }

                $arr['creator_type'] = $type;

                $arr['user_id'] = $session_data['id'];

                $arr['created'] = date('Y-m-d');

                $insert_id = $this->AdminModel->saveData($arr,'user_documents');

                if (!empty($_POST['students'])) {

                    foreach ($_POST['students'] as $key => $value) {

                        $arr1['user_id'] = $value;

                        $arr1['document_id'] = $insert_id;

                        $save = $this->AdminModel->saveData($arr1,'shared_documents');

                    }

                }

            }

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('teacher/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/upload_documents',$data);

        $this->load->view('layout/footer');

    }

    public function upload_documents()

    {

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        // $wheres = array('user_type'=>'4');

        $data['students_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['submit'])) {

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                if (!empty($_POST['students'])) {

                    $arr['student_id'] = implode(',', $_POST['students']);

                } else {

                    $arr['student_id'] = "0";

                }

                if ($session_data['user_type'] == '3') {

                    $type = "student";

                } else {

                    $type = "teacher";

                }

                $arr['creator_type'] = $type;

                $arr['user_id'] = $session_data['id'];

                $arr['created'] = date('y-m-d H:i:s');

                $arr['title'] = $_POST['title'];

                $arr['description'] = $_POST['description'];

                $insert_id = $this->AdminModel->saveData($arr,'user_documents');

                if (!empty($_POST['students'])) {

                    foreach ($_POST['students'] as $key => $value) {

                        $arr1['user_id'] = $value;

                        $arr1['document_id'] = $insert_id;

                        $save = $this->AdminModel->saveData($arr1,'shared_documents');

                    }

                }

            }
           
            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('teacher/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/upload_doc',$data);

        $this->load->view('layout/footer');

    }

    public function edit_documents()

    {

        $id = $_GET['id'];

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        $data['students_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);

        $data['get_document_info'] = $this->AdminModel->get_data_by_id('user_documents','id',$id);

        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['update'])) {

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                $arr['student_id'] = implode(',', $_POST['students']);

                $arr['user_id'] = $session_data['id'];

                //$arr['created'] = date('Y-m-d');

                //$save = $this->AdminModel->saveData($arr,'user_documents');

                $where1 = array('id'=>$id);

                $this->AdminModel->updateData($where1,$arr,'user_documents');

            }

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('teacher/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('teacher/edit_document',$data);

        $this->load->view('layout/footer');

    }

    public function download()

    {

        $files = $_GET['file'];

        $url = "http://lmsmalaysia.com/uploads/documents/".$files;

        $ext = pathinfo($url, PATHINFO_EXTENSION);

        if ($ext == 'png' && $ext == 'jpg' && $ext == 'jpeg') {

            if (exif_imagetype($url) == IMAGETYPE_PNG) {

                $type = exif_imagetype($url);

            } else {

                $type = $ext;

            }

        } else {

            $type = $ext;

        }

        

        header("Content-type:application/".$type);

        header("Content-Disposition:attachment;filename=".$url);

        readfile($url);

    }

    public function send_email($email,$message,$subject)

    {

        $this->load->library('email');

        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        // $this->email->from('no-reply@mylifelineapp.com');

        $this->email->from('adminksbb@lmsmalaysia.com');

        $this->email->to($email);

        $this->email->subject($subject);

        $this->email->message($message);

        $send = $this->email->send();

        return true;

        // $to = $email;

        // $subject = $subject;

        // $txt = $message;

        // $headers = "From: webmaster@example.com" . "\r\n" .

        // "CC: somebodyelse@example.com";

        // mail($to,$subject,$txt,$headers);

    }

    public function update_status()

    {

        $where = array('id'=>$_POST['id']);

        $arr['is_active']=$_POST['status'];

        $this->AdminModel->updateData($where,$arr,'users');

        $where1 = array('user_id'=>$_POST['id']);

        $arr1['status']=$_POST['status'];

        $this->AdminModel->updateData($where1,$arr1,'staffs');

    }

    public function delete_file()

    {

        $where = array("id" => $_GET['id']);

        $arr['file'] = "";

        $this->AdminModel->updateData($where,$arr,"user_documents");

        redirect(base_url('teacher/edit_documents?id='.$_GET['id']));

    }

    public function profile($id = null)

    {

        $data['id'] = $id;

        $data = array();

        if (!empty($id)) {

            $where = array('user_id' => $id);

            $data['user_info'] = $this->AdminModel->get_data_by_id('students','user_id',$id);
            

            // echo "<pre>"; print_r($data); die;

            $nation_id = array('NationalityID' => $data['user_info']['nationality']);

            $data['nationalities'] = $this->AdminModel->getData('nationalities', $nation_id);

            // if ($data['prospects_data'][0]['religion'] == "0") {

            //     $religion_id = array('id' => $data['prospects_data'][0]['religion']);

            //     $data['religion'] = $this->AdminModel->getData('religion', $religion_id);

            // } else {

            //     $data['religion'] = "";

            // }

            // $race_id = array('id' => $data['prospects_data'][0]['race']);

            // $data['race'] = $this->AdminModel->getData('race', $race_id);

            // $data['id'] = $id;

            $where_doc = array('prospectes_id' => $id);

            $data['religion']  = $this->AdminModel->getData('religion');

            $data['race']  = $this->AdminModel->getData('race');
            $data['intakeA']  = $this->AdminModel->getData('intake');

            $data['document'] = $this->AdminModel->getData('document', $where_doc);

            $where_contact = array('prospects_id' => $id);

            $data['contact'] = $this->AdminModel->getData('contact', $where_contact);

            // $data['program'] = $this->AdminModel->getData('programs', $where_contact);

            // $where_intake = array('id' => $data['program'][0]['intake']);

            //$data['intake'] = $this->AdminModel->getData('intake', $where_pro);

            $this->load->view('teacher/profile', $data);

        }

    }

}