  <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Interview Schedules</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Interview Schedules</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                        

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class=" col-md-12" id="">
									<form action="<?php echo base_url('addmision/interview');  ?>" method="POST">
											
                                            <div class="row">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select name="search_campus" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option value="">Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <?php
                                                        $selected = '';
                                                         foreach($campus as $key =>$value) { 

                                                            if(@$search_campus ==  $value['id']){
                                                                $selected = 'selected';
                                                            } 

                                                            ?>
                                                        <option value="<?php echo $value['id'];  ?>" <?php echo $selected; ?>><?php echo $value['name'];  ?></option>
                                                        <?php  } ?>
                                               
                                                    
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select name="" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                     <option value="" >Select </option>
                                                    <?php foreach($program_data as $programs) { ?>
                                                    <option value="<?php echo  $programs['id'] ?>"><?php echo  $programs['name'] ?></option>
                                                    <?php  } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Intake</strong></label>
                                                <select name="" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option value="">2019/2</option>
                                                    <option value="">2019/3</option>
                                                </select>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <input  type="submit" class="btn btn-primary waves-effect waves-light" value="Search" name="search">
                                    </div>
                                    </div>
                                    
                                    
                                    </form>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Interview Schedules</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="<?php echo  base_url('addmision/add_interview');?>">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Campus
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Program
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Date/Time
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                    $i =1;
                                                 foreach ($interview_data as $key => $value) {
                                                    # code...
                                                ?> 
                                                <tr id="current_row<?php echo $value['id']; ?>">
                                                    <th scope="row"><?php echo $i; ?></th>
                                                    <td><?php echo $value['campus_name']; ?></td>
                                                    <td><?php echo $value['program_name']; ?></td>
                                                    <td class="scroll_date"><?php echo $value['intake'] . '/' . $value['month']; ?></td>
                                                    <td><?php echo $value['date'] . '/ '. $value['time']; ?></td>
                                                <td><a href="<?php echo base_url('addmision/edit_interview/'.$value['id']);  ?>"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button></a>
                                                  <a href="javascript:void(0)"   onclick="deleteRow(<?php echo $value['id']; ?>,  'interview')"> <span><i  class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span> </a>
                                                    </td>
                                                </tr>
                                                <?php $i++; }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
             <script type="text/javascript">
        
    function deleteRow(id, table) {
    var url  = '<?php echo  base_url();?>';

     var x=confirm("Are you sure to delete record?")
      if (x) {
        $.ajax({
             data: { 'id' : id, 'table':table},
             type: "post",
             url: url + "Application/deleteRow",
             success: function(data){
                  $('#current_row'+id).remove();
             }
        });
        return true;
      } else {
        return false;
      }
       

    }

    </script>>
