<?php $session_data = $this->session->userdata['loggedInData']; ?>
<style type="text/css">
    .icons_all li {
    /*display: inline-block;*/
    padding: 0px 17px;
    /*position: relative;*/
}
.dataTables_wrapper .dataTables_length {
    float: left;
    float: right !important;
    position: absolute;
    right: 0px;
    top: -6px;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>  Announcement</h4> -->
                        <!--<ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active"> Announcement</li>
                        </ol>-->
                    </div>
                </div>
            </div>
                    <!-- end row -->
                        <div class="col-12">
                            <?php echo $this->session->flashdata('msg'); ?>
                                                       
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="hearding">
                            <h4 class="mt-0 header-title left_side">Programs  </h4>
                        </div>
                        <?php if($session_data['user_type'] != "3") { ?>
                        <div class="add_more">
                            <!-- <button type="button" class="btn btn-info waves-effect waves-light">Export</button>  -->
                            <a class="btn color_another waves-effect waves-light btn-addon" href="<?php echo base_url('programs/create_program'); ?>">
                                <i class="fa fa-plus"></i>Add New
                            </a>
                        </div> 
                        <?php } ?>
                        <div class="clearfix"></div>
                        <hr>
                        <table id="dtBasicExample" class="table table-hover pt-4 " cellspacing="0" width="100%">
                            <thead class="black white-text">
                                <tr>
                                <th class="no_sort">No</th>
                                    <th class="th-sm">Program Name
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    
                                    <?php if ($session_data['user_type'] != '3') { ?>
                                    <th class="th-sm">Action
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <?php } ?>
                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($program_data)) {
                                    $i = 1;
                                    foreach ($program_data as $key => $value) { 
                                    	//echo $value['name'];
                                    	
                                    	
                                ?>
                         <tr id="current_row<?php echo $value['id']; ?>">
                                    <th scope="row"><?php echo $i; ?></th>
                                    <td><?php echo $value['name'] ?></td>
                                   
                                      <!-- <td><?php echo ($value['from_date'] !='0000-00-00')? date('d/m/Y', strtotime($value['from_date'])) :'--';  ?></td>
                                    <td><?php echo ($value['to_date'] !='0000-00-00')? date('d/m/Y', strtotime($value['to_date'])) :'--';  ?></td>
                                    <td><?php echo "Admin"; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($value['created'])); ?></td>-->
                                    <?php if ($session_data['user_type'] != '3') { ?>
                                    
                                    <td>
                                        <ul class="icons_all">
                                            <li>
                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805" href="<?php echo base_url('programs/edit_program?id='.$value['id']); ?>">
                                                <img src="http://lmsmalaysia.com/public/images/edit_edit.png">
                                                </a>
                                            </li>
                                            <li>
                                                <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'programs')">
                                                    <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png">
                                                </a>
                                            </li>
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php $i++; } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
</body>
</html>