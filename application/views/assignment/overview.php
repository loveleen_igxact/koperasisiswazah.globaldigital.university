<style type="text/css">
    .border-bottom{
        border:1px solid red;
    }
    .border-bottom-black {
        border: 1px solid black;
        width: 86px;
}
.td_img img {
    border: 1px solid #e7eaed;
    width: 80%;
    padding: 7px;
}
.height-table{
    height: 513px;
    overflow-y: scroll;
}
.add_more {
    font-size: 15px;
    width: 180px;
}
.icons_all {
    padding: 0px;
    width: 100%;
}
</style>
<!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Application</h4>-->
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active">New Application</li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    </div>
                  </div>
                    <!-- end row -->
                        
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="add_more">
                                        <ul class="icons_all">
                                            <li>
                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="">
                                                    <img src="http://lmsmalaysia.com/public/images/edit_edit.png"> Edit 
                                                 </a>
                                            </li>
                                            <li>
                                                <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="">
                                                    <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"> Delete
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                     <h4 class="mt-0" style="text-transform: uppercase; letter-spacing: 2px;">Nam eget laoreet sem Nullam rhoncus sed metus</h4>
                                            <div class="media m-b-30">
                                                <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80">
                                                <div class="media-body">
                                                    <h5 class="mt-0 font-16">Halina qutor</h5>
                                                    <p><span><img src="http://lmsmalaysia.com/public/images/calander.png" width="15">&nbsp; 10 Jan 2019</span>
                                                        <br>
                                                        <span><strong>Status : <span style="color: #febb14"> Pending</span></strong></span>
                                                    </p>
                                                </div>
                                            </div>
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <div class="col-12">
                                        <h5><strong>Description</strong></h5>
                                        <div class="border-bottom-black"></div>
                                        <p class="pt-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                                    </div>
                                    <div class="col-12">
                                        <div class="attach_ment">
                                            <h5>Attachment</h5>
                                            <div class="border-bottom-black"></div> 
                                            <div class="table-responsive height-table pt-3 ">
                                                <table class="table ">
                                                        <tbody>
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60">
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr> 
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pt-3">
                                            <h5>Review</h5>
                                            <div class="border-bottom-black"></div>
                                            <div class="form-check-inline pt-3">
                                                <input type="checkbox" name="staff_tab" class="form-check-input" value="staff">
                                                <label class="form-check-label"> Mark as Review &nbsp; </label>
                                            </div>
                                            <div class="col-md-6 p-0 pt-3">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="4" placeholder="Type message here"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12 pt-3 p-0">
                                                <button class="color_another btn" type="button"> Reviewed</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </div>