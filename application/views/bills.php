<?php if (!empty($bill_data)) {
        $i = 1;
    foreach ($bill_data as $key => $data) {
?>
<div class="modal fade" id="adjustment<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header tran-heading text-center ">
<div class="col-md-12 text-center">
<h5 class="modal-title" id="exampleModalLabel">Add New Collection</h5>
</div>
<!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button> -->
</div>
<hr>
<div class="modal-body">
<div class="container">
<div class="row">
<hr>
<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
<thead class="black white-text">
<tr>
<th>No</th>
<th class="th-sm">Doc No
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
<th class="th-sm"> Item
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
<th class="th-sm">Description
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
<th class="th-sm">Max Amount
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
<th class="th-sm">Full
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
<th class="th-sm">Amount
<i class="fa fa-sort float-right" aria-hidden="true"></i>
</th>
</tr>
</thead>
<tbody>
<tr>
<th scope="row"><?php echo $i; ?></th>
<td>IVX02675</td>
<td>YURAN PENGAJIAN (UKM)</td>
<td>YURAN PENGAJIAN (UKM) - YURAN SEMESTER 1</td>
<td>6800</td>
<td>
<input type="checkbox" name="">
</td>
<td>
<input type="text" class="form-control" name="">
</td>
</tr>
</tbody>
</table>
<div class="clearfix"></div>
<div class="col-md-4">
<div class="form-group">

<label class="control-label"><strong> Credit Note Type</strong></label>

<select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

<option>Please Select</option>

<!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->

<option value="AK">option</option>

<option value="HI">option</option>
</select>
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<div class="form-group">
<label><strong>Date Issue</strong></label>
<div>
<div class="input-group">
<input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
<div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
</div>
</div>
<!-- input-group -->
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<div class="form-group">
<label><strong>Total</strong></label>
<input type="text" class="form-control" name="">
</div>
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<div class="form-group">
<label><strong>Remark</strong></label>
<textarea class="form-control" rows="3"></textarea>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary btn-lg">Save</button>
<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>
<?php $i++; } } ?>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Bills</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active">Bills</li>
                        </ol>
                    </div>
                </div>
            <!-- end row -->
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div id="prospect"></div>
                            <div style="clear:both"></div>
                            <form method="post">
                                <div class="search ">
                                    <div class="form-group">
                                        <label>Search</label>
                                        <input type="text" class="form-control serch" required="" placeholder="Search by Student Name or Document No..." name="search"> 
                                        <span class="serch_icon"><button class="btn btn-dark" type="submit" name="submit">Search</button></span>
                                    </div>
                                </div>
                            </form>
                    <!-- <h6>Filter by Prospect Name</h6> -->
                    <!-- Collapse buttons -->
                    <div class="button">
                    <a class="btn btn-primary " gradient="peach" type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Bill Filter
                    </a>
                    </div>
                    <!-- / Collapse buttons -->

                    <!-- Collapsible element -->
                    <div class="row" style="padding-top: 10px;">
                    <div class="collapse col-md-12" id="collapseExample">
                    <div class="row">
                    <div class="col-md-4">
                    <div class="form-group">
                    <label class="control-label"><strong>Issued By</strong></label>
                    <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                    <option>Select</option>
                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                    <option value="AK">option</option>
                    <option value="HI">option</option>
                    </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                    <div class="form-group">
                    <label><strong>Date Range</strong></label>
                    <div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                        <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                        </div>
                    </div>
                    <!-- input-group -->
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                    <label class="control-label"><strong>Student Status</strong></label>
                    <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                    <option>Select</option>
                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                    <option value="AK">option</option>
                    <option value="HI">option</option>
                    </select>
                    </div>
                    </div>
                    <div class="col-md-12 pt-2">
                    <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>
                    </div>
                    </div>
                    </div>
                    </div>
                    <!-- / Collapsible element -->  
                    </div>     
                </div>
            <!---card 2 table-->
            <div class="card m-b-30">
            <div class="card-body">
            <div class="hearding">
            <h4 class="mt-0 header-title left_side">Bills</h4>
            </div>
            <div class="add_more">
            <a href="<?php echo base_url(); ?>finance/create_bills">
            <button type="button" class="btn btn-info waves-effect waves-light">Add New</button>
            </a>
            <a href="<?php echo base_url(); ?>finance/group_billing">
            <button type="button" class="btn btn-dark waves-effect waves-light">Add Group Biling</button>
            </a> 
            </div>
            <div class="clearfix"></div>
            <hr>
            <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead class="black white-text">
                    <tr>
                        <th>No</th>
                        <th class="th-sm">Doc No
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Date
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Student Name
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Description
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Amount
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Outstanding
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Action
                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php if (!empty($bill_data)) {
                    $i = 1;
                foreach ($bill_data as $key => $data) {
                  ?>
                    <tr id="current_row<?php echo $data['id']; ?>">
                        <th scope="row"><?php echo $i; ?></th>
                        <td><a href="">IV1243</a></td>
                        <td><?php echo date('d/m/Y',strtotime($data['date'])); ?></td>
                        <td><?php echo $data['student_name']; ?></td>
                        <td><?php echo $data['description']; ?></td>
                        <td><?php echo $data['amount']; ?></td>
                        <td><?php echo $data['amount']; ?></td>
                        <td><a href="<?php echo  base_url('finance/collect_bills/'.$data['id']); ?>"><button data-toggle="tooltip" data-placement="top" title="Collect"><i class="far fa-money-bill-alt" style="font-size:25px; cursor: pointer;" ></i></button></a>
                        <button  data-toggle="modal" data-target="#adjustment<?php echo $data['id']; ?>" data-placement="top" title="Adjustment"><i class="mdi mdi-lead-pencil" style="font-size:25px; color: #eb2129; cursor: pointer;"></i></button>
                        <a href="javascript:void(0)" onclick="deleteRow(<?php echo $data['id']; ?>, 'bills')"> <button data-toggle="tooltip" data-placement="top" title="Void" ><i class="mdi mdi-block-helper" style="font-size:25px; color: #4856a4; cursor: pointer;"></i></button></a>
                        </td>
                    </tr>
                <?php $i++; } } else { ?>
                    <tr><td>No Record Found.</td></tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
            </div>      
            </div>
            </div>
        </div>
    <!-- container-fluid -->
    </div>
</div>