<?php $this->session->flashdata('email_sent'); ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 </head>
 <body>
 	<div class="content-page">
   		 <div class="content">
        	<div class="container-fluid">
        	   <div class="row">
	                <div class="col-sm-12">
	                    <div class="page-title-box">
	                        <h4 class="page-title"><span><i class="fa fa-key bg_icon_color"></i></span>  Reset password</h4>
	                        <!--<ol class="breadcrumb">-->
	                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
	                        <!--    <li class="breadcrumb-item active"> Documents</li>-->
	                        <!--</ol>-->
	                    </div>
	                </div>
           		 </div>
        		   <form class="pure-form" method="post">
					    <fieldset>
					       <div class="form-group col-md-6">
					        <input type="password" class="form-control hh" placeholder="Enter Current Password" id="old_password" required name="password" />
					        </div> 
					        <div class="form-group col-md-6">
					        <input type="password" class="form-control hh" placeholder="Enter New Password" id="password" required name="NewPassword" />
					    </div>
					    <div class="form-group col-md-6">
					        <input type="password" class="form-control hh" placeholder="Confirm New Password" id="confirm_password" required>
					    </div>
					     <div class="form-group col-md-6 p-0">
					        <button type="submit"  class="btn btn-info w-md waves-effect waves-light btn-lg" style="width:100%; background-color:#3d42cf; border-1px solid #3d42cf;" name="submit">Confirm</button>
					    </div>
					    </fieldset>
				   </form>
        	</div>
        </div>		
    </div>  

        <script type="text/javascript">
				        var password = document.getElementById("password")
				  , confirm_password = document.getElementById("confirm_password");

				function validatePassword(){
				  if(password.value != confirm_password.value) {
				    confirm_password.setCustomValidity("Passwords Don't Match");
				  } else {
				    confirm_password.setCustomValidity('');
				  }
				}

				password.onchange = validatePassword;
				confirm_password.onkeyup = validatePassword;
    	</script>  
 
 </body>
 </html>
 