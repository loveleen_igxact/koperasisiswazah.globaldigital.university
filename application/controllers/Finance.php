<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL & ~E_NOTICE);
class Finance extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');   // loads admin model
        $this->CI = & get_instance();
        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login')); 
        }
    }
    public function index()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('welcome_message');
        $this->load->view('layout/footer');
    }

    public function prospects() {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');

        $data['prospect_data'] = $this->AdminModel->getData('prospect');
        //echo '<pre>';print_r($data['prospect_data']); die;
        $this->load->view('prospects' , $data);
        $this->load->view('layout/footer');
    }


    public function add_prospects() {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        if(isset($_POST['submit'])) {
            unset($_REQUEST['submit']);
            $this->db->insert('prospect',$_REQUEST);
             $this->prospects();
            return;
        } else {
            $this->load->view('add_prospects');

        }
        $this->load->view('layout/footer');
    }


    public function follw() {
      $data['follw_data'] = $this->AdminModel->getData('follow_up');

        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('follow_up', $data);
        $this->load->view('layout/footer');

    }
    public function agencies() {
        if(isset($_POST['search_btn'])) {
            echo '<pre>'; print_r($_REQUEST); die;
        }

        $data['agencies_data'] = $this->AdminModel->getData('agency');
        $agency =  array();
        foreach ($data['agencies_data'] as  $records) {
            $agency[$records['id']] = $records;

        }
        $data['agency'] =  $agency;
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('agency',$data);    
        $this->load->view('layout/footer');
    }
    public function add_agencies() {
       if(isset($_POST['registration_no'])) { 
             $this->db->insert('agency',$_REQUEST);
            $this->agencies();
            return;
        }
    }

    public function edit_agencies() {


        if(isset($_POST['registration_no'])) {

            $table = 'agency';
            $where =  array('id'=> $_REQUEST['id']);

            unset($_REQUEST['id']);

            $data =  $_REQUEST;
            $this->AdminModel->updateData($where,$data,$table);
            $this->agencies();
            return;
        }
    }


    public function  update_agency(){

        $userrow =  urldecode($_REQUEST['obj']);
        $data['json_data'] =  json_decode($userrow);

        $abc =   $this->load->view('update', $data);

    }


    public function deleteRow() {

        $this->AdminModel->row_delete($_REQUEST['id'], $_REQUEST['table']);
    }

    public function agent() {
        $agent  =  array();
        $data['agencies_data'] = $this->AdminModel->getData('agency');

        $data['agent_data'] = $this->AdminModel->getData('agent');

        foreach ($data['agent_data'] as $agents) {

            $agent[$agents['id']] =   $agents;
        }
        $data['agents'] =$agent;
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('agent',$data);    
        $this->load->view('layout/footer');
    }

    public function add_agent()  {

        if(isset($_POST['agency_id'])) {
             $this->db->insert('agent',$_REQUEST);
            $this->agent();
            return;
        } 
    }


    public  function  update_agent() {


        $userrow =  urldecode($_REQUEST['obj']);
        $data['json_data'] =  json_decode($userrow);

        $abc =   $this->load->view('agent_update', $data);

    }

    public function edit_agent() {

        echo '<pre>'; print_r($_REQUEST); die;
        if(isset($_POST['registration_no'])) {

            $table = 'agent';
            $where =  array('id'=> $_REQUEST['id']);

            unset($_REQUEST['id']);

            $data =  $_REQUEST;
            $this->AdminModel->updateData($where,$data,$table);
            $this->agencies();
            return;
        }
    }
    ////////////////////////// FINANCE ////////////////////////////////////
    public function bills() {
        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login'));
        }
        $session_data = $this->session->userdata['loggedInData'];
        if ($session_data['user_type'] == '3') {
            $where = $session_data['id'];
        } else {
            $where = '';
        }
        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $data['bill_data'] = $this->AdminModel->searchBills($search);
        } else {
            $data['bill_data'] = $this->AdminModel->getBills($where);
        }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/bills',$data);    
        $this->load->view('layout/footer');
    }
    public function group_billing() {
        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $data['bill_data'] = $this->AdminModel->searchBills($search);
        } else {
            $where = '';
            $data['bill_data'] = $this->AdminModel->getBills($where);
        }
        $data['programs'] = $this->AdminModel->getData('campus_program');
        $data['faculty'] = $this->AdminModel->getData('faculty');
        $data['intake'] = $this->AdminModel->getData('intake');
        $where = array('user_type' => '3');
        $data['users'] = $this->AdminModel->getData('users',$where);
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/group_billing',$data);    
        $this->load->view('layout/footer');
    }
    public function collect_bills() {
        $data['student_id'] = $_GET['origin'];
        $data = array();
        if(!empty($_GET['origin'])){
            $data['bills'] = $this->AdminModel->getCollectBills($_GET['origin']);
            $data['users_info'] = $this->AdminModel->get_data_by_id('students','id',$_GET['origin']);
            $where_method = array("is_active"=>1);
            $data['payment_methods'] = $this->AdminModel->getData('payment_methods',$where_method);
            $data['banks'] = $this->AdminModel->getData('banks',$where_method);
            $data['intake'] = $this->AdminModel->getData('intake');
            $data['programs'] = $this->AdminModel->getData('programs');
            $user_id_value=$data['users_info']['user_id'];
            $where =  array('id'=> $user_id_value);
            $data['user_profile'] = $this->AdminModel->getData('users',$where);
            $this->load->view('layout/header');
            $this->load->view('layout/sidebar');
            $this->load->view('finance/collect_bills', $data);
            $this->load->view('layout/footer');
        }
    }
    public function collect_b() {
        $data['student_id'] = $_GET['origin'];
        $data = array();
        if(!empty($_GET['origin'])){
            $data['bills'] = $this->AdminModel->getCollectBills($_GET['origin']);
            $data['users_info'] = $this->AdminModel->get_data_by_id('students','user_id',$_GET['origin']);
            $this->load->view('layout/header');
            $this->load->view('layout/sidebar');
            $this->load->view('finance/collect_b', $data);
            $this->load->view('layout/footer');
        }
    }
    public function generate_collect()
    {
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $arr = array(
                    //'student_id'=>$_GET['origin'],
                    'date'=>$_POST['date'],
                    'payment_method'=>$_POST['payment_method'],
                    'bill_id'=>$_POST['total_val'],
                    'amount'=>$_POST['total'],
                    'created_at' => date('Y-m-d H:i:s'),

                );
        $get_bill_info = $this->AdminModel->get_data_by_id('bills','id',$_POST['total_val']);
        $get_payment_info = $this->AdminModel->get_data_by_id('payments','bill_id',$_POST['total_val']);
        if ($_POST['total']>$get_payment_info['balance'] && !empty($get_payment_info)) {
           $overpayment=$_POST['total']-$get_payment_info['balance'];
        }
      else{
                if(empty($get_payment_info) && $_POST['total']>$get_bill_info['amount']){
                          $overpayment=$_POST['total']-$get_bill_info['amount'];
                }
                else{
                $overpayment='0.00';
                 }
      }
        $get_students_info = $this->AdminModel->get_data_by_id('students','id',$_POST['student_id']);
        $toatlpaidAmount=$get_bill_info['paid']+$_POST['total'];
        if ($toatlpaidAmount > $get_bill_info['amount']) {

           $remaining_amount='0.00';
        }else{
        $remaining_amount = $get_bill_info['amount']-$get_bill_info['paid']-$_POST['total'];
        }
        $paid = $get_bill_info['paid']+$_POST['total'];
        //$remaining_amount = $_POST['from_amount']-$_POST['total'];
        if ($remaining_amount == '0') {
            $array['status'] = "1";
            $outstanding = '0.00';
        } else {
            $outstanding = $remaining_amount;
        }
        // echo "outstanding".$outstanding;
        // echo "paid".$paid;
        // die;
       // $save = $this->AdminModel->saveData($arr,'bill_payments');
        $get_last_payment = $this->AdminModel->get_last_payment();
        $value = "1";
        $docment_no = $get_last_payment->id+$value;
        $doc_no = "OR0".$docment_no;
        $arr1 = array(
            'doc_no'=>$doc_no,
            'bill_id'=>$_POST['total_val'],
            'paid'=>$_POST['total'],
            'amount'=>$get_bill_info['amount'],
            'campus_id'=>$get_bill_info['p1_campus'],
            'intake_id'=>$get_bill_info['intake'],
            'balance'=>$outstanding,
            'overpayment_amount'=>$overpayment,
            'method_id'=>$_POST['payment_method'],
            'remarks'=>$_POST['remarks'],
            'bank_id'=>$_POST['bank_id'],
            'program_id'=>$get_bill_info['program_id'],
            'ref_no'=>$_POST['Reference_no'],
            'ref_date'=>date('Y-m-d', strtotime($_POST['date'])),
            'student_id'=>$_POST['student_id'],
            'description'=>$_POST['desc'],
            'issued_at'=>date('Y-m-d', strtotime($_POST['date'])),
            'faculty_id'=>1,
            'created_at' =>$_POST['date'],
            'updated_at' => date('Y-m-d H:i:s'),
        );
       

        $insert_payment = $this->AdminModel->saveData($arr1,'payments');
        $where =  array('id'=> $_POST['total_val']);
        $table = "bills";
        $array['balance'] = $outstanding;
        $array['paid'] = $paid;
        $this->AdminModel->updateData($where,$array,$table);
        echo "1";
        // if (!empty($save)) {
        //     echo "1";
        // } else {
        //     echo "0";
        // }
    }
    public function payment()
    {
        // $data['details']=$this->AdminModel->statement_of_account();
       // echo "<pre>"; print_r($data);

        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login'));
        }
        $session_data = $this->session->userdata['loggedInData'];
        if ($session_data['user_type'] == '3') {
            $where = $session_data['id'];
        } else {
            $where = '';
        }
        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $data['bill_data'] = $this->AdminModel->searchBills($search);
        } else {
            $data['bill_data'] = $this->AdminModel->getBills($where);
        }

      $this->load->view('layout/header');
      $this->load->view('layout/sidebar');
      $this->load->view('finance/payment',$data);    
      $this->load->view('layout/footer');
    }
    public function transactions() {
        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $data['transaction_data'] = $this->AdminModel->searchTransactions($search);
        } else {
            $data['transaction_data'] = $this->AdminModel->getTransactions();
        }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/transactions',$data);    
        $this->load->view('layout/footer');
    }
    public function get_data_by_id($table,$field,$id)
    {
        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);
        return $get_data_by_id;
    }
    public function create_bills() {
        $where = array('type' => '1');
        $data['student_dataSerial'] = $this->AdminModel->student_dataSerial();
        $data['student_data'] = $this->AdminModel->getData('students',$where);
        $data['programs'] = $this->AdminModel->getData('campus_program');
        $data['programsdd'] = $this->AdminModel->getData('programs');
        date_default_timezone_set("Asia/Kuala_Lumpur");
        if (isset($_POST['genrate'])) {
            $i = 0;
            $getStudentInfo = $this->AdminModel->get_data_by_id('students','id',$_POST['student_id']);
            $checkCollectId = $this->AdminModel->get_data_by_id('collect','user_id',$_POST['student_id']);
            if ($checkCollectId == true) {
                $collect_id = $checkCollectId['id'];
            } else {
                $res = array('user_id'=>$_POST['student_id']);
                $collect_id = $this->AdminModel->saveData($res,'collect');
            }
            // $get_student_info = $this->AdminModel->get_data_by_id('students','user_id',$_POST['student_id']);
            // $stu_id = $get_student_info['id'];
            $get_student_info = $this->AdminModel->get_data_by_id('student_programs','student_id',$_POST['student_id']);
            $student_program_id = $get_student_info['id'];
            $arr = array(
                'student_id'=>$_POST['student_id'],
                'student_name'=>$getStudentInfo['name'],
                'program'=>$_POST['program'],
                'student_program_id'=>$student_program_id,
                'date'=>date('Y-m-d', strtotime($_POST['date'])),
                'due_date'=>date('Y-m-d', strtotime($_POST['due_date'])),
                //'prospects_id'=>$getStudentInfo['prospect_id'],
                'collect_id'=>$collect_id,
                'amount' => $_POST['amount'],
                'balance' => $_POST['amount'],
                'description' => $_POST['description'],
                'unit_price' => $_POST['unit_price'],
                //'discount' => $_POST['discount'],
                //'account' => $_POST['account'],
                //'amount_is' => $_POST['amount_is'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $insert_id = $this->AdminModel->saveData($arr,'bills');
            // $insert_payment = $this->AdminModel->saveData($arr,'payments');
            $val = "IVX00211";
            $table = 'bills';
            $wheres =  array('id'=> $insert_id);
            $array['doc_no'] =  "IVX0".$insert_id;
            $this->AdminModel->updateData($wheres,$array,$table);
            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Bill Created Successfully.</div>');
            redirect(base_url('finance/bills'));
        }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/create_bills',$data);    
        $this->load->view('layout/footer');
    }
    public function collection() {
        $collection  =  array();
        $where = array('user_type' => '3');
        $data['student_data'] = $this->AdminModel->getData('users',$where);
        // if (isset($_POST['submit'])) {
        //     $search = $_POST['search'];
        //     $data['collection_data'] = $this->AdminModel->searchCollection('collection',$search);
        // } else {
            $data['collection_data'] = $this->AdminModel->getCollections();
        //}
        if (isset($_POST['save'])) {
            $getStudentInfo = $this->AdminModel->get_data_by_id('users','id',$_POST['student_id']);
            $checkCollectId = $this->AdminModel->get_data_by_id('collect','user_id',$_POST['student_id']);
            if ($checkCollectId == true) {
                $collect_id = $checkCollectId['id'];
            } else {
                $res = array('user_id'=>$_POST['student_id']);
                $collect_id = $this->AdminModel->saveData($res,'collect');
            }
            redirect(base_url('finance/collect_bills?id='.$collect_id.'&origin='.$_POST['student_id']));
        }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/collection',$data);    
        $this->load->view('layout/footer');
    }
    public function searchCollection()
    {
        $search = $_POST['search_data'];
        $collection_data = $this->AdminModel->searchCollection('collection',$search);
        if($collection_data == true) {
                $html = '<table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">
                <thead class="black white-text">
                    <tr>
                    <th class="no_sort">No</th> 
                        <th class="th-sm">Student Name
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Student No
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">IC/Passport
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Email
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Outstanding
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm no_sort">Action
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm all_select_checkbox" width="170px">Invoice Reminder
                        </th>
                    </tr>
                </thead>
                <tbody>';
                $i = 1;
                foreach($collection_data as $data) {
                    $get_student_info = $this->AdminModel->get_data_by_id('students','id',$data['student_id']);
                    // print_r($get_student_info['email']);
                    $student_name = $get_student_info['name'];
                    $html .= '
                    <tr>
                      <th scope="row">'.$i.'</td>
                      <td>'.$student_name.'</td>
                       <td>'.$get_student_info['student_no'].'</td>
                      <td>'.$data['id_no'].'</td>
                      <td>'.$get_student_info['email'].'</td>
                      <td>'.$data['balance'].'</td>
                      <td class="text-center" ><a class="first_icon" data-toggle="tooltip" data-placement="top" title="Collect" href="http://lmsmalaysia.com/finance/collect_bills?origin='.$data['student_id'].'"><img src="http://lmsmalaysia.com/public/images/money_icon.png"></a></td>
                        <td class="text-center" ><input type="checkbox" name="invoice_reminder" data-id="'.$get_student_info['email'].'" class="bcd" id="invoice_checkbox_'. $i.'"><input type="hidden" name="amt" data-id='.$data['amount'].' class="amt"></td></tr>';
                $i++; 
            }
                $html .= '</tbody></table>';
                echo $html;
            } else {
                echo 'No Record Found';
            }
    }
    public function upload_collection() {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/upload_collection');    
        $this->load->view('layout/footer');
    }

    public function upload_data()
    {
        $filename=$_FILES["fileToUpload"]["tmp_name"];  
        $imageFileType = pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION);
        if($imageFileType == "csv" ) {
         if($_FILES["fileToUpload"]["size"] > 0)
         {
            $file = fopen($filename, "r");
            $i=1;
            
              while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
              {
                if ($i == 1) {
                  $i++; continue;
                }
                $data = array(
                    "account_number"=>$getData[0],
                    "student_name"=>$getData[1],
                    "id_no"=>$getData[2],
                    "ref_no"=>$getData[3],
                    "amount"=>$getData[4],
                    // "phone"=>$getData[5],
                );
                $this->db->insert('collection',$data);
                $this->collection();
               
              }  //redirect('finance/collection');
            
              
           } else {
           }
         } else {
           
        }
    }
 public function pay()
    {
        $data['det'] = $this->AdminModel->get_data_by_id('users','id',$_REQUEST['id']);
        // $data111=$bill_data->doc_no;
        // echo $data111;
        // echo "string1111";
        // die();
         // PRINT_R($data);DIE();
        redirect('https://uat.kiplepay.com/wcgatewayinit.php');
        // $this->load->view('payform',$data); 
       
    }

    public function ipayresponse()
    {
       
         $this->load->view('response'); 
       
    }

    public function refunds()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/refunds');    
        $this->load->view('layout/footer');
    }
    public function invoice_reminder()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/invoice_reminder');    
        $this->load->view('layout/footer');
    }

    public function invoice_remind()
    {
        // print_r($_POST['vid']);
        // $str = implode(',', $array);
        $email = $_POST['vid'];
         // $email ='loveleen.igxact@gmail.com'; 
         // $from_email='admin$igxact.com';
        // print_r($email);
       
        // $subject = 'Invoice Reminder';
        //     $message = "Hii, This is invoice Reminder";
            // $this->send_email($value,$message,$subject);

            // $userMailResponse = $this->sendMailAdmin($from_email, $email, $subject, $message);

        
        foreach($email as $value) {
         
            $finalemail=$value;
            $from_email='financeksbb@lmsmalaysia.com';
            $subject = 'Invoice Reminder';
            $message = "Hi,
Please note that your invoice is due. Please make your payment as soon as possible to avoid future difficulties.
Please ignore this e-mail if payment has been made.

Thanks and Regards,
Koperasi Siswazah Bangi Berhad";
            // die;
            // $this->send_email($value,$message,$subject);
            $userMailResponse = $this->sendMailAdmin($from_email, $finalemail, $subject, $message);
             
        }
            // print_r($_POST['vid']);
       
    }
    public function receipt()
    {
        $docnumber=$_GET['docnumber'];
        
        date_default_timezone_set("Asia/Kuala_Lumpur");
       $data['details111'] = $this->AdminModel->getReceipt($docnumber);
        $data['get_last_payment'] = $this->AdminModel->get_last_payment();
       

 $get_payment_logs = $this->AdminModel->get_data_by_id('payment_logs','ord_key',$_REQUEST['ord_key']);
 if ($get_payment_logs>0) {
    $this->load->view('finance/account_statement',$data);    
    // echo "failure";
     # code...
 }
 // echo $get_payment_logs;
 // print_r($get_payment_logs);
 // print_r($_REQUEST['ord_key']);

        $data_response = array(
             'ord_shipname' =>$_REQUEST['ord_shipname'],
             'ord_mercID' =>$_REQUEST['ord_mercID'],
             'ord_date' =>$_REQUEST['ord_date'],
             'ord_totalamt' =>$_REQUEST['amountPaid'],
             'ord_shipcountry' =>$_REQUEST['ord_shipcountry'],
             'payment_Date' =>$_REQUEST['ord_date'],
             'ord_telephone' =>$_REQUEST['ord_telephone'],
             'ord_mercref' =>$_REQUEST['ord_mercref'],
             'ord_gstamt' =>$_REQUEST['ord_gstamt'],
             'ord_email' =>$_REQUEST['ord_email'],
             'ord_svccharges' =>$_REQUEST['ord_svccharges'],
             'bill_Id' =>$_REQUEST['docnumber'],
             'ord_delcharges' =>$_REQUEST['ord_delcharges'],
             'returncode'=>$_REQUEST['returncode'],
             'ord_key'=>$_REQUEST['ord_key'],
             'wcID'=>$_REQUEST['wcID']

         
    );
         $WorkingArray = json_decode(json_encode($data),true);
        
        if($_REQUEST['returncode']==100 && $_REQUEST['amountPaid']>0 && empty($get_payment_logs)){
             
             $where =array('bill_id' => $billId);
             $paymentData=$this->AdminModel->getData('payments',$where);
             
             if($WorkingArray['details111']['balance'] < $_REQUEST['amountPaid']){
                $balanceAmount='0.00';
             }else{
                $balanceAmount=$WorkingArray['details111']['balance']-$_REQUEST['amountPaid'];
             }
             $get_last_payment = $this->AdminModel->get_last_payment();
        $value = "1";
        $docment_no = $get_last_payment->id+$value;
        $doc_no = "OR0".$docment_no;
            // $this->AdminModel->add_paymentLogs($data);
            $arrPayment = array(
               
            'doc_no'=> $doc_no,
            'bill_id'=>$WorkingArray['details111']['id'],
            'paid'=>$_REQUEST['amountPaid'],
            'amount'=>$WorkingArray['details111']['amount'],
            'campus_id'=>$WorkingArray['details111']['campus_id'],
            'intake_id'=>$WorkingArray['details111']['intake_id'],
            'balance'=>$WorkingArray['details111']['balance']-$_REQUEST['amountPaid'],
            'method_id'=>19,
            'overpayment_amount'=>0.00,
            'remarks'=>$WorkingArray['details111']['remarks'],
            'bank_id'=>19,
            'ref_no'=>$_REQUEST['ord_mercref'],
            'program_id'=>$WorkingArray['details111']['student_program_id'],
            'ref_date'=>date('Y-m-d', strtotime($WorkingArray['details111']['date'])),
            'student_id'=>$WorkingArray['details111']['student_id'],
            'description'=>$WorkingArray['details111']['description'],
            'issued_at'=>date('Y-m-d', strtotime($WorkingArray['details111']['issued_at'])),
            'faculty_id'=>1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
            $billId=$WorkingArray['details111']['id'];
             
             // $arrPayment['overpayment_amount']=$overpayment;
             $wherebill =array('id' => $billId);
            $get_bill_info = $this->AdminModel->getData('bills',$wherebill);
            $paidAmount=$get_bill_info[0]['paid']+$_REQUEST['amountPaid'];
          
            $remaining_amount=$get_bill_info[0]['amount']-$get_bill_info[0]['paid']-$_REQUEST['amountPaid'];
            $array['balance'] = $remaining_amount;
            $array['paid'] = $paidAmount;
            $where =  array('id'=> $billId);
            $table = "bills";
       

                 $this->AdminModel->saveData($data_response,'payment_logs');                   
                $this->AdminModel->updateData($where,$array,$table);
                $this->AdminModel->saveDataPayment($arrPayment,'payments');

                                    $email = "financeksbb@lmsmalaysia.com";
                                    $from_email = "financeksbb@lmsmalaysia.com";
                                    $this->load->library('email');

                                    $this->email->from($from_email, 'Message');

                                    $this->email->to($email);

                                    $this->email->subject('New Payment');

                                    $this->email->message('Payment is made');      
                                    // die();                   
                           
                           
                                        //Send mail

                                    if ($this->email->send()) {

                                        $this->session->set_flashdata("email_sent", "Email sent successfully.");
                                        // echo "success";

                                    } else {

                                        $this->session->set_flashdata("email_sent", "Error in sending Email.");
                                         echo "failure";
                                        //
                                    }
                $this->load->view('layout/header');
                $this->load->view('layout/sidebar');
                $this->load->view('finance/receipt',$data);    
                $this->load->view('layout/footer');

        
        }else{
            // echo "Opps! there is some error occur";
            // $this->AdminModel->saveData($data_response,'payment_logs');

            redirect(base_url('Finance/payment'));
        }
       
    }
    public function receipt_before()
    {   
        $data['programsdd'] = $this->AdminModel->getData('programs');
        $data['details'] = $this->AdminModel->getReceipt_before($_GET['id']);
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/receipt_before',$data);    
        $this->load->view('layout/footer');
    }
     public function test()
    {   
       
       
        $this->load->view('finance/test');    
       
    }

    public function receipt_transactions()
    {
      
        $data['details'] = $this->AdminModel->getRecipt_Transactions($_GET['id'],$_GET['amount'],$_GET['docNo']);
        // echo "<pre>";
        // print_r($data['details']);
        $data['programdata'] = $this->AdminModel->getData('programs');
        $data['paymentdata'] = $this->AdminModel->getData('payment_methods');
        $data['bankdata'] = $this->AdminModel->getData('banks');

        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/transaction_receipt',$data);    
        $this->load->view('layout/footer');
    }
    public function signout()
    {
        session_destroy();
        redirect(base_url('login'));
    }

    public function sendMailAdmin($from_email="financeksbb@lmsmalaysia.com",$to_email="financeksbb@lmsmalaysia.com",$subject="adminTestSubject",$message="test"){

        



            $this->load->library('email');



            $this->email->from($from_email, 'Invoice Details');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);

             if ($this->email->send()) {

                

                $this->session->set_flashdata("email_sent", "Email sent successfully.");

                //echo "success";

                return true;

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                //echo "failure";

                return false;

                //  $this->load->view('email_form');

            }



    }

    public function send_email($email,$message,$subject)
    {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        // $this->email->from('no-reply@mylifelineapp.com');
        $this->email->from('financeksbb@lmsmalaysia.com');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
        return true;
    }
    public function thankYou()
    {
        $this->load->view('finance/thankyou');    
    }
    public function accountStatement()
    {
         if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login'));
        }
        $session_data = $this->session->userdata['loggedInData'];
         $data['payment_methods'] = $this->AdminModel->getData('payment_methods');
        
        if ($session_data['user_type'] == '3') {
            $where = $session_data['id'];
        } else {
            $where = '';
        }
        
        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $data['bill_data'] = $this->AdminModel->searchBills($search);
        } else {
            $data['bill_data'] = $this->AdminModel->getpaymentData($where);
        }
        
     
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('finance/account_statement',$data);    
        $this->load->view('layout/footer');
    }
    public function get_students_program()
    {
        $student_id = $_GET['student_id'];
        // print_r($student_id);
        // echo "string1111";
        $get_student_info = $this->AdminModel->get_data_by_id('students','id',$student_id);
        // $stu_id = $get_student_info['id'];
        // $get_student_info = $this->AdminModel->get_data_by_id('student_programs','student_id',$student_id);
        if ($get_student_info['eligible_program']==0) {
            $get_student_info1 = $this->AdminModel->get_data_by_id('student_programs','student_id',$student_id); 
        $program_id = $get_student_info1['program_id'];
           
        }
        else{
         $program_id = $get_student_info['eligible_program'];
        }
        
        $get_program = $this->AdminModel->get_data_by_id('programs','id',$program_id);
        echo $get_program['name'];
        //echo $get_program;
    }
}