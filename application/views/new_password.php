 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH;?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH;?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH;?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .auth-panel {
            background-color: #fff;
            height: 100%;
            position: absolute;
            right: 0;
            top: 0;
            overflow-y: auto;
        }
    </style>
</head>
<body>
 <!-- Begin page -->
   <div class="wrapper-page">
    <h3 class="text-center m-0" style="padding-bottom: 40px;"><a href="#" class="logo logo-admin"><img src="http://lmsmalaysia.com/public/images/logo_png_lower.png" width="30%" height="100%" alt="logo"></a></h3>
<div class="card">
            <div class="card-body" style="box-shadow: 0 0 20px #5d5b5b;">
                <h3 class="text-center m-0  pt-5">
                   Add New Password
                </h3>
                <div class="">
                    <!-- <form class="form-horizontal m-t-30 label_text" action="#" method="post">
                        <div class="form-group">
                            <input type="text" name="password" class="form-control hh" id="password" placeholder="Enter Your New Password">
                        </div>
                          <div class="form-group">
                            <input type="text" name="password" class="form-control hh" id="password" placeholder="Confirm password">
                        </div>

                        <div class="col-md-12 p-0">
                            <button class="btn btn-info w-md waves-effect waves-light btn-lg" style="width:100%; background-color:#3d42cf; border-1px solid #3d42cf;" type="submit" name="submit">Send</button>
                        </div>
                        <div class="col-12 m-t-20 text-center"><span>Already Have Account ?</span><a href="<?php echo base_url('login'); ?>" class="" style="color:blue;"> Login</a>
                        </div>
                    </form> -->
                    <form class="form-horizontal m-t-30 label_text" method="post">
    <fieldset>
       <div class="form-group">
            <input type="password" class="form-control hh" placeholder="Enter New Password" id="password" required name="password">
       </div>
       <div class="form-group">
            <input type="password"  class="form-control hh" placeholder="Confirm New Password" id="confirm_password" required>
       </div>
        <div class="col-md-12 p-0">
            <button type="submit" class="btn btn-info w-md waves-effect waves-light btn-lg" style="width:100%; background-color:#3d42cf; border-1px solid #3d42cf;" name="submit">Confirm</button>
        </div>
    </fieldset>
</form>
                </div>
            </div>
    </div>
</div>
<style>
body{
    /*background:#fff;*/
    background-image: url(http://lmsmalaysia.com/public/images/bg1.jpg);
    background-size: cover;
}
.wrapper-page{
    max-width:600px;
} 
.btn-info:hover {
    background-color: #585dd4 !important;
}
.hh {
    box-shadow: 4px 5px 20px 0px #cec9c9;
}
    </style>
</style>
    <!-- jQuery  -->
    <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH;?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH;?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo PLUG_PATH;?>/pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH;?>js/app.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.js"></script>
    <script type="text/javascript">
        var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
    </script>
</body>
</html>
