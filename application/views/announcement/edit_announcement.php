<?php //print_r($get_announcement); die; ?><div class="content-page">

<!-- Start content -->

<div class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-sm-12">

                <div class="page-title-box">

                    <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Edit Announcement</h4>

                    <!--<ol class="breadcrumb">-->

                    <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                    <!--    <li class="breadcrumb-item active"> Announcement</li>-->

                    <!--</ol>-->

                </div>

            </div>

        </div>

    </div>

                <!---card 2 table-->

			<div class="card m-b-30">

				<div class="card-body">

                    <div class="hearding">

                        <h4 class="mt-0 header-title left_side">Announcements</h4>

                    </div>

                    <div class="clearfix"></div>

                    <hr>

                    <form method="post" enctype='multipart/form-data'>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Subject<span class="sa" style="color: red;">*</span></label>

                                <input type="text" class="form-control" required="" placeholder="" name="subject" value="<?php echo $get_announcement['subject']; ?>"> 

                            </div>

                        </div>

                    

                    <div class="col-md-12">
                        <label>Description<span class="sa" style="color: red;">*</span></label>
                        <textarea type="text" class="form-control" rows="4"  placeholder="Enter Description" name="message" id="message"><?php echo $get_announcement['announcement']; ?></textarea>

                       <!--  <textarea class="form-control ckeditor" name="message" placeholder="Enter News Letter" id="message" style="width:100%; height:100px!important; resize:none;" required /><?php echo $get_announcement['announcement']; ?></textarea> -->

                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12">

                        <div class="form-group">

                            <label><strong>Publish Date<span class="sa" style="color: red;">*</span></strong></label>

                            <div>

                                <div class="input-daterange input-group" id="date-range">
                                    <?php 
                                        $date_from=date('Y-m-d', strtotime($get_announcement['from_date']));
                                        $date_to=date('Y-m-d', strtotime($get_announcement['to_date']));
                                    ?>
                                
                                <input type="date" class="form-control" placeholder="From" name="from_date" required="" value="<?php echo($date_from); ?>">

                                    <input type="date" class="form-control" placeholder="To" name="to_date" required="" value="<?php echo($date_to); ?>">

                                </div>

                            </div>

                        </div>

                    </div>
                    <br>

                    <div class="row">

                        <div class="col-md-12">
                             <label class="cantrol-label"><strong>Uploaded file</strong></label>
                           <?php  
                           $url = "http://lmsmalaysia.com/uploads/documents/".$get_announcement['file1'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); ?>
                         

                            <?php if($get_announcement['file1'] == '') { ?>

                            
                            <?php } else { ?>

                             <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="150" width="150" src="http://lmsmalaysia.com/uploads/documents/<?php echo $get_announcement['file1'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="50" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>



                            <?php } ?> 
                            <input type="file" id="file-input" name="fileToUpload[]"  multiple="">

                           <!--  <mdb-select [options]="optionsSelect" [multiple]="true" placeholder="Choose your option"></mdb-select> -->

                           
                        </div>

                    </div>

                   <!--  <div class="col-md-6"> -->

                       <!--  <div class="form-group">

                            <label class="cantrol-label"><strong> Files</strong></label>

                            <?php if($get_announcement['file1'] == '') { ?>

                            <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file1">

                            <?php } else { ?>

                            <input class="form-control" type="text" name="file1" readonly value="<?php echo $get_announcement['file1']; ?>"><a href="<?php echo base_url('announcement/delete_file?id='.$get_announcement['id']."&type=file1") ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>



                            <?php } ?> 

                        </div>

                        <div class="form-group">

                            <?php if($get_announcement['file2'] == '') { ?>

                            <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file2" value="<?php echo $get_announcement['file2']; ?>">

                            <?php } else { ?>

                            <input class="form-control" type="text" name="file2" readonly="" value="<?php echo $get_announcement['file2']; ?>"><a href="<?php echo base_url('announcement/delete_file?id='.$get_announcement['id']."&type=file2") ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>



                            <?php } ?>

                        </div> -->

                       <!--  <div class="form-group">

                            <?php if($get_announcement['file3'] == '') { ?>

                            <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-2" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file3" value="<?php echo $get_announcement['file3']; ?>">

                            <?php } else { ?>

                            <input class="form-control" type="text" readonly="" name="file3" value="<?php echo $get_announcement['file3']; ?>"><a href="<?php echo base_url('announcement/delete_file?id='.$get_announcement['id']."&type=file3") ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>



                            <?php } ?>

                        </div>
 -->
                       <!--  <strong><span style="color: red;">Note*</span>  = The maximum files is 3. Please delete one file, to add another file</strong> -->

                  <!--   </div> -->

                    

                  <div class="col-md-12 pt-3">

                <label class="cantrol-label"><strong> Publish To</strong> </label><br>

                <!-- <div class="custom-control custom-radio custom-control-inline">

                  <input type="radio" class="custom-control-input" id="defaultInline1" name="tab" mdbinputdirective="" onclick="staff_none();" value="staff_none">

                  <label class="custom-control-label" for="defaultInline1">None</label>

                </div> -->

                <div>

                  <input type="checkbox"  name="staff_tab"   value="staff" <?php if($get_announcement['publish_to_staff'] == '1') { ?> checked <?php } else if($get_announcement['publish_to_staff'] == 'staff') { ?> checked <?php } ?>>

                  <label> Staff</label>

                </div>

                <div>

                  <input type="checkbox"  name="student_tab" value="student" <?php if($get_announcement['publish_to_student'] == '1') { ?> checked<?php } else if($get_announcement['tab'] == 'student') { ?> checked <?php } ?>>

                  <label> Student</label>

                </div>

            </div>

            <!-- <div>

                <label>Select Classes</label><br>

                <?php if(!empty($classes)) { 

                        foreach ($classes as $class) { 

                            $selected_classes = explode(',',$get_announcement['classes']); 

                            ?>

                        <input <?php if (in_array($class, $selected_classes)) { ?> checked

                        <?php } ?> type="checkbox" name="classes[]" value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?><br>

                <?php } } ?>

            </div> -->

            <div class="clearfix"></div>

           <!--  <div>

                <label>Select Subjects</label><br>

                <?php if(!empty($subjects)) { 

                        foreach ($subjects as $subject) { 

                            $selected_subjects = explode(',',$get_announcement['subjects']); 

                ?>

                        <input <?php if (in_array($subject, $selected_subjects)) { ?> checked

                        <?php } ?> type="checkbox" name="subjects[]" value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?><br>

                <?php } } ?>

            </div> -->

            <div class="clearfix"></div>

        </div>

                <div class="col-md-12 text-center">

                    <button class="btn-lg color_another btn" type="submit" name="update">Update</button>

                    <button class="btn-lg btn-secondary btn" type="submit" name="cancel">Cancel</button>

                </div>

            </div>

            </div>

            </form>

		</div>

	</div>

            </div>

        </div>

    </div>

    <!-- container-fluid -->

</div>

        </div>



   





       <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <!-- <script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script> -->

       <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>

       <script type="text/javascript">

        $(document).ready(function(){ lSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = dsl.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = dsl.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                dsl.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                dsl.resetColor(clrName);

            });



            var ds2 = $('#dualSelectExample_1').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = ds2.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = ds2.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                ds2.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                ds2.resetColor(clrName);

            });



            //third

            var ds3 = $('#dualSelectExample_2').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = ds3.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = ds3.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                ds3.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                ds3.resetColor(clrName);

            });

            //end

            //four

               var ds4 = $('#dualSelectExample_3').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = ds4.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = ds4.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                ds4.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                ds4.resetColor(clrName);

            });

            //end

            //fifth

             var ds5 = $('#dualSelectExample_4').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = ds5.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = ds5.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                ds5.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                ds5.resetColor(clrName);

            });

            //end

            //sixth

              var ds6 = $('#dualSelectExample_5').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = ds6.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = ds6.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                ds5.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                ds6.resetColor(clrName);

            });

            //end

        });





        //second

           $(document).ready(function(){

            var dsl = $('#dualSelectExampl').DualSelectList({

                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],

                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],

                'colors' : {

                    'itemText' : 'white',

                    'itemBackground' : 'rgb(0, 51, 204)',

                    'itemHoverBackground' : '#0066ff'

                }

            });



            $('#getSel').click(function(){

                var res = dsl.getSelection();

                var str = '';

                for (var n=0; n<res.length; ++n) str += res[n] + '\n';

                $('#selResult').val(str);

            });



            $('#addSel').click(function(){

                var items = $('#addIterms').val().split('\n');

                var res = dsl.setCandidate(items);

                $('#addIterms').val('');

            });



            $('#setColor').click(function(){

                var clrName = $('#colorSelector').val();

                var clrValue = $('#colorValue').val();

                dsl.setColor(clrName, clrValue);

            });



            $('#resetColor').click(function(){

                var clrName = $('#colorSelector').val();

                dsl.resetColor(clrName);

            });

        });

        function staff()

        {

            $("#staff_filter").show();

        }

        function staff_none()

        {

            $("#staff_filter").hide();

            $("#dept").hide();

        }

        function staff_all()

        {

            //$("#staff_filter").hide();

            $("#dept").hide();

        }

        function staff_selected_filter()

        {

            $("#dept").show();

        }

        function student_selected_filter()

       {

        $("#all_functions").show();

       }

       function student()

       {

        $("#student_filter").show();

       }

       function student_all()

       {

        $("#all_functions").hide();

       }

       function student_none()

       {

        $("#student_filter").hide();

        $("#all_functions").hide();

       }

    </script>

</body>

</html>