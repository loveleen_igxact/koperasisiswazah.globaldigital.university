<?php $session_data = $this->session->userdata['loggedInData']; ?>
<link href="<?php echo  CSS_PATH; ?>css/bootstrap_multiselect.css" rel="stylesheet" type="text/css">
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Upload Documents</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                        <!--    <li class="breadcrumb-item active">Upload Documents</li>-->
                        <!--</ol>-->
                    </div>
                </div>
            </div>
        </div>
    
    <div class="card m-b-30">
        <div class="card-body">
            <form action="<?php echo base_url(); ?>teacher/upload_documents" class="" enctype="multipart/form-data" method="post">
            <h4 class="mt-0 header-title">Upload Documents</h4>
            <div class="m-b-30">
                <div><input type="file" name="fileToUpload[]" required="" multiple><span></span></div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php if ($session_data['user_type'] == '3') { ?>
                    <label for="exampleInput"><strong>Select Teacher</strong></label><br>
                    <select id="lstFruits" class="form-control" multiple="multiple" name= "students[]">
                        <?php if(!empty($teachers_data)) { 
                          foreach ($teachers_data as $value) { ?>
                        <option value="<?php echo $value['user_id']; ?>"><?php echo $value['name']; ?></option>
                        <?php } } ?>
                    </select>
                    <?php } else { ?>
                    <label for="exampleInput"><strong>Select Student</strong></label><br>
                    <select id="lstFruits" class="form-control" multiple="multiple" name= "students[]">
                        <?php if(!empty($students_data)) { 
                          foreach ($students_data as $value) { ?>
                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                        <?php } } ?>
                    </select>
                    <?php } ?>
                    
                </div>
            </div>
            <div class="datemalaysia">
                <?php date_default_timezone_set("Asia/Kuala_Lumpur"); ?>
                    <input type="hidden" name="date" value="<?php echo(date('d-m-Y H:i:s'));?>">
            </div>
            <div class="text-center m-t-15">
                <button type="submit" name="submit" class="btn btn-dark btn-lg waves-effect waves-light">Process File</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>

<script type="text/javascript">
  $(function () {
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
        $('#btnSelected').click(function () {
            var selected = $("#lstFruits option:selected");
            var message = "";
            selected.each(function () {
            });
        });
    });
</script>