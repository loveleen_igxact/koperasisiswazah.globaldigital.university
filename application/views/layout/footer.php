<footer class="footer">Copyright© 2019 LMS</footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->
<!-- jQuery  -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo JS_PATH; ?>js/bootstrap.bundle.min.js"></script>
<script src="<?php echo JS_PATH; ?>js/metisMenu.min.js"></script>
<script src="<?php echo JS_PATH; ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo JS_PATH; ?>js/waves.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo PLUG_PATH; ?>select2/js/select2.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
<script src="http://phpslick.com/lms/public/pages/form-advanced.js"></script>
<!-- <script src="<?php echo base_url(); ?>public/pages/form-advanced.js"></script> -->
<script src="<?php echo PLUG_PATH; ?>morris/morris.min.js"></script>
<script src="<?php echo PLUG_PATH; ?>raphael/raphael-min.js"></script>
<script src="<?php echo JS_PATH; ?>pages/dashboard.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- App js -->
<script type="text/javascript">
// Language change only
$(document).ready(function() {
  $('#dtBasicExample').dataTable( {
    "oLanguage": {
      "sLengthMenu": "Show Entries _MENU_"
    }
  } );
} );
</script>
<script type="text/javascript">
  $(".button_more").click(function() {
     $(".button_more span").html($(".button span").html() == 'More Filter' ? 'Less Filter' : 'More Filter');
});
</script>

<script type="text/javascript">
$(document).ready(function() { 
    $('#dtBasicExample').DataTable();
} );
       function deleteRow(id,table) {
          var url  = '<?php echo  base_url();?>';
          var x=confirm("Are you sure to delete record?")
          if (x) {
              $.ajax({
                  data: { 'id' : id,'table':table},
                   type: "post",
                   url: url + "Application/deleteRow",
                  success: function(data){
                    if (table == 'inbox') {
                      window.location.href = url+"Inbox"; 
                    } else {
                      $('#current_row'+id).remove();
                    }
                  }
              });
              return true;
          } else {
            return false;
          }
        }
   </script>

<script src="<?php echo JS_PATH; ?>js/app.js"></script>
<script src="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.js"></script>         
</body>
</html>
