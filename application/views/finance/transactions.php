<style type="text/css">

.dataTables_wrapper .dataTables_length {

    float: left;

    float: right !important;

    position: absolute;

    right: 0px;

    top: -40px;

}

.hidden{

    display: none;

}

.serch_icon {

    position: absolute;

    bottom: 2px;

    right: 17px;

}

table.dataTable tbody th, table.dataTable tbody td {

    padding: 8px 17px !important;

}

</style>

<div class="modal fade" id="collection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

          <div class="modal-header tran-heading text-center ">

            <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">Add New Collection</h5>

            </div>

           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> -->

          </div>

          <hr>

          <div class="modal-body">

           <div class="container">

            <div class="row">

                <hr>

                <div class="clearfix"></div>

                <div class="col-md-12">

                    <div class="form-group">



                        <label class="control-label"><strong> Student</strong></label>



                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">



                                <option>Please Select</option>



                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->



                                    <option value="AK">option</option>



                                    <option value="HI">option</option>

                            </select>

                    </div>

                </div>

            </div>

           </div>

          </div>

          <div class="modal-footer">

            <button type="button" class="btn btn-primary btn-lg">Save</button>

            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>

          </div>

        </div>

      </div>

    </div>



        <div class="content-page">

            <!-- Start content -->

            <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="page-title-box">

                                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Transaction</h4> -->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active">Transaction</li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                    <!-- end row -->

                        <div class="col-12">

                            <div class="card m-b-30 hidden">

                                <div class="card-body">

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                    <form method="post">

                                        <div class="search ">

                                            <div class="form-group">

                                                <label>Search</label>

                                                <input type="text" class="form-control serch" required="" placeholder="Search by Student Name or Document No..." name="search"> 

                                                <span class="serch_icon"><button class="btn btn-success" type="submit" name="submit">Search</button></span>

                                            </div>

                                        </div>

                                    </form>

                                        <!-- <div class="button">

                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                                            Transaction More Filter

                                            </a>

                                        </div>



                                        <div class="row" style="padding-top: 10px;">

                                        <div class="collapse col-md-12" id="collapseExample">

                                            <div class="row">

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label>Date Range</label>

                                                        <div>

                                                            <div class="input-daterange input-group" id="date-range">

                                                                <input type="text" class="form-control" name="start">

                                                                <input type="text" class="form-control" name="end">

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label class="control-label"><strong>Transaction Type</strong></label>

                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                                <option>Please Select</option>

                                                                <option value="AK">All</option>

                                                                <option value="HI">option</option>

                                                            </select>

                                                    </div>

                                                </div>



                                    <div class="col-md-12 pt-2">

                                        <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>

                                    </div>

                                    </div>

                                        </div>

                                    </div> -->

                                </div>

                                </div>

                            </div>

                        </div>

                            <!---card 2 table-->

                            

                                <div class="card m-b-30">

                                    <div class="card-body">

                                        <div class="hearding">

                                            <h4 class="mt-0 header-title left_side">Transaction</h4>

                                        </div>

                                        <div class="clearfix"></div>

                                        <!-- <hr> -->

                                        <form method="post">

                                        <div class="search ">

                                            <div class="form-group col-md-4 p-0 pt-3">

                                                

                                                <input type="text" class="form-control serch" required="" placeholder="Search by Student Name or Document No..." name="search"> 

                                                <span class="serch_icon"><button class="btn color_another" type="submit" name="submit">Search</button></span>

                                            </div>

                                        </div>

                                    </form>

                                        <table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">

                                            <thead class="black white-text">

                                                <tr>

                                                <th class="no_sort"></th> 

                                                    <th class="th-sm">Doc No

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Date

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Student Name

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Bill ID No

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                    <th class="th-sm">Description

                                                    </th>

                                                    <th class="th-sm no_sort">Amount

                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                    </th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php if (!empty($transaction_data)) {

                                                    $i = 1;

                                                foreach ($transaction_data as $key => $data) {

                                                    $get_data_by_id = $this->CI->get_data_by_id('users','id',$data['student_id']);  ?>

                                                <tr>

                                                    <th scope="row"></th>
                                                    <?php 
                                                    if ($data['paid']==null) {
                                                    	$amountPiad=$data['adjustment'];
                                                    }
                                                    else{
                                                    	$amountPiad=$data['paid'];
                                                    }
                                                   ?>

                                                    <td><a style="color: #1414da; text-decoration: underline;" target="_blank" href="<?php echo  base_url('finance/receipt_transactions?id='.$data['bill_id'].'&amount='.$amountPiad.'&docNo='.$data['doc_no']); ?>"><?php echo $data['doc_no']; ?></a></td>

                                                    <td><?php echo date('d/m/Y',strtotime($data['created_at'])); ?></td>

                                                    <td><?php $student_info = $this->CI->get_data_by_id('students','id',$data['student_id']); echo $student_info['name'] ?></td>

                                                    <td><?php echo $data['bill_id']; ?></td>

                                                    <td><?php echo $data['description']; ?></td> 

                                                    <td><a href=""><?php echo $data['paid']; ?></a>

                                                    </td>

                                                </tr>

                                                <?php $i++; } } else { ?>

                                                <tr><td>No Record Found.</td></tr>

                                                <?php } ?>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>      

                        </div>

                    </div>

                </div>

            </div>

            <style>

                table.dataTable thead .no_sort {

                    background-image: none !important;

                    pointer-events: none;

                }

            </style>