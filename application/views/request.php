<?php

require_once 'IPay88.class.php';
    if(isset($_POST['submit'])){

  $amount = $_POST['payment'];
$session_data = $this->session->userdata('loggedInData');
    $email = $session_data['email'];
    $name = $session_data['name'];
   // echo $name;
}


$ipay88 = new IPay88();

$ipay88->setMerchantKey(1234556);

$ipay88->setField('PaymentId', time());
$ipay88->setField('RefNo', "ORDS" . rand(10000,99999999));
$ipay88->setField('Amount', $amount);
$ipay88->setField('Currency', 'myr');
$ipay88->setField('ProdDesc', 'Testing');
$ipay88->setField('UserName', $name);
$ipay88->setField('UserEmail', $email);
$ipay88->setField('UserContact', '0123456789');
$ipay88->setField('Remark', 'Some remarks here..');
$ipay88->setField('Lang', 'utf-8');
$ipay88->setField('ResponseURL', 'http://lmsmalaysia.com/Finance/ipayresponse');

$ipay88->generateSignature();

$ipay88_fields = $ipay88->getFields();
//echo "<pre>"; print_r($ipay88_fields);die();

//echo $ipay88->requery(array('MerchantCode' => /*YOUR_MERCHANT_CODE*/, 'RefNo' => 'IPAY0000000001', 'Amount' => '1.00'));

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>iPay88 - Test - Request</title>
</head>

<body>
  <h1>iPay88 payment gateway</h1>

  <?php if (!empty($ipay88_fields)): ?>
    <form action="<?php echo Ipay88::$epayment_url; ?>" method="post">
      <table>
        <?php foreach ($ipay88_fields as $key => $val): ?>
          <tr>
            <td><label><?php echo $key; ?></label></td>
            <td><input type="text" name="<?php echo $key; ?>" value="<?php echo $val; ?>" /></td>
          </tr>
        <?php endforeach; ?>
        <tr>
          <td colspan="2"><input type="submit" value="Submit" name="Submit" /></td>
        </tr>
      </table>
    </form>
  <?php endif; ?>
</body>

</html>