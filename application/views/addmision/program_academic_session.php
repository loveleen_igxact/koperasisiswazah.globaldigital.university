
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Admissions / Academic Sessions / Course Academic Sessions</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Update Course Academic Session</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"><strong>Academic Session</strong></label>
                                                    <input type="text" class="form-control" value="MAC 2017" placeholder="" readonly="" name="">
                                                </div>
                                            </div>  
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"><strong>Program</strong></label>
                                                    <input type="text" class="form-control" value="SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)" placeholder="" readonly="" name="">
                                                </div>
                                            </div>  
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Academic Session<span style="color: red;">*</span></label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Registration Session<span style="color: red;">*</span></label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Subject Add Drop Session</label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Subject Withdraw Session</label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Attendance Entry Session<span style="color: red;">*</span></label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Coursework Mark Entry Session<span style="color: red;">*</span></label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Final Exam Mark Entry Session<span style="color: red;">*</span></label>
                                                    <div>
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                            <input type="text" class="form-control" name="end" placeholder="End Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        <div class=" col-md-12 text-center">
                                            <a href="#" class="btn btn-lg btn-primary">Save</a>
                                             <a href="#" class="btn btn-lg btn-info">Cancel</a>
                                        </div>
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
          