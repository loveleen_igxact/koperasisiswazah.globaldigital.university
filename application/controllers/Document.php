<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Document extends CI_Controller {



    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     *      http://example.com/index.php/welcome

     *  - or -

     *      http://example.com/index.php/welcome/index

     *  - or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */

    function __construct() {

        parent::__construct();

        $this->load->library(array('form_validation','session'));

        $this->load->helper(array('form','url'));

        $this->load->model('AdminModel');   // loads admin model

        $this->CI = & get_instance();

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

    }

    public function documents()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $where = array('user_id'=>$session_data['id']);

        if ($session_data['user_type'] == '3') {

            $type = "student";

        } else {

            $type = "teacher";

        }
        // echo "string22";
        // print_r($session_data['id']);

        $data['my_documents'] = $this->AdminModel->getMyDocuments($session_data['id'],$type);

        $data['shared_documents'] = $this->AdminModel->getSharedDocuments($session_data['id'],$type);
        $data['satff_name'] = $this->AdminModel->getData('staffs');


        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('document/all_documents',$data);

        $this->load->view('layout/footer');

    }

    public function all_documents()

    {


        $session_data = $this->session->userdata['loggedInData'];

        $where = array('user_id'=>$session_data['id']);

        if ($session_data['user_type'] == '3') {

            $type = "student";

        } else {

            $type = "teacher";

        }
        // echo "string222";
        // print_r($session_data['id']);


        $data['my_documents'] = $this->AdminModel->getMyDocuments($session_data['id'],$type);

        $data['shared_documents'] = $this->AdminModel->getSharedDocuments($session_data['id'],$type);

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('document/documents',$data);

        $this->load->view('layout/footer');

    }

    public function upload_documents_new()

    {

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        // $wheres = array('user_type'=>'4');

        $data['students_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);

        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['submit'])) {

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                if (!empty($_POST['students'])) {

                    $arr['student_id'] = implode(',', $_POST['students']);

                } else {

                    $arr['student_id'] = "0";

                }

                if ($session_data['user_type'] == '3') {

                    $type = "student";

                } else {

                    $type = "teacher";

                }

                $arr['creator_type'] = $type;

                $arr['user_id'] = $session_data['id'];

                $arr['created'] = date('Y-m-d');

                $insert_id = $this->AdminModel->saveData($arr,'user_documents');

                if (!empty($_POST['students'])) {

                    foreach ($_POST['students'] as $key => $value) {

                        $arr1['user_id'] = $value;

                        $arr1['document_id'] = $insert_id;

                        $save = $this->AdminModel->saveData($arr1,'shared_documents');

                    }

                }

            }

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('document/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('document/upload_documents',$data);

        $this->load->view('layout/footer');

    }

    public function upload_documents()

    {

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        // $wheres = array('user_type'=>'4');

        $data['students_data'] = $this->AdminModel->getData('users',$where);
        $data['student_dataSerial'] = $this->AdminModel->student_dataSerial();

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['submit'])) {

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                if (!empty($_POST['students'])) {

                    $arr['student_id'] = implode(',', $_POST['students']);

                } else {

                    $arr['student_id'] = "0";

                }

                if ($session_data['user_type'] == '3') {

                    $type = "student";

                } else {

                    $type = "teacher";

                }

                $arr['creator_type'] = $type;

                $arr['user_id'] = $session_data['id'];

                $arr['created'] = date('y-m-d H:i:s');

                $arr['title'] = $_POST['title'];

                $arr['description'] = $_POST['description'];

                $insert_id = $this->AdminModel->saveData($arr,'user_documents');

                if (!empty($_POST['students'])) {

                    foreach ($_POST['students'] as $key => $value) {

                        $arr1['user_id'] = $value;

                        $arr1['document_id'] = $insert_id;

                        $save = $this->AdminModel->saveData($arr1,'shared_documents');

                    }

                }

            }
           
            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('document/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('document/upload_doc',$data);

        $this->load->view('layout/footer');

    }

    public function edit_documents()

    {

        $id = $_GET['id'];

        $where = array('user_type'=>'3');

        $wheres = array('status'=>'1');

        $data['students_data'] = $this->AdminModel->getData('users',$where);

        $data['teachers_data'] = $this->AdminModel->getData('staffs',$wheres);

        $data['get_document_info'] = $this->AdminModel->get_data_by_id('user_documents','id',$id);

        $session_data = $this->session->userdata['loggedInData'];

        if (isset($_POST['update'])) {
            
            if (isset($_POST['fileToUpload'])) {
                
                foreach ($_POST['fileToUpload'] as $key => $value) {
               $name=$value;
                $arr['file'] = $name;

                $arr['student_id'] = implode(',', $_POST['students']);
                $arr['title']=$_POST['title'];
                $arr['description']=$_POST['description'];

                $arr['user_id'] = $session_data['id'];
                $where1 = array('id'=>$id);

                

            } }
            else{
                
            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                $arr['file'] = $name;

                $arr['student_id'] = implode(',', $_POST['students']);

                $arr['user_id'] = $session_data['id'];
                $arr['title']=$_POST['title'];
                $arr['description']=$_POST['description'];

                //$arr['created'] = date('Y-m-d');

                //$save = $this->AdminModel->saveData($arr,'user_documents');

                $where1 = array('id'=>$id);


            }  }
                $this->AdminModel->updateData($where1,$arr,'user_documents');

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Document Uploaded Successfully.</div>');

            redirect(base_url('document/documents'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('document/edit_document',$data);

        $this->load->view('layout/footer');

    }

    public function download()

    {

        $files = $_GET['file'];

        $url = "http://lmsmalaysia.com/uploads/documents/".$files;

        $ext = pathinfo($url, PATHINFO_EXTENSION);

        if ($ext == 'png' && $ext == 'jpg' && $ext == 'jpeg') {

            if (exif_imagetype($url) == IMAGETYPE_PNG) {

                $type = exif_imagetype($url);

            } else {

                $type = $ext;

            }

        } else {

            $type = $ext;

        }

        

        header("Content-type:application/".$type);

        header("Content-Disposition:attachment;filename=".$url);

        readfile($url);

    }

    public function send_email($email,$message,$subject)

    {

        $this->load->library('email');

        $config['mailtype'] = 'html';

        $this->email->initialize($config);

        // $this->email->from('no-reply@mylifelineapp.com');

        $this->email->from('chaseapp@webfreaksolution.com');

        $this->email->to($email);

        $this->email->subject($subject);

        $this->email->message($message);

        $send = $this->email->send();

        return true;

        // $to = $email;

        // $subject = $subject;

        // $txt = $message;

        // $headers = "From: webmaster@example.com" . "\r\n" .

        // "CC: somebodyelse@example.com";

        // mail($to,$subject,$txt,$headers);

    }


         public function delete_file()

    {

        $where = array("id" => $_GET['id']);

        $arr['file'] = "";

        $this->AdminModel->updateData($where,$arr,"user_documents");

        redirect(base_url('document/edit_documents?id='.$_GET['id']));

    }
 

    }