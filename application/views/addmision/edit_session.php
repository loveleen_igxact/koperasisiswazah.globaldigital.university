
 <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> New Academic Session</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">New Academic Session</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <form action="<?php echo base_url('addmision/edit_session/'.$id); ?>" method="post">
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Code<span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="code" required="">
                                                    <option value="">Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
														<option value="2019" <?php echo ($session_data[0]['code'] == '2019') ?'selected':''; ?>)>2019</option>
                                                        <option value="2018" <?php echo ($session_data[0]['code'] =='2018') ?'selected':''; ?>>2018</option>
                                                        <option value="2017" <?php echo ($session_data[0]['code'] == '2017') ?'selected':''; ?>>2017</option>
                                                        <option value="2016" <?php echo ($session_data[0]['code'] =='2016' ) ?'selected':''; ?>>2016</option>
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Month<strong><span style="color: red;"></span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="month" required="">
                                                    <?php 

                                                    for ($m=1; $m<=12; $m++) {

                                                         $selected1 = ''; 
                                                        if($m == $session_data[0]['month']) {
                                                                $selected1 = 'selected'; 

                                                        }
													$month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
													<option <?php echo $selected1;  ?> value="<?php echo $m; ?>"><?php echo $month ?> </option>
													<?php 	} ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name<span style="color: red;">*</span> </label>
                                            <input type="text" class="form-control " required="" placeholder="" name="name" <?php echo $session_data[0]['name']; ?>> 
                                         </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Program<strong><span style="color: red;"></span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="program">
                                            <?php foreach ($campus_data as $key => $value) {
                                                $selected = ''; 
                                                if(@$value['id'] == @$session_data[0]['program']) {
                                                        $selected = 'selected'; 

                                                }
                                             ?>
                                                 <option  <?php echo $selected;  ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php $datas =  explode('_', @$session_data[0]['academic_session']);

                                               
                                                 ?>
                                            <div class="form-group">
                                            <label>Academic Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range1">
                                                    <input type="text" required class="form-control" name="astart" placeholder="Start Date" value="<?php echo @$datas[0];?>">
                                                    <input type="text" class="form-control" name="aend" placeholder="End Date" value="<?php echo @$datas[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                            <div class="form-group">
                                                <?php $data1 =  explode('_', $session_data[0]['registration_session']);

                                               
                                                 ?>
                                            <label>Registration Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" required class="form-control" name="regstart" placeholder="Start Date" value="<?php echo @$data1[0];?>">
                                                    <input type="text" class="form-control" name="regend" placeholder="End Date" value="<?php echo @$data1[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                         <?php $data2 =  explode('_', $session_data[0]['subject_add_drop_session']);

                                               
                                                 ?>
                                            <div class="form-group">
                                            <label>Subject Add Drop Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range1">
                                                    <input type="text" class="form-control" name="sadstart" placeholder="Start Date" value="<?php echo @$data2[0];?>">
                                                    <input type="text" class="form-control" name="sadend" placeholder="End Date" value="<?php echo @$data2[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                         <?php $data3 =  explode('_', $session_data[0]['subject_withdraw']);

                                               
                                                 ?>
                                            <div class="form-group">
                                            <label>Subject withdraw Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range2">
                                                    <input type="text" class="form-control" name="swstart" placeholder="Start Date" value="<?php echo @$data3[0];?>">
                                                    <input type="text" class="form-control" name="swend" placeholder="End Date" value="<?php echo @$data3[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php $data4 =  explode('_', $session_data[0]['acknowlegment_session']);

                                               
                                                 ?>
                                            <div class="form-group">
                                            <label>Add Drop Acknowlegment Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range3">
                                                    <input type="text" class="form-control" name="adkstart" placeholder="Start Date" value="<?php echo @$data4[0];?>">
                                                    <input type="text" class="form-control" name="adkend" placeholder="End Date" value="<?php echo @$data4[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php $data5 =  explode('_', $session_data[0]['attendance_entry']);
                                                 ?>
                                            <div class="form-group">
                                            <label>Attendance Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range4">
                                                    <input type="text" class="form-control" name="adestart" required placeholder="Start Date" value="<?php echo @$data5[0];?>">
                                                    <input type="text" class="form-control" name="adeend" required placeholder="End Date" value="<?php echo @$data5[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                         <?php $data6 =  explode('_', $session_data[0]['coursework_mark_entry']);
                                                 ?>
                                            <div class="form-group">
                                            <label>Coursework Mark Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range5">
                                                    <input type="text" class="form-control" required name="cmestart" placeholder="Start Date" value="<?php echo @$data6[0];?>">
                                                    <input type="text" class="form-control" name="cmeend" placeholder="End Date" value="<?php echo @$data6[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php $data7 =  explode('_', $session_data[0]['final_exam']);
                                                 ?>
                                            <div class="form-group">
                                            <label>Final Exam Mark Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range6" id="date-range6">
                                                    <input type="text" required class="form-control" name="femstart" placeholder="Start Date" value="<?php echo @$data7[0];?>">
                                                    <input type="text" required class="form-control" name="femend" placeholder="End Date" value="<?php echo @$data7[1];?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <label class="control-label"><strong>Description</strong></label>
                                             
                                           <div><textarea required="" name="description" class="form-control" placeholder="" rows="5"><?php echo $session_data[0]['description']; ?></textarea></div>
                                        </div>
                                    </div>
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <input type="submit" name="submit" class="btn btn-lg btn-primary" value="Save">
                                         <a href="#" class="btn btn-lg btn-primary">Cancel</a>
                                    </div>
                                </div>
                                
                            </div>
                    </form>         
                        </div>
                    </div>
                </div>

           
                <!-- container-fluid -->
            </div>
            <!-- content -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			 $('#date-range1 ,#date-range2,#date-range3,#date-range4,#date-range5,#date-range6').datepicker({
              });
		});
	</script>
	</script>