<?php //print_r($students_data); die; 
$session_data = $this->session->userdata['loggedInData']; ?>

    <script>
function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("text");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
     text.style.display = "none";
  }
}
</script>

<style type="text/css">
    .dataTables_wrapper .dataTables_length {
    float: left;
    float: right !important;
    position: absolute;
    right: 14px;
    top: -10px;
}
.serch_icon {
    position: absolute;
    right: 16px !important;
    bottom: 17px !important;
}
.table td {
    width: 16.6%;
}
table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 17px !important;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Student</h4> -->
                    </div>
                </div>
            </div>
    <div class="col-12">
        <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
    <div class="card m-b-30">
        <div class="card-body">
            <div class="hearding">
                <div class="left_side col-md-12">
                    <h4 class="mt-0 header-title ">Student List </h4>
                </div>
                <div class="clearfix"></div>
                <form method="post" action="">
                <div class="container-fluid">
                <div class=" row right_side">
                    <div class="col-md-12">
                        <div class="form-group">
                                <label>Search</label>
                                <input type="text" class="form-control serch" placeholder="Search by Student Name or ID" name="search_text"> 
                                <span class="serch_icon"><button class="btn color_another" type="submit" name="search">Search</button></span>
                         </div>
                    </div>
                    <div class="col-12 button">
                        <button class="btn color_another button_more " type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <span>More Filter</span>
                        </button>
                    </div>
                    <div class="collapse col-md-12 pt-3" id="collapseExample">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Class</label>
                                    <select class="form-control" tabindex="-1" aria-hidden="true" name="class" required="">
                                        <option selected disabled> Please Select</option>
                                        <option value="1">first </option>
                                        <option value="2">second </option>
                                        <option value="3">third </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <select class="form-control" tabindex="-1" aria-hidden="true" name="subject" required="">
                                        <option selected disabled> Please Select</option>
                                        <option value="1">java </option>
                                        <option value="2">php </option>
                                        <option value="3">android </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn color_another" type="submit" name="search">Search</button>
                            </div> 
                        </div>
                    </div>
                   <!--  <div class="col-md-12" id="text" style="display:none">
                        <div class="col-md-6 p-0">
                            <div class="form-group">
                                <label>Class</label>
                                    <select class="form-control" tabindex="-1" aria-hidden="true" name="class" required="">
                                        <option selected disabled> Please Select</option>
                                        <option value="1">first </option>
                                        <option value="2">second </option>
                                        <option value="3">third </option>
                                    </select>
                            </div>
                        </div>    
                   </div> -->
                   <!-- <div class="col-md-12">
                            <div class="btn_search">
                                <label>&nbsp</label><br>
                            <button class="btn color_another" type="submit" name="search">Search</button>
                            </div>
                        </div> -->
                </div>
            </div>
                </form>
            </div>
        
<?php if ($session_data['user_type'] == '1') { ?>
<!-- <div class="add_more">
<a href="<?php //echo base_url('teacher/create'); ?>">
    <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
</a>
</div>  -->
<?php } ?>
<div class="clearfix"></div>
<hr>
<table id="dtBasicExample" class="table table-hover  pt-3" cellspacing="0" width="100%">
    <thead class="black white-text">
        <tr>
            <!-- <th class="no_sort">No</th> -->
            <th class="th-sm">Name
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
           <!--  <th class="th-sm">Class
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
            <th class="th-sm">Subject
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th> -->
            <th class="th-sm">Phone
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
            <th class="th-sm">Email
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
            <th class="th-sm">ID
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
            <?php if($session_data['user_type'] == '4') { ?>
            <th class="th-sm">Send Message
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
            <?php } ?>
            <th class="th-sm">Action
                <i class="fa fa-sort float-right" aria-hidden="true"></i>
            </th>
        </tr>
    </thead>
<tbody>
    <?php if (!empty($students_data)) {
        // echo "<pre>";
        // print_r($students_data);
        $i = 1;
        foreach ($students_data as $key => $value) { 
    ?>
    <tr id="current_row<?php echo $value['id']; ?>">
        <!-- <th scope="row"><?php echo $i; ?></th> -->
        <td><a style="color: blue;" href="<?php echo base_url(); ?>student/profile/<?php echo $value['user_id'] ?>"><?php echo $value['name']; ?></a></td>
        
        <!--  <td class="text-center"><?php if($value['class_id'] == 1){ echo "first";}elseif($value['class_id'] == 2){echo "second";}elseif($value['class_id'] == 3){echo "third";} ?>
         </td>
         
         <td class="text-center"><?php if($value['subject_id'] == 1){ echo "java";}elseif($value['subject_id'] == 2){echo "php";}elseif($value['subject_id'] == 3){echo "android";} ?>
         </td>
         -->
        
        <td><?php echo $value['phone']; ?></td>
        <td><a style="color: blue;" target="_blank" href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a></td>
      
        <td><?php echo $value['id_no']; ?></td>
        <?php if($session_data['user_type'] == '4') { ?>
        <td class="text-center"><a class="first_icon" href="<?php echo base_url('Inbox/compose_email?id='.$value['user_id']) ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></td>
        <?php } ?>
        <td>
            <?php  if($session_data['user_type'] == '1'){ ?>
 <a href="<?php echo base_url('student/edit_std/?id='.$value['user_id']); ?>">
                <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
            </a>
      <?php } 
      else{
        echo "No permision";
      }?>
           
            <!-- <a href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'users')">
                <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-delete-forever" style="font-size:25px; color: #eb2129;"></i></button>
            </a> -->
        </td>
    </tr>
    <?php $i++; } }?>
</tbody>
</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
        </div>
    </div>
   
</body>
</html>