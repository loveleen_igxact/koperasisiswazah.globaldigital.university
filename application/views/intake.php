    
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Intakes</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Intakes</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    
                                    <form action="<?php echo base_url('addmision/intake'); ?>" method="POST">
                                     <div class="search ">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" name="stu_name" class="form-control serch" required="" placeholder="Search by Student Name"> 
                                                        <span class="serch_icon">
															
													<input type="submit" value="Search" value="search" class="btn btn-dark" type="submit"></span>
                                                     </div>
                                                </div>
                                       </form>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            intake Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
										 <form action="<?php echo base_url('addmision/intake'); ?>" method="POST">
                                            <div class="row">
                                         <div class="col-md-4">
                                                    <label>Status</label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full <?php echo (@$Status =='1')?'focus active':''; ?>">
                                                        <input type="radio" value="1" name="Status" id="option2" > Active </label>
                                                        <label class="btn btn-full <?php echo (@$Status =='0')?'focus active':''; ?>">
                                                        <input type="radio"  value="0" name="Status" id="option3"> No</label>
                                                    </div>
                                                </div>

                                    <div class="col-md-12 pt-2">
                                        <input type="submit" class="btn btn-dark waves-effect waves-light" name="s_search" value="Search">
                                    </div>
                                    </div>
									</form>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Intakes</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="<?php echo base_url();?>addmision/addintake">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New Intake</button>
                                            </a> 
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                     <th class="th-sm">Student ID Running No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Registration Days
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php if(!empty($intake_data)) { 
											foreach($intake_data as $key => $values) { 
											?>
                                                <tr>
                                                    <th scope="row"><?php echo $values['id']; ?></th>
                                                    <td><?php echo $values['month']; ?></td>
                                                    <td><?php echo $values['studentntnoprefix']; ?></td>
                                                    <td><<?php echo $values['offer_reply_last_date']; ?>- <?php echo $values['online_application_session']; ?><br><?php echo $values['program']; ?><br></td>
                                                    <td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full  <?php echo ($values['status'] =='1')?'focus active':''; ?>">
                                                        <input type="radio" name="options" id="option2"> Active </label>
                                                        <label class="btn btn-full <?php echo ($values['status'] =='0')?'focus active':''; ?>">
                                                        <input type="radio" name="options" id="option3"> Inactive</label>
                                                    </div></td>
                                                    <td><a href=""><button data-toggle="tooltip" data-placement="top" title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;" ></i></button></a>
                                                   <a href="javascript:void(0)"   onclick="deleteRow(<?php echo $values['id']; ?>,  'add_intake')"> <span><i  class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span> </a>
                                                   <a href="registration_day.html"> <button data-toggle="tooltip" data-placement="top" title="Registration day"><i class="mdi mdi-tooltip-edit" style="font-size:25px; color: #4856a4;"></i></button></a>
                                                    </td>
                                                </tr>
                                            <?php  }  } ?>
                                          
                                 
                                          
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->

 <script type="text/javascript">
        
    function deleteRow(id, table) {
    var url  = '<?php echo  base_url();?>';

     var x=confirm("Are you sure to delete record?")
      if (x) {
        $.ajax({
             data: { 'id' : id, 'table':table},
             type: "post",
             url: url + "Application/deleteRow",
             success: function(data){
                  $('#current_row'+id).remove();
             }
        });
        return true;
      } else {
        return false;
      }
       

    }

    </script>>
