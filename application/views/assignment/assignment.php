<style type="text/css">
    .serch_icon {
    position: absolute;
    bottom: 2px;
    right: 18px;
}
.dataTables_wrapper .dataTables_length {
    float: left;
    float: right !important;
    position: absolute;
    right: 0px;
    top: -40px;
}
</style>
<!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Application</h4>-->
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active">New Application</li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- end row -->
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="heading">
                                        <h4 class="mt-0 header-title left_side">ASSIGENMENT</h4>
                                    </div>
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <hr>
                                    <div class="table-responsive">
                                        <form action="" method="post"> 
                                            <div class=" col-md-4 form-group p-0">
                                                <input type="text" class="form-control s" value="" name="search" placeholder="Search here"> 
                                                <span class="serch_icon"><button class="btn color_another" type="submit">Search</button></span>
                                             </div>
                                        </form>
                                        <table id="dtBasicExample" class="table  table-hover " cellspacing="0" width="100%">
                                                <thead class="black white-text">
                                                    <tr>
                                                        <th class="th-sm">Assigement Name
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm">Start
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm">Expire
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm">Description
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm">Status
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm">Attachemnts
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                        <th class="th-sm no_sort">Action
                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                        <td >
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #4fb726;"><strong>Reviewed</strong>  </span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #ec5f1d;"><strong> Expired</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #ec5f1d;"><strong> Expired</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                             2 files
                                                            </a>
                                                        </td>
                                                        <td><a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #4fb726;"><strong>Reviewed</strong>  </span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td><a href="#">
                                                            <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>SIAW SING ONG SIAW SING </td>
                                                        <td>09/05/2017</td>
                                                        <td>11/05/2018</td>
                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                        <td><span class="info_info" style="color: #ec5f1d;"><strong> Expired</strong></span></td>
                                                        <td>
                                                            <a style="color: blue;" href="#">
                                                                2 files
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#">
                                                                <img src="http://lmsmalaysia.com/public/images/eye_icon.png">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
