<style type="text/css">

    .dataTables_wrapper .dataTables_length {

    float: left;

    float: right !important;

    position: absolute;

    right: 0px;

    top: -39px;

}

table.dataTable tbody th, table.dataTable tbody td {

    padding: 8px 18px !important;

}

</style>

<div id="wrapper">

       <div class="modal fade" id="collection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

          <div class="modal-header tran-heading text-center ">

            <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">Add New Collection</h5>

            </div>

           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> -->

          </div>

          <hr>

          <div class="modal-body">

           <div class="container">

            <div class="row">

                <hr>

                <div class="clearfix"></div>

                <div class="col-md-12">

                <form method="post">

                    <div class="form-group">

                        <label class="control-label"><strong> Student</strong></label>

                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="student_id">

                            <option>Please Select</option>

                            <?php if (!empty($student_data)) {

                                    foreach ($student_data as $key => $data) {

                                ?>

                                <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>

                            <?php } } ?>

                        </select>

                    </div>

                </div>

            </div>

           </div>

          </div>

          <div class="modal-footer">

            <button type="submit" class="btn btn-primary btn-lg" name="save">Save</button>

            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>

          </div>

          </form>

        </div>

      </div>

    </div>

        <div class="content-page">

            <!-- Start content -->

            <div class="content">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">

                            <?php echo $this->session->flashdata('msg'); ?>

                            <div class="page-title-box">

                                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Colletions</h4> -->

                                <!--<ol class="breadcrumb">-->

                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->

                                <!--    <li class="breadcrumb-item active">Colletions</li>-->

                                <!--</ol>-->

                            </div>

                        </div>

                    </div>

                    </div>

                    <!-- end row -->

                        <div class="col-12" style="display: none;">

                            <div class="card m-b-30">

                                <div class="card-body">

                                    <div id="prospect"></div>

                                    <div style="clear:both"></div>

                                    <!-- <form method="post"> -->

                                    <!-- <div class="search ">

                                            <div class="form-group">

                                                <label>Search</label>

                                                <input type="text" class="form-control serch" required="" placeholder="Search by name, IC/Pssport or student No" name="search" id="search_data"> 

                                                <span class="serch_icon">

                                                <button class="btn btn-success" type="btn" name="submit" id="search">Search</button></span>

                                            </div>

                                        </div> -->

                                    <!-- </form> -->

                                </div>

                            </div>

                        </div>

                            <div class="card m-b-30">

                                <div class="card-body">

                                    <div class="hearding">

                                        <h4 class="mt-0 header-title left_side">Collections</h4>

                                       

                                        <div class="text-right">

                                      <a class="btn color_another btn-addon" href="#" id="send"><i class="fas fa-share-square"></i>Send Invoice Reminder</a>

                                    </div>

                                    </div>

                                    <div class="clearfix"></div>

                                    <!-- <hr> -->

                                    <div class="search col-md-4 p-0 pt-3 ">

                                            <div class="form-group">

                                                <label>Search</label>

                                                <input type="text" class="form-control serch" required="" placeholder="Search by name, IC/Passport or student No" name="search" id="search_data"> 

                                                <span class="serch_icon">

                                                <button class="btn color_another" type="btn" name="submit" id="search">Search</button></span>

                                            </div>

                                        </div>

                                    <!-- <div class="text-right">

                                      <a class="btn color_another btn-addon" href="<?php echo base_url('Finance/invoice_remind') ?>" id="send"><i class="fas fa-share-square"></i>Send Invoice Reminder</a>

                                    </div> -->

                                     <div class="text-left">

                                            <label><strong>Select All</strong></label>

                                            <input type="checkbox" class="abc" id="allCheckbox"> 

                                        </div>

                                    

                                    <table id="dtBasicExample" class="table table-hover pt-2" cellspacing="0" width="100%" >

                                        <thead class="black white-text">

                                            <tr>

                                            <!-- <th class="no_sort">No</th>  -->

                                                <th class="th-sm">Student Name

                                                    <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                </th>

                                                <th class="th-sm">Student No

                                                    <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                </th>

                                                <th class="th-sm">IC/Passport

                                                    <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                </th>

                                                <th class="th-sm">Outstanding

                                                    <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                </th>

                                                <th class="th-sm no_sort">Action

                                                    <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                                </th>

                                                 <th class="th-sm all_select_checkbox" width="170px">Invoice Reminder

                                                   

                                                </th>

                                            

                                            </tr>

                                        </thead>

                                        <tbody><!-- <?php echo "<pre>";
                                         print_r($collection_data['amount']);
                                          $balance=$collection_data['amount']-$collection_data['paid'];
                                          echo $balance;
                                          ?>
 -->
                                            <?php if (!empty($collection_data)) {

                                                $i = 1;

                                                foreach ($collection_data as $key => $data) {
                                                    $balance=$data['amount']-$data['paid'];
                                                    // echo $balance;
                                                    

                                            ?>

                                            <tr>

                                                <!-- <th scope="row"><?php echo $i; ?></th> -->

                                                <td><?php $student_info = $this->CI->get_data_by_id('students','id',$data['student_id']); echo $student_info['name'] ?></td>

                                                <td><?php echo $data['student_no']; ?></td>

                                                <td><?php echo $data['id_no']; ?></td>

                                                <td ><?php echo $data['balance']; ?></td>

                                                <td class="text-center" ><a class="first_icon" data-toggle="tooltip" data-placement="top" title="Collect" href="<?php echo  base_url('finance/collect_bills?origin='.$data['student_id']); ?>"><img src="http://lmsmalaysia.com/public/images/money_icon.png"></i></a>

                                                </td>

                                                <td class="text-center" ><input type="checkbox" name="invoice_reminder[]" data-id="<?php echo $data['email']; ?>" class="bcd" id='invoice_checkbox_<?php echo $i; ?>'><input type="hidden" name="amt" data-id="<?php echo $data['amount']; ?>" class="amt" value="<?php echo $data['id']; ?>"></td>

                                            </tr>

                                            <?php $i++; } } else { ?>

                                            <tr><td>No Record found!</td></tr>

                                            <?php } ?>

                                        </tbody>

                                    </table>

                                </div>

                            </div>      

                        </div>

                    </div>

                </div>

                <!-- container-fluid -->

            </div>

<style>

    table.dataTable thead .no_sort {

        background-image: none !important;

        pointer-events: none;

    }

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

    var href1 = "<?php echo base_url('Finance/searchCollection') ?>";

    $("#search").click(function(){

        var search_data = $("#search_data").val();

        $.ajax({

            type: 'POST',

            url: href1,

            data:{search_data:search_data},

            success: function(html) {

                //alert(data);

                if(html != 'No Record Found') {

                    $('#dtBasicExample').html(html);

                    var table = $('#dtBasicExample').DataTable();

                    

                        table.reload();

                } else  {



                    $('#dtBasicExample').html(html);

                }

                //$("#dtBasicExample").html(data);

            },

        });

    });

    $(document).ready(function(){

        $("#allCheckbox").on("click", function () {

            //$("input[name='invoice_reminder[]']").each( function () {

                $("input[name='invoice_reminder[]']").prop('checked', $(this).prop("checked")); 

                //alert($(this).val());

                //$('.bcd').prop('checked', $(this).prop("checked")); 

            //});

           //  var rows = $('#dtBasicExample').find('tbody tr');

           //  checked = $(this).prop('checked'); 

           //  $.each(rows, function() {

           //    var checkbox = $($(this).find('td').eq(0)).find('input').prop('checked', checked);

           // });

            //$('.bcd').prop('checked', $(this).prop("checked"));  

        });



        $('.bcd').change(function(){ //".checkbox" change 

            if($('.bcd:checked').length == $('.bcd').length){

                $('.abc').prop('checked',true);

            } else {

                $('.abc').prop('checked',false);

            }

        });

        $(document).on('click','#send',function(e){
           

          e.preventDefault();

            if($('#invoice_checkbox').prop("checked") == false){

                alert('Select a student');

            } else {

                var vid = [];  
                $(".bcd:checked").each(function() {  
                    vid.push($(this).attr('data-id'));
                });

                var amt = [];  
                $(".amt").each(function() {  
                    amt.push($(this).attr('data-id'));
                });
                var href = "<?php echo base_url('Finance/invoice_remind') ?>";
                $.ajax({
                    type: 'POST',
                    url: href,
                    data:{vid:vid,amt:amt},
                    success: function(data) {
                       
                    alert('Mail Sent to selected students.');
                    location.reload();
                    },
                });
            }
        });   

    });

</script>