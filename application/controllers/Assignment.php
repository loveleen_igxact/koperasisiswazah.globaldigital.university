<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');	// loads admin model
        $this->CI = & get_instance();
        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login')); 
        }
    }

    public function index()
    {
        $session_data = $this->session->userdata['loggedInData'];
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('assignment/assignment');
        $this->load->view('layout/footer');
    }
    public function overview()
    {
        $session_data = $this->session->userdata['loggedInData'];
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('assignment/overview');
        $this->load->view('layout/footer');
    }
    public function view()
    {
        $session_data = $this->session->userdata['loggedInData'];
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('assignment/view');
        $this->load->view('layout/footer');
    }
    public function assignment2()
    {
        $session_data = $this->session->userdata['loggedInData'];
    	$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('assignment/assignment2');
		$this->load->view('layout/footer');
    }
    public function send_email($email,$message,$subject)
    {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        // $this->email->from('no-reply@mylifelineapp.com');
        $this->email->from('chaseapp@webfreaksolution.com');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
        return true;
    }
    public function create_assignment()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('assignment/create_assignment');
        $this->load->view('layout/footer');
    }
}