<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Programs extends CI_Controller {



    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     *      http://example.com/index.php/welcome

     *  - or -

     *      http://example.com/index.php/welcome/index

     *  - or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */



    function __construct() {

        parent::__construct();

        $this->load->library(array('form_validation','session'));

        $this->load->helper(array('form','url'));

        $this->load->model('AdminModel');   // loads admin model

        $this->CI = & get_instance();

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

    }



    public function index()

    {

        $session_data = $this->session->userdata['loggedInData'];

        // if ($session_data['user_type'] == '1') {

        //     $data['announcement_data'] = $this->AdminModel->getData('announcements');

        // } else if ($session_data['user_type'] == '3') {

        //     $where = array('tab'=>$session_data['id']);

        ?>

        

            <?php

            $data['program_data'] = $this->AdminModel->getData('programs');

            // $data['program_data'] = $this->AdminModel->get_programs();

        // } else if ($session_data['user_type'] == '4') {

        //  $data['announcement_data'] = $this->AdminModel->get_announcements($session_data['user_type'],$session_data['id']);

        // } else {

        //     $data['announcement_data'] = array();

        // }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('programs/programs',$data);

        $this->load->view('layout/footer');

    }

    public function create_program()

    {

        $session_data = $this->session->userdata['loggedInData'];

        // if ($session_data['user_type'] == '1') {

        //     $data['announcement_data'] = $this->AdminModel->getData('announcements');

        // } else if ($session_data['user_type'] == '3') {

        //     $where = array('tab'=>$session_data['id']);

                 

        $data['campus_program_data'] = $this->AdminModel->get_campus();

        // } else if ($session_data['user_type'] == '4') {

        //  $data['announcement_data'] = $this->AdminModel->get_announcements($session_data['user_type'],$session_data['id']);

        // } else {

        //     $data['announcement_data'] = array();

        // }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('programs/create_program',$data);

        $this->load->view('layout/footer');



        if (isset($_POST['submit'])){

            $arr = array(

                    'name'=>$_POST['name'],
                    'campus'=>$_POST['p1_campus'],

                );

               //$arr['program_name'] = $_POST['name'];

            $insert_id = $this->AdminModel->saveData($arr,'campus_program');

            $insert_id = $this->AdminModel->saveData($arr,'programs');

            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">New Program has been Added Successfully</div>');

            redirect(base_url('programs'));

        }

    }

    public function edit_program()

    {

        $id = $_GET['id'];

        $data['program_data'] = $this->AdminModel->getData('programs');

        $data['campus_program_data'] = $this->AdminModel->get_campus();

        if (isset($_POST['update'])) {

            $arr = array(

                'name'=>$_POST['name'],

                           

            );

            $where = array('id'=>$id);

            //$this->AdminModel->updateData($where,$arr,'campus_program');

            $this->AdminModel->updateData($where,$arr,'programs');



            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Program has been changed - </div>');

                redirect(base_url('programs'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('programs/edit_program',$data);

        $this->load->view('layout/footer');

    }

    public function delete_program()

    {

        $where = array("id" => $_GET['id']);

        $type = $_GET['type'];

        $arr[$type] = "";

        $this->AdminModel->updateData($where,$arr,"programs");

        redirect(base_url('programs/edit_program'));

    }





}