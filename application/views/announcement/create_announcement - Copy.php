<?php //echo "<pre>"; print_r($departments); die; ?><div class="content-page">
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Announcement</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                        <li class="breadcrumb-item active"> Announcement</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
<!---card 2 table-->
	<div class="card m-b-30">
		<div class="card-body">
            <div class="hearding">
                <h4 class="mt-0 header-title left_side">Announcements</h4>
            </div>
            <div class="clearfix"></div>
            <hr>
            <form method="post" enctype='multipart/form-data'>
            <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Subject<span class="sa" style="color: red;">*</span></label>
                    <input type="text" class="form-control" required="" placeholder="" name="subject"> 
                </div>
            </div>
            <div class="col-md-12">
                <textarea class="form-control ckeditor" name="message" placeholder="Enter News Letter" id="message" style="width:100%; height:100px!important; resize:none;" required /></textarea>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label><strong>Publish Date<span class="sa" style="color: red;">*</span></strong></label>
                    <div>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" class="form-control" placeholder="From" name="from_date" required="">
                            <input type="text" class="form-control" placeholder="To" name="to_date" required="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <mdb-select [options]="optionsSelect" [multiple]="true" placeholder="Choose your option"></mdb-select>
                    <label>Example label</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="cantrol-label"><strong> Files</strong></label>
                    <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file1">
                </div>
                <div class="form-group">
                    <!-- <label class="cantrol-label"><strong> Files</strong></label> -->
                    <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file2">
                </div>
                <div class="form-group">
                    <!-- <label class="cantrol-label"><strong> Files</strong></label> -->
                    <input type="file" class="filestyle" data-buttonname="btn-secondary" id="filestyle-2" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" name="file3">
                </div>
                <strong><span style="color: red;">Note*</span>  = The maximum files is 3. Please delete one file, to add another file</strong>
            </div>
            <br>
            <div class="col-md-12 pt-3">
                <label class="cantrol-label"><strong> Publish To</strong> </label><br>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" class="custom-control-input" id="defaultInline1" name="tab" mdbinputdirective="" onclick="staff_none();" value="staff_none">
                  <label class="custom-control-label" for="defaultInline1">None</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" class="custom-control-input" id="defaultInline2" name="tab" mdbinputdirective="" onclick="staff();" value="staff">
                  <label class="custom-control-label " for="defaultInline2"> Staff</label>
                </div>
                
                <div id="staff_filter" class=" pt-3" style="display: none;">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" checked="checked" id="defaultInline4" name="tab1" mdbinputdirective="" onclick="staff_all();" value="staff_all">
                        <label class="custom-control-label" for="defaultInline4">All</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" class="custom-control-input" id="defaultInline5" name="tab1" mdbinputdirective="" onclick="staff_selected_filter();" value="staff_filter">
                      <label class="custom-control-label" for="defaultInline5">Filter</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12"  id="dept" style="display: none; margin-left: 30%; ">
                <label class="cantrol-label"><strong>Department</strong></label>
                <div id="dualSelectExample" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                    <?php if (!empty($departments)) {
                        foreach ($departments as $key => $dept) {
                    ?>
                        <input type="checkbox" value="<?php echo $dept['id']; ?>" name="dept[]">
                        <label class=""  for="defaultInline4"><?php echo $dept['name']; ?></label><br>
                    <?php } } ?>
                </div>
            </div> 
            <div class="col-md-12 pt-3">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="defaultInline6" name="tabs" mdbinputdirective="" onclick="student_none();" value="student_none">
                    <label class="custom-control-label" for="defaultInline6">None</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" class="custom-control-input" id="defaultInline7" name="tabs" mdbinputdirective="" onclick="student();" value="student">
                  <label class="custom-control-label " for="defaultInline7"> Student</label>
                </div>
                <div id="student_filter" class="hide pt-3" style="display: none;" >
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" checked="checked" id="defaultInline8" name="stu_tab" mdbinputdirective="" value="student_all" onclick="student_all();">
                        <label class="custom-control-label" for="defaultInline8">All</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" class="custom-control-input" id="defaultInline9" name="stu_tab" mdbinputdirective="" value="student_filter" onclick="student_selected_filter();">
                      <label class="custom-control-label" for="defaultInline9">Filter</label>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="hide row "  id="all_functions" style="display: none">
                <div class="col-md-12 " style="margin-left: 30%;">
                    <label class="cantrol-label"><strong>Faculty</strong></label>
                    <div id="dualSelectExample_1" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                        <?php if (!empty($faculty)) {
                        foreach ($faculty as $key => $val) {
                        ?>
                        <input type="checkbox" value="<?php echo $val['id']; ?>" name="faculty[]">
                        <label class=""  for="defaultInline4"><?php echo $val['name']; ?></label><br>
                        <?php } } ?>
                    </div>
                </div>
                <div class="col-md-12" style="margin-left: 30%;">
                    <label class="cantrol-label pt-3"><strong>Program</strong></label>
                    <div id="dualSelectExample_2" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                        <?php if (!empty($campus_program)) {
                        foreach ($campus_program as $key => $program) {
                        ?>
                        <input type="checkbox" value="<?php echo $program['id']; ?>" name="programs[]">
                        <label class=""  for="defaultInline4"><?php echo $program['name']; ?></label><br>
                        <?php } } ?>
                    </div>
                </div>
                <div class="col-md-12" style="margin-left: 30%;">
                         <label class="cantrol-label pt-3"><strong>Intake</strong></label>
                    <div id="dualSelectExample_3" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                        <?php if (!empty($intakes)) {
                        foreach ($intakes as $key => $intake) {
                        ?>
                        <input type="checkbox" value="<?php echo $intake['id']; ?>" name="intakes[]">
                        <label class=""  for="defaultInline4"><?php echo $intake['intake']."/".$intake['month']; ?></label><br>
                        <?php } } ?>
                    </div>
                </div>
                <div class="col-md-12" style="margin-left: 30%;">
                         <label class="cantrol-label pt-3"><strong>Academic Session</strong></label>
                    <div id="dualSelectExample_4" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                        <?php if (!empty($academic_session)) {
                        foreach ($academic_session as $key => $session) {
                        ?>
                        <input type="checkbox" value="<?php echo $session['id']; ?>" name="sessions[]">
                        <label class=""  for="defaultInline4"><?php echo $session['name']; ?></label><br>
                        <?php } } ?>
                    </div>
                </div>
                <div class="col-md-12" style="margin-left: 30%;">
                         <label class="cantrol-label pt-3"><strong>Class</strong></label>
                    <div id="dualSelectExample_5" style="width:500px; height:300px; background-color:#F0F0F0; padding:10px;">
                        <?php if (!empty($classes)) {
                        foreach ($classes as $key => $class) {
                        ?>
                        <input type="checkbox" value="<?php echo $class['id']; ?>" name="classes[]">
                        <label class=""  for="defaultInline4"><?php echo $class['class']; ?></label><br>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-12 text-center">
            <button class="btn-primary btn" type="submit" name="submit">Save</button>
            <button class="btn-dark btn" type="button">Cancel</button>
        </div>
    </div>

                        </div>
                    </form>
						</div>
					</div>
            </div>
        </div>
    </div>
    <!-- container-fluid -->
</div>
</div>

       <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!-- <script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script> -->
       <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
       <script type="text/javascript">
        $(document).ready(function(){ lSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = dsl.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = dsl.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                dsl.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                dsl.resetColor(clrName);
            });

            var ds2 = $('#dualSelectExample_1').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = ds2.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = ds2.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                ds2.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                ds2.resetColor(clrName);
            });

            //third
            var ds3 = $('#dualSelectExample_2').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = ds3.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = ds3.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                ds3.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                ds3.resetColor(clrName);
            });
            //end
            //four
               var ds4 = $('#dualSelectExample_3').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = ds4.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = ds4.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                ds4.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                ds4.resetColor(clrName);
            });
            //end
            //fifth
             var ds5 = $('#dualSelectExample_4').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = ds5.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = ds5.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                ds5.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                ds5.resetColor(clrName);
            });
            //end
            //sixth
              var ds6 = $('#dualSelectExample_5').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = ds6.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = ds6.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                ds5.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                ds6.resetColor(clrName);
            });
            //end
        });


        //second
           $(document).ready(function(){
            var dsl = $('#dualSelectExampl').DualSelectList({
                'candidateItems' : ['Item 01', 'Item 02', 'Item 03', 'Item 04', 'Item 05', 'Item 06', 'Item 07'],
                'selectionItems' : ['Item 08', 'Item 09', 'Item 03'],
                'colors' : {
                    'itemText' : 'white',
                    'itemBackground' : 'rgb(0, 51, 204)',
                    'itemHoverBackground' : '#0066ff'
                }
            });

            $('#getSel').click(function(){
                var res = dsl.getSelection();
                var str = '';
                for (var n=0; n<res.length; ++n) str += res[n] + '\n';
                $('#selResult').val(str);
            });

            $('#addSel').click(function(){
                var items = $('#addIterms').val().split('\n');
                var res = dsl.setCandidate(items);
                $('#addIterms').val('');
            });

            $('#setColor').click(function(){
                var clrName = $('#colorSelector').val();
                var clrValue = $('#colorValue').val();
                dsl.setColor(clrName, clrValue);
            });

            $('#resetColor').click(function(){
                var clrName = $('#colorSelector').val();
                dsl.resetColor(clrName);
            });
        });

       function staff()
       {
        $("#staff_filter").show();
       }
       function staff_none()
       {
        $("#staff_filter").hide();
       }
        function staff_all()
        {
            $("#dept").hide();
        }
       function staff_selected_filter()
       {
        $("#dept").show();
       }
       function student_selected_filter()
       {
        $("#all_functions").show();
       }
       function student()
       {
        $("#student_filter").show();
       }
       function student_all()
       {
        $("#all_functions").hide();
       }
       function student_none()
       {
        $("#student_filter").hide();
        $("#all_functions").hide();
       }
    </script>

</body>
</html>