<?php //print_r($prospects_data); die; ?><!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="static_mneu_bar">
    <div class="slimscroll-menu" id="remove-scrolls">
        <div id="sidebar-menu_2">
            <ul class="metismenu" id="side-menu_2">
                        <!-- <li class="menu-title">Main</li> -->
                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Overview</span></a></li>
                <li><a href="profile.html" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Personal</span></a></li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Documents</span></a></li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-contact-mail"></i><span> Contacts</span></a></li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i><span> Marketing <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                        <li><a href="marketing_profile.html">Marketing</a></li>
                        <li><a href="email.html">Emails</a></li>
                        <li><a href="letter.html">Letters</a></li>
                        <li><a href="fines.html">Fines</a></li>
                    </ul>
                </li>

                 <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-human-handsup"></i><span> Admissions <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                         <li><a href="application_profile.html">Application</a></li>
                        <li><a href="register_profile.html">Registration</a></li>
                        <li><a href="semester_register.html">Semester Registration</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="past_education.html">Past Eduction</a></li>
                        <li><a href="letter.html">Letters</a></li>
                        <li><a href="fines.html">Fines</a></li>
                    </ul>
                </li>

                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Student Portal</span></a></li>

                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-weight"></i><span> Finance<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                        <li><a href="general.html">General</a></li>
                        <li><a href="ledger.html">Ledger</a></li>
                        <li><a href="collect.html">Collection</a></li>
                        <li><a href="account_statement.html">Account Statement</a></li>
                        <li><a href="student_bank.html">Bank Account</a></li>
                        <li><a href="student_sponsor.html">Sponsors</a></li>
                        <li><a href="fee_structure.html">Fee Structure</a></li>
                        <li><a href="installment.html">Installments </a></li>
                        <li><a href="fines.html">Fines </a></li>
                    </ul>
                </li>
                 <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Academic<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                     <li><a href="program_academic.html">Programs</a></li>
                        <li><a href="credit_transfer.html">Credit Transfer</a></li>
                        <li><a href="student_subject.html">Courses</a></li>
                        <li><a href="fines.html">Fines</a></li>
                    </ul>
                </li>

                <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i><span> Exams<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                    <ul class="submenu">
                      <li><a href="publish_result_slip.html">Publish Result Slip</a></li>
                        <li><a href="result.html">Results</a></li>
                        <li><a href="fines.html">Fines</a></li>
                    </ul>
                </li>
                <li><a href="note.html" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Notes</span></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="content-page" style="margin-left: 460px">
            <!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Finance / Collection</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                        <li class="breadcrumb-item active"> Finance</li>
                    </ol>
                        </div>
                    </div>
                </div>
                <!-- end row -->
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div id="prospect"></div>
                                <div style="clear:both"></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="main-img-preview">
                                            <?php if(empty( @$users_info['profile_pic'])) { 
                                                $image = 'http://phpslick.com/lms/public/images/no-image.png';

                                            } else  {

                                                $image = 'http://phpslick.com/lms/uploads/'.$prospects_data['profile_pic'];

                                            } ?>
                                            <img class="thumbnail img-preview" src="<?php echo @$image; ?>" title="Preview Logo" style="width: 390px;">
                                        </div>
                                        <div class="input-group">
                                            <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->
                                            <div class="input-group-btn" style="width: 100%;">
                                                <div class="fileUpload btn btn-danger fake-shadow">
                                                    <span ><i class="mdi mdi-camera"  style="font-size: 20px"></i></span>
                                                    <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p><strong><?php echo $users_info['name']; ?></strong></p>
                                        <P>Identification : <?php echo $users_info['id_no']; ?></P>
                                        <P>Certificate :</P>
                                        <P>Student No : GP12425</P>
                                        <P class="sas" style="color: red;" >Student No : MARCH - JULY 2018(2018/3)</P>
                                        <P>Student No : Active</P> 
                                    </div>
                                    <div class="col-md-4" style="padding-top: 20px">
                                        <P>Faculty : xyz</P>
                                        <P>Program : xyz</P>
                                        <P>Intake : 2018/3</P>
                                        <P>Semester : 1</P>  
                                    </div> 
                                </div>
                                </div>
                                    <!-- / Collapsible element -->  
                            </div>
                        </div>
                    </div>
                            <!---card 2 table-->
					<div class="card m-b-30">
						<div class="card-body">
                            <div class="hearding">
                                <h4 class="mt-0 header-title left_side">Collection</h4>
                            </div> 
                            <div class="clearfix"></div>
                            <hr> 
                            <div class="row">
                                <div class="clearfix">
                                </div>
                                <div class="col-md-4">
                                <div class="lable">
                                    <label><strong>&nbsp Knockoff By &nbsp</strong></label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="selection1" checked="checked" name="inlineDefaultRadiosExamplea" mdbinputdirective="">
                                    <label class="custom-control-label " for="selection1">Selection</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="fifo" name="inlineDefaultRadiosExamplea" mdbinputdirective="">
                                    <label class="custom-control-label " for="fifo">FIFO</label>
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="lable">
                                    <label><strong>&nbsp Knockoff From &nbsp</strong></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" checked="checked" id="new_collection" name="inlineDefaultRadiosExample" mdbinputdirective="">
                                    <label class="custom-control-label " for="new_collection">New Collection</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="overpayment" name="inlineDefaultRadiosExample" mdbinputdirective="">
                                    <label class="custom-control-label " for="overpayment"> Overpayment ( 0.00)</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="deposit" name="inlineDefaultRadiosExample" mdbinputdirective="">
                                    <label class="custom-control-label " for="deposit">Deposit ( 0.00)</label>
                                </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><strong>Total</strong></label>
                                        <input type="text" class="form-control" value="0.00" required="" placeholder="Template Name" id="total" onkeyup="myFunction(this.value)">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><strong>Balance</strong></label>
                                        <input type="text" class="form-control" value="0.00" readonly="" required="" placeholder="Template Name" id="balance">
                                    </div>
                                </div>
                                <form method="post">
                            <table id="selection" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead class="black white-text">
                                    <tr>
                                        <th>Doc No</th>
                                        <th class="th-sm">Account
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> description
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> Amount
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> Paid
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                         <th class="th-sm"> Adjustment
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> Balance
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> Pay Full
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                        <th class="th-sm"> Amount Pay()
                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($bills)) {
                                        $amount = '';
                                        foreach ($bills as $key => $value) {
                                            $amount += $value['amount'];
                                    ?>
                                    <input type="hidden" name="" id="stubalance_<?php echo $value['id'] ?>" value="<?php echo $value['amount'] ?>">
                                    <input type="hidden" name="" id="bill_<?php echo $value['id'] ?>" value="<?php echo $value['id'] ?>">
                                    <tr>
                                        <td><?php echo $value['doc_no'] ?></td>
                                        <td><?php echo $value['account'] ?></td>
                                        <td><?php echo $value['description'] ?></td>
                                        <td><?php echo $value['amount'] ?></td>
                                        <td><?php echo $value['paid'] ?></td>
                                        <td><?php echo $value['outstanding'] ?></td>
                                        <td id="student_balance"><?php echo $value['amount'] ?></td>
                                        <td>
                                            <input type="checkbox" name="pay_full" value="<?php echo $value['id'] ?>" disabled="" id="pay_full<?php echo $value['id'] ?>">
                                        </td> 
                                        <td><input class="form-control" type="hidden" name="" value="" readonly="" id="total_paying<?php echo $value['id'] ?>"></td>
                                    </tr>
                                    <input type="hidden" name="bill_id" value="<?php echo $value['id'] ?>" id="bill_id<?php echo $value['id'] ?>">
                                    <?php } ?>
                                    <tr>
                                        <td>Total Amount</td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo $amount ?></td>
                                        <td>0.00</td>
                                        <td>0.00</td>
                                        <td><?php echo $amount ?></td>
                                        <td></td> 
                                        <td id="total_paid"></td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Date<span style="color: red;">*</span></strong></label>
                                    <div>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date" required="" autocomplete="off">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                     <!-- input-group -->
                                    </div>
                                </div>
                                <span id="dateErr" style="color: red;"></span>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Description</strong></label>
                                    <input type="text" class="form-control" value="" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Total Collection<span style="color: red;">*</span></strong></label>
                                    <input type="text" class="form-control" value="" required="" placeholder="" id="total_collection" readonly="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="payment">
                                    <label class="control-label"><strong>Payment Method<span style="color: red;">*</span></strong></label>
                                    <select class="form-control" tabindex="-1" aria-hidden="true" name="payment_method" id="payment_method">
                                    <!-- <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="payment_method" id="payment_method"> -->
                                        <option value="select">Select</option>
                                        <option value="ATM Account Transfer">ATM Account Transfer</option>
                                        <option value="BDA">BDA</option>
                                        <option value="Bank Draft">Bank Draft</option>
                                        <option value="Bank-in Slip">Bank-in Slip</option>
                                        <option value="CDM">CDM</option>
                                        <option value="CIMB Clicks">CIMB Clicks</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Cheque Deposit">Cheque Deposit</option>
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Debit Card">Debit Card</option>
                                        <option value="IBG Online Transfer">IBG Online Transfer</option>
                                        <option value="MARA">MARA</option>
                                        <option value="Money Order">Money Order</option>
                                        <option value="PTPTN">PTPTN</option>
                                        <option value="Postal Money">Postal Money</option>
                                        <option value="T.T">T.T</option>
                                    </select>
                                </div>
                                <span id="payment_methodErr" style="color: red;"></span>
                            </div> 
                            <div id="bank_details" style="display: none;">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"><strong>Bank Name</strong></label>
                                    <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="bank_name" id="bank_name" >
                                        <option>Select</option>
                                        <option value="">Option1</option>
                                        <option value="">Option2</option>
                                        <option value="">Option3</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Doc No</strong></label>
                                    <input type="text" class="form-control" value="" placeholder=""  id="doc_no" name="doc_no">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Document Date</label>
                                    <div>
                                        <div class="input-daterange input-group" id="date-range1">
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="doc_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Attachment</label>
                                    <div>
                                        <div class="input-daterange input-group" id="date-range1">
                                            <input type="file" class="form-control" name="attachment" placeholder="Start Date" id="attachment">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label>Remarks</label>
                                    <div>
                                        <div class="form-group" >
                                            <textarea name="remarks" class="form-control" type="text" rows="3" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" name="genrate" class="btn btn-primary" id="genrate">Genrate</button>
                            </div>
                        </form>
                        </div>
					</div>
				</div>
            </div>
        </div>
    </div>
                <!-- container-fluid -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    var total_val = '';
    var from_amount = '';
    $("#genrate").click(function(){
        var payment_method = $("#payment_method").val();
        var date = $("#datepicker-autoclose").val();
        var total = $("#total").val();
        // $("input:checkbox").on('click', function() {
        //     var val = this.value;
        //     if (("#pay_full"+val).is(":checked")) {
        //         var id = $("bill_"+val).val();
        //     } else {
        //         id = "0";
        //     }
        // });
        // if(!$("#pay_full").is(":checked")) {
        //     alert('Please Select any full payment.')
        //     return false;
        // } else 
        if(date == '') {
            $("#dateErr").html('This Field is required.');
            return false;
        } else if(payment_method == 'select') {
            $("#payment_methodErr").html('This Field is required.');
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: 'generate_collect',
                data: {payment_method:payment_method,date:date,total:total,total_val:total_val,from_amount: from_amount},
                success: function(html){
                    alert(html);
                    // if (html == "1") {
                    //     return true;
                    // } else {
                    //     return false;
                    // }
                }
            });
            
        }
    });
    
    $("#total").keyup(function(){
      var val = parseInt(this.value);

      $('input[id^="stubalance_"]').each(function (i,el) {
            var value = $(this).attr('value');
            var id = $(this).attr('id');
            var result= id.split('_');
            var res = result[1];
            $("#total_paying"+res).prop('type', 'text');
            if(val <= value)
            {
                from_amount = value;
                $("#pay_full"+res).removeAttr("disabled");
            } else {
                $("#pay_full"+res).prop("disabled", true );
                $("#total_paying"+val).val('');
                $("#total_paid").html('');
            }
        });
        
        $("#total_collection").val(val);
        $("#balance").val(val);
    });

    $("input:checkbox").on('click', function() {
        var val = this.value;
        var amount = $("#total").val();
        var $box = $(this);
        if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $("#total_paying"+val).val('');
            $(group).prop("checked", false);
            $box.prop("checked", true);
            $("#total_paying"+val).val(amount);
            total_val = val;
            $("#total_paid").html(amount);
        } else {
            $box.prop("checked", false);
            $("#total_paying"+val).val('');
        }
    });
    $("#payment_method").on('change',function(){
        $("#bank_details").show();
    });
</script>
<script type="text/javascript">
$(function() {
    $('#datepicker').datepicker({
        minDate : new Date()
    });
});
 $(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
 $(document).ready(function() {
        $("#selection1").click(function(){
            if ($("#selection1").is(":checked")) {
                $("#selection").show();
            }
        });
        $("#fifo").click(function(){
            if ($("#fifo").is(":checked")) {
                $("#selection").hide();
                $("#anoter").show();
            }
        });
        $("#new_collection").click(function(){
            if ($("#new_collection").is(":checked")) {
               // $("#payment").hide();
                $("#payment").show();
            }
        });

        $("#deposit").click(function(){
            if ($("#deposit").is(":checked")) {
                $("#payment").hide();
                //$("#anoter").show();
            }
        });
    });
    $("#overpayment").click(function(){
        alert('here');
        $("#total").val('0.00');
        $("#total_collection").val('0.00');
        $("#balance").val('0.00');
        if ($("#overpayment").is(":checked")) {
            $("#payment").hide();
        }
    });
</script>
<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html> 