<link rel="stylesheet" type="text/css" media="screen"

     href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

<div class="content-page">

<div class="content">

<div class="container-fluid">

    <div class="row">

        <div class="col-sm-12">

            <?php echo $this->session->flashdata('msg'); ?>

            <div class="page-title-box">

                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Finance / Add Bill </h4> -->

                

            </div>

        </div>

    </div>

</div>

    <!-- end row -->

            <div class="card m-b-30">

                <div class="card-body">

                    <div id="prospect"></div>

                    <div style="clear:both"></div>

                    <form method="post">

                         <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label class="control-label"><strong>Bill To<span style="color: red;">*</span></strong></label>

                                        <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="student_id" required="" id="student_id">

                                            <option>Select</option>

                                            <?php if (!empty($student_data)) {
                                                // echo "<pre>";
                                                // print_r($student_data);


                                                foreach ($student_dataSerial as $key => $data) {

                                            ?>

                                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->

                                            <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>

                                            <?php } } ?>

                                        </select>

                                </div>

                                <span id="studentErr" style="color: red;"></span>

                            </div>

                            <div class="col-md-4" style="display: none;" id="program">

                                <label class="control-label"><strong>Program</strong></label>
                                <div class="form-group">

                                    <input type="text" class="form-control " required="" placeholder="Program" name= "program" id="programs" value="">   

                                </div>
                                <!-- <?php
                                foreach ($programsdd as $key => $value) {
                                    if ($data['eligible_program']==$value['id']) {
                                        // echo "test";
                                        $programvalue= $value['name'];
                                   
                                } }
                                 
                                
                                 ?> -->

                                <!-- <div class="form-group">
                                    <input type="" class="form-control" name="program" value="<?php print_r($programvalue); ?>">
                                </div> -->

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label><strong>Date<span style="color: red;">*</span></strong></label>

                                    <div>

                                        <div class="input-group">

                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="date" required="" autocomplete="off">

                                            <div class="input-group-append">

                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>

                                            </div>

                                        </div>

                                     <!-- input-group -->

                                    </div>

                                </div>

                                <span id="dateErr" style="color: red;"></span>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label><strong>Due Date<span style="color: red;">*</span></strong></label>

                                    <div>

                                        <div class="input-group">

                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="due_date" required="" autocomplete="off">

                                            <div class="input-group-append">

                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>

                                            </div>

                                        </div>

                                    <!-- input-group -->

                                    </div>

                                </div>

                                <span id="due_dateErr" style="color: red;"></span>

                            </div>

                            <!-- <div class="col-md-4">

                                <div class="form-group">

                                    <label class="control-label"><strong>Amount Is<span style="color: red;">*</span></strong></label>

                                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="amount_is" required="" id="amount_is">

                                            <option>Select</option>

                                            <option value="No Tax">No Tax</option>

                                            <option value="Tax Inclusive">Tax Inclusive</option>

                                            <option value="Tax Exclusive">Tax Exclusive</option>

                                        </select>

                                </div>

                                <span id="amount_isErr" style="color: red;"></span>

                            </div> -->

                    <!-- </div> -->

                        

                            <!-- / Collapse buttons -->



                      

                         </div>

                         <div class="card m-b-30">

                        <div class="card-body">

                            <div class="hearding">

                                <h4 class="mt-0 header-title left_side"> New Bill</h4>

                            </div>

                            <div class="clearfix"></div>

                            <hr>

                            <table id="dtBasicExample" class="table table-hover order-list" cellspacing="0" width="100%">

                                <thead class="black white-text">

                                    <tr>

                                        <th class="th-sm">Account<span style="color: red;">*</span>

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                         <th class="th-sm">Description

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                        <th class="th-sm">Quantity

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                        <th class="th-sm">Unit Price()

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                        <th class="th-sm">Discount(%)

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                        <!-- <th class="th-sm">Tax Rate

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th> -->

                                        <th class="th-sm">Amount

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

                                        <!-- <th class="th-sm">Add New

                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>

                                        </th>

 -->                                    </tr>

                                </thead>

                                <tbody>

                                    <tr id='addr0' class="tbl_rows">

                                        <td> 

                                            <div class="form-group">

                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="account" required="" id='account' placeholder="Please Select">

                                                <!-- <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="account" required="" id='account'> -->

                                                    <option value="" disabled selected>Please Select </option>

                                                   <!-- <option value="KK - YURAN PENGAJIAN (KK)" >KK - YURAN PENGAJIAN (KK)</option> -->
                                                   <option value="YURAN UKM" >YURAN UKM</option>

                                                <!--<option value="YURAN HOSTE" >YURAN HOSTEL</option>-->

                                                <!--<option value="YURAN PENGAJIAN (IPGKB)" >YURAN PENGAJIAN (IPGKB)</option>-->

                                                <!--<option value="YURAN PENGAJIAN (IPTAR)" >YURAN PENGAJIAN (IPTAR)</option>-->

                                                <!--<option value="YURAN PENGAJIAN (UKM)" >YURAN PENGAJIAN (UKM)</option>-->

                                                </select>

                                                <span id="accountErr" style="color: red;"></span>

                                            </div>

                                        </td>

                                        <td>

                                        <div class="form-group">

                                            <div>

                                                <textarea class="form-control" rows="3" name="description" id="description"></textarea>

                                            </div>

                                        </div>

                                        </td>

                                        <td>

                                            <div class="form-group">

                                                <input type="text" class="form-control " required="" placeholder="Quantity" name="quantity" id="quantity"value="1" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">   

                                            </div>

                                        </td>

                                       <td>

                                            <div class="form-group">

                                                <input type="text" class="form-control " required="" placeholder="Unit Price" name="unit_price" id="unit_price" value="0.00" onkeyup="calculate_unitprice(this.value);"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');">   

                                            </div>

                                        </td>

                                        <td>

                                            <div class="form-group">

                                                <input type="text"  class="form-control " placeholder="%" name="discount" id="discount">   

                                            </div>

                                        </td>

                                     

                                        <td>

                                            <div class="form-group">

                                                <input type="text" class="form-control " required="" placeholder="Amount" name="amount" value="0.00" id="amount">   

                                            </div>

                                        </td>

                                        <!-- <td>

                                            <button onclick="education_fields();" data-toggle="tooltip" title="" data-original-title="Add More" type="button"><i class="ion-plus-circled" style="font-size:25px;"></i></button>

                                        </td> -->

                                    </tr>

                                    <tr id='addr1'></tr>

                                </tbody>

                            </table>

                        <div class="float-right">

                            <p><strong>Subtotal</strong> = <span id="subtotal"></span></p>

                            <hr>

                            <p><strong>Discount</strong> = <span id="res_discount"></span></p>

                            <hr>

                            <p><strong style="font-size: 18px"> Total  = <span id="total"></span></strong></p>

                            <hr>

                        </div>

                        <div class="clearfix"></div>

                        <!-- <div id="education_fields">

                        </div> -->

                        <div class="col-md-12 text-center">

                            <button type="submit" name="genrate" class="btn color_another btn-lg" id="genrate">Generate</button>

                            <button type="button" class="btn btn-lg btn-secondary more_border">Cancel</button>

                        </div>

                    </form>

                    </div>

                </div>

                </div>

            </div>

        

            </div>

        </div>

    </div>



<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script type="text/javascript">

$(function() {

    $('#datepicker-autoclose').datepicker({

        minDate : new Date()

    });

});

$(function() {

    $('#datepicker').datepicker({

        minDate : new Date()

    });

});

$(document).ready(function() {

    $("#subtotal").html('0.00');

    $("#res_discount").html('0.00');

    $("#total").html('0.00');

var brand = document.getElementById('logo-id');

brand.className = 'attachment_upload';

brand.onchange = function() {

document.getElementById('fakeUploadLogo').value = this.value.substring(12);

};



// Source: http://stackoverflow.com/a/4459419/6396981

function readURL(input) {

if (input.files && input.files[0]) {

var reader = new FileReader();



reader.onload = function(e) {

    $('.img-preview').attr('src', e.target.result);

};

reader.readAsDataURL(input.files[0]);

}

}

$("#logo-id").change(function() {

readURL(this);

});

});

</script>



<script type="text/javascript">

function selectAll(){

var items=document.getElementsByName('acs');

for(var i=0; i<items.length; i++){

if(items[i].type=='checkbox')

items[i].checked=true;

}

}



function UnSelectAll(){

var items=document.getElementsByName('acs');

for(var i=0; i<items.length; i++){

if(items[i].type=='checkbox')

items[i].checked=false;

}

}  





 function calculate_unitprice(obj) { 

        //var unit_price = $("#unit_price").val();

        var unit_price = obj;

       // alert(unit_price);

        var res_discount = $("#discount").val();

        var quantity = $("#quantity").val();

       

        var result = (unit_price*res_discount)/100;

         var discount_amount = result*quantity;

        // alert(discount_amount);

        if ( parseInt(unit_price) != '0.00' && parseInt($("#discount").val()) != '') {

            var price = (unit_price - result)*quantity;

            $("#subtotal").html(price);

            $("#res_discount").html(discount_amount);

            $("#total").html(price);

            $("#amount").val(price);

        } else if (parseInt(unit_price) != '0.00') {

            $("#subtotal").html(unit_price);

            $("#total").html(unit_price);

            $("#amount").val(unit_price);

        }

    }

         

</script>



<script type="text/javascript">

    $("#genrate").click(function(){

        var account = $("#account").val();

        var student_id = $("#student_id").val();

        var amount_is = $("#amount_is").val();

        var date = $("#datepicker-autoclose").val();

        var due_date = $("#datepicker").val();

        if(student_id == 'Select') {

            $("#studentErr").html('This Field is required.');

            return false;

        } else if (date == '') {

            $("#dateErr").html('This Field is required.');

            return false;

        } else if (due_date == '') {

            $("#due_dateErr").html('This Field is required.');

            return false;

        }  else if (amount_is == 'Select') {

            $("#amount_isErr").html('This Field is required.');

            return false;

        } else if (account == '') {

            $("#accountErr").html('This Field is required.');

            return false;

        } else {

            return true;

        }

    });

    

   

    $("#discount").keyup(function(){

        if ($("#unit_price").val() != '0.00') {

            var unit_price = $("#unit_price").val();

            var quantity = $("#quantity").val();

            var res_discount = $("#discount").val();

            var result = (unit_price*res_discount)/100;

             var discount_amount = result*quantity;

            var price = (unit_price - result)*quantity;

            $("#subtotal").html(price);

            $("#res_discount").html(discount_amount);

            $("#total").html(price);

            $("#amount").val(price);

        }

    });

    $("#datepicker-autoclose").keyup(function(){

        var datepicker = $("#datepicker-autoclose").val().length;

        if(datepicker>0) {

            $('#dateErr').html('');

         }

    });

    $("#account").on('change',function(){

       var acct = $("#account").val();

        $("#description").val(acct);

    });

$(document).ready(function () {

    $("table.order-list").on("click", ".ibtnDel", function (event) {

        $(this).closest("tr").remove();       

        counter -= 1

    });

});

$(function() {

    $('#datepicker').datetimepicker({

      pickTime: false

    });

});

</script>

<script>

var room = 1;

var i = 1;

function education_fields() {

    $('#addr'+i).html("<td><div class='form-group'><select class='form-control select2' tabindex='-1' aria-hidden='true' name='Schoolname[account][]' required='' id='accounts"+i+"'><option>Please Select</option><option value='AK'>option</option><option value='HI'>option</option></select></div></td><td><div class='form-group'><div><textarea class='form-control' rows='3' name='Schoolname[description][]' id='descriptions"+i+"'></textarea></div></div></td><td><div class='form-group'><input type='number' class='form-control' required='' placeholder='' name='Schoolname[quantity][]' id='quantitys"+i+"'></div></td><td><div class='form-group'><input type='text' class='form-control' required='' placeholder='Unit Price' name='Schoolname[unit_price][]' value='0.00' id='unit_prices"+i+"' onkeyup='myFunction("+i+")'></div></td><td><div class='form-group'><input type='text' class='form-control' placeholder='%' name='Schoolname[discount][]' id='discounts"+i+"' onkeyup='discFunction("+i+")'></div></td><td><div class='form-group'><input type='text' class='form-control '' required='' placeholder='Amount' name='Schoolname[amount][]' value='0.00' id='amounts"+i+"'></div></td><td><button class='btn btn-danger' type='button' onclick='remove_education_fields("+ i +");'> x </button> </td>");



       $('#dtBasicExample').append('<tr id="addr'+(i+1)+'" class="tbl_rows1"></tr>');

       i++; 

}

function remove_education_fields(rid) {

 if(i>1){

      $("#addr"+(i-1)).html('');

      i--;

    }

}

$("#student_id").on('change',function(){

    $("#program").show();

    var student_id = $("#student_id").val();

    //$("#programs").val($("#student_id").val());

    //$("#selected_student_id").html($("#student_id").val());

    // localStorage.setItem("selected_student_id", $("#student_id").val());

    // var selected_student = localStorage.getItem("selected_student_id");

    //sessionStorage.setItem("selected_student_id", "testing");

    // sessionStorage.setItem("selected_student_id", $("#student_id").val());

    // var selected_studentdocument.getElementById("result").innerHTML = sessionStorage.getItem("selected_student_id");

    $.ajax({

          url: 'get_students_program',

          type: 'get',

          data: {student_id : student_id},

         success: function(response) {

            //var arr = response.toString();

            //var arr = $.parseJSON(response);

            $("#programs").val(response);

            // alert(arr[0]->name);

          // if (response == 'true' ) {

          //  $('#phone_valid').val('0');

          //  return true;

          // } else if (response == 'false') {

          //  $('#phoneErr').html('Mobile Number Already Exist.');

          //  e.preventDefault();

          //  $('#phone_valid').val('1');

          // }

       }

    });

});

function myFunction(val) {



    var unit_price1 = $("#unit_price").val();

    var res_discount1 = $("#discount").val();

    var quantity1 = $("#quantity").val();

    var result1 = (unit_price1*res_discount1)/100;

    var discount1 = result1*quantity1;





    var unit_price = $("#unit_prices"+val).val();

    var res_discount = $("#discounts"+val).val();

     var quantitys = $("#quantitys"+val).val();

    var result = (unit_price*res_discount)/100;

     var discount = result*quantitys;



     var total_discount = discount1 +discount;

    if ($("#unit_price").val() != '0.00' && $("#discount").val() != '') {

        var price1 = (unit_price1 - result1)*quantity1;



        if ($("#unit_prices"+val).val() != '0.00' && $("#discounts"+val).val() != '') {

            var price = (unit_price - result)*quantitys;

            alert(result1);

            $("#amounts"+val).val(price);

            var total_price = parseFloat(price1) + parseFloat(price)

            

        } else if ($("#unit_prices"+val).val() != '0.00') {

            var total_price = parseFloat(price1) + parseFloat(unit_price);

            $("#amounts"+val).val(unit_price);

        }

        $("#subtotal").html(total_price);

        $("#res_discount").html(total_discount);

        $("#total").html(total_price);

        // else if ($("#unit_prices"+val).val() != '0.00') {

        //     $("#subtotal").html(unit_price);

        //     $("#total").html(unit_price);

        //     $("#amounts"+val).val(unit_price);

        // }

    }

}

function discFunction(val) {

    var unit_price = $("#unit_prices"+val).val();

    var quantitys = $("#quantitys"+val).val();

   

    if (unit_price != '0.00') {

        var res_discount = $("#discounts"+val).val();

        var discount  =  (parseFloat(unit_price) * parseFloat(res_discount))/100;

        var discount_amount = discount * quantitys;

        var price_total = (unit_price - res_discount)*quantitys;

        //$('#amounts'+val).val(amounts);

        $('#amounts'+val).val(price_total);

        if ($("#unit_price").val() != '0.00' && $("#discount").val() != '') {

            var result1 = ($("#unit_price").val()*$("#discount").val())/100;

            

            

            var result2 = (parseFloat(unit_price) *  parseFloat(res_discount))/100;

            var discount = parseFloat(result1) + parseFloat(result2);

            

            var i;

         for (i = 1; i <= val ; i++) { 

				 

				console.log($("#discounts"+i).val());

				 

			} 

						

            

            $("#res_discount").html(discount);

            var price1 = $("#unit_price").val() - result1;

            var price2 = unit_price - result2;

            var total_price = parseFloat(price1) + parseFloat(price2);

            $("#subtotal").html(total_price);

            $("#total").html(total_price);

            // var discount = parseInt($("#discount").val()) + parseInt($("#discounts"+val).val());

        } else {

            var discount = (unit_price*res_discount)/100;

            $("#res_discount").html(discount);

            var total_price = unit_price - result2;

            $("#subtotal").html(total_price);

            $("#total").html(total_price);

            //var discount = $("#discounts"+val).val()

        }

        //var result = (unit_price*res_discount)/100;

        //$("#amounts"+val).val(price);

    }

}



</script>

<style type="text/css">

.form-control, .thumbnail {

border-radius: 2px;

}

.btn-danger {

background-color: #B73333;

}



/* File Upload */

.fake-shadow {

box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);

}

.fileUpload {

bottom: 43px;

position: relative;

overflow: hidden;

left: 17px;

}

.fileUpload #logo-id {

position: absolute;

top: 0;

right: 0;

margin: 0;

padding: 0;

font-size: 33px;

cursor: pointer;

opacity: 0;

filter: alpha(opacity=0);

}

.img-preview {

max-width: 100%;

width: 100%;

}

.thumbnail {

display: block;

padding: 4px;

margin-bottom: 20px;

line-height: 1.428571429;

background-color: #fff;

border: 1px solid #ddd;

border-radius: 4px;

-webkit-transition: all .2s ease-in-out;

transition: all .2s ease-in-out;

}

</style>

</body>

</html>

