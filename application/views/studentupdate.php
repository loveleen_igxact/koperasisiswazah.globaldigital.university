
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
    <link rel="shortcut icon" href="assets/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH; ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/style1.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>

                        </form>
                    </li>
                    <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary ">Advance Search</button>
                            </a>
                        </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <strong class="admin_name">Admin</strong> <img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a><a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
 <?php $this->load->view('layout/sidebar'); ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
         <?php $this->load->view('layout/stickmenu'); ?>
     
	 <div class="content-page" style="margin-left: 460px">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Profile / Student Update</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active"> Student Update</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="main-img-preview">
                                                <img class="thumbnail img-preview" src="assets/images/small/img-7.jpg" title="Preview Logo" style="width: 390px;">
                                            </div>
                                            <div class="input-group">
                                                <!-- <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled"> -->
                                                <div class="input-group-btn" style="width: 100%;">
                                                    <div class="fileUpload btn btn-danger fake-shadow">
                                                        <span ><i class="mdi mdi-camera"  style="font-size: 20px"></i></span>
                                                        <input id="logo-id" name="logo" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p><strong><?php echo $prospects_data[0]['name']; ?></strong></p>
                                            <P>Identification : <?php echo $prospects_data[0]['id_no']; ?></P>
                                            <P>Certificate :</P>
                                             <!--<P>Student No : GP12425</P>
                                            <P class="sas" style="color: red;" >Student No : MARCH - JULY 2018(2018/3)</P>
                                            <P>Student No : Active</P> -->
                                        </div>
                                        <!-- <div class="col-md-4" style="padding-top: 20px">
                                            <P>Faculty : xyz</P>
                                            <P>Program : xyz</P>
                                            <P>Intake : 2018/03</P>
                                            <P>Semester : 1</P>  
                                        </div> -->
                                    </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                            </div>
                        </div>
                            <!---card 2 table-->
							<form action="<?php echo base_url('application/update_student/'.$id) ?>" method="post">
								<div class="card m-b-30">
									<div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Personal Information</h4>
                                        </div> 
                                        <div class="clearfix"></div>
                                        <hr> 
                                    <div class="row">
                                        <div class="col-md-4">
                                        <label>International</label><br>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input type="radio" name="international" value="no" id="option2" <?php echo (@$prospects_data[0]['international'] == 'no' )? 'checked' :''; ?>> No </label>
                                                        <label class="btn btn-full ">
                                                        <input type="radio" name="international" value="yes" id="option3" <?php echo (@$prospects_data[0]['international'] == 'yes' )? 'checked' :''; ?>> Yes</label>
                                                    </div>
                                    </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>ID Type<span style="color: red;">*</span></strong></label>
                                                    
													<select name="idtype" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                                          <option value="">Select</option>
                                                        <option value="2" <?php echo (@$prospects_data[0]['idtype'] == '2')?'selected' :'' ?>>Passport</option>
                                                        <option value="1" <?php echo (@$prospects_data[0]['idtype']=='1' )?'selected' :'' ?>>Identification Certificate</option>
                                                        
                                                    
                                                </select>
													
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>ID No<span style="color: red;">*</span></strong></label>
                                                <input type="text"  name="id_no"class="form-control " required="" placeholder="eg: 123456789" value="<?php echo @$prospects_data[0]['id_no']?>"> 

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Name<span style="color: red;">*</span></strong></label>
                                            <input type="text" class="form-control "  name="name" required="" placeholder="" value="<?php echo @$prospects_data[0]['name']?>"> 

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Secondary Name</strong></label>
								<input type="text" class="form-control "  name="secondary_name"  placeholder="" value="<?php echo @$prospects_data[0]['secondary_name']?>"> 
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                        <label><strong>Gender</strong></label><br>
                                       
                                      <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline3" name="gender" <?php echo (@$prospects_data[0]['gender'] == 'male' )? 'checked' :''; ?> mdbinputdirective="" value="male" required>
                                              <label class="custom-control-label" for="defaultInline3">Male</label>
                                            </div>
                                           
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline4" <?php echo (@$prospects_data[0]['gender'] == 'female' )? 'checked' :''; ?> name="gender" mdbinputdirective="" value="female" required>
                                              <label class="custom-control-label" for="defaultInline4">Female</label>
                                            </div>
											
                                        </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Email<span style="color: red;">*</span></strong></label>
											   <input type="Email" name="email" class="form-control " required="" placeholder="" value="<?php echo @$prospects_data[0]['email']?>"> 

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Phone<span style="color: red;">*</span></strong></label>
                                                 <input type="text" name="phone" class="form-control " required="" placeholder="" value="<?php echo @$prospects_data[0]['phone']?>"> 

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Nationality</strong></label>
                                                    
													<select name="nationality" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required ="">
                                                    <option value="">Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                      
                                            <option value="1">Afghan</option>
                                                <option value="2" <?php echo (@$prospects_data[0]['salutation'] == '2')?'selected' :'' ?>>Albanian</option>
                                                <option value="3" <?php echo (@$prospects_data[0]['salutation'] == '3')?'selected' :'' ?>>Algerian</option>
                                                <option value="184" <?php echo (@$prospects_data[0]['salutation'] == '184')?'selected' :'' ?>>American</option>
                                                <option value="4" <?php echo (@$prospects_data[0]['salutation'] == '4')?'selected' :'' ?>>Andorran</option>
                                                <option value="5" <?php echo (@$prospects_data[0]['salutation'] == '5')?'selected' :'' ?>>Angolan</option>
                                                <option value="6" <?php echo (@$prospects_data[0]['salutation'] == '6')?'selected' :'' ?>>Antiguans</option>
                                                <option value="7" <?php echo (@$prospects_data[0]['salutation'] == '7')?'selected' :'' ?>>Argentinean</option>
                                                <option value="8" <?php echo (@$prospects_data[0]['salutation'] == '8')?'selected' :'' ?>>Armenian</option>
                                                <option value="9" <?php echo (@$prospects_data[0]['salutation'] == '9')?'selected' :'' ?>>Australian</option>
                                                <option value="10" <?php echo (@$prospects_data[0]['salutation'] == '10')?'selected' :'' ?>>Austrian</option>
                                                <option value="11" <?php echo (@$prospects_data[0]['salutation'] == '11')?'selected' :'' ?>>Azerbaijani</option>
                                                <option value="12" <?php echo (@$prospects_data[0]['salutation'] == '12')?'selected' :'' ?>>Bahamian</option>
                                                <option value="13" <?php echo (@$prospects_data[0]['salutation'] == '13')?'selected' :'' ?>>Bahraini</option>
                                                <option value="14" <?php echo (@$prospects_data[0]['salutation'] == '14')?'selected' :'' ?>>Bangladeshi</option>
                                                <option value="15" <?php echo (@$prospects_data[0]['salutation'] == '15')?'selected' :'' ?>>Barbadian</option>
                                                <option value="23" <?php echo (@$prospects_data[0]['salutation'] == '23')?'selected' :'' ?>>Batswana</option>
                                                <option value="16" <?php echo (@$prospects_data[0]['salutation'] == '15')?'selected' :'' ?>>Belarusian</option>
                                                <option value="17" <?php echo (@$prospects_data[0]['salutation'] == '16')?'selected' :'' ?>>Belgian</option>
                                                <option value="18" <?php echo (@$prospects_data[0]['salutation'] == '17')?'selected' :'' ?>>Belizean</option>
                                                <option value="19" <?php echo (@$prospects_data[0]['salutation'] == '18')?'selected' :'' ?>>Beninese</option>
                                                <option value="20" <?php echo (@$prospects_data[0]['salutation'] == '19')?'selected' :'' ?>>Bhutanese</option>
                                                <option value="21"<?php echo (@$prospects_data[0]['salutation'] == '20')?'selected' :'' ?>>Bolivian</option>
                                                <option value="22" <?php echo (@$prospects_data[0]['salutation'] == '21')?'selected' :'' ?>>Bosnian</option>
                                                <option value="24" <?php echo (@$prospects_data[0]['salutation'] == '22')?'selected' :'' ?>>Brazilian</option>
                                                <option value="183" <?php echo (@$prospects_data[0]['salutation'] == '23')?'selected' :'' ?>>British</option>
                                                <option value="25" <?php echo (@$prospects_data[0]['salutation'] == '24')?'selected' :'' ?>>Bruneian</option>
                                                <option value="26" <?php echo (@$prospects_data[0]['salutation'] == '25')?'selected' :'' ?>>Bulgarian</option>
                                                <option value="27" <?php echo (@$prospects_data[0]['salutation'] == '26')?'selected' :'' ?>>Burkinabe</option>
                                                <option value="120" <?php echo (@$prospects_data[0]['salutation'] == '27')?'selected' :'' ?>>Burmese</option>
                                                <option value="28" <?php echo (@$prospects_data[0]['salutation'] == '28')?'selected' :'' ?>>Burundian</option>
                                                <option value="29" <?php echo (@$prospects_data[0]['salutation'] == '29')?'selected' :'' ?>>Cambodian</option>
                                                <option value="30" <?php echo (@$prospects_data[0]['salutation'] == '30')?'selected' :'' ?>>Cameroonian</option>
                                                <option value="31">Canadian</option>
                                                <option value="32">Cape Verdean</option>
                                                <option value="33">Central African</option>
                                                <option value="34">Chadian</option>
                                                <option value="35">Chilean</option>
                                                <option value="36">Chinese</option>
                                                <option value="37">Colombian</option>
                                                <option value="38">Comoran</option>
                                                <option value="39">Congolese</option>
                                                <option value="41">Costa Rican</option>
                                                <option value="43">Croatian</option>
                                                <option value="44">Cuban</option>
                                                <option value="45">Cypriot</option>
                                                <option value="46">Czech</option>
                                                <option value="47">Danish</option>
                                                <option value="48">Djibouti</option>
                                                <option value="49">Dominican</option>
                                                <option value="124">Dutch</option>
                                                <option value="51">East Timorese</option>
                                                <option value="52">Ecuadorean</option>
                                                <option value="53">Egyptian</option>
                                                <option value="182">Emirian</option>
                                                <option value="55">Equatorial Guinean</option>
                                                <option value="56">Eritrean</option>
                                                <option value="57">Estonian</option>
                                                <option value="58">Ethiopian</option>
                                                <option value="59">Fijian</option>
                                                <option value="137">Filipino</option>
                                                <option value="60">Finnish</option>
                                                <option value="61">French</option>
                                                <option value="62">Gabonese</option>
                                                <option value="63">Gambian</option>
                                                <option value="64">Georgian</option>
                                                <option value="65">German</option>
                                                <option value="66">Ghanaian</option>
                                                <option value="67">Greek</option>
                                                <option value="68">Grenadian</option>
                                                <option value="69">Guatemalan</option>
                                                <option value="70">Guinean</option>
                                                <option value="72">Guyanese</option>
                                                <option value="73">Haitian</option>
                                                <option value="74">Honduran</option>
                                                <option value="75">Hungarian</option>
                                                <option value="76">Icelander</option>
                                                <option value="77">Indian</option>
                                                <option value="78">Indonesian</option>
                                                <option value="79">Iranian</option>
                                                <option value="80">Iraqi</option>
                                                <option value="81">Irish</option>
                                                <option value="82">Israeli</option>
                                                <option value="83">Italian</option>
                                                <option value="42">Ivorian</option>
                                                <option value="84">Jamaican</option>
                                                <option value="85">Japanese</option>
                                                <option value="86">Jordanian</option>
                                                <option value="87">Kazakhstani</option>
                                                <option value="88">Kenyan</option>
                                                <option value="92">Kuwaiti</option>
                                                <option value="93">Kyrgyz</option>
                                                <option value="94">Laotian</option>
                                                <option value="95">Latvian</option>
                                                <option value="96">Lebanese</option>
                                                <option value="98">Liberian</option>
                                                <option value="99">Libyan</option>
                                                <option value="100">Liechtensteiner</option>
                                                <option value="101">Lithuanian</option>
                                                <option value="102">Luxembourger</option>
                                                <option value="103">Macedonian</option>
                                                <option value="104">Malagasy</option>
                                                <option value="105">Malawian</option>
                                                <option value="106">Malaysian</option>
                                                <option value="107">Maldivan</option>
                                                <option value="108">Malian</option>
                                                <option value="109">Maltese</option>
                                                <option value="110">Marshallese</option>
                                                <option value="111">Mauritanian</option>
                                                <option value="112">Mauritian</option>
                                                <option value="113">Mexican</option>
                                                <option value="114">Micronesian</option>
                                                <option value="115">Moldovan</option>
                                                <option value="116">Monacan</option>
                                                <option value="117">Mongolian</option>
                                                <option value="118">Moroccan</option>
                                                <option value="119">Mozambican</option>
                                                <option value="121">Namibian</option>
                                                <option value="122">Nauruan</option>
                                                <option value="123">Nepalese</option>
                                                <option value="125">New Zealander</option>
                                                <option value="187">Ni-Vanuatu</option>
                                                <option value="126">Nicaraguan</option>
                                                <option value="128">Nigerian</option>
                                                <option value="127">Nigerien</option>
                                                <option value="90">North Korean</option>
                                                <option value="129">Norwegian</option>
                                                <option value="130">Omani</option>
                                                <option value="131">Pakistani</option>
                                                <option value="132">Palauan</option>
                                                <option value="133">Panamanian</option>
                                                <option value="134">Papua New Guinean</option>
                                                <option value="135">Paraguayan</option>
                                                <option value="136">Peruvian</option>
                                                <option value="138">Polish</option>
                                                <option value="139">Portuguese</option>
                                                <option value="140">Qatari</option>
                                                <option value="141">Romanian</option>
                                                <option value="142">Russian</option>
                                                <option value="143">Rwandan</option>
                                                <option value="144">Saint Lucian</option>
                                                <option value="54">Salvadoran</option>
                                                <option value="147">Samoan</option>
                                                <option value="148">San Marinese</option>
                                                <option value="149">Sao Tomean</option>
                                                <option value="150">Saudi</option>
                                                <option value="151">Senegalese</option>
                                                <option value="152">Serbian</option>
                                                <option value="153">Seychellois</option>
                                                <option value="154">Sierra Leonean</option>
                                                <option value="155">Singaporean</option>
                                                <option value="156">Slovakian</option>
                                                <option value="157">Slovenian</option>
                                                <option value="158">Solomon Islander</option>
                                                <option value="159">Somali</option>
                                                <option value="160">South African</option>
                                                <option value="91">South Korean</option>
                                                <option value="161">Spanish</option>
                                                <option value="162">Sri Lankan</option>
                                                <option value="163">Sudanese</option>
                                                <option value="164">Surinamer</option>
                                                <option value="165">Swazi</option>
                                                <option value="166">Swedish</option>
                                                <option value="167">Swiss</option>
                                                <option value="168">Syrian</option>
                                                <option value="169">Taiwanese</option>
                                                <option value="170">Tajik</option>
                                                <option value="171">Tanzanian</option>
                                                <option value="172">Thai</option>
                                                <option value="173">Togolese</option>
                                                <option value="174">Tongan</option>
                                                <option value="175">Trinidadian or Tobagonian</option>
                                                <option value="176">Tunisian</option>
                                                <option value="177">Turkish</option>
                                                <option value="179">Tuvaluan</option>
                                                <option value="180">Ugandan</option>
                                                <option value="181">Ukrainian</option>
                                                <option value="185">Uruguayan</option>
                                                <option value="186">Uzbekistani</option>
                                                <option value="189">Venezuelan</option>
                                                <option value="190">Vietnamese</option>
                                                <option value="191">Yemenite</option>
                                                <option value="192">Zambian</option>
                                                <option value="193">Zimbabwean</option>
                   
                                                    
                                                </select>
                                        
													
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Race</strong></label>
                                                     <select name="race"  class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                                    <option value="">Select</option>
                                                     <option value="15" <?php echo (@$prospects_data[0]['race'] == '15')?'selected' :'' ?>>Bajau</option>
                                                <option value="16" <?php echo (@$prospects_data[0]['race'] == '16')?'selected' :'' ?>>Bidayuh</option>
                                                <option value="17" <?php echo (@$prospects_data[0]['race'] == '17')?'selected' :'' ?>>Bisaya</option>
                                                <option value="18" <?php echo (@$prospects_data[0]['race'] == '18')?'selected' :'' ?>>Brunei</option>
                                                <option value="19" <?php echo (@$prospects_data[0]['race'] == '19')?'selected' :'' ?>>Bugis</option>
                                                <option value="12" <?php echo (@$prospects_data[0]['race'] == '12')?'selected' :'' ?>>Bumiputra Sabah</option>
                                                <option value="13" <?php echo (@$prospects_data[0]['race'] == '13')?'selected' :'' ?>>Bumiputra Sarawak</option>
                                                <option value="2" <?php echo (@$prospects_data[0]['race'] == '2')?'selected' :'' ?>>Chinese</option>
                                                <option value="20" <?php echo (@$prospects_data[0]['race'] == '20')?'selected' :'' ?>>Dusun</option>
                                                <option value="21" <?php echo (@$prospects_data[0]['race'] == '21')?'selected' :'' ?>>Iban</option>
                                                <option value="22" <?php echo (@$prospects_data[0]['race'] == '22')?'selected' :'' ?>>India Muslim</option>
                                                <option value="3" <?php echo (@$prospects_data[0]['race'] == '3')?'selected' :'' ?>>Indian</option>
                                                <option value="23" <?php echo (@$prospects_data[0]['race'] == '23')?'selected' :'' ?>>Kadazan</option>
                                                <option value="24" <?php echo (@$prospects_data[0]['race'] == '24')?'selected' :'' ?>>Lun Bawang</option>
                                                <option value="25" <?php echo (@$prospects_data[0]['race'] == '25')?'selected' :'' ?>>Lundayeh</option>
                                                <option value="1" <?php echo (@$prospects_data[0]['race'] == '1')?'selected' :'' ?>>Malay</option>
                                                <option value="26" <?php echo (@$prospects_data[0]['race'] == '26')?'selected' :'' ?>>Melanau</option>
                                                <option value="27" <?php echo (@$prospects_data[0]['race'] == '27')?'selected' :'' ?>>Melayu (Chaniago)</option>
                                                <option value="28" <?php echo (@$prospects_data[0]['race'] == '28')?'selected' :'' ?>>Melayu Brunei</option>
                                                <option value="29" <?php echo (@$prospects_data[0]['race'] == '29')?'selected' :'' ?>>Murut</option>
                                                <option value="10" <?php echo (@$prospects_data[0]['race'] == '10')?'selected' :'' ?>>Others</option>
                                                <option value="30" <?php echo (@$prospects_data[0]['race'] == '30')?'selected' :'' ?>>Punjabi</option>
                                                <option value="31" <?php echo (@$prospects_data[0]['race'] == '31')?'selected' :'' ?>>Serani</option>
                                                <option value="32" <?php echo (@$prospects_data[0]['race'] == '32')?'selected' :'' ?>>Siam</option>
                                                </select>
                                      
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Religion</small></strong></label>
                                                    <select  name="religion" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                                    <option value="">Select</option>
                                                     <option value="2" <?php echo (@$prospects_data[0]['religion'] == '2')?'selected' :'' ?>>Buddhism</option>
                                                <option value="3" <?php echo (@$prospects_data[0]['religion'] == '3')?'selected' :'' ?>>Christianity</option>
                                                <option value="4" <?php echo (@$prospects_data[0]['religion'] == '4')?'selected' :'' ?>>Hinduism</option>
                                                <option value="1" <?php echo (@$prospects_data[0]['religion'] == '1')?'selected' :'' ?>>Islam</option>
                                                <option value="8" <?php echo (@$prospects_data[0]['religion'] == '8')?'selected' :'' ?>>Malayalam</option>
                                                <option value="7" <?php echo (@$prospects_data[0]['religion'] == '7')?'selected' :'' ?>>Others</option>
                                                <option value="9" <?php echo (@$prospects_data[0]['religion'] == '9')?'selected' :'' ?>>Punjabi</option>
                                                <option value="5" <?php echo (@$prospects_data[0]['religion'] == '5')?'selected' :'' ?>> Sikhism</option>
                                                </select>
                                            </div>
                                        </div>
<div class="col-md-12 personal">
                                        <h4>Address</h4>
                                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>Address Line 1</strong></label>
                                            <input type="text" name="address_line1" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong> Address Line 2</strong></label>
                                            <input type="text" name="address_line2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>City</strong></label>
                                            <input type="City" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Country</strong></label>
                                                <select name="country" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                      <option value="1">Afghanistan</option>
                                                <option value="2">Albania</option>
                                                <option value="3">Algeria</option>
                                                <option value="4">American Samoa</option>
                                                <option value="5">Andorra</option>
                                                <option value="6">Angola</option>
                                                <option value="7">Anguilla</option>
                                                <option value="8">Antarctica</option>
                                                <option value="9">Antigua And Barbuda</option>
                                                <option value="10">Argentina</option>
                                                <option value="11">Armenia</option>
                                                <option value="12">Aruba</option>
                                                <option value="13">Australia</option>
                                                <option value="14">Austria</option>
                                                <option value="15">Azerbaijan</option>
                                                <option value="16">Bahamas The</option>
                                                <option value="17">Bahrain</option>
                                                <option value="18">Bangladesh</option>
                                                <option value="19">Barbados</option>
                                                <option value="20">Belarus</option>
                                                <option value="21">Belgium</option>
                                                <option value="22">Belize</option>
                                                <option value="23">Benin</option>
                                                <option value="24">Bermuda</option>
                                                <option value="25">Bhutan</option>
                                                <option value="26">Bolivia</option>
                                                <option value="27">Bosnia and Herzegovina</option>
                                                <option value="28">Botswana</option>
                                                <option value="29">Bouvet Island</option>
                                                <option value="30">Brazil</option>
                                                <option value="31">British Indian Ocean Territory</option>
                                                <option value="32">Brunei</option>
                                                <option value="33">Bulgaria</option>
                                                <option value="34">Burkina Faso</option>
                                                <option value="35">Burundi</option>
                                                <option value="36">Cambodia</option>
                                                <option value="37">Cameroon</option>
                                                <option value="38">Canada</option>
                                                <option value="39">Cape Verde</option>
                                                <option value="40">Cayman Islands</option>
                                                <option value="41">Central African Republic</option>
                                                <option value="42">Chad</option>
                                                <option value="43">Chile</option>
                                                <option value="44">China</option>
                                                <option value="45">Christmas Island</option>
                                                <option value="46">Cocos Keeling] Islands</option>
                                                <option value="47">Colombia</option>
                                                <option value="48">Comoros</option>
                                                <option value="51">Cook Islands</option>
                                                <option value="52">Costa Rica</option>
                                                <option value="53">Cote D\Ivoire (Ivory Coast)</option>
                                                <option value="54">Croatia Hrvatska]</option>
                                                <option value="55">Cuba</option>
                                                <option value="56">Cyprus</option>
                                                <option value="57">Czech Republic</option>
                                                <option value="50">Democratic Republic Of The Congo</option>
                                                <option value="58">Denmark</option>
                                                <option value="59">Djibouti</option>
                                                <option value="60">Dominica</option>
                                                <option value="61">Dominican Republic</option>
                                                <option value="62">East Timor</option>
                                                <option value="63">Ecuador</option>
                                                <option value="64">Egypt</option>
                                                <option value="65">El Salvador</option>
                                                <option value="66">Equatorial Guinea</option>
                                                <option value="67">Eritrea</option>
                                                <option value="68">Estonia</option>
                                                <option value="69">Ethiopia</option>
                                                <option value="70">External Territories of Australia</option>
                                                <option value="71">Falkland Islands</option>
                                                <option value="72">Faroe Islands</option>
                                                <option value="73">Fiji Islands</option>
                                                <option value="74">Finland</option>
                                                <option value="75">France</option>
                                                <option value="76">French Guiana</option>
                                                <option value="77">French Polynesia</option>
                                                <option value="78">French Southern Territories</option>
                                                <option value="79">Gabon</option>
                                                <option value="80">Gambia The</option>
                                                <option value="81">Georgia</option>
                                                <option value="82">Germany</option>
                                                <option value="83">Ghana</option>
                                                <option value="84">Gibraltar</option>
                                                <option value="85">Greece</option>
                                                <option value="86">Greenland</option>
                                                <option value="87">Grenada</option>
                                                <option value="88">Guadeloupe</option>
                                                <option value="89">Guam</option>
                                                <option value="90">Guatemala</option>
                                                <option value="91">Guernsey and Alderney</option>
                                                <option value="92">Guinea</option>
                                                <option value="93">Guinea-Bissau</option>
                                                <option value="94">Guyana</option>
                                                <option value="95">Haiti</option>
                                                <option value="96">Heard and McDonald Islands</option>
                                                <option value="97">Honduras</option>
                                                <option value="98">Hong Kong S.A.R.</option>
                                                <option value="99">Hungary</option>
                                                <option value="100">Iceland</option>
                                                <option value="101">India</option>
                                                <option value="102">Indonesia</option>
                                                <option value="103">Iran</option>
                                                <option value="104">Iraq</option>
                                                <option value="105">Ireland</option>
                                                <option value="106">Israel</option>
                                                <option value="107">Italy</option>
                                                <option value="108">Jamaica</option>
                                                <option value="109">Japan</option>
                                                <option value="110">Jersey</option>
                                                <option value="111">Jordan</option>
                                                <option value="112">Kazakhstan</option>
                                                <option value="113">Kenya</option>
                                                <option value="114">Kiribati</option>
                                                <option value="115">Korea North</option>
                                                <option value="116">Korea South</option>
                                                <option value="117">Kuwait</option>
                                                <option value="118">Kyrgyzstan</option>
                                                <option value="119">Laos</option>
                                                <option value="120">Latvia</option>
                                                <option value="121">Lebanon</option>
                                                <option value="122">Lesotho</option>
                                                <option value="123">Liberia</option>
                                                <option value="124">Libya</option>
                                                <option value="125">Liechtenstein</option>
                                                <option value="126">Lithuania</option>
                                                <option value="127">Luxembourg</option>
                                                <option value="128">Macau S.A.R.</option>
                                                <option value="129">Macedonia</option>
                                                <option value="130">Madagascar</option>
                                                <option value="131">Malawi</option>
                                                <option value="132">Malaysia</option>
                                                <option value="133">Maldives</option>
                                                <option value="134">Mali</option>
                                                <option value="135">Malta</option>
                                                <option value="136">Man Isle of]</option>
                                                <option value="137">Marshall Islands</option>
                                                <option value="138">Martinique</option>
                                                <option value="139">Mauritania</option>
                                                <option value="140">Mauritius</option>
                                                <option value="141">Mayotte</option>
                                                <option value="142">Mexico</option>
                                                <option value="143">Micronesia</option>
                                                <option value="144">Moldova</option>
                                                <option value="145">Monaco</option>
                                                <option value="146">Mongolia</option>
                                                <option value="147">Montserrat</option>
                                                <option value="148">Morocco</option>
                                                <option value="149">Mozambique</option>
                                                <option value="150">Myanmar</option>
                                                <option value="151">Namibia</option>
                                                <option value="152">Nauru</option>
                                                <option value="153">Nepal</option>
                                                <option value="154">Netherlands Antilles</option>
                                                <option value="155">Netherlands The</option>
                                                <option value="156">New Caledonia</option>
                                                <option value="157">New Zealand</option>
                                                <option value="158">Nicaragua</option>
                                                <option value="159">Niger</option>
                                                <option value="160">Nigeria</option>
                                                <option value="161">Niue</option>
                                                <option value="162">Norfolk Island</option>
                                                <option value="163">Northern Mariana Islands</option>
                                                <option value="164">Norway</option>
                                                <option value="165">Oman</option>
                                                <option value="166">Pakistan</option>
                                                <option value="167">Palau</option>
                                                <option value="168">Palestinian Territory Occupied</option>
                                                <option value="169">Panama</option>
                                                <option value="170">Papua new Guinea</option>
                                                <option value="171">Paraguay</option>
                                                <option value="172">Peru</option>
                                                <option value="173">Philippines</option>
                                                <option value="174">Pitcairn Island</option>
                                                <option value="175">Poland</option>
                                                <option value="176">Portugal</option>
                                                <option value="177">Puerto Rico</option>
                                                <option value="178">Qatar</option>
                                                <option value="49">Republic Of The Congo</option>
                                                <option value="179">Reunion</option>
                                                <option value="180">Romania</option>
                                                <option value="181">Russia</option>
                                                <option value="182">Rwanda</option>
                                                <option value="183">Saint Helena</option>
                                                <option value="184">Saint Kitts And Nevis</option>
                                                <option value="185">Saint Lucia</option>
                                                <option value="186">Saint Pierre and Miquelon</option>
                                                <option value="187">Saint Vincent And The Grenadines</option>
                                                <option value="188">Samoa</option>
                                                <option value="189">San Marino</option>
                                                <option value="190">Sao Tome and Principe</option>
                                                <option value="191">Saudi Arabia</option>
                                                <option value="192">Senegal</option>
                                                <option value="193">Serbia</option>
                                                <option value="194">Seychelles</option>
                                                <option value="195">Sierra Leone</option>
                                                <option value="196">Singapore</option>
                                                <option value="197">Slovakia</option>
                                                <option value="198">Slovenia</option>
                                                <option value="199">Smaller Territories of the UK</option>
                                                <option value="200">Solomon Islands</option>
                                                <option value="201">Somalia</option>
                                                <option value="202">South Africa</option>
                                                <option value="203">South Georgia</option>
                                                <option value="204">South Sudan</option>
                                                <option value="205">Spain</option>
                                                <option value="206">Sri Lanka</option>
                                                <option value="207">Sudan</option>
                                                <option value="208">Suriname</option>
                                                <option value="209">Svalbard And Jan Mayen Islands</option>
                                                <option value="210">Swaziland</option>
                                                <option value="211">Sweden</option>
                                                <option value="212">Switzerland</option>
                                                <option value="213">Syria</option>
                                                <option value="214">Taiwan</option>
                                                <option value="215">Tajikistan</option>
                                                <option value="216">Tanzania</option>
                                                <option value="217">Thailand</option>
                                                <option value="218">Togo</option>
                                                <option value="219">Tokelau</option>
                                                <option value="220">Tonga</option>
                                                <option value="221">Trinidad And Tobago</option>
                                                <option value="222">Tunisia</option>
                                               
                                                </select>
                                       

											  </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><strong>State</strong></label>
                                                <select name="state" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                         <option value="1780">Beit Hanania</option>

                                                       <option value="1781">Ben Gurion Airport</option>
                                                       <option value="1782">Bethlehem</option>
                                                       <option value="1783">Caesarea</option><option value="1784">Centre</option><option value="1785">Gaza</option><option value="1786">Hadaron</option><option value="1787">Haifa District</option><option value="1788">Hamerkaz</option><option value="1789">Hazafon</option><option value="1790">Hebron</option>
                                                    
                                                </select>
                                       
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong> Postcode</strong></label>
                                            <input type="text" class="form-control " name="postcode" placeholder=""> 
                                         </div>
                                    </div>
                                </div>
                                </div>
                                            <div class="clearfix"></div>

                                            <div class=" col-md-12 text-center">
                                                <button type="submit" class="btn btn-lg btn-primary">Save</button>
                                                <a href="#" class="btn btn-lg btn-dark">Cancel</a>
                                            </div>
                                     </div>
									</div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    <!-- jQuery  -->
    <script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH; ?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH; ?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo JS_PATH; ?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH; ?>js/app.js"></script>
    <script src="<?php echo JS_PATH; ?>js/sasa.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.js"></script>
       <script type="text/javascript">
        $(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
    </script>
<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html> 