
 <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> New Academic Session</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">New Academic Session</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <form action="<?php echo base_url('addmision/add_session'); ?>" method="post">
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Code<span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="code" required="">
                                                    <option value="">Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
														<option value="2019">2019</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2016">2016</option>
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Month<strong><span style="color: red;"></span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="month" required="">
                                                    <?php for ($m=1; $m<=12; $m++) {
													$month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
													<option value="<?php echo $m; ?>"><?php echo $month ?> </option>
													<?php 	} ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name<span style="color: red;">*</span> </label>
                                            <input type="text" class="form-control " required="" placeholder="" name="name"> 
                                         </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Program<strong><span style="color: red;"></span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="program">
                                                 <option value="17">SPEND - SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)</option>
                                                <option value="22">SPEND - SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)</option>
                                                <option value="23">SPEND - SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)</option>
                                                <option value="18">SPEND - SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)</option>
                                                <option value="7">SPEND(GB) - SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)</option>
                                                <option value="19">SPEND(GE) - SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT</option>
                                                <option value="1">SPEND(GF) - SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)</option>
                                                <option value="4">SPEND(GG) - SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)</option>
                                                <option value="5">SPEND(GH) - SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)</option>
                                                <option value="10">SPEND(GK) - SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)</option>
                                                <option value="15">SPEND(GL) - SARJANA PENDIDIKAN (TESL)</option>
                                                <option value="14">SPEND(GM) - SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)</option>
                                                <option value="6">SPEND(GN) - SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)</option>
                                                <option value="12">SPEND(GO) - SARJANA PENDIDIKAN (PENDIDIKAN KHAS)</option>
                                                <option value="3">SPEND(GQ) - SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)</option>
                                                <option value="8">SPEND(GR) - SARJANA PENDIDIKAN (PENDIDIKAN SAINS)</option>
                                                <option value="2">SPEND(GS) - SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)</option>
                                                <option value="9">SPEND(GT) - SARJANA PENDIDIKAN (PENGURUSAN SUKAN)</option>
                                                <option value="20">SPEND(GU) - SARJANA PENDIDIKAN BAHASA ARAB</option>
                                                <option value="11">SPEND(GV) - SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)</option>
                                                <option value="13">SPEND(KP) - SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)</option>
                       
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Academic Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range1">
                                                    <input type="text" required class="form-control" name="astart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="aend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Registration Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" required class="form-control" name="regstart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="regend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Subject Add Drop Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range1">
                                                    <input type="text" class="form-control" name="sadstart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="sadend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Subject withdraw Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range2">
                                                    <input type="text" class="form-control" name="swstart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="swend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Add Drop Acknowlegment Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range3">
                                                    <input type="text" class="form-control" name="adkstart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="adkend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Attendance Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range4">
                                                    <input type="text" class="form-control" name="adestart" required placeholder="Start Date">
                                                    <input type="text" class="form-control" name="adeend" required placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Coursework Mark Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range5">
                                                    <input type="text" class="form-control" required name="cmestart" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="cmeend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Final Exam Mark Entry Session<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range6" id="date-range6">
                                                    <input type="text" required class="form-control" name="femstart" placeholder="Start Date">
                                                    <input type="text" required class="form-control" name="femend" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <label class="control-label"><strong>Description</strong></label>
                                             
                                           <div><textarea required="" name="description" class="form-control" placeholder="" rows="5"></textarea></div>
                                        </div>
                                    </div>
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <input type="submit" name="submit" class="btn btn-lg btn-primary" value="Save">
                                         <a href="#" class="btn btn-lg btn-primary">Cancel</a>
                                    </div>
                                </div>
                                
                            </div>
                    </form>         
                        </div>
                    </div>
                </div>

           
                <!-- container-fluid -->
            </div>
            <!-- content -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			 $('#date-range1 ,#date-range2,#date-range3,#date-range4,#date-range5,#date-range6').datepicker({
              });
		});
	</script>
	</script>