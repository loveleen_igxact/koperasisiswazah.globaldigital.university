<div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Admission Application</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Admission Application</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="search ">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" class="form-control serch" required="" placeholder="Search by Name , ID No.."> 
                                                        <span class="serch_icon"><button class="btn btn-dark" type="button">Search</button></span>
                                                     </div>
                                                </div>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Application Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"> <strong>Application Status</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>intake</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Application Type</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                           <div class="form-group">
                                                <label><strong>Application Date</strong></label>
                                                <div>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                    <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                    </div>
                                                    <!-- input-group -->
                                                </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>
                                    </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Application List</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="addmission_add_application.html">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Programs
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                    <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                                </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                     <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                     <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                     <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                     <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td><span style="font-weight: 600; color: #eb2129;">Tiger Nixon</span><br>Identification certificate: <span style="font-weight: 600; color: #eb2129;">123456789 <br>Phone no: 123456789</td>
                                                    <td>1. Lorem Ipsum is simply dummy text of the<br> 
                                                        Status: Accepted
                                                    </td>
                                                    <td>----</td>
                                                    <td>Accepted</td>
                                                     <td><a href="">
                                                        <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                    </a>
                                                    <a href="">
                                                     <button data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="mdi mdi-eye" style="font-size:25px;"></i></button>
                                                    </a>
                                                    </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
       