<?php  
 $intke = array('1' => '2016/02' , '2' =>' 2017/02','3' => '2018/03', '4' =>'2019/02');

$programs  =  array(

   '10'  => 'SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)' ,
   '13' => 'SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)',
   
   '14' => 'SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)',
   "17" =>'SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)',
   '6' =>'SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)',
   
   '12' => 'SARJANA PENDIDIKAN (PENDIDIKAN KHAS)',
	'22' => 'SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)',
	'3' => 'SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)', 
	'2' => ' SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)',
	'5'=> 'SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)',
	'8'=>'SARJANA PENDIDIKAN (PENDIDIKAN SAINS)',
	'23'=>'SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)',
	'11'=>'SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)',
	'9' => 'SARJANA PENDIDIKAN (PENGURUSAN SUKAN)',
	"18" =>'SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)',
	"1" => 'SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)',
	"7" => 'SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)',
	"4" => 'SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)',
	"15" =>'SARJANA PENDIDIKAN (TESL)',
	"20" => 'SARJANA PENDIDIKAN BAHASA ARAB',
	"19" => 'SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT'
);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
  <link rel="shortcut icon" href="assets/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH;?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH;?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH;?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH;?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!---assign -->
    <!--add agency Modal -->
    <div class="modal fade" id="assign_prospect" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">Assign Prospect</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <div class="col-md-12">
                    <hr>
                    <p>1 Prostect to Assign</p>
                    <hr>
                </div>
                <div class="col-md-12 row ">
                <h6>Marketing Staff</h6>
                </div>
               <!--  <ul class="assign_pros">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul> -->
               
                    <div class="col-md-5 ">
                        <div class="form-group">
                            <input type="text" class="form-control s" required="" placeholder="try ABC 123  "> 
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="text" class="form-control s" required="" placeholder="try ABC 123  "> 
                        </div>
                    </div>
                     <div class="col-md-5 ">
                        <div class="form-group">
                           <textarea required="" class="form-control" rows="7"></textarea>
                        </div>
                    </div>
                      <div class="col-md-2 text-center" style="margin: 50px auto;">
                        <span class="df"><i class="ti-arrows-horizontal"></i></span>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <textarea required="" class="form-control" rows="7"></textarea>
                        </div>
                    </div>

                
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-lg">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!--end-->
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                    <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary ">Advance Search</button>
                            </a>
                            </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <strong class="admin_name">Admin</strong> <img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a>  <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a></div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
       <?php $this->load->view('layout/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Unassigned Prospects</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Unassigned Prospects</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="search col-md-12" style="padding: 0px;">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" class="form-control s" required="" placeholder="Filter by Prospect Name"> 
                                                        <span class="serch_icon"><button class="btn btn-dark" type="button">Search</button></span>
                                                     </div>
                                                </div>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            More Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Nationality</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Race</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Nationality</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Country</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>State</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="button col-md-12 text-center">

                                            <a class="btn btn-primary" gradient="peach type=" button"="" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">

                                             Search

                                            </a>

                                        </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
								
                            </div>
                            <!---card 2 table-->
							
								<div class="card m-b-30">
									<div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Prospect List</h4>
                                        </div>
                                           <div class="add_more">
                                                <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                                <button type="button" class="btn btn-dark waves-effect waves-light" data-toggle="modal" data-target="#assign_prospect">Assign</button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="ds">
                                            <button type="button" class="btn btn-info waves-effect waves-light main" onclick='selectAll()' value="Select All">Select All</button>
                                            <a href="#">
                                                <button type="button" class="btn btn-dark waves-effect waves-light" onclick='UnSelectAll()' value="Unselect All">Clear</button>
                                            </a>
                                        </div>  
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                    <th></th>
                                                <th>No</th>
                                                    <th class="th-sm">Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                   
                                                    <th class="th-sm">Intake
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Programs
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Stages
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Created By
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Created At
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
												<?php foreach($prospects_data  as $value) { ?>
												
                                                <tr>
                                                    <td><div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="materialUnchecked"  name="acs" value="Mobile">
                                                    
                                                    </div></td>
                                                    <th scope="row">1</th>
                                                    <td><span style="font-weight: 600; color: #eb2129;"><?php echo $value['name'];  ?></span> <br>Phone no: <?php echo $value['phone'];  ?></td>
                                                    
                                                    <td> <?php echo @$intke[$value['Intake']] ;  ?></td>
                                                    <td><?php echo @$programs[$value['p1_program']];  ?></td>
                                                    <td>Leads</td>
                                                    <td>Test</td>  
                                                    <td><?php  echo date('Y-m-d', strtotime($value['created_at'])); ?></td> 
                                                    <td>
<!--
														<a href=""><span><i class="mdi mdi-account-box" style="font-size:29px;" ></i></span></a>
                                                    <a href=""> <span><i class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span></a>
-->
                                                    </td>
                                                </tr>
                                            <?php  } ?>
                                           

                                            </tbody>
                                        </table>

									</div>
								</div>
							
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
   
   

<script type="text/javascript">
            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

            <!-- jQuery  -->
     <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH;?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH;?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH;?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo JS_PATH;?>/pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH;?>js/app.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.js"></script>
</body>
</html>
