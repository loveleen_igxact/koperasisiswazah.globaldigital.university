<!DOCTYPE html>
<html lang="en">


<body>
   
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
  
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
      
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Admission / Document Checklist </h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Admission </li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Document Checklist </h4>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Document
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Submitted
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 

                                //  PRINT_R($posts);

                                    foreach($doc_data as $rows): ?>
                                                <tr>
                                                    <th scope="row"><?php echo $rows->document_id; ?></th>
                                                    <td><?php echo $rows->name; ?></td>
                                                    <td><strong><i class="fas fa-check"></i></strong></td>
                                                </tr>
                                                <?php endforeach; ?>
                                         
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
        </div>
    </div>
            <!-- content -->
            
</body>
</html>