<?php  
$faculty =  array("1" => "GG - FAKULTI PENDIDIKAN");
$Session =  array("1" => "2017/03 - MAC 2017",
				"2" => "2017/03 - MEI - JUN 2017",
				"3" => "2018/03 - MAC 2018",
				"4" =>"2018/03 - MARCH - JULY 2018");
				
				
	$course =  array("1" =>"GGGA6213 - TEORI DAN AMALAN",
   "2"=>"GGGA6243 - PERANCANGAN ",
   "3"=>"GGGB6012 - PENULISAN AKADEMIK 1 ",
   "4"=>"GGGB6013 - KAEDAH PENYELIDIKAN 1 ",
   "5"=>"GGGB6022 - PENULISAN AKADEMIK2 ",
   "6"=>"GGGB6023 - KAEDAH PENYELIDIKAN 2 ",
   "7"=>"GGGB6133 - TEORI-TEORI KESIHATAN MENTTAL", 
   "8"=>"GGGB6213 - PSIKOLOGI PENGAJARAN DAN PEMBELAJARAN LANJUTAN ",
   "9"=>"GGGB6223 - PSIKOLOGI PERKEMBANGAN LANJUTAN ",
   "10"=>"GGGB6243 - DINAMIKA KUMPULAN DAN PERFUNGSIAN MENTAL ",
   "11"=>"GGGB6253 - TEORI DAN AMALAN KAUNSELING ",
   "12"=>"GGGB6263 - ISU-ISU DALAM PENDIDIKAN KHAS", 
   "13"=>"GGGB6273 - TEORI PERKEMBANGAN KANAK-KANAK ",
   "14"=>"GGGB6293 - TEORI DAN PROSES PERKEMBANGAN KANAK-KANAK DENGAN KEPERLUAN KHAS ",
   "15"=>"GGGB6333 - TEORI DALAM PENGUKURAN DAN PENILAIAN ",
   "16"=>"GGGB6383 - PERKEMBANGAN KERJAYA ",
   "17"=>"GGGB6413 - PSIKOLOGI KEMAHIRAN BERFIKIR ",
   "18"=>"GGGB6423 - PSIKOLOGI SILANG BUDAYA" );
			




?> 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
    <link rel="shortcut icon" href="assets/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH; ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="modal fade" id="add_class" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <form action="<?php echo  base_url('Acadmic/add_classes');?> " method="POST" >
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">New Class</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Academic Session<span style=" color: red;">*</span></strong></label>
                            <select name="academic_session" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option value="">Select</option>
                                   <option value="1">2017/03 - MAC 2017</option>
									<option value="2">2017/03 - MEI - JUN 2017</option>
									<option value="3">2018/03 - MAC 2018</option>
									<option value="4">2018/03 - MARCH - JULY 2018</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Faculty<span style=" color: red;">*</span></strong></label>
                            <select name="faculty" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                <option value="">Select</option>
							  <option value="1">GG - FAKULTI PENDIDIKAN</option>
                            </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"><strong> Course<span style=" color: red;">*</span></strong></label>
                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="course" required="" >
                               <option value="">Select</option>
                               <option value="1">GGGA6213 - TEORI DAN AMALAN KURIKULUM</option>
							   <option value="2">GGGA6243 - PERANCANGAN KURIKULUM</option>
							   <option value="3">GGGB6012 - PENULISAN AKADEMIK 1</option>
							   <option value="4">GGGB6013 - KAEDAH PENYELIDIKAN 1</option>
							   <option value="5">GGGB6022 - PENULISAN AKADEMIK2</option>
							   <option value="6">GGGB6023 - KAEDAH PENYELIDIKAN 2</option>
							   <option value="7">GGGB6133 - TEORI-TEORI KESIHATAN MENTTAL</option>
							   <option value="8">GGGB6213 - PSIKOLOGI PENGAJARAN DAN PEMBELAJARAN LANJUTAN</option>
							   <option value="9">GGGB6223 - PSIKOLOGI PERKEMBANGAN LANJUTAN</option>
							   <option value="10">GGGB6243 - DINAMIKA KUMPULAN DAN PERFUNGSIAN MENTAL</option>
							   <option value="11">GGGB6253 - TEORI DAN AMALAN KAUNSELING</option>
							   <option value="12">GGGB6263 - ISU-ISU DALAM PENDIDIKAN KHAS</option>
							   <option value="13">GGGB6273 - TEORI PERKEMBANGAN KANAK-KANAK</option>
							   <option value="14">GGGB6293 - TEORI DAN PROSES PERKEMBANGAN KANAK-KANAK DENGAN KEPERLUAN KHAS</option>
							   <option value="15">GGGB6333 - TEORI DALAM PENGUKURAN DAN PENILAIAN</option>
							   <option value="16">GGGB6383 - PERKEMBANGAN KERJAYA</option>
							   <option value="17">GGGB6413 - PSIKOLOGI KEMAHIRAN BERFIKIR</option>
							   <option value="18">GGGB6423 - PSIKOLOGI SILANG BUDAYA</option>
							
							   
					</select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Class<span style="color: red;">*</span></strong></label>
                       <input type="text" name="class" class="form-control serch" required="" placeholder="Example:ABC 123-1">
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong>Lecturer</strong> </label>
                        <input type="text" name="lecturer" class="form-control serch"  placeholder="Please Select">
                     </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><strong> Total Student</strong></label>
                        <input type="number" name="total_student" class="form-control serch"  placeholder="">
                     </div>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong> Duplicate</strong></label><br>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">

                        <label class="btn btn-full active">
                        <input type="radio" name="duplicate" id="option2"  value="1"> Yes </label>
                        <label class="btn btn-full ">
                        <input type="radio" name="duplicate" id="option3" value="0"> No</label>
                    </div>
                </div>
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            
          </div>
        </div>
      </div>
      
      </form>
    </div>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                   <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary ">Advance Search</button>
                            </a>
                            </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <strong class="admin_name">Admin</strong><img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a>  <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a></div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
       <?php $this->load->view('layout/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Academic / Class</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Classes</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="add_more">
                                            <button type="button" class="btn btn-dark waves-effect waves-light" data-toggle="modal" data-target="#add_class">Add New</button>
                                        </div> 
                                        <div class="clearfix"></div>
                                        
								<form action="<?php echo base_url('Acadmic/classes'); ?>" method="POST">

                                    <div class="row">
                                        <!-- / Collapse buttons -->
<!--
-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                        <label class="control-label"><strong>Faculty<span style="color: red;">*</span></strong></label>
                                                            <select name="faculty" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="faculty"> 
                                                                <option>Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
																<?php
															
																foreach($faculty_data as $faculties) {


																	?>
																	<option value="<?php echo $faculties['id']; ?>"><?php echo $faculties['name'];  ?></option>
																<?php  } ?>  
																
                                                            </select>
                                                    </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                        <label class="control-label"><strong>Academic Session<span style="color: red;">*</span></strong></label>
                                                            <select name="academic_session" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                                                                <option value="">Select</option>
                                                              
																<?php foreach($academic_session as $course_n) { ?>
																	<option value="<?php echo $course_n['id']; ?>"><?php echo $course_n['name'];  ?></option>
																<?php  } ?>  
                                                            </select>
                                                    </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                        <label class="control-label"><strong>Course</strong></label>
                                                            <select name="course" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option value="">Select</option>
														<?php foreach($course_data as $coursest) { ?>
															<option value="<?php echo $coursest['id']; ?>"><?php echo $coursest['name'];  ?></option>
														<?php  } ?>
													</select>
                                                    </div>
                                        </div>
                                    </div>
                                        <div class="button_ text-center">
                                            <button class="btn btn-primary" type="submit"> Search</button>
                                        </div>

                                </div>
<!--
                              
-->
                            </div>
                            
                              </form>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Classes</h4>
                                        </div>
                                          <!--  <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="add_intake.html">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New Intake</button>
                                            </a> 
                                        </div> -->
                                        <div class="clearfix"></div>
                                        <hr>
                                        <?php 
                                        //echo  '<pre>';print_r($class_list);
                                        if(isset($class_list)) {  ?>
                                       <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true"> Academic Session</i>
                                                    </th>
                                                     <th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true">Faculty</i>
                                                    </th>
                                                    <th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true">Course</i>
                                                    </th>
                                                    <th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true">Lecturer</i>
                                                    </th>
                                                    <th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true">No of Student</i>
                                                    </th>
													<th class="th-sm">
                                                        <i class="fa fa-sort float-right" aria-hidden="true">Action</i>
                                                    </th>
													
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php  
												$i =1;
												foreach($class_list as $class) {   ?>
                                                 <tr id="current_row<?php echo $class['id'];  ?>">
                                                <td><?php echo $i;  ?></td>
                                                    <td class="th-sm">
                                                       <?php echo $Session[$class['academic_session']] ; ?>
                                                    </td>
                                                     <td class="th-sm">
                                                         <?php echo $faculty[$class['faculty']] ; ?>
                                                    </td>
                                                    <td class="th-sm">
                                                         <?php echo $course[$class['course']] ; ?>
                                                    </td>
                                                    <td class="th-sm">
                                                         <?php echo $class['lecturer'] ; ?>
                                                    </td>
                                                    <td class="th-sm">
                                                         <?php echo $class['total_student'] ; ?>
                                                    </td>
													<td class="th-sm">
                                                         <a href="#" class="btn-primary btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:20px;"> </i></a>
                                                        <a href="<?php echo base_url('acadmic/student/'.$class['id']); ?>" class="btn-info btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Manage Student"><i class="fas fa-users" style="font-size:20px;"></i></a>
                                                        <a href="<?php echo base_url('acadmic/mark/'.$class['id']); ?>" class="btn-info btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assessment Mark Entry"><i class="mdi mdi-account-card-details" style="font-size:20px;"></i></a>
                                                        <a href="<?php echo base_url('acadmic/attendances/'.$class['id']); ?>" class="btn-info btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Attendance"><i class="fas fa-calendar-alt" style="font-size:20px;"></i></a>
                                                        <a href="<?php echo base_url('acadmic/attendances/'.$class['id']); ?>" class="btn-info btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Course Status"><i class="fas fa-list" style="font-size:20px;"></i></a><br>
                                                        <a href="javascript:void(0)" onclick="deleteRow(<?php echo $class['id']; ?>, 'classes')" class="btn-danger btn mt-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-delete-forever" style="font-size:20px;"></i></a>
                                                    </td>
                                                </tr>
                                                    <?php $i++; } ?>
                                            </tbody>
                                        </table> 
                                        <?php } ?>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript">
	

     function deleteRow(id,table) {
         var url  = '<?php echo  base_url();?>';

         var x=confirm("Are you sure to delete record?")
         if (x) {
             $.ajax({
                 data: { 'id' : id,'table':table},
                 type: "post",
                 url: url + "Application/deleteRow",
                 success: function(data){
                     $('#current_row'+id).remove();
                 }
             });
             return true;
         } else {
             return false;
         }


     }
</script>

      <!-- jQuery  -->
    <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>
    <script src="<?php echo JS_PATH;?>plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH; ?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo CSS_PATH;?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH;?>js/app.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.js"></script>
</body>
</html>
