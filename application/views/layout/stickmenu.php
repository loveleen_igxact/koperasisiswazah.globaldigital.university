   <div class="static_mneu_bar">
            <div class="slimscroll-menu" id="remove-scrolls">
                <div id="sidebar-menu_2">
                    <ul class="metismenu" id="side-menu_2">
                        <!-- <li class="menu-title">Main</li> -->
                        <li><a href="<?php echo base_url('application/profile/'.$id) ; ?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Overview</span></a></li>
                        <li><a href="<?php echo base_url('application/profile/'.$id) ; ?>" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Personal</span></a></li>
                        <li><a href="<?php echo base_url('application/document/'.$id) ; ?>" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Documents</span></a></li>
                        <li><a href="<?php echo base_url('application/contact/'.$id) ; ?>" class="waves-effect"><i class="mdi mdi-contact-mail"></i><span> Contacts</span></a></li>
                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i><span> Marketing <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url('application/marketing/'.$id) ; ?>">Marketing</a></li>
                                <li><a href="<?php echo base_url('application/email/'.$id) ; ?>">Emails</a></li>
                                <li><a href="<?php echo base_url('application/Letter/'.$id) ; ?>">Letters</a></li>
                                <li><a href="<?php echo base_url('application/fine/'.$id) ; ?>">Fines</a></li>
                            </ul>
                        </li>

                         <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-human-handsup"></i><span> Admissions <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                                 <li><a href="<?php echo base_url('application/addm_app/'.$id); ?>">Application</a></li>
                                <li><a href="<?php echo base_url('application/register_profile/'.$id); ?>">Registration</a></li>
                                <li><a href="<?php echo base_url('application/register_sem/'.$id); ?>">Semester Registration</a></li>
                                <li><a href="<?php echo base_url('application/timeline/'.$id); ?>">Timeline</a></li>
                                <li><a href="<?php echo base_url('application/add_past/'.$id); ?>">Past Eduction</a></li>
                                <li><a href="<?php echo base_url('application/letter/'.$id) ; ?>">Letters</a></li>
                                <li><a href="<?php echo base_url('application/fine/'.$id) ; ?>">Fines</a></li>
                            </ul>
                        </li>

                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Student Portal</span></a></li>

                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-weight"></i><span> Finance<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                                <li><a href="javascript:void(0)">General</a></li>
                                <li><a href="javascript:void(0)">Ledger</a></li>
                                <li><a href="javascript:void(0)">Collection</a></li>
                                <li><a href="javascript:void(0)">Account Statement</a></li>
                                <li><a href="javascript:void(0)">Bank Account</a></li>
                                <li><a href="javascript:void(0)">Sponsors</a></li>
                                <li><a href="javascript:void(0)">Fee Structure</a></li>
                                <li class=""><a href="javascript:void(0)">Installments </a></li>
                                <li><a href="javascript:void(0)">Fines </a></li>
                            </ul>
                        </li>
                         <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-line"></i><span> Academic<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                             <li><a href="javascript:void(0)">Programs</a></li>
                                <li><a href="javascript:void(0)">Credit Transfer</a></li>
                                <li><a href="javascript:void(0)">Courses</a></li>
                                <li><a href="javascript:void(0)">Fines</a></li>
                            </ul>
                        </li>

                        <li><a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-briefcase-check"></i><span> Exams<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                            <ul class="submenu">
                              <li><a href="publish_result_slip.html">Publish Result Slip</a></li>
                                <li><a href="javascript:void(0)">Results</a></li>
                                <li><a href="javascript:void(0)">Fines</a></li>
                            </ul>
                        </li>
                        <li><a href="note.html" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-primary badge-pill float-right"></span> <span> Notes</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
