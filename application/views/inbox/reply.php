<div class="content-page" >
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                       <!--  <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Inbox</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active">Inbox</li>
                        </ol> -->
                    </div>
                </div>
            </div>
        </div>
                    <!-- end row -->
                <div class="mb-3">
                    <div class="card">
                        <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="To" value="<?php echo $get_student_info['email']; ?>">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control ckeditor" name="reply" placeholder="Enter News Letter" id="message" style="width:100%; height:100px!important; resize:none;" required ></textarea>
                            </div>  
                        </div>
                    </div>
                    <div class="btn-toolbar form-group mb-0">
                        <div class="">
                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light"><span>Send</span> <i class="fab fa-telegram-plane m-l-10"></i></button>
                            <!-- <button type="submit" name="save_draft" class="btn btn-primary waves-effect waves-light"><span>Save As Draft</span> <i class="fas fa-pen-square"></i></button> -->
                        </div>
                    </div>
                    </form>
                </div>
            
        </div>
                <!-- end Col-9 -->
        </div>
                
                </div>
					
                </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
            <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
            <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        jQuery(document).ready(function() {

            $('.summernote').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });
        });
    </script>
       <script type="text/javascript">
        $(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
    </script>
       
 <script type="text/javascript">
            function selectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('acs');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>