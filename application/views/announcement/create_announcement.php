<?php $session_data = $this->session->userdata['loggedInData']; ?>
<style type="text/css">
    .image-upload img {
    cursor: pointer;
}
    .image-upload >input {
    display: none;
}
    .image-upload p {
    position: absolute;
    top: 33px;
    left: 69px;
    font-size: 14px;
}

.image-upload {
    background-color: #f9f9fb;
    padding: 8px;
    border: 1px solid #cccccc;
    position: relative;
}
.left_right_content p {
    font-size: 15px;
}
.uploading_images {
    background-color: #f9f9fb;
    margin: 10px auto;
    padding: 7px;
    border: 1px solid #cccc;
}
.no_margin_bottom {
    margin-bottom: 4px;
}
.uploading_images {
    background-color: #f9f9fb;
    margin: 10px auto;
    padding: 7px;
    overflow-y: scroll;
    border: 1px solid #cccc;
    overflow-x: hidden;
    height: 223px;
}
.outer_formsts{
    background-color: #f4f4f4;
    padding: 6px;
}
</style>
<link href="<?php echo  CSS_PATH; ?>css/bootstrap_multiselect.css" rel="stylesheet" type="text/css">
<div class="content-page">
    <div class="content">
         <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                       <!--  <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>UPLOAD DOCUMENTS</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                            <li class="breadcrumb-item active">Upload Documents</li>
                        </ol> -->
                    </div>
                </div>
            </div>
        </div> 
    <div class="card m-b-30">
        <div class="card-body">
            <form action="<?php echo base_url(); ?>announcement/create_announcement" class="" enctype="multipart/form-data" method="post">
            <h4 class="mt-0 header-title">Create Announcement</h4>
            <div class="m-b-30">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left_right_content pt-3">
                            <p class="float-left" >Upload Files </p>
                            <p class="float-right">Size Limit 1 GB</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="image-upload">
                            <label for="file-input">
                                <img src="http://lmsmalaysia.com/public/images/up_folder.png">
                                <p><span style="color: #2091de;">Browse</span>Your files</p>
                            </label>
                            <input type="file" id="file-input" name="fileToUpload[]"  multiple="">
                        </div>
                        
                        <div class="form-group">
                            <label>Subject*</label>
                            <input type="text" class="form-control"  placeholder="Enter Title" name="subject"> 
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" class="form-control" rows="4"  placeholder="Enter Description" name="message"></textarea>
                        </div>
                        <div class="form-group pt-3">
                            <label><input type="checkbox" id="formButton">&nbsp;Publish Date</label>
                    <div class="form-group " id="form-toggle" style="display: none;">
                    <!-- <label><strong>Publish Date<span class="sa" style="color: red;">*</span></strong></label> -->
                    <div class="input-daterange input-group col-lg-12" id="date-range">
                         <input type="text" class="form-control" placeholder="From" id="datepicker-autoclose" name="from_date" required="" autocomplete="off">
                         <div class="input-group-append">
                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                         </div>
                         <input type="text" class="form-control" placeholder="To" id="datepicker-autoclose1" name="to_date" required="" autocomplete="off">
                         <div class="input-group-append">
                             <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                         </div>
                        <!-- <input type="text" class="form-control" placeholder="From" name="from_date">
                        <input type="text" class="form-control" placeholder="To" name="to_date"> -->
                    </div>
                </div>
                        </div>
                        <div class="form-group pt-3">
                            <label class="cantrol-label"><strong> Publish To :</strong> </label>
                                <div class="form-check-inline">
                                    <label class="form-check-label"> Staff &nbsp </label>
                                    <input type="checkbox"  name="staff_tab" class="form-check-input"   value="staff">
                                    <label class="form-check-label"> Student &nbsp </label>
                                    <input type="checkbox" class="form-check-input"  name="student_tab" value="student">
                                </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4">
                            <div class="outer_formsts">
                                <h6><strong> Supported file formats</strong></h6>
                                <p><strong>Image: jpg - png - gif</strong></p>
                                <p>Document: ppt - pptx - xls - doc<br>
                                    docx - rtf - pdf</p>
                                <p>Video: wav - wma - mp3 - mov - avi<br>
                                    mpeg - wmv mp4 - flv</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-left m-t-15">
                <button type="submit" name="submit" class="btn color_another btn-lg waves-effect waves-light">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>

<script type="text/javascript">
  $(function () {
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
        $('#btnSelected').click(function () {
            var selected = $("#lstFruits option:selected");
            var message = "";
            selected.each(function () {
            });
        });
    });
  $("#formButton").click(function(){
            $("#form-toggle").toggle();
        });

  $(function() {

    $('#datepicker-autoclose').datepicker({

        minDate : new Date()

    });

    });
  $(function() {

    $('#datepicker-autoclose1').datepicker({

        minDate : new Date()

    });

    });
</script>