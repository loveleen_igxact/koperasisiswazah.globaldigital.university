    <style type="text/css">
        ul.assig_list li::before {
        content: '';
        height: 6px;
        width: 6px;
        background: #606cb3;
        display: block;
        position: absolute;
        transform: rotate(45deg);
        top: 6px;
        left: 0;
    }

    ul.assig_list li {
        margin: 0;
        margin-bottom: 1em;
        padding-left: 1.5em;
        list-style: none;
        position: relative;
    }
    ul.assig_list {
        padding: 0px;
    }
    ul.icons_all i {
    font-size: 21px;
    color: #606cad;
    }
    .font-size {
        font-size: 14px;
        letter-spacing: 2px;
    }
    .td_img img {
        border: 1px solid #e7eaed;
        width: 100%;
        padding: 7px;
    }
    .icon_size i {
        font-size: 18px;
        margin-top: 6px;
    }
    .dsd {
    padding: 0px;
    }
    .dsd li {
    display: inline-block;
    vertical-align: top;
    }
    .dsd li img {
    width: 51px;
    height: 51px;
    }
    .li_bg {
        background-color: #f3f3f3;
        border-right: 1px solid #e4e4e4;
    }
    .li_bg a {
        font-size: 14px;
    }
    .nav-pills .nav-link, .nav-tabs .nav-link {
        color: #606cad;
        font-weight: 500;
    }
    #profile {
        height: 513px;
        overflow-y: scroll;
    }
    .add_more {
        font-size: 15px;
    }
    .right-border {
    border-right: 2px solid #b1b1b1;
    }
    </style>
     <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <!--<h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Application</h4>-->
                                    <!--<ol class="breadcrumb">-->
                                    <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                    <!--    <li class="breadcrumb-item active">New Application</li>-->
                                    <!--</ol>-->
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                       <!--  <div class="add_more">
                                            <ul class="icons_all">
                                                <li>
                                                    <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="">
                                                        <img src="http://lmsmalaysia.com/public/images/edit_edit.png"> Edit 
                                                </a>
                                                </li>
                                                <li>
                                                    <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="">
                                                        <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"> Delete
                                                </a>
                                                </li>
                                            </ul>
                                        </div> -->
                                        <div id="prospect"></div>
                                        <div style="clear:both"></div>
                                            <div class="row">
                                                <div class="card-body">
                                                <h4 class="mt-0" style="text-transform: uppercase; letter-spacing: 2px;">Nam eget laoreet sem Nullam rhoncus sed metus</h4>
                                                <p class="font-size"><span><img src="http://lmsmalaysia.com/public/images/calander.png">&nbsp <strong>Start:</strong>  <span class="right-border">10/05/2018 &nbsp&nbsp</span>    <strong>&nbsp&nbspExpire:</strong> 11/02/2019</span>
                                                   <p><strong>Status : <span style="color: #febb14"> Pending</span></strong></p>
                                                <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item li_bg">
                                                    <a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true">OVERVIEW</a>
                                                </li>
                                                <li class="nav-item li_bg">
                                                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false">ATTACHMENTS <span class="view_20">(20)</span></a>
                                                </li>
                                                <li class="nav-item li_bg">
                                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-selected="false">SUBMISSION <span class="view_20">(13)</span></a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane p-3 active show" id="home" role="tabpanel">
                                                   <!-- <div class="list_task">
                                                        <h5>Lorem Ipsum is simply dummy text </h5>
                                                        <ul class="assig_list">
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        </ul>
                                                    </div> -->
                                                    <div class="pera_graph">
                                                        <h5>Lorem Ipsum is simply dummy text </h5>
                                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                    </div>
                                                    <div class="list_task">
                                                        <h5>Lorem Ipsum is simply dummy text </h5>
                                                        <ul class="assig_list">
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                            <li>Lorem Ipsum has been the industry's standard dummy </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                              <td style="width: 7%" class="td_img">
                                                                <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                              </td>
                                                              <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                              </td>
                                                              <td class="text-right">
                                                                <ul class="icons_all">
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 7%" class="td_img">
                                                                    <img class="side_image d-flex" src="http://lmsmalaysia.com/public/images/dreft.png" alt="user" width="60" >
                                                                </td>
                                                                <td class="text-left">
                                                                  <h5 class="mt-0 font-16">Aenean vulputate velit eu nibh lobortis</h5>
                                                                  <p>Uploaded 18/08/219 <br>29GB</p>
                                                                </td>
                                                                <td class="text-right">
                                                                    <ul class="icons_all">
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Download" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/download_icon.png"></span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" href="javascript:void(0)"> <span><img src="http://lmsmalaysia.com/public/images/delete_icon_1.png"></span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane p-3" id="messages" role="tabpanel">
                                                    <p class="mb-0">
                                                    <div class="table-responsive">
                                                        <table id="dtBasicExample" class="table  table-hover " cellspacing="0" width="100%">
                                                                <thead class="black white-text">
                                                                    <tr>
                                                                        <th class="th-sm">Name
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                        <th class="th-sm">From
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                        <th class="th-sm">Description
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                        <th class="th-sm">Attachemnts
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                        <th class="th-sm">Status
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                        <th class="th-sm no_sort">Action
                                                                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #4fb726"><strong> Reviewed</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #ec5f1d"><strong>  Expired</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #ec5f1d"><strong>  Expired</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #4fb726"><strong> Reviewed</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #f0d130;"><strong> Pending</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <ul class="dsd">
                                                                                <li>
                                                                                    <img class="d-flex align-self-start rounded mr-3" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg" alt="Generic placeholder image" height="80" width="80"></li>
                                                                                <li>
                                                                                    SIAW SING ONG SIAW SING
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                        <td>09/05/2017</td>
                                                                        <td>Lorem Ipsum is simply dummy text of the printing and<br> typesetting industry. Lorem Ipsum has </td>
                                                                        <td >
                                                                            <a style="color: blue;" href="#">
                                                                                2 files
                                                                            </a>
                                                                        </td>
                                                                        <td><span class="info_info" style="color: #4fb726"><strong> Reviewed</strong></span></td>
                                                                        <td>
                                                                            <ul class="icons_all">
                                                                                <li>
                                                                                   <a href="#">
                                                                                        <span><i class="fas fa-tasks"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>   
                                                                                    <a href="#">
                                                                                        <span><i class="fas fa-envelope"></i></span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>