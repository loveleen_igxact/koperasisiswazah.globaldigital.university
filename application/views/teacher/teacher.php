<style>
    .sds input {
    display: none;
}
table.dataTable tbody th, table.dataTable tbody td {
    padding: 8px 17px !important;
}
.dataTables_wrapper .dataTables_length {
    float: left;
    float: right !important;
    position: absolute;
    right: 0px;
    top: 14px;
}
.table td {
    width: 12.5%;
}
</style>
<?php $session_data = $this->session->userdata['loggedInData']; ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>  Teacher</h4> -->
                        <!--<ol class="breadcrumb">-->
                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                        <!--    <li class="breadcrumb-item active"> Teacher</li>-->
                        <!--</ol>-->
                    </div>
                </div>
            </div>
                    <!-- end row -->
            <div class="col-12">
                <?php echo $this->session->flashdata('msg'); ?>
                <!-- <div class="card m-b-30">
                    <div class="card-body">
                        <div id="prospect"></div>
                        <div style="clear:both"></div>
                         <div class="search col-md-12" style="padding: 0px;">
                                        <div class="form-group">
                                            <label>Search</label>
                                            <input type="text" class="form-control s" required="" placeholder="Search Subject..."> 
                                            <span class="serch_icon"><button class="btn btn-success" type="button">Search</button></span>
                                         </div>
                                    </div>
                            <div class="button">
                                <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                  Announcement More Filter
                                </a>
                            </div>
                        <div class="row" style="padding-top: 10px;">
                            <div class="collapse col-md-12" id="collapseExample">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                                <label>Publish Date</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="start" placeholder="From">
                                                    <input type="text" class="form-control" name="end" placeholder="To">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"> <strong>Posted By</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label"><strong> Published To</strong> </label><br>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="defaultInline1">
                                            <label class="custom-control-label" for="defaultInline1">Staff</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" id="defaultInline2">
                                            <label class="custom-control-label" for="defaultInline2">Student</label>
                                        </div>
                                    </div>
                                        
                                    </div>
                                    <div class="button col-md-12 text-center">
                                        <a class="btn btn-primary" gradient="peach type=" button"="" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">Search</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div> -->
                                        <!-- / Collapsible element -->  
                </div>
            </div>
                            <!---card 2 table-->
                            
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="hearding">
                            <h4 class="mt-0 header-title left_side">Teacher </h4>
                        </div>
                        <?php  
                            if ($session_data['user_type'] != '3') { ?>
                        <div class="add_more">
                            <!-- <button type="button" class="btn btn-info waves-effect waves-light">Export</button>  -->
                            <a class="btn color_another waves-effect waves-light btn-addon" href="<?php echo base_url('teacher/create'); ?>">
                                <i class="fa fa-plus"></i>Add New
                            </a>
                        </div> 
                    <?php } ?>
                        <div class="clearfix"></div>
                        <!-- <hr> -->
                        <table id="dtBasicExample" class="table table-hover pt-5 " cellspacing="0" width="100%">
                            <thead class="black white-text">
                                <tr>
                                    <!-- <th class="no_sort">No</th> -->
                                    <th class="th-sm">Name
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm">Department
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm">Class
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm">Subject
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm">Email
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm">ID
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    
                                    <?php  
                                        if ($session_data['user_type'] != '3') { ?>
                                    <th class="th-sm">Status
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                    <th class="th-sm no_sort">Action
                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                    </th>
                                <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($teachers_data)) {
                                    $i = 1;
                                    foreach ($teachers_data as $key => $value) { 
                                       $get_departments = $this->CI->get_data_by_id('departments','id',$value['department_id']);
                                ?>
                                <tr id="current_row<?php echo $value['id']; ?>">
                                    <!-- <th scope="row"><?php echo $i; ?></th> -->
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo $get_departments['name']; ?></td>
                                     <td class="text-center"><?php if($value['class_id'] == 1){ echo "first";}elseif($value['class_id'] == 2){echo "second";}elseif($value['class_id'] == 3){echo "third";} ?>
                                     </td>
                                     
                                     <td class="text-center"><?php if($value['subject_id'] == 1){ echo "java";}elseif($value['subject_id'] == 2){echo "php";}elseif($value['subject_id'] == 3){echo "android";} ?>
                                     </td>
                                     <td><a target="_blank" href="mailto:<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a></td>
                                    <td><?php echo $value['id_no']; ?></td>
                                    
                                    <?php  
                                        if ($session_data['user_type'] != '3') { ?>
                                    <td>
                                        <div class="btn-group sds ">
                                            <input type="hidden" name="" value="<?php echo $value['user_id']; ?>" id="staff_id">
                                            <label class="btn btn-full <?php if($value['status'] == '1') { ?> active <?php } ?>">
                                            <input type="radio" name="options" id="option2" onclick="active(<?php echo $value['user_id']; ?>);"> Active </label>
                                            <label class="btn btn-full <?php if($value['status'] == '0') { ?> active <?php } ?>">
                                            <input type="radio" name="options" id="option3" onclick="inactive(<?php echo $value['user_id']; ?>);"> Inactive</label>
                                        </div>
                                    </td>
                                    <td>
                                        <ul class="icons_all">
                                            <li>
                                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805" href="<?php echo  base_url('teacher/edit?id='.$value['user_id']); ?>">
                                                    <img src="http://lmsmalaysia.com/public/images/edit_edit.png">
                                                </a>
                                            </li>
                                            <li>
                                                <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'staffs')">
                                                <img src="http://lmsmalaysia.com/public/images/delete_icon_1.png">
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php $i++; } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        //alert(document.location.origin);
        var base_url = document.location.origin;
        // $("#option2").click(function(){
        //     $.ajax({
        //         type: "POST",
        //         url: 'update_status',
        //         data: {status:1},
        //         success: function(html){
        //             alert(html)
        //         }
        //     });
        // });
        function active(val) //staff id
        {
            $.ajax({
                type: "POST",
                url: base_url+'/Teacher/update_status',
                data: {status:1,id:val},
                success: function(html){
                    location.reload();
                }
            });
        }
        function inactive(val) //staff id
        {
            $.ajax({
                type: "POST",
                url: base_url+'/Teacher/update_status',
                data: {status:0,id:val},
                success: function(html){
                    location.reload();
                }
            });
        }
    </script>
</body>
</html>