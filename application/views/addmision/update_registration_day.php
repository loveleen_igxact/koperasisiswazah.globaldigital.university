 <style type="text/css">
        .local {
            background-color: #edf1f2;
            position: absolute;
            bottom: 0px;
            left: 0px;
            padding: 9px;
            width: 97px;
            text-align: center;
            border: 1px solid #cfdadd;
        }
        .local_rm {
            background-color: #edf1f2;
            position: absolute;
            bottom: 0px;
            left: 96px;
            padding: 9px;
            width: 56px;
            text-align: center;
            border: 1px solid #cfdadd;
        }
        .locals {
            background-color: #edf1f2;
            position: absolute;
            bottom: 0px;
            left: 0px;
            padding: 9px;
            width:105px;
            text-align: center;
            border: 1px solid #cfdadd;
        }
        .local_rma {
            background-color: #edf1f2;
            position: absolute;
            bottom: 0px;
            left: 105px;
            padding: 9px;
            width: 56px;
            text-align: center;
            border: 1px solid #cfdadd;
        }
    </style>

        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Intake / Registration Days/ Update Registration Day</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Update Registration Day</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Intake</strong></label>
                                                <input type="text" class="form-control" value="2019/02" placeholder="" readonly="" name="">
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Registration name<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control " required="" value="2019/03 - UKM BANGI" placeholder=""> 
                                            <small>You Can Use: intake name as prefix,Example:2017/07-First intake</small> 
                                         </div>
                                    </div> 
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Campus<span style="color: red;">*</span></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="AK">August</option>
                                                        <option value="HI">option</option>
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Program<span style="color: red;">*</span></strong></label>
                                                <input type="text" class="form-control " required="" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="position: relative;">
                                            <label class="control-label">Deposit</label>
                                            <span class="local">Local</span>
                                            <span class="local_rm">RM</span>
                                            <input type="text" class="form-control local_deposit " required="" value="" placeholder="">
                                        </div>
                                        <div class="form-group" style="position: relative;">
                                            <label class="control-label"></label>
                                            <span class="locals">International</span>
                                            <span class="local_rma">RM</span>
                                            <input type="text" class="form-control local_deposit " required="" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Date<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="start" placeholder="Start Date">
                                                    <input type="text" class="form-control" name="end" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Time<span style="color: red;">*</span></label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="start" placeholder="From">
                                                    <input type="text" class="form-control" name="end" placeholder="To">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <label class="control-label"><strong>Description</strong></label>
                                           <div><textarea required="" class="form-control" placeholder="description" rows="5"></textarea></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Status</label><br>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-full active">
                                            <input type="radio" name="options" id="option2"> Active </label>
                                            <label class="btn btn-full ">
                                            <input type="radio" name="options" id="option3"> Inactive</label>
                                        </div>
                                    </div>
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <a href="#" class="btn btn-lg btn-primary">Save</a>
                                         <a href="#" class="btn btn-lg btn-primary">Cancel</a>
                                    </div>
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
 
</body>
</html>