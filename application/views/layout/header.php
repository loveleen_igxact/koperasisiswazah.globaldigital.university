<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">

    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>

    <meta content="Admin Dashboard" name="description">

    <meta content="Themesbrand" name="author">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/fav_lms.png">

    <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/morris/morris.css">

    <link href="<?php echo  CSS_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo  CSS_PATH; ?>css/metismenu.min.css" rel="stylesheet" type="text/css">



    <link href="<?php echo PLUG_PATH; ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo PLUG_PATH; ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo  CSS_PATH; ?>css/icons.css" rel="stylesheet" type="text/css">

    <link href="<?php echo  CSS_PATH; ?>css/style1.css" rel="stylesheet" type="text/css">

    <style type="text/css">

        .form-search {

            position: absolute;

            width: 50%;

            bottom: 7px;

            left: 310px;

        }

        .search_btn_top {

            position: absolute;

            right: 2px;

            top: 2px;

        }

    </style>

</head>

<body>

<!-- Begin page -->

<div id="wrapper">

    <!-- Top Bar Start -->

    <div class="topbar">

        <!-- LOGO -->

        <div class="topbar-left"><a href="javascript:void(0)" class="logo"><span><img src="http://lmsmalaysia.com/public/images/logo_lms.png" alt="" height="50"> </span><i><img src="http://lmsmalaysia.com/public/images/logo-sm.png" alt="" height="22"></i></a></div>

        <nav class="navbar-custom">

            <ul class="navbar-right d-flex list-inline float-right mb-0 ">

                <li class="dropdown notification-list">

                    <!-- <form role="search" class="app-search">

                        <div class="form-group mb-0 ">

                            <input type="text" class="form-control" placeholder="Search Student Name,Student No IC/Passport...">

                            <button type="submit"><i class="fa fa-search"></i></button>

                        </div>

                    </form> -->

                </li>

<!--

                <li class="pt-3">

                         <div class="form-group ">

                            <a href="advance_search.html">

                                <button class="btn btn-primary ">Advance Search</button>

                            </a>

                        </div>

                    </li>

-->

<!--

                <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">1</span></a>

                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                        <h6 class="dropdown-item-text">Notifications (258)</h6>

                        <div class="slimscroll notification-item-list">

                            <a href="javascript:void(0);" class="dropdown-item notify-item">

                                <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>

                                <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>

                            </a>

                        </div>

                       <a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>

                </li>

-->       <?php  $data = $this->session->userdata['loggedInData']; 

                if ($data['profile_pic'] != '') {

                    $profile_pic = $data['profile_pic'];

                } else {

                    $profile_pic = 'profile_icon.png';

                }

?>

                <li class="dropdown notification-list">

                    <div class="dropdown notification-list nav-pro-img">

                        <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><strong style="color: #666666;"><?php echo $data['name']; ?></strong>&nbsp&nbsp&nbsp  <img src="http://lmsmalaysia.com/uploads/<?php echo $profile_pic;?>" alt="user" class="rounded-circle"></a>

                     <!--<div class="dropdown-menu dropdown-menu-right profile-dropdown">

                        item <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a> 

                            <a class="dropdown-item" href="#"><i class="mdi mdi-wallet m-r-5"></i> My Wallet</a>  

                            <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right"></span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 

                             <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5"></i> Lock screen</a> 

                            <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="<?php //echo base_url(); ?>finance/signout"><i class="mdi mdi-power text-danger"></i> Logout</a></div>-->

                    </div>

                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">

                <li class="float-left">

                    <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>

                </li>

                <li class="d-none d-md-block  form-search">

                    <form method="post" action="<?php echo base_url('Teacher/students_list'); ?>">

                        <div class="form-group">

                            <!--<input type="text" class="form-control " name="search_data" placeholder="Search Student Name, Student No, IC/Passport..." value="" form-search=""><button class="color_another btn search_btn_top" type="button">Search </button>

                            <button class="color_another btn search_btn_top" type="submit" name="search_button">Search </button>-->




                                <input type="text" class="form-control serch" placeholder="Search Student Name, Student No, IC/Passport..." name="search_text"> <button class="color_another btn search_btn_top" type="button">Search </button>

                                <button class="color_another btn search_btn_top" type="submit" name="search_button">Search </button>

                        </div>

                    </form>

                    <!-- <div class="dropdown pt-3 d-inline-block"><a class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Create</a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item" href="#">Something else here</a>

                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a></div>

                    </div> -->

                </li>

            </ul>

        </nav>

    </div>

    <?php

    // if (isset($_POST['search']) ) {

    //     if (!empty($_POST)) {

    //         $data['students_data'] = $this->AdminModel->searchStudent($_POST);

    //     } else {

    //         $data['students_data'] = $this->AdminModel->getData('students');

    //     }

    // }

    ?>