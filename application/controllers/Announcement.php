<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');	// loads admin model
        $this->CI = & get_instance();
        if(!isset($this->session->userdata['loggedInData'])) {
            redirect(base_url('login')); 
        }
    }

    public function index()
    {
        $session_data = $this->session->userdata['loggedInData'];
        // if ($session_data['user_type'] == '1') {
        //     $data['announcement_data'] = $this->AdminModel->getData('announcements');
        // } else if ($session_data['user_type'] == '3') {
        //     $where = array('tab'=>$session_data['id']);
            $data['announcement_data'] = $this->AdminModel->get_announcements($session_data['user_type'],$session_data['id']);
        // } else if ($session_data['user_type'] == '4') {
        // 	$data['announcement_data'] = $this->AdminModel->get_announcements($session_data['user_type'],$session_data['id']);
        // } else {
        //     $data['announcement_data'] = array();
        // }
    	$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('announcement/announcement',$data);
		$this->load->view('layout/footer');
    }
    public function send_email($email,$message,$subject)
    {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        // $this->email->from('no-reply@mylifelineapp.com');
        $this->email->from('adminksbb@lmsmalaysia.com');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
        return true;
        // $to = $email;
        // $subject = $subject;
        // $txt = $message;
        // $headers = "From: webmaster@example.com" . "\r\n" .
        // "CC: somebodyelse@example.com";
        // mail($to,$subject,$txt,$headers);
    }
public function create_announcement()
    {
        // $data['classes'] = $this->AdminModel->getData('class');
        $data['subjects'] = $this->AdminModel->getData('subjects');
        $session_data = $this->session->userdata['loggedInData'];
        date_default_timezone_set("Asia/Kuala_Lumpur");
        
        if (isset($_POST['submit'])) {
            // echo "string111";
           //  echo "<pre>";
           //  print_r($_FILES["fileToUpload"]);
           // echo "string111";
           //  print_r($_FILES["fileToUpload"]["name"]);
            // print_r($_POST); die;
            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name)

            {

                $name= time().'_'.$image_name;
                // print_r($name);

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);

                // $arr['file1'] = $name;

            $from_date = date('Y-m-d', strtotime($_POST['from_date']));
            $to_date = date('Y-m-d', strtotime($_POST['to_date']));
            
            if(strtotime($_POST['from_date']) ==''){
               $from_date = " ";
            }
            
            if(strtotime($_POST['to_date']) == '') {
                $to_date = " ";
            }
            if(empty($_POST['student_tab'])){
               $student_tab ='0';
            }else{
                $student_tab='1';
            }
            
             if(empty($_POST['staff_tab'])){
               $staff_tab ='0';
            }else{
                $staff_tab ='1';
            }
            
            $arr = array(
                'subject' => $_POST['subject'],
                'announcement' => $_POST['message'],
                'created_by' => $session_data['id'],
                'from_date' => $from_date,
                'to_date' => $to_date,
                'created' =>  date('Y-m-d H:i:s'),
                'publish_to_student'=>$student_tab,
                'publish_to_staff'=>$staff_tab,
                'file1'=> $name
                
            );
        
            // print_r($arr);
            // die;
           
            // $subject = "Regarding Announcement";
            // $message = "New Announcement is created";
            // if (!empty($_POST['staff_tab']) && !empty($_POST['student_tab'])) {
            //     $arr['tab'] = 'all';
            //     $get_staff = $this->AdminModel->getData('users');
            //     foreach ($get_staff as $key => $value) {
            //         if ($value['id'] != $session_data['id']) {
            //             $this->send_email('pankaj.igxact@gmail.com',$message,$subject);
            //             // $this->send_email($value['email'],$message,$subject);
            //         }
            //     }
            // } else if (!empty($_POST['staff_tab'])) {
            //     $arr['tab'] = 'staff';
            //     $where = array('user_type' => "4");
            //     $get_staff = $this->AdminModel->getData('users',$where);
            //     foreach ($get_staff as $key => $value) {
            //         if ($value['id'] != $session_data['id']) {
            //             $this->send_email('pankaj.igxact@gmail.com',$message,$subject);
            //         }
            //     }
            // } else if (!empty($_POST['student_tab'])) {
            //     $arr['tab'] = 'student';
            //     $where = array('user_type' => "3");
            //     $get_staff = $this->AdminModel->getData('users',$where);
            //     foreach ($get_staff as $key => $value) {
            //         if ($value['id'] != $session_data['id']) {
            //             $this->send_email('pankaj.igxact@gmail.com',$message,$subject);
            //         }
            //     }
            // }
            // if (!empty($_POST['classes'])) {
            //     $arr['classes'] = implode(',', $_POST['classes']);
            //     foreach ($_POST['classes'] as $key => $class_id) {
            //         $where = array('class_id' => $class_id);
            //         $get_students = $this->AdminModel->getData('students',$where);
            //         foreach ($get_students as $key => $value) {
            //            $this->send_email('pankaj.igxact@gmail.com',$message,$subject);
            //         }

            //     }
            // } else if (!empty($_POST['subjects'])) {
            //     $arr['subjects'] = implode(',', $_POST['subjects']);
            //     foreach ($_POST['subjects'] as $key => $subject_id) {
            //         $where = array('subject_id' => $subject_id);
            //         $get_students = $this->AdminModel->getData('students',$where);
            //         foreach ($get_students as $key => $value) {
            //            $this->send_email('pankaj.igxact@gmail.com',$message,$subject);
            //         }
            //     }
            // }
            // if(isset($_FILES["file1"])) {
            //       if($_FILES["file1"]["name"] != '') {
            //         $name = time().'_'.$_FILES["file1"]["name"];
            //         $tmp_name=$_FILES["file1"]["tmp_name"];
            //         $error=$_FILES["file1"]["error"];
            //         $path='uploads/announcements/'.$name;
            //         move_uploaded_file($tmp_name,$path);
            //         $arr['file1'] = $name;
            //       } else {
            //         $arr['file1'] = '';
            //       }
            // } 
            // if(isset($_FILES["file2"])) {
            //       if($_FILES["file2"]["name"] != '') {
            //         $name = time().'_'.$_FILES["file2"]["name"];
            //         $tmp_name=$_FILES["file2"]["tmp_name"];
            //         $error=$_FILES["file2"]["error"];
            //         $path='uploads/announcements/'.$name;
            //         move_uploaded_file($tmp_name,$path);
            //         $arr['file2'] = $name;
            //       } else {
            //         $arr['file2'] = '';
            //       }
            // } if(isset($_FILES["file3"])) {
            //       if($_FILES["file3"]["name"] != '') {
            //         $name = time().'_'.$_FILES["file3"]["name"];
            //         $tmp_name=$_FILES["file3"]["tmp_name"];
            //         $error=$_FILES["file3"]["error"];
            //         $path='uploads/announcements/'.$name;
            //         move_uploaded_file($tmp_name,$path);
            //         $arr['file3'] = $name;
            //       } else {
            //         $arr['file3'] = '';
            //       }
            // }
            // echo "<pre>";
            // print_r($arr);
            
           
            $save = $this->AdminModel->saveData($arr,'announcements');
            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Record Successfully Added.</div>');
             redirect(base_url('announcement'));
        } }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('announcement/create_announcement',$data);
        $this->load->view('layout/footer');
    }
    public function edit_announcement() {
        $session_data = $this->session->userdata['loggedInData'];
        $announcement_id = $_GET['id'];
        $data['get_announcement'] = $this->AdminModel->get_data_by_id('announcements','id',$announcement_id);
        // $data['classes'] = $this->AdminModel->getData('class');
        // $data['subjects'] = $this->AdminModel->getData('subjects');
        if(isset($_POST['update'])) {
            $table = 'announcements';
            $where =  array('id'=> $announcement_id);
                if(empty($_POST['student_tab'])){
               $student_tab ='0';
            }else{
                $student_tab='1';
            }
            
             if(empty($_POST['staff_tab'])){
               $staff_tab ='0';
            }else{
                $staff_tab ='1';
            }

            foreach($_FILES["fileToUpload"]["name"] as $key => $image_name){
                // print_r($_POST['to_date']);
                // print_r($_POST['from_date']);
                // $fromdate=date('d/m/Y', strtotime($_POST['from_date'])); 
                // $todate=date('d/m/Y', strtotime($_POST['to_date']));   
                
                // print_r($fromdate);
                if (empty($image_name)) {
                   $name=$data['get_announcement']['file1'];
                }else{
                  $name= time().'_'.$image_name;  
                }
                // if (empty($_POST['from_date'])) {
                //    $fromdate=$data['get_announcement']['from_date'];
                // }else{
                //   $fromdate=date('d/m-d', strtotime($_POST['from_date']));  
                // }
                //  if (empty($_POST['to_date'])) {
                //    $todate=$data['get_announcement']['to_date'];
                // }else{
                //   $todate=date('Y-m-d', strtotime($_POST['to_date']));  
                // }

                $tmp_name=$_FILES["fileToUpload"]["tmp_name"][$key];

                $error=$_FILES["fileToUpload"]["error"][$key];

                $path='uploads/documents/'.$name;

                move_uploaded_file($tmp_name,$path);
            }
            $arr = array(
                'subject' => $_POST['subject'],
                'announcement' => $_POST['message'],
                'from_date' =>$_POST['from_date'],
                'to_date' => $_POST['to_date'],
                'created' => date('Y-m-d'),
                'publish_to_student'=>$student_tab,
                'publish_to_staff'=>$staff_tab,
                'file1'=> $name

            );
            $subject = "Regarding Announcement";
            $message = "Announcement is Updated";
            if (!empty($_POST['staff_tab']) && !empty($_POST['student_tab'])) {
                $get_staff = $this->AdminModel->getData('users');
                foreach ($get_staff as $key => $value) {
                    if ($value['id'] != $session_data['id']) {
                        //$this->send_email($value['email'],$message,$subject);
                    }
                }
            } else if (!empty($_POST['staff_tab'])) {
                $wheres = array('user_type' => "4");
                $get_staff = $this->AdminModel->getData('users',$wheres);
                foreach ($get_staff as $key => $value) {
                    if ($value['id'] != $session_data['id']) {
                       // $this->send_email($value['email'],$message,$subject);
                    }
                }
            } else if (!empty($_POST['student_tab'])) {
                $wheres = array('user_type' => "3");
                $get_staff = $this->AdminModel->getData('users',$wheres);
                foreach ($get_staff as $key => $value) {
                    if ($value['id'] != $session_data['id']) {
                        //$this->send_email($value['email'],$message,$subject);
                    }
                }
            }
            // if (!empty($_POST['classes'])) {
            //     $arr['classes'] = implode(',', $_POST['classes']);
            //     foreach ($_POST['classes'] as $key => $class_id) {
            //         $where = array('class_id' => $class_id);
            //         $get_students = $this->AdminModel->getData('students',$where);
            //         foreach ($get_students as $key => $value) {
            //            //$this->send_email($value['email'],$message,$subject);
            //         }

            //     }
            // } else if (!empty($_POST['subjects'])) {
            //     $arr['subjects'] = implode(',', $_POST['subjects']);
            //     foreach ($_POST['subjects'] as $key => $subject_id) {
            //         $where = array('subject_id' => $subject_id);
            //         $get_students = $this->AdminModel->getData('students',$where);
            //         foreach ($get_students as $key => $value) {
            //            //$this->send_email($value['email'],$message,$subject);
            //         }
            //     }
            // }
            if(isset($_FILES["file1"])) {
                  if($_FILES["file1"]["name"] != '') {
                    $name = time().'_'.$_FILES["file1"]["name"];
                    $tmp_name=$_FILES["file1"]["tmp_name"];
                    $error=$_FILES["file1"]["error"];
                    $path='uploads/announcements/'.$name;
                    move_uploaded_file($tmp_name,$path);
                    $arr['file1'] = $name;
                  } else {
                    $arr['file1'] = '';
                  }
            }if(isset($_FILES["file2"])) {
                  if($_FILES["file2"]["name"] != '') {
                    $name = time().'_'.$_FILES["file2"]["name"];
                    $tmp_name=$_FILES["file2"]["tmp_name"];
                    $error=$_FILES["file2"]["error"];
                    $path='uploads/announcements/'.$name;
                    move_uploaded_file($tmp_name,$path);
                    $arr['file2'] = $name;
                  } else {
                    $arr['file2'] = '';
                  }
            }if(isset($_FILES["file3"])) {
                  if($_FILES["file3"]["name"] != '') {
                    $name = time().'_'.$_FILES["file3"]["name"];
                    $tmp_name=$_FILES["file3"]["tmp_name"];
                    $error=$_FILES["file3"]["error"];
                    $path='uploads/announcements/'.$name;
                    move_uploaded_file($tmp_name,$path);
                    $arr['file3'] = $name;
                  } else {
                    $arr['file3'] = '';
                  }
            }
            // echo $arr['file1']; die;
            $this->AdminModel->updateData($where,$arr,$table);
            $this->session->set_flashdata('msg', '<div class="alert alert-success" style=" color:#000 !important;">Record Successfully Updated.</div>');
            redirect(base_url('announcement'));
        }
        if(isset($_POST['cancel'])) {
            redirect(base_url('announcement'));
        }
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('announcement/edit_announcement',$data);
        $this->load->view('layout/footer');
    }
    public function delete_file()
    {
        $where = array("id" => $_GET['id']);
        $type = $_GET['type'];
        $arr[$type] = "";
        $this->AdminModel->updateData($where,$arr,"announcements");
        redirect(base_url('announcement/edit_announcement'));
    }
    public function view_announcement()
    {

        $session_data = $this->session->userdata['loggedInData'];
         $data['announcement_data'] = $this->AdminModel->get_announcements($session_data['user_type'],$session_data['id']);
           
        $announcement_id = $_GET['id'];
        $data['get_announcement'] = $this->AdminModel->get_data_by_id('announcements','id',$announcement_id);
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('announcement/view_announcement',$data);
        $this->load->view('layout/footer');
    }

}
?>