<div id="wrapper">

       <div class="modal fade" id="collection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">Add New Collection</h5>
            </div>
           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
          <div class="modal-body">
           <div class="container">
            <div class="row">
                <hr>
                <div class="clearfix"></div>
                <div class="col-md-12">
                <form method="post">
                    <div class="form-group">
                        <label class="control-label"><strong> Student</strong></label>
                        <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="student_id">
                            <option>Please Select</option>
                            <?php if (!empty($student_data)) {
                                    foreach ($student_data as $key => $data) {
                                ?>
                                <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
                            <?php } } ?>
                        </select>
                    </div>
                </div>
            </div>
           </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-lg" name="save">Save</button>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div>
      </div>
    </div>
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Colletions</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Colletions</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                    <form method="post">
                                        <div class="search ">
                                            <div class="form-group">
                                                <label>Search</label>
                                                <input type="text" class="form-control serch" required="" placeholder="Search by name, IC/Pssport or student No" name="search"> 
                                                <span class="serch_icon"><button class="btn btn-dark" type="submit" name="submit">Search</button></span>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Collection Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label"><strong>Status</strong></label>
                                                            <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                <option>Please Select</option>
                                                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                                <option value="AK">All</option>
                                                                <option value="HI">option</option>
                                                            </select>
                                                    </div>
                                                </div>

                                    <div class="col-md-12 pt-2">
                                        <button type="button" class="btn btn-dark waves-effect waves-light">Search</button>
                                    </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                </div>
                                
                            </div>
                            <!---card 2 table-->
                            
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Collections</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#collection">Add New</button>
                                            <a href="<?php echo base_url('Finance/upload_collection') ?>">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add Group Collection</button>
                                            </a> 
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th> 
                                                    <th class="th-sm">Student Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Student No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">IC/Passport
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Outstanding
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (!empty($collection_data)) {
                                                    $i = 1;
                                                    foreach ($collection_data as $key => $data) {
                                                ?>
                                                <tr>
                                                    <th scope="row"><?php echo $i; ?></th>
                                                    <td><?php echo $data['student_name']; ?></td>
                                                    <td><?php echo $data['id_no']; ?></td>
                                                    <td><?php echo $data['ref_no']; ?></td>
                                                    <td><?php echo $data['amount']; ?></td>
                                                   
                                                    <td><a href=""><button data-toggle="tooltip" data-placement="top" title=""><i class="far fa-money-bill-alt" style="font-size:25px;" ></i></button></a>
                                                    </td>
                                                </tr>
                                                <?php $i++; } } else { ?>
                                                <tr><td>No Record found!</td></tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>      
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
