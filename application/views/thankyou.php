 <!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">

    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>

    <meta content="Admin Dashboard" name="description">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/fav_lms.png">

    <link href="<?php echo PLUG_PATH;?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="<?php echo PLUG_PATH;?>select2/css/select2.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo PLUG_PATH;?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo CSS_PATH;?>css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH;?>css/metismenu.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH;?>css/icons.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH;?>css/style.css" rel="stylesheet" type="text/css">

    <!-- Dropzone css -->

    <link href="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">

    <style type="text/css">

    .auth-panel {

    background-color: #fff;

    height: 100%;

    position: absolute;

    right: 0;

    top: 0;

    overflow-y: auto;

}

    </style>

</head>

<body>



 <!-- Begin page -->

    <div class="container text-center" style="margin: 3% auto;">

        <h3 class="text-center m-0" style="padding-bottom: 40px;">

            <a href="http://lmsmalaysia.com" class="logo logo-admin">

                <img src="http://lmsmalaysia.com/public/images/logo_png_lower.png" width="15%" height="100%" alt="logo">

            </a>

        </h3>

        <div class="card">

            <div class="card-body" style="box-shadow: 0 0 20px #5d5b5b; padding: 100px;">

                    <h3 class="text-center m-0">

                        <img src="http://lmsmalaysia.com/public/images/icon_check.png">

                        <!--<i class="ti-check-box fa-3x mb-3 animated rotateIn" style="color:#318f05;"></i>-->

                    </h3>

                    <div class="upper_thank">

                        <h3>Thank you for submitting your prospect</h3>

                        <!-- <p>A representative from koperasisiwazah Globaldigital University will contact you soon</p> -->
                        <p>A representative from Koperasi Siswazah Bangi Berhad – Universiti Kebangsaan Malaysia will contact you soon.</p>
                    </div>         

                    <div class="col-12 m-t-20 text-center back_home">

                        <a href="<?php echo base_url('login'); ?>">Back to Home page</a>

                    </div>

            </div>

        </div>

    </div>

<style>

body{

    /*background:#fff;*/

    background-image: url(http://lmsmalaysia.com/public/images/bg1.jpg);

    background-size: cover;

}

.wrapper-page{

    max-width:600px;

} 

.btn-info:hover {

    background-color: #585dd4 !important;

}

.hh {

    box-shadow: 4px 5px 20px 0px #cec9c9;

}

.upper_thank h3 {

    color: #318f05;

    letter-spacing: 2px;

}

.upper_thank p {

    font-size: 18px;

    padding-top: 10px;

}

.back_home a {

    text-decoration: underline;

    color: blue;

    font-size: 15px;

}

    </style>

</style>

    <!-- jQuery  -->

    <script src="<?php echo JS_PATH;?>js/jquery.min.js"></script>

    <script src="<?php echo JS_PATH;?>js/bootstrap.bundle.min.js"></script>

    <script src="<?php echo JS_PATH;?>js/metisMenu.min.js"></script>

    <script src="<?php echo JS_PATH;?>js/jquery.slimscroll.js"></script>

    <script src="<?php echo JS_PATH;?>js/waves.min.js"></script>

    <script src="<?php echo PLUG_PATH;?>jquery-sparkline/jquery.sparkline.min.js"></script>

    <!-- Plugins js -->

    <script src="<?php echo PLUG_PATH;?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

    <script src="<?php echo PLUG_PATH;?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script src="<?php echo PLUG_PATH;?>select2/js/select2.min.js"></script>

    <script src="<?php echo PLUG_PATH;?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>

    <script src="<?php echo PLUG_PATH;?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>

    <script src="<?php echo PLUG_PATH;?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Plugins Init js -->

    <script src="<?php echo PLUG_PATH;?>/pages/form-advanced.js"></script>

    <!-- App js -->

    <script src="<?php echo JS_PATH;?>js/app.js"></script>

    <!-- Dropzone js -->

    <script src="<?php echo PLUG_PATH;?>dropzone/dist/dropzone.js"></script>

</body>

</html>

