
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Finance / Ledger</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Ledger</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Ledger</h4>
                                        </div><!-- 
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="new_academic_session.html">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
                                            </a>
                                        </div> -->
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                    <th class="th-sm">Date
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Doc No
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Description
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Debit()
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Credit()
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Balance
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                      <?php 

                                //  PRINT_R($posts);

                                    foreach($details as $det): ?>
                                                      <td><?php echo $det->date; ?></td>
                                                    <td> <a href="Reciept?id=<?php echo $det->doc_no;?>" style="color: blue;"><?php echo $det->doc_no; ?></a></td>
                                                   
                                                    <td><?php echo $det->description; ?></td>
                                                    <td>NA</td>
                                                    <td>NA</td>
                                                    <td><?php echo $det->amount; ?></td>
                                                      <?php endforeach; ?>
                                                </tr>
                                                <tr style="font-weight:bold">
                                                  <td colspan="3" class="text-right">
                                                    Total
                                                  </td>
                                                  <td class="">
                                                     NA
                                                  </td>
                                                  <td class="">
                                                    NA
                                                  </td>
                                                  <td class="">
                                                     NA
                                                  </td>

                                                  
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
   
</body>
</html>