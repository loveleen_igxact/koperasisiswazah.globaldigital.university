
        <div class="content-page">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Edit Program </h4>
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active"> New Staff </li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    
                    <!-- end row -->
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="col-md-12 hearding pt-3">
                                <h4 class="mt-0 header-title left_side">Edit Program Form</h4>
                            </div>
                            <hr>
                                <div class="card-body">
                                    <form method="post">
                                    <div class="row">
                                        <div id="prospect"></div>
                                        <div style="clear:both"></div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Name of the program<span style="color: red;">*</span></label>
                                                <?php foreach ($program_data as $key => $value) {
                                                    if($_GET['id']==$value['id']){
                                                ?> 
                                                <input type="text" class="form-control " required="" placeholder="Example : Azmirul Ahmad" name="name" value="<?php echo $value['name']; ?>">
                                                 <?php } ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Campus<span style="color: red;">*</span></label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="p1_campus">
                                                    <option> Please Select</option>
                                                    <?php if (!empty($campus_program_data)) { 
                                                        foreach ($campus_program_data as $key => $values) { ?>
                                                    <option value="<?php echo $values['id']; ?>"><?php echo $values['name']; ?> </option>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                           
                                      
                                        <div class="col-md-12 pt-3 text-center">
                                            <button class="btn color_another btn-lg" type="submit" name="update">Update</button>
                                            <button class="btn btn-secondary btn-lg" type="submit">Cancel</button>
                                        </div>
                                    </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>