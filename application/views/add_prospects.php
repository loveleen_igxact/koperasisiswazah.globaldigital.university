<div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Prospects</h4> -->
                                <!--<ol class="breadcrumb">-->
                                <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                                <!--    <li class="breadcrumb-item active">Add New Prospect</li>-->
                                <!--</ol>-->
                            </div>
                        </div>
                    </div>
                </div>
                        <?php echo $this->session->flashdata('msg'); ?>
                    <!-- end row -->


                     <form action="<?php echo base_url('Application/add_prospects'); ?>" method="POST">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                            <div class="row">
                                                <div class="personal col-12">
                                                    <h6>Personal Information</h6>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>International</label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input name="international" type="radio" name="options" id="option2" value="o"> No </label>
                                                        <label class="btn btn-full ">
                                                        <input name="international" type="radio" name="options" id="option3" value="y"> Yes</label>
                                                    </div>
                                                </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                        <label><strong>Gender<span style="color: red;">*</span></strong></label><br>
                                       
                                            
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline3" required="" mdbInputDirective name="gender" value="m">
                                              <label class="custom-control-label" for="defaultInline3">Male</label>
                                            </div>
                                           
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" class="custom-control-input" id="defaultInline4" mdbInputDirective name="gender" value="f">
                                              <label class="custom-control-label" for="defaultInline4">Female</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>ID Type <span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2  tabindex="-1" name="idtype" aria-hidden="true" required>
                                                  
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                        <option value="1">Passport</option>
                                                        <option value="2">Identity Card</option>
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ID No<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control " required="" placeholder="eg: 123456789" name="id_no"> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control " required="" placeholder="" name="name"> 
                                         </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Secondary Name</label>
                                            <input name="secondary_name" type="text" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email<span style="color: red;">*</span></label>
                                            <input name="email" type="Email"  class="form-control " required="" placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Phone<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="phone" required="" placeholder=""> 
                                         </div>
                                         <?php 
                                         // $t=time();
                                            date_default_timezone_set("Asia/Kuala_Lumpur");
                                                // echo date('d-m-Y H:i:s');
                                            
                                            ?>
                                         <input type="hidden" class="form-control" name="created_at" value="<?php echo(date('d-m-Y H:i:s'));  ?>" > 
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Emergency contact<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="emergency_contact" required="" placeholder="" required=""> 
                                         </div>
                                    </div>
                                    
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Nationality</strong></label>
                                                <select class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true" name="nationality">
                                                    <option>Select</option>
                                                   <?php foreach($nationalities as $nationality) { ?>
													 <option value="<?php echo $nationality['NationalityID']  ?>"><?php echo $nationality['Nationality'];?></option>
													 
													 <?php  } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Race</strong></label>
                                                <select name="race" class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                <?php foreach($race as $r) { ?>
                                                     <option value="<?php echo $r['id']  ?>"><?php echo $r['name']  ?></option>
                                                     
                                                     <?php  } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Religion</strong></label>
                                                <select name="religion" class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     
                                                       <?php foreach($religion as $religon) { ?>
                                                     <option value="<?php echo $religon['id']  ?>"><?php echo $religon['name']  ?></option>
                                                     
                                                     <?php  } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Class</strong></label>
                                                <select name="class_id" class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     
                                                       <?php foreach($class as $cls) { ?>
                                                     <option value="<?php echo $cls['id']  ?>"><?php echo $cls['class_name']  ?></option>
                                                     
                                                     <?php  } ?>
                                                </select>
                                        </div>
                                    </div> -->
                                   <!--  <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Subject</strong></label>
                                                <select name="subject_id" class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true">
                                                    <option>Select</option>
                                                     
                                                       <?php foreach($subject as $sub) { ?>
                                                     <option value="<?php echo $sub['id']  ?>"><?php echo $sub['subject_name']  ?></option>
                                                     
                                                     <?php  } ?>
                                                </select>
                                        </div>
                                    </div> -->
                                    
                                    <div class="col-md-6 personal">
                                        <h6>Address</h6>
                                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>Address Line 1</strong></label>
                                            <input name="address_line1" type="text" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong> Address Line 2</strong></label>
                                            <input type="text" name="address_line2" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>City</strong></label>
                                            <input type="text" name="city" class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Country</strong></label>
                                                <select name="country" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">
                                                    <option>Select</option>
                                                    <?php foreach($country as  $value) {  ?>
                                                      <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>
                                                      <?php  } ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong>State</strong></label>
                                             <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="state" id="state">
                                                    <option>Select</option>
                                                   
                                                </select>
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><strong> Postcode</strong></label>
                                            <input type="text" name="postcode"  class="form-control "  placeholder=""> 
                                         </div>
                                    </div>
                                </div>
                                </div>
                                    <div class="col-md-6 personal">
                                        <h6>Programs</h6>
                                    
                                   
                                        <div class="form-group">
                                            <label class="control-label"><strong>Intake<span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="Intake" required="" />
                                                   <option value=""> select </option>
                         <?php foreach ($intakeP as $key => $value) { ?>
                          <option value="<?php echo $value['id']  ?>" <?php echo (@$prospects_data[0]['intake'] ==  $value['intake']) ? 'selected' : '' ?>>
                             <?php echo $value['intake']  ?></option>
                         <?php } ?>
                                                </select>
                                                
                                        </div>
                                    
                                    <div class="col-md-8">
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <!--    <h6>Program 1<span style="color: red;">*</span></h6>  -->
                                        <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p1_campus" required="">
                                                    <option value="">Select</option>
                                                     <option value="1">UKM BANGI</option>
                                                    <option value="2">IPGKTR</option>
                                                    <option value="3">IPGKB</option>
                                                    <option value="4">KOTA KINABALU</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program 1</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p1_program" required="">
                                                    <option value="">Select</option>
                                                    <?php foreach($campus_program as  $programs) { ?>
                                                     <option value="<?php echo $programs['id'] ?>"><?php echo $programs['name'] ?></option>
                                                  <?php  } ?>
                                                </select>
                                        </div>
                                          <div class="form-group">
                                            <label class="control-label"><strong>Program 2</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p2_program">
                                                    <option>Select</option>
                                                       <?php foreach($campus_program as  $programs) { ?>
                                                     <option value="<?php echo $programs['id'] ?>"><?php echo $programs['name'] ?></option>
                                                  <?php  } ?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Study Mode</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p1_study_mode" required="">
                                                    <option value="">Select</option>
                                                     <option value="1">FULL TIME</option>
                                                    <option value="2">PART TIME</option>
                                                    <option value="3">DISTANT LEARNING</option>
                                                    <option value="4">E-LEARNING</option>
                                                </select>
                                        </div>
                                </div>
                              <!--   <div class="col-md-6">
                                            <h6>Program 2</h6>
                
                                        <div class="form-group">
                                            <label class="control-label"><strong>Campus</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p2_campus">
                                                    <option>Select</option>
                                                           <option value="1">UKM BANGI</option>
                                                            <option value="2">IPGKTR</option>
                                                            <option value="3">IPGKB</option>
                                                            <option value="4">KOTA KINABALU</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Program</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p2_program">
                                                    <option>Select</option>
                                                       <?php// foreach($campus_program as  $programs) { ?>
                                                     <option value="<?php //echo $programs['id'] ?>"><?php //echo $programs['name'] ?></option>
                                                  <?php // } ?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Study Mode</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="p2_study_mode">
                                                    <option>Select</option>
                                                     <option value="1">FULL TIME</option>
                                                    <option value="2">PART TIME</option>
                                                    <option value="3">DISTANT LEARNING</option>
                                                    <option value="4">E-LEARNING</option>
                                                </select>
                                        </div>
                                </div>  -->

                            </div>
                            <div class="clearfix"></div>
                            </div>
                               <div class="col-md-6 personal">
                                            <h6>Recruited by<span style="color: red;"></span></h6>
                                            <div class="row">
                                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Marketing Source</strong></label>
                                                <select class="form-control select2 " tabindex="-1" aria-hidden="true" name="marketing_source">
                                                    <option value="">Select</option>
                                                    
                                                    <option value="8">Call in</option>
                                                    <option value="13">Database Import</option>
                                                    <option value="6">Education Fair</option>
                                                    <option value="11">Fax</option>
                                                    <option value="5">NS Camp</option>
                                                    <option value="10">Online</option>
                                                    <option value="14">Online Application</option>
                                                    <option value="4">Open Day</option>
                                                    <option value="12">Others</option>
                                                    <option value="9">Post</option>
                                                    <option value="1">Road Show</option>
                                                    <option value="3">School Fair</option>
                                                    <option value="2">School Visit</option>
                                                    <option value="7">Walk in</option>
                                                </select>
                                        </div>
                                    </div>
                                  <!--  <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Marketing Staff</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="marketing_staff">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                    </div>  -->
                                </div>
                                      <!--  <div class="form-group">
                                            <label class="control-label"><strong>Agent</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name ="agent">
                                                    <option>Select</option>
                                                     <option value="1">Betelgeous Romanee Contee</option>
                                                <option value="2">Satella Emilia</option>
                                                </select>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label"><strong>Staff Get Student</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="staff_get_student">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>  -->
                                     <!--   <div class="form-group">
                                             <label class="control-label"><strong>Other</strong></label>
                                           <div><textarea required="" class="form-control" rows="5"></textarea></div>
                                        </div>   -->
                                </div>

                              <!--  <div class="col-md-6 personal">
                                            <h4>Other</h4>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Last Institution</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="last_institution">
                                                    <option>Select</option>
                                                     <option value="AK">option</option>
                                                     <option value="HI">option</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Potential Scholarship</strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="potential_scholarship">
                                                    <option>Select</option>
                                                     <option value="1">PTPTN</option>
                                                     <option value="2">yayasan selangor</option>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Information Source <small>(How Do You Know About Us?)</small></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="information_source">
                                                    <option>Select</option>
                                                    
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><strong>Remark<span style="color: red;">*</span></strong></label>
                                           <div><textarea required="" class="form-control" rows="5" name="remark"></textarea></div>
                                        </div>   
                                </div> -->
                              <!--  <div class="col-md-6">
                                   
                                        <div class="form-group">
                                        <label><strong>Tag</strong></label><br>
                                      
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input value="all" type="radio" required class="custom-control-input" id="2"  mdbinputdirective="" name="tag" name="All">
                                              <label class="custom-control-label all" for="2">All</label>
                                            </div>
                                           
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input value="hot" type="radio" required class="custom-control-input" id="3"  mdbinputdirective="" name="tag">
                                              <label class="custom-control-label hot" for="3" value="Hot">Hot</label>
                                            </div>
                                          
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" value="warm" required class="custom-control-input" id="4"  mdbinputdirective="" name="tag" value="Warm">
                                              <label class="custom-control-label warm" for="4">Warm</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio custom-control-inline">
                                              <input type="radio" value="cold" required class="custom-control-input" id="5" mdbinputdirective="" name="tag" value="cold">
                                              <label class="custom-control-label cold" for="5">Cold</label>
                                            </div>
                                        </div>
                                   
                                </div> -->

                                 
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <input type="submit" style="background-color:#4856a4; border-color:#4856a4;" name="submit" value="Submit" class="btn btn-lg color_another">
                                         <a href="#" class="btn btn-lg btn-danger more_border">Cancel</a>
                                    </div>
                                </div>
                                
                            </div>
                        
                    </form>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
             <style>
                 .personal h6 {
                    background-color: #4856a4;
                    text-transform: uppercase;
                    border-top-right-radius: 5px;
                    border-top-left-radius: 5px;
                }
             </style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script>    
$(document).ready(function() {
 $('#country').change(function() {
	   var id  =  $(this).val();
	   $.ajax({
			type: "POST",
			url: "<?php echo base_url('application/get_state') ?>",
			data: {'id' : id },
			success:  function(data){
			//	alert(data);
				$('#state').html(data);
				//window.location.reload();
				
			}
		});
	 
	})
	
})
</script>
