<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>
    <meta content="Admin Dashboard" name="description">
       <link rel="shortcut icon" href="assets/images/fav_lms.png">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo PLUG_PATH; ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo PLUG_PATH; ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo CSS_PATH; ?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Dropzone css -->
    <link href="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left"><a href="index.html" class="logo"><span><img src="assets/images/logo_lms.png" alt="" height="50"> </span><i><img src="assets/images/logo-sm.png" alt="" height="22"></i></a></div>
            <nav class="navbar-custom">
                <ul class="navbar-right d-flex list-inline float-right mb-0">
                    <li class="dropdown notification-list d-none d-sm-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0 ">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                    <li class="pt-3">
                         <div class="form-group ">
                            <a href="advance_search.html">
                                <button class="btn btn-primary ">Advance Search</button>
                            </a>
                            </div>
                    </li>
                    <li class="dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ti-bell noti-icon"></i> <span class="badge badge-pill badge-danger noti-icon-badge">3</span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <!-- item-->
                            <h6 class="dropdown-item-text">Notifications (258)</h6>
                            <div class="slimscroll notification-item-list">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                    <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                </a>
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                    <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span></p>
                                </a>
                            </div>
                            <!-- All--><a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a></div>
                    </li>
                    <li class="dropdown notification-list">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                 <strong class="admin_name">Admin</strong> <img src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item--><a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a>  <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a> 
                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a></div>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
      <?php  $this->load->view('layout/sidebar'); ?>
        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Intake</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Add New Intake</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <form method="post" action="<?php echo base_url('addmision/addintake');?>">
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><strong>Intake<span style="color: red;">*</span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="intake">
                                                    <option disabled selected>Select</option>
                                                    <?php foreach ($intake_data as $key => $value) { ?>
                                                       <option value="<?php echo $value['intake']; ?>"><?php echo $value['intake']; ?></option>
                                                   <?php  } ?>
                                                        
                                                        
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Month<strong><span style="color: red;"></span></strong></label>
                                             <?php $months = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.'); ?>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="month">
                                                    <option disabled selected>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                                       <?php  foreach ($months as $key => $value) {
                                                          
                                                       ?> 
                                                       <option value="<?php echo $key; ?>"><?php echo $value ; ?></option>
                                                     <?php  } ?> 
                                                    
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Student No Prefix<span style="color: red;">*</span> <span style="font-size: 15px;"><a href="#"><i class="mdi mdi-help-circle" style="color: red;"></i></a></span></label>
                                            <input type="text" class="form-control " required="" placeholder="" name="studentntnoprefix"> 
                                         </div>
                                    </div>
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Program<span style="color: red;">*</span><strong><span style="color: red;"></span></strong></label>
                                                <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="program">
                                                    <option disabled selected>Select</option>
                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                                    <?php foreach ($campus_data as $key => $value) { ?>
                                                       <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                   <?php  } ?>
                                                    </optgroup>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                            <label>Online Application Session</label>
                                            <div>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="start" placeholder="Start Date" >
                                                    <input type="text" class="form-control" name="end" placeholder="End Date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                                <label><strong>Offer Reply Last Date</strong></label>
                                                <div>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose" name="offer_reply_last_date">
                                                    <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                    </div>
                                                    <!-- input-group -->
                                                </div>
                                                </div>
                                    </div>
                                    <div class="col-md-4">
                                    	<div class="form-group">
                                             <label class="control-label"><strong>Description</strong></label>
                                           <div><textarea required="" class="form-control" rows="5"></textarea></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Status</label><br>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-full active">
                                            <input type="radio" name="status" id="option2" value="1"> Active </label>
                                            <label class="btn btn-full ">
                                            <input type="radio" name="status" id="option3" value="0"> Inactive</label>
                                        </div>
                                    </div>
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <input type="submit" name="submit" class="btn btn-lg btn-primary" value="Save">
                                         <a href="#" class="btn btn-lg btn-primary">Cancel</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    </div>
                </div>

                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    <script src="<?php echo JS_PATH; ?>js/jquery.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/metisMenu.min.js"></script>
    <script src="<?php echo JS_PATH; ?>js/jquery.slimscroll.js"></script>
    <script src="<?php echo JS_PATH; ?>js/waves.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo PLUG_PATH; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo PLUG_PATH; ?>select2/js/select2.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo PLUG_PATH; ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
    <!-- Plugins Init js -->
    <script src="<?php echo JS_PATH; ?>pages/form-advanced.js"></script>
    <!-- App js -->
    <script src="<?php echo JS_PATH; ?>js/app.js"></script>
    <!-- Dropzone js -->
    <script src="<?php echo PLUG_PATH; ?>dropzone/dist/dropzone.js"></script>
</body>
</html>