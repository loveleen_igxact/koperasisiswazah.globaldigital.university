<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('form','url'));
        $this->load->model('AdminModel');	// loads admin model

    }
	public function index()
	{
		//echo  '<pre>'; print_r($_REQUEST); 
		if($_POST) {
		
			$password = md5($_POST['password']);
			$usrname =  $_POST['email']; 
			
		//	echo $password;die(); 
			$checkLogin = $this->AdminModel->checkLogin($usrname,$password);
		//	echo  '<pre>'; print_r($checkLogin[0]->user_type); die;  
			if(!empty($checkLogin)){
				$session_data = array('id' => $checkLogin[0]->id,'name' => $checkLogin[0]->name, 'email' => $checkLogin[0]->email,'user_type'=> $checkLogin[0]->user_type,'prospect_id'=>$checkLogin[0]->prospect_id,'profile_pic'=>$checkLogin[0]->profile_pic);
			    $this->session->set_userdata('loggedInData', $session_data);	
				 if($session_data['user_type'] == 1)	{
				redirect(base_url('application/prospects'));
			}	elseif ($session_data['user_type'] == 3) {
			    
			        redirect(base_url('Teacher'));
			    
					
					}else{
                    if($checkLogin[0]->is_active=='0') {
			        $this->session->set_flashdata('message', '<div class="alert alert-danger" style=" color:#000 !important;">Your Account is not Active.</div>');
				    redirect(base_url('login'));
			    } else {
			        redirect(base_url('Teacher/students_list'));
			    }
					}		
			}
			else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" style=" color:#000 !important;">Wrong Email/Password.</div>');
				redirect(base_url('login'));					  
			}
			array_get('password', $password);
		}
		$this->load->view('login');

	}
	public function forgot_password()
	{
	   if (isset($_POST['submit'])) {
	   	// echo "string111";
	   // print_r($_POST['email']);
	   $where = array('email' => $_POST['email']);

	   $result = $this->AdminModel->getData('users', $where);
	   if (!empty($result)) {
 
	   	$to_email=$_POST['email'];
	   	$from_email='adminksbb@lmsmalaysia.com';
	   	$subject='Access Your Forgotten Password';
// $message='Please click on this link: http://lmsmalaysia.com/login/new_password/?email='.$to_email;
$message='Hi,
 
Need assistance to recover your password.
Click here : http://lmsmalaysia.com/login/new_password/?email='.$to_email.'
Thank you!
Koperasi Siswazah Bangi Berhad';
$this->load->library('email');



            $this->email->from($from_email, 'Access Your Forgotten Password');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);
if ($this->email->send()) {

                

                $this->session->set_flashdata("email_sent", "Email sent successfully.");

               echo "<script>
              alert('Message has been sent successfully');
              window.location.href = 'http://lmsmalaysia.com/';
              </script>";

                // redirect(base_url());

                //echo "success";

                return true;

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                //echo "failure";

                return false;

                //  $this->load->view('email_form');

            }
	   }
       else{
        // echo "Email ID is not registered.Please enter correct Email ID.";
              echo "<script>
              alert('Email ID is not registered.Please enter correct Email ID.');
              window.location.href = 'http://lmsmalaysia.com/login/forgot_password';
              </script>";
       }
	   // echo "string";
	   }
	    $this->load->view('forgot_password');
	}


public function new_password(){
	$emailid = $this->input->get('email', TRUE);
	// print_r($color_id);

	  if (isset($_POST['submit'])) {
// print_r($_POST['password']);
// print_r($emailid);
 $where = array('email' =>$emailid);
 $datas = array('password' =>md5($_POST['password']));
 $updatepasword = $this->AdminModel->updateData($where,$datas,'users');
 $to_email=$emailid;
	   	$from_email='adminksbb@lmsmalaysia.com';
	   	$subject='Your password has been Changed.';
$message='Hi,

You have been secured with your new password.

Thank You!
Koperasi Siswazah Bangi Berhad';
$this->load->library('email');
            $this->email->from($from_email, 'Your password has been Changed');

            $this->email->to($to_email);

            $this->email->subject($subject);

            $this->email->message($message);
if ($this->email->send()) {
                $this->session->set_flashdata("email_sent", "Email sent successfully.");
                 echo "<script>
              alert('Password has been updated successfully');
              window.location.href = 'http://lmsmalaysia.com/';
              </script>";

               // echo "Message has been sent successfully";

                return true;

            } else {

                $this->session->set_flashdata("email_sent", "Error in sending Email.");

                echo "failure";
                return false;
            }
 
	  }
	$this->load->view('new_password');
}

    
}
