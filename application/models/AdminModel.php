<?php
class AdminModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function saveData($data, $tablename)
    {
        $this->db->insert($tablename, $data);
        //$this->db->last_query();
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }


    public function updateData($where, $data, $table)
    {

        $update = $this->db->update($table, $data, $where);
        return $update;
    }

    public function getData($table, $where = array(), $search = array())
    {
        $this->db->select('*');
        $this->db->from($table);

        if (!empty($where) || $where != false) {
            $this->db->where($where);
        }
        if(!empty($search) || $search != false){
        if (!empty($search['order'])) {
            $this->db->order_by('id','DESC');
        } else {
            $this->db->like($search);
        }
       } 
        $query = $this->db->get();
       // echo $this->db->last_query(); //die;
        $result = $query->result();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }
    public function getDataApplication($table, $where = array())
    {
        $this->db->select('*');
        $this->db->from($table);
if (!empty($where) || $where != false) {
            $this->db->where($where);
        }
            $this->db->order_by('updated_at','DESC');
       
           
        $query = $this->db->get();
       // echo $this->db->last_query(); //die;
        $result = $query->result();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }
    function searchCollection($table,$search){
       // $sql =  $this->db->query ("SELECT * from collection  WHERE student_name LIKE '%$search%' OR  id_no LIKE '%$search%' ORDER BY id ASC");
       //  return $sql->result_array();
        $sql =  $this->db->query ("SELECT b.* ,u.name,u.id_no from bills as b inner join students as u on b.student_id = u.id WHERE (u.name LIKE '%$search%' OR id_no LIKE '%$search%') AND (b.balance != '0.00' OR b.balance != '') ORDER BY b.id ASC ");
        //echo $this->db->last_query(); die;
        return $sql->result_array();
    }
    function searchStudent($data) 
    {
        if (!empty($data['class']) && !empty($data['subject'])) {
            $class = $data['class'];
            $subject = $data['subject'];
            $where = "WHERE class_id = $class AND subject_id = $subject";
        } else if (!empty($data['class'])) {
            $where = "WHERE class_id = ".$data['class']."";
        } else if (!empty($data['subject'])) {
            $where = "WHERE subject_id = ".$data['subject']."";
        }
        if (!empty($data['search_text'])) {
            $search = $data['search_text'];
            if (!empty($where)) {
                $sql =  $this->db->query ("SELECT * from students $where AND (name LIKE '%$search%' OR id_no LIKE '%$search%') ORDER BY id ASC ");
            } else {
                $sql =  $this->db->query ("SELECT * from students WHERE (name LIKE '%$search%' OR id_no LIKE '%$search%') ORDER BY id ASC ");
            }
        } else if (!empty($where)) {
            $sql =  $this->db->query ("SELECT * from students $where ORDER BY id ASC ");
        } else {
            $sql =  $this->db->query ("SELECT * from students ORDER BY id ASC ");
        }
        return $sql->result_array();
    }

    public function row_delete($id, $tablename)
    {
        $this->db->where('id', $id);
        $this->db->delete($tablename);
        return '1';
    }
    public function get_data_by_id($tbl, $field = 0, $value = 0)
    {
        if (!empty($field)) {
            $this->db->where($field, $value);
        }
        return $this->db->get($tbl)->row_array();
        
    }
    function getCount($tbl,$field=0,$value=0){
        if(!empty($field)){
            $this->db->where($field,$value);
        }
        return $this->db->get($tbl)->num_rows();
    }
    function add_bills($data)
    {
        $date = 'Y-m-d';
        $sql = "INSERT INTO bills_info SET bill_id = '".$data['bill_id']."',account = '".$data['account']."', amount = '".$data['amount']."',description = '".$data['description']."',unit_price = '".$data['unit_price']."',discount = '".$data['discount']."',tax_rate = '".$data['tax_rate']."',created = '$date'"; 
        $this->db->query($sql);
        if (!empty($data['Schoolname'])) {
            for($i=0; $i< sizeof($data['Schoolname']['amount']); $i++) {
                $sql = "INSERT INTO bills_info SET bill_id = '".$data['bill_id']."',account = '".$data['Schoolname']['account'][$i]."', amount = '".$data['Schoolname']['amount'][$i]."',description = '".$data['Schoolname']['description'][$i]."',unit_price = '".$data['Schoolname']['unit_price'][$i]."',discount = '".$data['Schoolname']['discount'][$i]."',tax_rate = '".$data['Schoolname']['tax_rate'][$i]."',created = '$date'"; 
                $this->db->query($sql);
            } 
        }
        return true;
    }
    function getBills($where)
    {
        if (!empty($where)) {
            $where = "WHERE user_id='$where'";
        }
       // $sql =  $this->db->query ("SELECT b.student_name,b.student_id,b.program_id,b.quantity,b.date,b.due_date,b.program_id,b.prospects_id,b.collect_id,b.doc_no,b_i.* from bills as b inner join bills_info as b_i on b.id = b_i.bill_id $where ORDER BY b_i.id ASC");
       
        $sql =  $this->db->query ("SELECT b.* ,u.name,u.id_no,u.email,u.id_no,u.student_no from bills as b inner join students as u on b.student_id = u.id $where ");
       //echo $this->db->last_query(); die;
        return $sql->result_array();
    }
    function getpaymentData($where)
    {
        // if (!empty($where)) {
        //     $where1 = "WHERE user_id='$where'";
        // }
       // $sql =  $this->db->query ("SELECT b.student_name,b.student_id,b.program_id,b.quantity,b.date,b.due_date,b.program_id,b.prospects_id,b.collect_id,b.doc_no,b_i.* from bills as b inner join bills_info as b_i on b.id = b_i.bill_id $where ORDER BY b_i.id ASC");
       
        $sql =  $this->db->query ("SELECT p.* ,u.name,u.id_no from payments as p inner join students as u on p.student_id = u.id WHERE user_id=$where");
       //echo $this->db->last_query(); die;
        return $sql->result_array();
    }
    function getTransactions()
    {
        $sql =  $this->db->query ("SELECT p.* ,u.name,u.id_no from payments as p inner join students as u on p.student_id = u.id");
        // $sql =  $this->db->query ("SELECT b.* ,u.name,u.id_no from bills as b inner join students as u on b.student_id = u.id WHERE balance = '0.00'");
        return $sql->result_array();
    }
    function getCollectBills($where)
    {
        if (!empty($where)) {
            $where = "WHERE b.student_id='$where' AND balance != '0.00'";
            // $where = "WHERE user_id='$where' AND balance != '0.00'";
            
        }
        $sql =  $this->db->query ("SELECT b.* ,u.name,u.eligible_program from bills as b inner join students as u on b.student_id = u.id $where ");
        //echo $this->db->last_query(); die;
        return $sql->result_array();
    }
    function searchTransactions($search)
    {
        //$sql =  $this->db->query ("SELECT b.student_name,b.student_id,b.program,b.quantity,b.date,b.due_date,b.program,b.amount_is,b.prospects_id,b.collect_id,b.doc_no,b_i.* from bills as b inner join bills_info as b_i on b.id = b_i.bill_id WHERE student_name LIKE '%$search%' OR doc_no LIKE '%$search%' ORDER BY b_i.id ASC");
        $sql =  $this->db->query ("SELECT p.* ,u.name,u.id_no from payments as p inner join students as u on p.student_id = u.id WHERE u.name LIKE '%$search%' OR doc_no LIKE '%$search%' ORDER BY p.id ASC ");
        return $sql->result_array();
    }
    function searchBills($search)
    {
        //$sql =  $this->db->query ("SELECT b.student_name,b.student_id,b.program,b.quantity,b.date,b.due_date,b.program,b.amount_is,b.prospects_id,b.collect_id,b.doc_no,b_i.* from bills as b inner join bills_info as b_i on b.id = b_i.bill_id WHERE student_name LIKE '%$search%' OR doc_no LIKE '%$search%' ORDER BY b_i.id ASC");
        $sql =  $this->db->query ("SELECT b.* ,u.name,u.id_no,u.student_no from bills as b inner join students as u on b.student_id = u.id WHERE u.name LIKE '%$search%' OR doc_no LIKE '%$search%' ORDER BY b.id ASC ");
        return $sql->result_array();
    }
    function get_selected_users($arr)
    {
        $sql =  $this->db->query ("SELECT b.student_name,b.student_id,b.program,b.quantity,b.date,b.due_date,b.program,b.amount_is,b.prospects_id,b.collect_id,b.doc_no,b_i.* from users as u inner join prospects as p on p.id = u.prospects_id WHERE student_name LIKE '%$search%' OR doc_no LIKE '%$search%' ORDER BY b_i.id ASC");
        return $sql->result_array();
    }
    function getCollections()
    {
        // if (!empty($where)) {
        //  $where = "WHERE student_id='$where' AND (b_i.amount != 0 OR b_i.amount != '') AND status='0'";
        // }
        // $sql =  $this->db->query ("SELECT u.name,u.email,u.id_no,b.student_name,b.student_id,b.program,b.quantity,b.date,b.due_date,b.program,b.amount_is,b.prospects_id,b.collect_id,b.doc_no,b_i.* from bills as b inner join bills_info as b_i on b.id = b_i.bill_id inner join users as u on u.id = b.student_id WHERE b_i.status = '0'  ORDER BY b_i.id ASC");
        // return $sql->result_array();
        $sql =  $this->db->query ("SELECT u.name,u.email,u.id_no,u.student_no,b.student_name,b.*  from bills as b inner join students as u on u.id = b.student_id WHERE b.balance != '0.00'  ORDER BY b.id ASC");
        return $sql->result_array();
    }
    public function checkLogin($username, $password)
    {
       //echo $password;
       // echo $username;die();
        $where = "email='$username' AND password='$password'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_staff($staff_id)
    {
        $sql =  $this->db->query ("SELECT u.*,u.id as user_id,s_i.* from users as u inner join staff_info as s_i on u.id = s_i.staff_id WHERE user_type = '4' AND u.id = $staff_id");
        return $sql->row_array();
    }
    public function doc_ckecklist()
    {
        $q = $this->db->select('*')->from('document_checklist')->get();
        if ($q->num_rows()) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function statement_of_account()
    {
        $data = $this->session->userdata['loggedInData'];
         $sql =  $this->db->query ("SELECT bills.student_id,bills.doc_no,bills.date,bills_info.discount,bills_info.amount,bills_info.description,bills_info.paid from bills  join bills_info  on bills.id = bills_info.bill_id  ");

         return $sql->row_array();

        // $q = $this->db
        //     ->select('bills.student_id,bills.doc_no,bills.date,bills_info.discount,bills_info.amount,bills_info.description,bills_info.paid')
        //     ->from('bills')
        //     ->join(' bills_info', 'bills.id = bills_info.bill_id')
        //     ->where('bills.student_id', $data['id'])
        //     ->get();

        // return $q->result_array();
    }

     public function Reciept($doc_no)
    {
        $data = $this->session->userdata['loggedInData'];

        $q = $this->db
            ->select('bills.student_id,bills.program,bills.doc_no,bills.date,bills_info.discount,bills_info.amount,bills_info.description,users.name,users.id,users.id_no')
            ->from('bills')
            ->join(' bills_info', 'bills.id = bills_info.bill_id')
            ->join(' users', 'bills.student_id = users.id')
            ->where('bills.student_id', $data['id'])
            ->where('bills.doc_no', $doc_no)
            ->get();

        return $q->result();
    }

     public function getproData($table,$data)
    {

       // print_r($data);die();
        $q = $this->db->select('*')->where_in('id', $data)->from($table)->get();
        if ($q->num_rows()) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getSharedDocuments($id,$type)
   {
        $sql =  $this->db->query ("SELECT u_d.* from user_documents as u_d inner join shared_documents as s_d on u_d.id = s_d.document_id WHERE s_d.user_id = $id AND creator_type != '$type'");
        return $sql->result_array();
   }
   public function checkEmail($email)
   {
        $sql =  $this->db->query ("SELECT * from users WHERE email = '$email'");
        return $sql->row_array();
   }
   
   public function checkEmailprospect($email)
   {
        $sql =  $this->db->query ("SELECT * from prospect WHERE email = '$email'");
        return $sql->row_array();
   }
   public function checkIdNo($id_no)
   {
        $sql =  $this->db->query ("SELECT * from students WHERE id_no = '$id_no'");
        return $sql->row_array();
   }
    public function getMyDocuments($id,$type)
   { 
        $sql =  $this->db->query ("SELECT * from user_documents WHERE user_id = $id AND creator_type = '$type'");
        return $sql->result_array();
   }
   public function get_announcements($user_type,$user_id)
   {
        if($user_type == "3") {
            $where = "WHERE publish_to_student='1' ";
        } else if($user_type == "1") {
            $where = "";
        } else if($user_type == "4") {
            $where = "WHERE publish_to_staff='1'";
        }
        $sql =  $this->db->query ("SELECT * from announcements $where");
        return $sql->result_array();
   }
    public function getReceipt($docnumber)
    {
        // $data = $this->session->userdata['loggedInData'];
        $sql =  $this->db->query ("SELECT s.*,b.* from bills as b inner join students as s on b.student_id = s.id WHERE b.id = $docnumber ORDER BY b.id DESC ");
        // echo 'SELECT s.*,b.* from bills as b inner join students as s on b.student_id = s.user_id WHERE b.student_id = $bill_id ORDER BY b.id DESC ';

        return $sql->row();
        // $data = $this->session->userdata['loggedInData'];
            // $this->db->select('*');
            // $this->db->from('bills');
            // $this->db->join('students', 'bills.student_id = students.user_id');
            // $this->db->where('bills.id', $bill_id);
            // $query = $this->db->get();
            // return $query->row();
        // $q = $this->db
        //     ->select('*')
        //     ->from('bills')
        //     ->join('students', 'bills.student_id = students.user_id')
        //     ->where('bills.id', $bill_id)
        //     ->get();
        // return $q->row();
    }

    public function getReceipt_before($bill_id)
    {
        $sql =  $this->db->query ("SELECT s.*,b.* from bills as b inner join students as s on b.student_id = s.id WHERE b.id = $bill_id ORDER BY b.id DESC ");
        return $sql->row();
    }    
    function getRecipt_Transactions($userId,$amount,$docNumbr)
    {
        $sql =  $this->db->query ("SELECT p.* ,u.name,u.id_no,u.eligible_program,u.student_no from payments as p inner join students as u on p.student_id = u.id WHERE  bill_id=$userId  AND p.doc_no='$docNumbr' AND (paid=$amount OR adjustment=$amount)");
        return $sql->result_array();
    }   
    public function getSentMsgs($sender_id)
    {
        $sql =  $this->db->query ("SELECT DISTINCT* from inbox WHERE from_id = $sender_id AND msg_type = '1' AND from_status = '1' group by unique_no ORDER BY id DESC");
        return $sql->result_array();
    }

    public function get_programs()
   {
        
        $sql =  $this->db->query ("SELECT * FROM `campus_program` ");
        return $sql->result_array();
   }
 public function get_campus()
   {
        $sql =  $this->db->query (" SELECT * FROM `campus` ");
        return $sql->result_array();
   }
   function add_paymentLogs($data)
    {
       $billId=$_GET['docnumber'];
      // $sql = "SELECT * FROM `payment_logs` WHERE `bill_Id` = $billId"; 
      //   $result=$this->db->query($sql);
      //   // return true;

    

    $where = "bill_Id='$billId'";
        $this->db->select('*');
        $this->db->from('payment_logs');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return false;
        } else {
            $sql = "INSERT INTO payment_logs SET ord_shipname = '".$_GET['ord_shipname']."',ord_mercID = '".$_GET['ord_mercID']."',ord_date = '".$_GET['ord_date']."',ord_totalamt = '".$_GET['amountPaid']."',ord_shipcountry = '".$_GET['ord_shipcountry']."',ord_telephone = '".$_GET['ord_telephone']."',ord_mercref = '".$_GET['ord_mercref']."',ord_gstamt = '".$_GET['ord_gstamt']."',ord_email = '".$_GET['ord_email']."',ord_svccharges = '".$_GET['ord_svccharges']."',bill_Id = '".$_GET['docnumber']."',ord_delcharges = '0:00'"; 
        $this->db->query($sql);
        
        return true;
        }
    // $query = $this->db->get('payment_logs');
    // $this->db->where(`bill_Id`,$billId);
    // if (!empty($query->result_array())){
    //     $sql = "INSERT INTO payment_logs SET ord_shipname = '".$_GET['ord_shipname']."',ord_mercID = '".$_GET['ord_mercID']."',ord_date = '".$_GET['ord_date']."',ord_totalamt = '".$_GET['amountPaid']."',ord_shipcountry = '".$_GET['ord_shipcountry']."',ord_telephone = '".$_GET['ord_telephone']."',ord_mercref = '".$_GET['ord_mercref']."',ord_gstamt = '".$_GET['ord_gstamt']."',ord_email = '".$_GET['ord_email']."',ord_svccharges = '".$_GET['ord_svccharges']."',bill_Id = '".$_GET['docnumber']."',ord_delcharges = '0:00'"; 
    //     $this->db->query($sql);
        
    //     return true;
    // }
    // else{
    //     return false;
    // }

    
    }
    function add_paidDetails($docnumber,$balance){
     
        $sql= "UPDATE `bills` SET `IsPaid`=1,`balance`=$balance WHERE id=$docnumber";
        $this->db->query($sql);
        return true;

  
    }

    function save_superorder($data)
    {
      
       
        $sql = "INSERT INTO payment_logs SET ord_shipname = '".$_GET['ord_shipname']."',ord_mercID = '".$_GET['ord_mercID']."',ord_date = '".$_GET['ord_date']."',ord_totalamt = '".$_GET['amountPaid']."',ord_shipcountry = '".$_GET['ord_shipcountry']."',ord_telephone = '".$_GET['ord_telephone']."',ord_mercref = '".$_GET['ord_mercref']."',ord_gstamt = '".$_GET['ord_gstamt']."',ord_email = '".$_GET['ord_email']."',ord_svccharges = '".$_GET['ord_svccharges']."',ord_delcharges = '0:00'"; 
        $this->db->query($sql);
        
        return true;
    }
    public function account_statement()
    {
        $data = $this->session->userdata['loggedInData'];
         $sql =  $this->db->query ("SELECT bills.id,bills.student_id,bills.doc_no,bills.date,bills.description,bills.doc_no,bills.amount,bills.IsPaid from bills where IsPaid='1' AND student_id='4586' ");

         return $sql->row_array();

    }
    public function get_last_payment()
    {
        $sql =  $this->db->query ("SELECT * FROM `payments` ORDER BY id DESC");
        return $sql->row();
    }
    public function getEmailStudent($where)
    {
        
        $sql =  $this->db->query ("SELECT email FROM `students` where user_id=$where");
        // echo "SELECT * FROM `students` where user_id=".$where;
        //  $this->db->select('email');
        // $this->db->from('students');
        // $this->db->where($where);
        // $query = $this->db->get();
         return $sql->result();
        
    }
     public function student_dataSerial()
    {
        $sql =  $this->db->query ("SELECT * FROM `students` WHERE type ='1' ORDER BY name");
        return $sql->result_array();
    }
    public function stuentdata_upload($where)
    {
        // print_r($where);
$value=implode(",",$where);
           // $arrayName = array($where);
        $sql =  $this->db->query ("SELECT * FROM `students` WHERE user_id  IN ($value)");
        return $sql->result_array();
    }
    public function getdatauser($whereuserId,$password)
    {
        $sql =  $this->db->query ("SELECT * FROM `users` WHERE  password='$password' AND id='$whereuserId'");
        return $sql->result();
    }

     public function saveDataPayment($data, $tablename)
    {    
        $where = "bill_Id='$data[bill_id]'  AND balance = 0";
        $this->db->select('*');
        $this->db->from('payments');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return false;
        } else{
        $this->db->insert($tablename, $data);
        //$this->db->last_query();
        $insert_id = $this->db->insert_id();
        return $insert_id;
        }
    }

}




