<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>  Documents</h4>
                        <!--<ol class="breadcrumb">-->
                        <!--    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>-->
                        <!--    <li class="breadcrumb-item active"> Documents</li>-->
                        <!--</ol>-->
                    </div>
                </div>
            </div>
        <!-- end row -->
        <div class="col-12">
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
        </div>                            
        <div class="card m-b-30">
            <div class="card-body">
                <div class="hearding">
                    <h4 class="mt-0 header-title left_side">Documents Form </h4>
                </div>
                <div class="add_more">
                    <a href="<?php echo base_url('document/upload_documents'); ?>">
                    <button type="button" class="btn color_another waves-effect waves-light">Upload</button>
                </a>
                </div> 
                <div class="clearfix"></div>
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">My Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Shared Documents</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane p-3 active show" id="home1" role="tabpanel">
                        <table id="dtBasicExample" class="table table-hover" cellspacing="0" width="100%">
                    <thead class="black white-text">
                        <tr>
                            <th class="no_sort" >No</th>
                            <th class="th-sm">File
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Posted Date
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm no_sort">Action
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($my_documents)) {
                    $i = 1;
                    foreach ($my_documents as $key => $value) { 
                        $url = "http://lmsmalaysia.com/uploads/documents/".$value['file'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); 
                    ?>
                        <tr id="current_row<?php echo $value['id']; ?>">
                            <th scope="row"><?php echo $i; ?></th>
                            <td>
                                <?php 
                                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="60" width="60" src="http://lmsmalaysia.com/uploads/documents/<?php echo $value['file'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>
                            </td>
                            <td><?php echo date('d/m/Y', strtotime($value['created'])); ?></td>
                            <td>
                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805" href="<?php echo base_url('document/edit_documents?id='.$value['id']); ?>">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a class="second_icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="javascript:void(0)" onclick="deleteRow(<?php echo $value['id']; ?>, 'user_documents')"><i class="far fa-trash-alt"></i></a>
                                <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="<?php echo base_url("document/download?file=".$value['file']) ?>"><i class="fas fa-download"></i></a>
                                <?php } else { ?>
                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/documents/<?php echo $value['file']; ?>"><i class="fas fa-download"></i></a>
                                <?php } ?>
                                
                            </td>
                        </tr>
                    <?php $i++; } } ?>
                    </tbody>
                </table>
                    </div>
                    <div class="tab-pane p-3" id="profile1" role="tabpanel">
                        <table id="dtBasicExample" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead class="black white-text">
                        <tr>
                            <th class="no_sort">No</th>
                            <th class="th-sm">File
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm">Posted Date
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                            <th class="th-sm no_sort">Action
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($shared_documents)) {
                    $i = 1;
                    foreach ($shared_documents as $key => $val) { 
                        $url = "http://lmsmalaysia.com/uploads/documents/".$val['file'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); 
                    ?>
                        <tr id="current_row<?php echo $val['id']; ?>">
                            <th scope="row"><?php echo $i; ?></th>
                            <td>
                                <?php 
                                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="60" width="60" src="http://lmsmalaysia.com/uploads/documents/<?php echo $val['file'] ?>">
                                <?php } else if($ext == 'csv') { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/download.jpg" rel="stylesheet">
                                <?php } else if($ext == 'pdf') { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/attachment.png" rel="stylesheet">
                                <?php } else { ?>
                                    <img height="60" width="60" src="<?php echo IMAGE; ?>/text_file.png" rel="stylesheet">
                                <?php } ?>
                            </td>
                            <td>test</td>
                            <td>test</td>
                            <td><?php echo date('m/d/Y', strtotime($val['created'])); ?></td>
                            <td>
                                <!-- <a href="<?php echo base_url('document/edit_documents?id='.$val['id']); ?>">
                                    <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" aria-describedby="tooltip652805"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                </a>
                                <a href="javascript:void(0)" onclick="deleteRow(<?php echo $val['id']; ?>, 'user_documents')"> <button data-toggle="tooltip" data-placement="top" title="Void" ><i class="mdi mdi-block-helper" style="font-size:25px; color: #4856a4; cursor: pointer;"></i></button></a> -->
                                <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <a class="first_icon"  data-toggle="tooltip" data-placement="top" title="Download file" href="<?php echo base_url("document/download?file=".$val['file']) ?>"><i class="fas fa-download"></i></a>
                                <?php } else { ?>
                                <a class="first_icon" data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/documents/<?php echo $val['file']; ?>"><i class="fas fa-download"></i></a>
                                <?php } ?>
                                
                            </td>
                        </tr>
                    <?php $i++; } } ?>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</body>
</html>