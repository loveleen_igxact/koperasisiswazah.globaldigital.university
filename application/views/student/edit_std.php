        <div class="content-page">
            <!-- Start content -->
           
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span>Edit Student information</h4>
                               
                            </div>
                        </div>
                                                                  
                    <!-- end row -->
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="col-md-12 hearding pt-3">
                                <h4 class="mt-0 header-title left_side">Edit Student Information</h4>
                            </div>
                            <hr>
                                <div class="card-body">
                                    <form method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div id="prospect"></div>
                                        <div style="clear:both"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?php
                                                 $url = "http://lmsmalaysia.com/uploads/".$user_info['image_path'];
                                $ext = pathinfo($url, PATHINFO_EXTENSION); ?>
                                                <?php if($user_info['image_path'] == '') { ?>

                            
                            <?php } else { ?>

                             <?php if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') { ?>
                                    <img height="150" width="150" src="http://lmsmalaysia.com/uploads/<?php echo $user_info['image_path'] ?>">
                                <?php } ?>



                            <?php } ?> 
                           
                                                  <input type="file" name="fileToUpload" id="fileToUpload">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="hidden" name="student_number" value="<?php echo $user_info['id']; ?>" >
                                                <label>Name<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="Example : Azmirul Ahmad" name="name" value="<?php echo $user_info['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Identification Certificate<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="" name="id_no" value="<?php echo $user_info['id_no']; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Salutation<span style="color: red;">*</span></label>
                                                <input disabled name="salutation_id" class="form-control " required="" placeholder="" value="<?php 
                                                if ($user_info['gender'] == 'F' || $user_info['gender'] == 'f') {
                                                    if($user_info['marital_id'] == '2'){
                                                    echo "Mrs";
                                                    # code...
                                                }else{
                                                    echo "Miss";

                                                } }else{
                                                    echo "Mr";
                                                }
                                                ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone Number<span style="color: red;">*</span></label>
                                               <!--  <?php echo "<pre>"; echo $user_info['phone']; ?> -->
                                                <input type="text" name="phone" class="form-control " required="" placeholder="" value="<?php echo $user_info['phone']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email<span style="color: red;">*</span></label>
                                                <input type="text" class="form-control " required="" placeholder="" name="email" value="<?php echo $user_info['email']; ?>">
                                                <!-- <span>This will be used as a password for first time creating a staff.</span> -->
                                            </div>
                                        </div>
                                             <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Gender<span style="color: red;">*</span></label><br>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <!-- <?php echo $user_info['gender']; ?> -->

                                <input type="radio" class="custom-control-input" id="defaultInline3" name="gender" <?php echo ($user_info['gender'] == 'M' || $user_info['gender'] == 'm') ? 'checked' : ''; ?> mdbinputdirective="" value="M" required>
                                <label class="custom-control-label" for="defaultInline3">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="defaultInline4" <?php echo ($user_info['gender'] == 'F' || $user_info['gender'] == 'f') ? 'checked' : ''; ?> name="gender" mdbinputdirective="" value="F" required>
                                <label class="custom-control-label" for="defaultInline4">Female</label>                                                </div>
                                                <!-- <input type="text" class="form-control " required="" placeholder="" name="gender" value="<?php echo $user_info['gender']; ?>"> -->

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nationality<span style="color: red;">*</span></label>
                                                <select name="nationality" class="form-control" tabindex="-1" aria-hidden="true" required ="">
                                                    <option value="">Please select</option>
                                                    
                                            <?php foreach($nationalities as $nation) { 
                                                if($user_info['nationality']==$nation['NationalityID']){ ?>
                                                    <option selected="selected" value="<?php echo $nation['NationalityID']; ?>"><?php echo $nation['Nationality']; ?></option>  
                                                    <?php
                                                }

                                                else{ ?>
                                                <option value="<?php echo $nation['NationalityID']; ?>"><?php echo $nation['Nationality']; ?></option>  
                                                                                            
                                                     <?php } } ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Religion</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="religion">
                                                    <option value="">Please select</option>
                                                    <?php foreach($religion as $religions) { 
                                                        if($user_info['religion']==$religions['id']){ ?>
<option selected="selected" value="<?php echo $religions['id']; ?>"><?php echo $religions['name']; ?></option> 
                                                        <?php 
                                                    }
                                                       else{ ?>


                                                    <option value="<?php echo $religions['id']; ?>"><?php echo $religions['name']; ?></option>  
                                                     <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Race</label>
                                                <select class="form-control" tabindex="-1" aria-hidden="true" name="race">
                                                    <option value="">Please select</option>
                                                    <?php foreach($race as $races) {
                                                            if ($user_info['race']== $races['id']) { ?>
                                                                <option selected="selected" value="<?php echo $races['id']; ?>"><?php echo $races['name']; ?></option>

                                                             <?php  
                                                            }
                                                    else{ ?>
                                                    <option value="<?php echo $races['id']; ?>"><?php echo $races['name']; ?></option>  
                                                     <?php  } } ?>

                                                </select>
                                            </div>
                                        </div>
                                          <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Marital Status</label>
                                                <select required class="form-control" tabindex="-1" aria-hidden="true" name="marital_status">
                                                    
                                               <option value="2" <?php echo ($user_info['marital_id'] == '2') ? 'selected' : '' ?>>Married</option>
                                            <option value="1" <?php echo ($user_info['marital_id'] == '1' || $user_info['marital_id'] =='0') ? 'selected' : '' ?>>Single</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Place of birth</label>
                                                <input name="place_of_birth" type="text" class="form-control "  value="<?php echo $user_info['place_of_birth'] ?>" placeholder="" >
                                               
                                                
                                            </div>
                                         </div>
                                         <div class="col-md-4">   
                                            <div class="form-group">
                                                <label>Date of birth</label>
                                               <input  name="date_of_birth" value="<?php echo $user_info['date_of_birth'];?>" type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">

                                                <!-- <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                </div> -->
                                            </div>
                                           </div>  
                                           <div class="col-md-4">   
                                            <div class="form-group">
                                                <label>Student number</label>
                                               <input  name="studentNo" value="<?php echo $user_info['student_no'];?>" type="text" class="form-control" placeholder="Enter student number" id="student_number_id" required>
                                            </div>
                                           </div>    
                                            <div class="col-md-12 pt-3 text-center">
                                                <button class="btn color_another btn-lg" type="submit" name="update">Update</button>
                                                <button class="btn btn-secondary btn-lg" type="submit" name="cancel">Cancel</button>
                                            </div>
                                    </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.form-control, .thumbnail {
    border-radius: 2px;
}
.btn-danger {
    background-color: #B73333;
}

/* File Upload */
.fake-shadow {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
}
.fileUpload {
    bottom: 43px;
    position: relative;
    overflow: hidden;
    left: 17px;
}
.fileUpload #logo-id {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 33px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.img-preview {
    max-width: 100%;
    width: 100%;
}
.thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.428571429;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
    </style>
</body>
</html>