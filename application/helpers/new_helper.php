<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_country'))
{
    function get_country($id = '1')
    {
        $ci=& get_instance();
    	$ci->load->database(); 
	    $sql = "select name  from countries where id = " .$id; 
	    $query = $ci->db->query($sql);
	    $row = $query->result();
	    return $row[0]['name'];
    }  

 }
 if ( ! function_exists('get_state'))
{
    function get_state($id = '')
    {
        $ci=& get_instance();
    	$ci->load->database(); 
	    $sql = "select name  from states where id = " .$id; 
	    $query = $ci->db->query($sql);
	    $row = $query->result();
	    return $row[0]->name;
    }   
}
 if ( ! function_exists('get_nationality'))
{
    function get_nationality($id = '')
    {
        $ci=& get_instance();
    	$ci->load->database(); 
	    $sql = "select Nationality  from nationalities where NationalityID = " .$id; 
	    $query = $ci->db->query($sql);
	    $row = $query->result();
	   
	    return $row[0]->Nationality;
    }  
} 
if ( ! function_exists('get_religion'))
{
    function get_religion($id = '')
    {
       $ci=& get_instance();
    	$ci->load->database(); 
	    $sql = "select name  from religion where id = " .$id; 
	    $query = $ci->db->query($sql);
	    $row = $query->result();
	   
	    return $row[0]->name;
    } 
}

if ( ! function_exists('get_race'))
{
    function get_race($id = '')
    {
        $ci=& get_instance();
    	$ci->load->database(); 
	    $sql = "select name  from race where id = " .$id; 
	    $query = $ci->db->query($sql);
	    $row = $query->result();
	   
	    return $row[0]->name;
    } 
}
if ( ! function_exists('get_campus'))
{
   function get_campus($id = '')
    {
        $ci=& get_instance();
        $ci->load->database(); 
       //  $query = $this->db->select('name')->where('id', $id)->from('campus')->get();
        $sql = "select name  from campus where id = " .$id; 
        $query = $ci->db->query($sql);
        $row = $query->result();
       
        return $row[0]->name;
    } 

}

if ( ! function_exists('get_program'))
{
    function get_program($id = '')
    {
        if(!empty($id)) {
        $ci=& get_instance();
        $ci->load->database(); 
        
        $sql = "select name  from campus_program where id = " .$id; 
        $query = $ci->db->query($sql);
        $row = $query->result();
         return $row[0]->name;
       } else  {
		   
		   return '';
		   }
       
       
    } 

}

if ( ! function_exists('get_program'))
{
    function get_study_mode($id = '')
    {
        $ci=& get_instance();
        $ci->load->database(); 
        $sql = "select name  from study_mode where id = " .$id; 
        $query = $ci->db->query($sql);
        $row = $query->result();
       
        return $row[0]->name;
    } 

}
