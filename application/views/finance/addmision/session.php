
 <?php 
 $program =  array("17" =>"SPEND - SARJANA PENDIDIKAN (PENDIDIKAN EKONOMI)",
                                                "22" => "SPEND - SARJANA PENDIDIKAN (PENDIDIKAN KOMPUTER)",
                                                "23" => "SPEND - SARJANA PENDIDIKAN (PENDIDIKAN SASTERA)",
                                                "18" => "SPEND - SARJANA PENDIDIKAN (PENILAIAN DAN PENGUKURAN)",
                                                "7" => "SPEND(GB) - SARJANA PENDIDIKAN (PSIKOLOGI PENDIDIKAN)",
                                                "19" => "SPEND(GE) - SARJANA PENDIDIKAN SUMBER TEKNOLOGI DAN MAKLUMAT",
                                                "1" => "SPEND(GF) - SARJANA PENDIDIKAN (PENTADBIRAN PENDIDIKAN)",
                                                "4" => "SPEND(GG) - SARJANA PENDIDIKAN (SOSIOLOGI PENDIDIKAN)",
                                                "5" => "SPEND(GH) - SARJANA PENDIDIKAN (PENDIDIKAN PRASEKOLAH)",
                                                "10" => "SPEND(GK) - SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)",
                                                "15" => "SPEND(GL) - SARJANA PENDIDIKAN (TESL)",
                                                "14" => "SPEND(GM) - SARJANA PENDIDIKAN (PENDIDIKAN BAHASA MELAYU)",
												"6" => "SPEND(GN) - SARJANA PENDIDIKAN (PENDIDIKAN ISLAM)",
                                                "12" => "SPEND(GO) - SARJANA PENDIDIKAN (PENDIDIKAN KHAS)",
                                                "3" => "SPEND(GQ) - SARJANA PENDIDIKAN (PENDIDIKAN MATEMATIK)",
                                                "8" => "SPEND(GR) - SARJANA PENDIDIKAN (PENDIDIKAN SAINS)",
												"2" => "SPEND(GS) - SARJANA PENDIDIKAN (PENDIDIKAN PERNIAGAAN DAN KEUSAHAWANAN)",
                                                "9" => "SPEND(GT) - SARJANA PENDIDIKAN (PENGURUSAN SUKAN)",
                                                "20" => "SPEND(GU) - SARJANA PENDIDIKAN BAHASA ARAB",
                                                "11" => "SPEND(GV) - SARJANA PENDIDIKAN (PENDIDIKAN SEJARAH)",
                                                "13" => "SPEND(KP) - SARJANA PENDIDIKAN (KURIKULUM DAN PEDAGOGI)"
												);
												
			 
                                                
                       
 
 ?>
 <div class="modal fade" id="update_program" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header tran-heading text-center ">
            <div class="col-md-12 text-center">
                <h5 class="modal-title" id="exampleModalLabel">Program</h5>
            </div>
           <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
          </div>
          <hr>
        <div class="modal-body">
            <div class="container">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Academic Session</label>
                            <input type="text" class="form-control " value="MAC 2017" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Program</label>
                            <input type="text" class="form-control " value="SARJANA PENDIDIKAN (BIMBINGAN DAN KAUNSELING)" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Academic Session</label>
                            <input type="text" class="form-control " value="27/05/2017 - 24/01/2018" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Registration Session</label>
                            <input type="text" class="form-control " value="27/05/2017 - 24/01/2018" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Subject Add Drop Session</label>
                            <input type="text" class="form-control " value="-" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Subject Withdraw Session</label>
                            <input type="text" class="form-control " value="-" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Attendance Entry Session</label>
                            <input type="text" class="form-control " value="-" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Coursework Mark Entry Session</label>
                            <input type="text" class="form-control " value="30/11/-1 - 30/11/-1" readonly="" required="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Final Exam Mark Entry Session</label>
                            <input type="text" class="form-control " value="30/11/-1 - 30/11/-1" readonly="" required="" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="program_academic_session">
                <button type="submit" class="btn btn-primary btn-lg">Update</button>
            </a>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            
          </div>
    </div>
</div>
</div>
 <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title-box">
                                <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Academinc Sessions</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Lms Dashboard</a></li>
                                    <li class="breadcrumb-item active">Academinc Sessions</li>
                                </ol
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                        <div class="col-12">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div id="prospect"></div>
                                    <div style="clear:both"></div>
                                     <div class="search ">
										 
										 <form action="<?php echo base_url('/addmision/admission_session'); ?>" method="POST">
                                                    <div class="form-group">
                                                        <label>Search</label>
                                                        <input type="text" value="<?php echo @$code_search; ?>" name="code_search" class="form-control serch"  placeholder="Search Code,Name..."> 
                                                        <span class="serch_icon">
															
															<input  class="btn btn-dark" type="submit" value="Search" name="search"></span>
                                                     </div>
                                                </div>
                                            </form>
                                    <!-- <h6>Filter by Prospect Name</h6> -->
                                        <!-- Collapse buttons -->
                                        <div class="button">
                                            <a class="btn btn-primary " gradient="peach type="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Academic Filter
                                            </a>
                                        </div>
                                        <!-- / Collapse buttons -->

                                        <!-- Collapsible element -->
                                        <form action="<?php echo base_url('/addmision/admission_session'); ?>" method="POST">
                                        <div class="row" style="padding-top: 10px;">
                                        <div class="collapse col-md-12" id="collapseExample">
                                            <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                           <div class="form-group">
                                                <label><strong>Academic Session</strong></label>
                                                <div>
                                                    <div class="input-group">
                                                        <input type="text" value="<?php echo @$academic_session; ?>" name="academic_session" class="form-control" placeholder="Select date range" id="datepicker-autoclose" name="search_sesseion" value="<?php echo @$search_sesseion;  ?>">
                                                    <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    </div>
                                                    </div>
                                                    <!-- input-group -->
                                                </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                                    <label>Status</label><br>
                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full <?php echo (@$status =='1')?'focus active':''; ?>">
                                                        <input type="radio" name="status" value="1" id="option2"> Active </label>
                                                        <label class="btn btn-full <?php echo (@$status =='0')?'focus active':''; ?>">
                                                        <input type="radio" name="status" value="0" id="option3"> Inactive</label>
                                                    </div>
                                                </div>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-dark waves-effect waves-light" value="Search" name="s_search">
                                    </div>
                                    </div>
                                        </div>
                                    </div>
                                        <!-- / Collapsible element -->  
                                        
                                    </form>
                                </div>      
                            </div>
                            <!---card 2 table-->
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="hearding">
                                            <h4 class="mt-0 header-title left_side">Academic Sessions</h4>
                                        </div>
                                           <div class="add_more">
                                            <button type="button" class="btn btn-info waves-effect waves-light">Export</button>
                                            <a href="<?php echo base_url('addmision/add_session') ?>">
                                                <button type="button" class="btn btn-dark waves-effect waves-light">Add New</button>
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead class="black white-text">
                                                <tr>
                                                <th>No</th>
                                                    <th class="th-sm">Code
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Name
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Period
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Program
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Status
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                    <th class="th-sm">Action
                                                        <i class="fa fa-sort float-right" aria-hidden="true"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											<?php if(!empty($session)) { 
											foreach($session as $key => $value) { 
											?>
                                                <tr id="current_row<?php echo $value['id']; ?>">
                                                    <th scope="row"><?php echo $value['id']; ?></th>
                                                    <td><?php echo $value['code'] . '/'. $value['month']; ?></td>
                                                    <td><?php echo $value['name']; ?></td>
                                                    <td><?php echo $value['academic_session']. '- ' . $value['registration_session']; ?></td>
                                                    <td class="scroll_date"><a href="" data-toggle="modal" data-target="#update_program"><?php echo @$program[$value['program']]; ?></a></td>
                                                    <td> <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                        <label class="btn btn-full active">
                                                        <input type="radio" name="options" id="option2"><?php echo ($value['status']=='1') ?'Active':''; ?>  </label>
                                                        <label class="btn btn-full ">
                                                        <input type="radio" name="options" id="option3"><?php echo ($value['status']=='1') ?'Inactive':''; ?> </label>
                                                    </div>
                                                    <td>
                                                        <a href="<?php echo base_url('addmision/edit_session/'.$value['id']); ?>"><button data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-table-edit" style="font-size:25px;"></i></button>
                                                        </a>
                                                         <a href="javascript:void(0)"   onclick="deleteRow(<?php echo $value['id']; ?>,  'session')"> <span><i  class="mdi mdi-delete-forever" style="font-size:30px; color: #eb2129;"></i></span> </a>
                                                        </a>
                                                    </td>
                                                </tr>
											<?php  } } ?>
												
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- container-fluid -->
            </div>
            <!-- content -->
             <footer class="footer">© 2018 LMS- <span class="d-none d-sm-inline-block"><i class="mdi mdi-heart text-danger"></i> </span>.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->

    <script type="text/javascript">
        
    function deleteRow(id, table) {
    var url  = '<?php echo  base_url();?>';

     var x=confirm("Are you sure to delete record?")
      if (x) {
        $.ajax({
             data: { 'id' : id, 'table':table},
             type: "post",
             url: url + "Application/deleteRow",
             success: function(data){
                  $('#current_row'+id).remove();
             }
        });
        return true;
      } else {
        return false;
      }
       

    }

    </script>>
