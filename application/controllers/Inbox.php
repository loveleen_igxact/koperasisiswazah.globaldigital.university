<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Kuala_Lumpur");


class Inbox extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

    function __construct() {

        parent::__construct();

        $this->load->library(array('form_validation','session'));

        $this->load->helper(array('form','url'));

        $this->load->model('AdminModel');	// loads admin model

        $this->CI = & get_instance();

        if(!isset($this->session->userdata['loggedInData'])) {

            redirect(base_url('login')); 

        }

    }

    public function index()

    {

        $session_data = $this->session->userdata['loggedInData'];

        if ($session_data['user_type'] == '3') {

            $where = array('to_id'=>$session_data['id'],'msg_type'=>'1','to_status'=>'1');

            $search = array('order'=>'DESC');

            $data['messages'] = $this->AdminModel->getData('inbox',$where,$search);

            $datas = array('to_status' => '0');

        } else {

            //$data['message_reply'] = $this->AdminModel->getData('message_reply',$where);

            $where = array('from_id'=>$session_data['id'],'msg_type'=>'1','from_status'=>'1');

            $search = array('order'=>'DESC');

            $data['messages'] = $this->AdminModel->getData('inbox',$where,$search); 

            $datas = array('from_status' => '0');

            //$data['messages'] = array();

        }

        if (isset($_POST['submit'])) {

            foreach ($_POST['checkbox'] as $key => $value) {

                $where = array('id'=>$value);

                $this->AdminModel->updateData($where,$datas,'inbox');

            }

            redirect(base_url('inbox'));

        }
        $this->load->helper('text');
        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/inbox',$data);

        $this->load->view('layout/footer');

    }

    public function compose_email()

    {

        $student_id = $_GET['id'];

        $session_data = $this->session->userdata['loggedInData'];

        $data['get_message_info'] = array();

        $data['get_student_info'] = $this->AdminModel->get_data_by_id('students','user_id',$student_id);

        if (isset($_POST['submit'])) {

            $random_number = substr(uniqid(rand(), True), 0, 6);

            $arr = array(

                'to_id'=>$student_id,

                'from_id'=>$session_data['id'],

                'subject'=>$_POST['subject'],

                'message'=>$_POST['message'],

                'msg_type'=>'1', //for inbox

                'f_read_status'=>'1', 

                't_read_status'=>'0', 

                'unique_no'=>$random_number, 

                'created'=>$_POST['created_at'],

            );

            $insert_data = $this->AdminModel->saveData($arr,'inbox');

            redirect(base_url('inbox/sent_message'));

        }

        if (isset($_POST['save_draft'])) {

            $arr = array(

                'to_id'=>$student_id,

                'from_id'=>$session_data['id'],

                'subject'=>$_POST['subject'],

                'message'=>$_POST['message'],

                'msg_type'=>'2', //for draft

                'f_read_status'=>'1',

                'created'=>$_POST['created_at'],

            );

            $insert_data = $this->AdminModel->saveData($arr,'inbox');

            redirect(base_url('inbox/draft'));

        } 
        $this->load->helper('text');

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/compose_email',$data);

        $this->load->view('layout/footer');

    }

    public function send_draft_message()

    {

        $message_id = $_GET['id'];

        $session_data = $this->session->userdata['loggedInData'];

        $data['get_message_info'] = $this->AdminModel->get_data_by_id('inbox','id',$message_id);

        $data['get_student_info'] = $this->AdminModel->get_data_by_id('users','id',$data['get_message_info']['to_id']);

        if (isset($_POST['submit'])) {

            $arr = array(

                'to_id'=>$data['get_message_info']['to_id'],

                'from_id'=>$session_data['id'],

                'subject'=>$_POST['subject'],

                'message'=>$_POST['message'],

                'msg_type'=>'1', //for inbox

                'f_read_status'=>'1', 

                't_read_status'=>'0', 

                'created'=>$_POST['created_at'],

            );

            
            $student_id=$data['get_message_info']['to_id'];
            $datastudent['studentdata'] = $this->AdminModel->getEmailStudent($student_id);
            $studentEmail=$datastudent['studentdata'][0]->email;
                                     $email = $studentEmail;
                                     $from_email = "admin@igxact.com";
                                    $this->load->library('email');

                                    $this->email->from($from_email, 'Message');

                                    $this->email->to($email);

                                    $this->email->subject($_POST['subject']);

                                    $this->email->message($_POST['message']);                         
                           
                           
                                        //Send mail

                                    if ($this->email->send()) {

                                        $this->session->set_flashdata("email_sent", "Email sent successfully.");
                                        // echo "success";

                                    } else {

                                        $this->session->set_flashdata("email_sent", "Error in sending Email.");
                                        // echo "failure";

                                        //  $this->load->view('email_form');

                                    }
            $wheres = array('id'=>$message_id);

            $this->AdminModel->updateData($wheres,$arr,'inbox');

            redirect(base_url('inbox/sent_message'));

        }

        if (isset($_POST['save_draft'])) {

            $arr = array(

                'to_id'=>$data['get_message_info']['to_id'],

                'from_id'=>$session_data['id'],

                'subject'=>$_POST['subject'],

                'message'=>$_POST['message'],

                'msg_type'=>'2', //for draft

                'f_read_status'=>'1', 

                't_read_status'=>'0', 

                'created'=>$_POST['created_at'],

            );

            $wheres = array('id'=>$message_id);

            $this->AdminModel->updateData($wheres,$arr,'inbox');

            redirect(base_url('inbox/draft'));

        } 

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/compose_email',$data);

        $this->load->view('layout/footer');

    }

    public function reply()

    {

        $message_id = $_GET['id'];

        $session_data = $this->session->userdata['loggedInData'];

        $data['get_message_info'] = $this->AdminModel->get_data_by_id('inbox','id',$message_id);

        if ($session_data['user_type'] == '3') {

            $user_id = $data['get_message_info']['from_id'];

            $datas['f_read_status'] = "0";

        } else {

            $user_id = $data['get_message_info']['to_id'];

            $datas['t_read_status'] = "0";

        }

        $datas['from_status'] = "1";

        $datas['to_status'] = "1";

        $data['get_student_info'] = $this->AdminModel->get_data_by_id('users','id',$user_id);

        if (isset($_POST['submit'])) {

            $arr = array(

                'sender_id'=>$session_data['id'],

                'msg_id'=>$message_id,

                'reply'=>$_POST['reply'],

                'created'=>date('Y-m-d H:i:sA'),

            );

            $insert_data = $this->AdminModel->saveData($arr,'message_reply');

            $wheres = array('id'=>$message_id);

            $datas['read_status'] = '0';

            $this->AdminModel->updateData($wheres,$datas,'inbox');

            redirect(base_url('inbox'));

        }

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/reply',$data);

        $this->load->view('layout/footer');

    }

    public function sent_message()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $where = array('from_id'=>$session_data['id'],'msg_type'=>'1','from_status'=>'1');

        $search = array('order'=>'DESC');

        $data['messages'] = $this->AdminModel->getData('inbox',$where,$search);

        //$data['messages'] = $this->AdminModel->getSentMsgs($session_data['id']);

        $data['page_type'] = "Sent Message";

        if ($session_data['user_type'] == '3') {

            $datas = array('to_status' => '0');

        } else {

            $datas = array('from_status' => '0');

        }

        if (isset($_POST['submit'])) {

            foreach ($_POST['checkbox'] as $key => $value) {

                $wheres = array('id'=>$value);

                $this->AdminModel->updateData($wheres,$datas,'inbox');

            }

            redirect(base_url('inbox/sent_message'));

        }
        $this->load->helper('text');

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/sent_message',$data);

        $this->load->view('layout/footer');

    }

    public function draft()

    {

        $session_data = $this->session->userdata['loggedInData'];

        $where = array('from_id'=>$session_data['id'],'msg_type'=>'2','from_status'=>'1');

        $search = array('order'=>'DESC');

        $data['messages'] = $this->AdminModel->getData('inbox',$where,$search);

        $data['page_type'] = "Draft";

        if ($session_data['user_type'] == '3') {

            $datas = array('to_status' => '0');

        } else {

            $datas = array('from_status' => '0');

        }

        if (isset($_POST['submit'])) {

            foreach ($_POST['checkbox'] as $key => $value) {

                $wheres = array('id'=>$value);

                $this->AdminModel->updateData($wheres,$datas,'inbox');

            }

            redirect(base_url('inbox/draft'));

        }
        $this->load->helper('text');

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/sent_message',$data);

        $this->load->view('layout/footer');

    }

    public function read_email()

    {

        $msg_id = $_GET['id'];

        $session_data = $this->session->userdata['loggedInData'];

        $wheres = array('id'=>$msg_id);

        $datas = array('read_status' => '1');

        $data['messages'] = $this->AdminModel->get_data_by_id('inbox','id',$msg_id);

        if ($session_data['user_type'] == '3') {

            $datas['t_read_status'] = "1";

            $id = $data['messages']['from_id'];

        } else {

            $datas['f_read_status'] = "1";

            $id = $data['messages']['to_id'];

        }

        $this->AdminModel->updateData($wheres,$datas,'inbox');

        $from_id = $data['messages']['from_id'];

        $to_id = $data['messages']['to_id'];

        $data['from_data'] = $this->AdminModel->get_data_by_id('users','id',$id);

        $data['to_data'] = $this->AdminModel->get_data_by_id('users','id',$from_id);

        $where = array('msg_id'=>$msg_id);

        $data['message_reply'] = $this->AdminModel->getData('message_reply',$where);

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/read_email',$data);

        $this->load->view('layout/footer');

    }

    public function create_new_message()

    {

        $session_data = $this->session->userdata['loggedInData'];
        $data['student_dataSerial'] = $this->AdminModel->student_dataSerial(); 
       date_default_timezone_set("Asia/Kuala_Lumpur"); 

        // $data['get_message_info'] = array();

        // $data['get_student_info'] = $this->AdminModel->get_data_by_id('students','user_id',$student_id);

        if (isset($_POST['submit'])) {

            $random_number = substr(uniqid(rand(), True), 0, 6);

            
            foreach ($_POST['student_ids'] as $key => $student_id) {
                date_default_timezone_set("Asia/Kuala_Lumpur"); 
                $arr = array(

                    'to_id'=>$student_id,

                    'from_id'=>$session_data['id'],

                    'subject'=>$_POST['subject'],

                    'message'=>$_POST['message'],

                    'msg_type'=>'1', //for inbox

                    'f_read_status'=>'1', 

                    't_read_status'=>'0', 

                    'unique_no'=>$random_number, 

                    'created'=>$_POST['created_at'],

                );      
                
               
            
            $datastudent['studentdata'] = $this->AdminModel->getEmailStudent($student_id);
            $studentEmail=$datastudent['studentdata'][0]->email;
         
          
                                     $email = $studentEmail;

                                     $from_email = "admin@igxact.com";
                                    $this->load->library('email');

                                    $this->email->from($from_email, 'Message');

                                    $this->email->to($email);

                                     // $this->email->set_header('mailtype', 'html');

                                    $this->email->subject($_POST['subject']);

                                    $this->email->set_mailtype("html");

                                    $messageS=$_POST['message'];
                                    // $this->email->attach("", "inline");

                                    
     // $text = strip_tags($messageS);


                                    $this->email->message($messageS); 
                                                             
                           
                           
                                        //Send mail

                                    if ($this->email->send()) {

                                        $this->session->set_flashdata("email_sent", "Email sent successfully.");
                                        // echo "success";

                                    } else {

                                        $this->session->set_flashdata("email_sent", "Error in sending Email.");
                                        // echo "failure";

                                        //  $this->load->view('email_form');

                                    }

                $insert_data = $this->AdminModel->saveData($arr,'inbox');


            }

            redirect(base_url('inbox/sent_message'));

        }

        if (isset($_POST['save_draft'])) {
            date_default_timezone_set("Asia/Kuala_Lumpur");
            foreach ($_POST['student_ids'] as $key => $student_id) {

            $arr = array(

                'to_id'=>$student_id,

                'from_id'=>$session_data['id'],

                'subject'=>$_POST['subject'],

                'message'=>$_POST['message'],

                'msg_type'=>'2', //for draft

                'f_read_status'=>'1',

                'created'=>$_POST['created_at'],

            );

            $insert_data = $this->AdminModel->saveData($arr,'inbox');

            redirect(base_url('inbox/draft'));

        } }

        $data['get_student_info'] = $this->AdminModel->getData('students'); 

        $this->load->view('layout/header');

        $this->load->view('layout/sidebar');

        $this->load->view('inbox/create_new_message',$data);

        $this->load->view('layout/footer');

    }

    public function get_data_by_id($table,$field,$id)

    {

        $get_data_by_id = $this->AdminModel->get_data_by_id($table,$field,$id);

        return $get_data_by_id;

    }

    public function getCount($table,$field,$id)

    {

        $get_data_by_id = $this->AdminModel->getCount($table,$field,$id);

        return $get_data_by_id;

    }


  

}