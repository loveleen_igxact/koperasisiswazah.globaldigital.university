<?php



$relation_arr = array("1" => "Father",

    "2"                       => "Mother",

    "3"                       => "Brother",

    "4"                       => "Sister",

    "5"                       => "Uncle",

    "6"                       => "Aunt",

    "7"                       => "Grandfather",

    "8"                       => "Grandmother",

    "9"                       => "Guardian",
    "10"                       => "Spouse",

);



$institute = array("22" => "INSTITUT PENDIDIKAN GURU (IPG)",

    "24"                    => "OTHERS",

    "8"                     => "UNIVERSITI ISLAM ANTARABANGSA MALAYSIA (UIAM)",

    "5"                     => "UNIVERSITI KEBANGSAAN MALAYSIA (UKM)",

    "3"                     => "UNIVERSITI MALAYA (UM)",

    "20"                    => "UNIVERSITI MALAYSIA KELANTAN (UMK)",

    "17"                    => "UNIVERSITI MALAYSIA PAHANG (UMP)",

    "18"                    => "UNIVERSITI MALAYSIA PERLIS (UNIMAP)",

    "10"                    => "UNIVERSITI MALAYSIA SABAH (UMS)",

    "2"                     => "UNIVERSITI MALAYSIA SARAWAK (UNIMAS)",

    "14"                    => "UNIVERSITI MALAYSIA TERENGGANU (UMT)",

    "11"                    => "UNIVERSITI PENDIDIKAN SULTAN IDRIS (UPSI)",

    "21"                    => "UNIVERSITI PERTAHANAN NASIONAL MALAYSIA (UPNM)",

    "6"                     => "UNIVERSITI PUTRA MALAYSIA (UPM)",

    "12"                    => "UNIVERSITI SAINS ISLAM ANTARABANGSA (USIM)",

    "4"                     => "UNIVERSITI SAINS MALAYSIA (USM)",

    "23"                    => "UNIVERSITI SULTAN AZLAN SHAH (USAS)",

    "19"                    => "UNIVERSITI SULTAN ZAINAL ABIDIN (UNISZA)",

    "16"                    => "UNIVERSITI TEKNIKAL MALAYSIA MELAKA (UTEM)",

    "7"                     => "UNIVERSITI TEKNOLOGI MALAYSIA",

    "13"                    => "UNIVERSITI TEKNOLOGI MARA (UITM)",

    "15"                    => "UNIVERSITI TUN HUSSEIN ONN (UTHM)",

    "9"                     => "UNIVERSITI UTARA MALAYSIA (UUM)",

    "-1"                    => "Others",

);



 $intake = array('1' => '2016' , '2' =>' 2017','3' => '2018','4'=>'2019');



 $campus = array('1' => 'UKM BANGI' , '2' =>'IPGKTR','3' => 'IPGKB','4' => 'KOTA KINABALU');



 $study = array('3' => 'DISTANCE LEARNING' , '4' =>'E-LEARNING','2' => 'PART TIME','1' => 'FULL TIME');

?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">

    <title>::LMS:Koperasi Siswazah Bangi Berhad &#8211; KSBB::</title>

    <meta content="Admin Dashboard" name="description">

    <link rel="shortcut icon" href="assets/images/fav_lms.png">

    <link href="<?php echo PLUG_PATH ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="<?php echo PLUG_PATH ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo PLUG_PATH ?>bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo CSS_PATH ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH ?>css/metismenu.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH ?>css/icons.css" rel="stylesheet" type="text/css">

    <link href="<?php echo CSS_PATH ?>css/style.css" rel="stylesheet" type="text/css">

    <!-- Dropzone css -->

    <link href="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">

  

</head>



<body>



<!--post educat-->





<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header tran-heading text-center ">

          <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">Review & Submit</h5>

            </div>

        

      </div>

      <div class="modal-body">



                 <form action="<?php echo base_url('application/review_submit/' . $id . '/step2/' . $action); ?>" method="post">



     <div class="container">

            <div class="row">



                <div class="col-md-12 pt-3">

                    <div class="form-group">

                        <label class="control-label"><strong> ELIGIBLE<span style="color: red;">*</span></strong></label>

                            <select name="eligible" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required>

                                <option value="">Select</option>

                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->

                   <option value=""></option>

                  <option value="1">ELIGIBLE</option>

                  <option value="2">INELIGIBLE </option>



                        </select>

                    </div>

                </div>

                <div class="col-md-12">

                  <div class="form-group">

                    <label class="control-label"><strong> Program<span style="color: red;">*</span></strong></label>
                    <?php 
                          $myArray = json_decode(json_encode($total_p), true);
                          
                          // $valueArray=array_reverse($myArray);
                          
                        ?>

                    <?php $get_program1 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p1_program']);

                      $get_program2 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p2_program']); ?>

                    <select name="eligible_program"  class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" >
                      <option value = "">Select</option>
                      <option value="<?php echo $get_program1['id'] ?>" ><?php echo $get_program1['name'] ?></option>
                      <option value="<?php echo $get_program2['id'] ?>" ><?php echo $get_program2['name'] ?></option>

                      <!-- <option value = "">Select</option>
                        <?php foreach($myArray as  $value) {  ?>
                         <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>
                        <?php  } ?> -->
                    </select>

                  </div>

                </div>



                <div class="col-md-12">

                    <div class="form-group">

             <button type="submit" class="btn color_another">Save</button>

        </div>

        </div>

            </div>

           </div>



          </form>





      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>

    </div>



  </div>

</div>





<!---- review---------------->

<div class="modal fade" id="contact_application1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

          <div class="modal-header tran-heading text-center ">

            <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">New Past Education</h5>

            </div>

           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> -->

          </div>

          <hr>

    <div class="modal-body">

      <form action="<?php echo base_url('application/add_education/' . $id . '/step2/' . $action); ?>" method="post">

           <div class="container">

            <div class="row">

                <hr>

                <div class="col-md-4">

                    <div class="form-group">

                        <label class="control-label"><strong> Qualification<span id="" style="color: red;">*</span> : <span id="post_job_qua">Diploma</span></strong></label>

                    </div>

                </div>

                <input type="hidden" name="education_id" id="education_id" value="">

                <div class="col-md-4">

                  <div class="form-group">

                      <label class="control-label"><strong>Institute</strong></label>

                      <select name="institute" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                        <option>Select</option>

                              <?php foreach($institutions as  $institutes) {  ?>

                        <option <?php if ($institutes['id'] == $prospects_data[0]['institute']){ ?> selected

                          

                        <?php } ?>  value="<?php echo $institutes['id'] ?>" ><?php echo $institutes['name'] ?></option>

                                <?php  } ?>

                      </select>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label><strong>Program</strong></label>

                        <input type="text" name="program" class="form-control " required="" placeholder="Diploma In Multimedia">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label><strong> Grade/CGPA</strong></label>

                        <input type="text" name="grade"  class="form-control " required="" placeholder="A / 3.00">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label><strong> Completion Year</strong></label>

                        <input type="text" name="completion_year" class="form-control " required="" placeholder="2000">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Remarks</label>

                        <input type="text" class="form-control " name="remark" required="" placeholder="Remarks">

                     </div>

                </div>

            </div>

           </div>

          </div>

          <div class="modal-footer">

            <button type="submit" class="btn btn-lg color_another">Save</button>

            <button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">Cancel</button>



          </div>

        </div>



  </form>

      </div>

    </div>







    <!--modal -->

     <div class="modal fade" id="contact_application" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

      <form action="<?php echo base_url('application/add_contact/' . $id . '/step3/' . $action); ?>" id="contact_formData" name="personal" method="post">

    <input type="hidden" value="" id="prospects_id" name="prospects_id">

      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

          <div class="modal-header tran-heading text-center ">

            <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">New Contact</h5>

            </div>

           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> -->

          </div>

          <hr>

          <div class="modal-body">

           <div class="container">

            <div class="row">

                <hr>

                <div class="col-md-4">

                    <div class="form-group">



                        <label class="control-label"><strong> Relationship<span style="color: red;">*</span></strong></label>



                            <select name="relationship" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required>



                                <option value="">Select</option>



                                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->

                  <option value="1">Father</option>

                  <option value="2">Mother</option>

                  <option value="3">Brother</option>

                  <option value="4">Sister</option>

                  <option value="5">Uncle</option>

                  <option value="6">Aunt</option>

                  <option value="7">Grandfather</option>

                  <option value="8">Grandmother</option>

                  <option value="9">Guardian</option>
                  <option value="10">Spouse</option>


                            </select>



                    </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Name<span style="color: red;">*</span></label>

                        <input type="text" name="name" class="form-control " required="" placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                  <div class="form-group">

                      <label class="control-label"><strong> ID Type<span style="color: red;">*</span></strong></label>

                        <select name="id_type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                          <option>Select</option>

                          <option value="1"> Passport</option>

                          <option value="2">Identity Card</option>

                        </select>

                  </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label> ID No<span style="color: red;">*</span></label>

                        <input name="id_no" type="text" class="form-control " required="" placeholder="eg:901021645632">

                     </div>

                </div>

                 <div class="col-md-4">

                    <div class="form-group">

                        <label class="control-label"><strong>Race</strong></label>

                          <select name="race" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                            <option>Select</option>

                                  <?php foreach($race as  $race_value) {  ?>

                            <option <?php //if ($race_value['id'] == $prospects_data[0]['race']){ //echo "selected"; ?> 

                            <?php //} ?>  value="<?php echo $race_value['id'] ?>" ><?php echo $race_value['name'] ?></option>

                                    <?php  } ?>

                          </select>

                    </div>

                </div>

                <div class="col-md-4">

                  <div class="form-group">

                      <label> Occupation<span style="color: red;">*</span></label>

                      <input type="text"  name="occupation" class="form-control " required="" placeholder="eg:Teacher/Banker">

                   </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Phone</label>

                        <input type="text" name="phone" class="form-control " required="" placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Secondary Phone</label>

                        <input type="text" name="secondary_phone" class="form-control "  placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Email</label>

                        <input type="Email" name="email" class="form-control "  placeholder="">

                     </div>

                </div>

                 <div class="col-md-4">

                    <div class="form-group">

                        <label>Anual Income</label>

                        <input type="number"  name="anual_income" class="form-control "  placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Liability</label>

                        <input type="number" name="liability"  class="form-control "  placeholder="">

                     </div>

                </div>

                <!--<div class="col-md-4">-->

                    <!-- Material unchecked -->

                <!--    <div class="custom-control custom-checkbox pt-4">-->

                <!--        <input type="checkbox" name="copy_address" class="custom-control-input" id="defaultUnchecked">-->

                <!--        <label class="custom-control-label" for="defaultUnchecked">Copy Address From Student</label>-->

                <!--    </div>-->

                <!--</div>-->

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Address 1<span style="color: red;">*</span> </label>

                        <input type="text" name="address_1" class="form-control " required="" placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Address 2 </label>

                        <input type="text" name="address_2" class="form-control "  placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                    <div class="form-group">

                        <label>City<span style="color: red;">*</span> </label>

                        <input type="text"  name="city" class="form-control " required="" placeholder="">

                     </div>

                </div>

                <div class="col-md-4">

                                     <div class="form-group">

                                            <label class="control-label"><strong>Country</strong></label>

                                                <select name="country" class="form-control" tabindex="-1" aria-hidden="true" id="contactcountry">

                                                    <option>Select</option>

                                                    <?php foreach($country as  $value) {  ?>

                                                      <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>

                                                      <?php  } ?>

                                                </select>
                                        </div>

                   <!--  <div class="form-group">

                      <label class="control-label"><strong>Country</strong></label>

                        <select name="country" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                          <option>Select</option>

                                <?php foreach($country as  $value) {  ?>

                          <option <?php //if ($value['id'] == $prospects_data[0]['country']) { ?> 

                            

                          <?php //} ?>  value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>

                                  <?php  } ?>

                        </select>

                    </div> -->

                </div>

                <div class="col-md-4">
                            <div class="form-group">
                                            <label class="control-label"><strong>State</strong></label>
                                            <select class="form-control select2 select2-hidden-accessiblee" tabindex="-1" aria-hidden="true" name="state" id="state">

                                                    <option>Select</option>

                                                <!-- <select name="statecontact" class="form-control" tabindex="-1" aria-hidden="true" id="contactstate">

                                                    <option>Select</option>

                                                    <?php foreach($states as  $value) {  ?>

                                                      <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>

                                                      <?php  } ?> -->

                                                </select>
                              </div>
                   
                    </div>

               

                <div class="col-md-4">

                    <div class="form-group">

                        <label>Postcode<span style="color: red;">*</span> </label>

                        <input type="text" name="postcode" class="form-control "  placeholder="">

                     </div>

                </div>

            </div>

           </div>

          </div>

          <div class="modal-footer">

            <button type="submit" class="btn btn-lg color_another" id="contact-form" >Save</button>

            <button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">Cancel</button>



          </div>

        </div>

      </div>

  </form>

    </div>







  <!-- document -->







  <div class="modal fade" id="document_application" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

<form id="uploadForm" action="upload.php" method="post">



      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

          <div class="modal-header tran-heading text-center ">

            <div class="col-md-12 text-center">

                <h5 class="modal-title" id="exampleModalLabel">Upload document</h5>

            </div>

           <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">&times;</span>

            </button> -->

          </div>

          <hr>

          <div class="modal-body">

           <div class="container">

            <div class="row">

                <hr>













        <div class="col-md-12">

                    <div class="form-group">



                        <label class="control-label"><strong> Document Type<span style="coolr: red;">*</span></strong></label>



                            <select required name="document_type" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">



                                  <?php



foreach ($doc_data as $rows): ?>

                                    <option value="<?php echo $rows->document_id; ?>"><?php echo $rows->name; ?></option>

                                     <?php endforeach;?>

                            </select>



                    </div>

                </div>

                <div class="col-md-12">

                    <div class="form-group">

                        <label>Name<span style="color: red;">*</span></label>

                        <input type="file" name="userImage" class="userImage form-control " required="" placeholder="">

                     </div>

                </div>

               <input type="hidden" name="step" value ="5">













            </div>

           </div>

          </div>

          <div class="modal-footer">

            <button type="submit" class="btn btn-lg color_another"  id="contact-form1" >Save</button>

            <button type="button" class="btn btn-lg btn-secondary" id="cans" data-dismiss="modal">Cancel</button>



          </div>

        </div>

      </div>

  </form>

    </div>











    <!--end-->

    <!-- Begin page -->

    <div id="wrapper">

        <!-- Top Bar Start -->

        <?php $this->load->view('layout/header');?>

        <!-- Top Bar End -->

        <!-- ========== Left Sidebar Start ========== -->

       <?php $this->load->view('layout/sidebar');?>

        <!-- Left Sidebar End -->



        <!-- ============================================================== -->

        <!-- Start right Content here -->

        <!-- ============================================================== -->

      <div class="content-page">

        <div class="content">

          <div class="container-fluid">

            <div class="row">

              <div class="col-sm-12">

                <div class="page-title-box">

                  <!-- <h4 class="page-title"><span><i class="ion-briefcase bg_icon_color"></i></span> Add New Application</h4> -->

                </div>

              </div>

            </div>

          </div>

                    <!-- end row -->

            <!-- <div class="col-12"> -->

              <div class="card m-b-30">

                <div class="card-body">

                  <div id="prospect"></div>

                    <div style="clear:both"></div>

                      <div class="row">

                        <div class="card-body">

                          <h4 class="mt-0 header-title">Application Form</h4>

                            <ul class="nav nav-tabs nav-tabs-custom" role="tablist">

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step1' || $setup == '') {echo 'active show';} else {echo '';}?>" id="personal" data-toggle="tab" href="#home1" role="tab" aria-selected="false">Personal</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step2') {echo 'active show';} else {echo '';}?>" id="past_education" data-toggle="tab" href="#profile1" role="tab" aria-selected="false">Past Education</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step3') {echo 'active show';} else {echo '';}?> " id="contact" data-toggle="tab" href="#messages1" role="tab" aria-selected="false">Contact</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step4') {echo 'active show';} else {echo '';}?> " id="program" data-toggle="tab" href="#settings1" role="tab" aria-selected="true">Program</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step5') {echo 'active show';} else {echo '';}?>" id="documents"  data-toggle="tab" href="#settings2" role="tab" aria-selected="true">Documents</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step6') {echo 'active show';} else {echo '';}?> "  id="upload_photos" data-toggle="tab" href="#settings3" role="tab" aria-selected="true">Upload Photos</a>

                              </li>

                              <li class="nav-item">

                              <a class="nav-link <?php if ($setup == 'step7') {echo 'active show';} else {echo '';}?>" id="review" data-toggle="tab" href="#settings4" role="tab" aria-selected="true">Review & Submit</a>

                              </li>

                            </ul>

                      <div class="tab-content">

                        <div class="tab-pane <?php if ($setup == 'step1' || $setup == '') {echo 'active show';} else {echo '';}?> p-3" id="home1" role="tabpanel">

                          <form action="<?php echo base_url('application/add_app1/' . $id . '/step1/' . $action); ?>" id="personalformData" name="personal" method="post">

                          <div class="row">

                            <div class="personal col-12">

                              <h6>Personal Information</h6>

                            </div>




                            <div class="col-md-4">

                              <label>International</label><br>

                              <!-- <div class="btn-group btn-group-toggle" data-toggle="buttons"> -->

                                <!-- <label class="btn btn-full active"> -->

                                  <div class="custom-control custom-radio custom-control-inline">

                                <input type="radio" class="custom-control-input" id="defaultInline3" name="international" <?php echo (@$prospects_data[0]['international'] == 'o') ? 'checked' : ''; ?> mdbinputdirective="" value="o" required>

                                <label class="custom-control-label" for="defaultInline3">No</label>

                                </div>

                                <div class="custom-control custom-radio custom-control-inline">

                                <input type="radio" class="custom-control-input" id="defaultInline4" <?php echo (@$prospects_data[0]['international'] == 'y') ? 'checked' : ''; ?> name="international" mdbinputdirective="" value="y" required>

                                <label class="custom-control-label" for="defaultInline4">Yes</label>

                               <!--  <input type="radio" name="international" value="o" id="option2" <?php echo (@$prospects_data[0]['international'] == 'o') ? 'checked' : ''; ?>> No </label>

                                <label class="btn btn-full ">

                                <input type="radio" name="international" value="y" id="option3" <?php echo (@$prospects_data[0]['international'] == 'y') ? 'checked' : ''; ?>> Yes</label> -->

                              </div>

                            </div>

                            <div class="col-md-8">

                              <div class="form-group">

                                <label><strong>Gender<span style="color: red;">*</span></strong></label><br>

                                <div class="custom-control custom-radio custom-control-inline">

                                <input type="radio" class="custom-control-input" id="defaultInline3" name="gender" <?php echo (@$prospects_data[0]['gender'] == 'm') ? 'checked' : ''; ?> mdbinputdirective="" value="m" required>

                                <label class="custom-control-label" for="defaultInline3">Male</label>

                                </div>

                                <div class="custom-control custom-radio custom-control-inline">

                                <input type="radio" class="custom-control-input" id="defaultInline4" <?php echo (@$prospects_data[0]['gender'] == 'f') ? 'checked' : ''; ?> name="gender" mdbinputdirective="" value="f" required>

                                <label class="custom-control-label" for="defaultInline4">Female</label>

                                </div>

                              </div>

                            </div>

                            <div class="col-md-4">

                              <div class="form-group">

                                <label class="control-label"><strong>ID Type <span style="color: red;">*</span></strong></label>

                                <select name="idtype" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                                  <option value="">Select</option>

                                  <option value="1" <?php echo (@$prospects_data[0]['idtype'] == '1') ? 'selected' : '' ?>>Passport</option>

                                  <option value="2" <?php echo (@$prospects_data[0]['idtype'] == '2') ? 'selected' : '' ?>>Identity Card</option>

                                </select>

                              </div>

                            </div>

                             <div class="col-md-4">

                                <div class="form-group">

                                    <label>ID No<span style="color: red;">*</span></label>

                                    <input type="text"  name="id_no" readonly class="form-control " placeholder="eg: 123456789" value="<?php echo @$prospects_data[0]['id_no'] ?>">

                                 </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label>Name<span style="color: red;">*</span></label>

                                    <input type="text" class="form-control "  name="name" required="" placeholder="" value="<?php echo @$prospects_data[0]['name'] ?>">

                                 </div>

                            </div>

                            <div class="col-md-4">

                              <div class="form-group">

                                <label class="control-label"><strong>Salutation<span style="color: red;"></span></strong></label>

                               <!-- <select name="salutation" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                                  <option value="">Select</option>

                                  <option value="Mr" <?php echo (@$prospects_data[0]['salutation'] == 'Mr') ? 'selected' : '' ?>>Mr.</option>

                                  <option value="Mrs" <?php echo (@$prospects_data[0]['salutation'] == 'Mrs') ? 'selected' : '' ?>>Mrs.</option>
                                  <option value="Miss" <?php echo (@$prospects_data[0]['salutation'] == 'Miss') ? 'selected' : '' ?>>Miss</option>
 -->
                                    <input type="text" readonly class="form-control "  name="salutation" required="" placeholder="" value="<?php 
                                             if(@$prospects_data[0]['gender'] == 'f'){
                                              
                                              echo "Miss";
                                            }
                                            else{
                                              echo "Mr";
                                            }
                                               ?> "> 
                                 


                              <!--   </select> -->

                              </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">

                                    <label>Place of Birth</label>

                                    <input name="place_of_birth" type="text" class="form-control "  value="<?php echo @$prospects_data[0]['place_of_birth'] ?>" placeholder="" >

                                 </div>

                            </div>

                            <div class="col-md-4">

                              <div class="form-group">

                                  <label><strong>Date Of Birth<span style="color: red;">*</span></strong></label>

                                  <div>

                                    <div class="input-group">

                                      <input required value="<?php echo @$prospects_data[0]['date_Of_birth'] ?>" name="date_of_birth" type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">

                                      <div class="input-group-append"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span>

                                      </div>

                                    </div>

                                </div>

                              </div>

                            </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Email<span style="color: red;">*</span></label>

                                            <input type="Email" name="email" class="form-control " required="" placeholder="" value="<?php echo @$prospects_data[0]['email'] ?>">

                                         </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Secondary Email</label>

                                            <input type="Email" name="secondary_email" class="form-control "  placeholder="" value="<?php echo @$prospects_data[0]['secondary_email'] ?>">

                                         </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Phone<span style="color: red;">*</span></label>

                                            <input type="text" name="phone" class="form-control " required="" placeholder="" value="<?php echo @$prospects_data[0]['phone'] ?>">

                                         </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Emergency contact<span style="color: red;">*</span> </label>

                                            <input type="text" class="form-control" name="emergency_contact" required="" value="<?php echo @$prospects_data[0]['emergency_contact'] ?>" placeholder="" required> 

                                        </div>

                                        

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Nationality</strong></label>

                                                <select name="nationality" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required ="">

                                                    <option value="">Select</option>

                                                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->



                                            <?php foreach($nationalities as $nation) { ?>

                                                     <option value="<?php echo $nation['NationalityID']  ?>" <?php echo (@$prospects_data[0]['nationality'] ==  $nation['NationalityID']) ? 'selected' : '' ?>>

                                                      <?php echo $nation['Nationality'];  ?></option>
                                                     <?php  } ?>



                                                </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Race</strong></label>

                                                <select name="race"  class="form-control " tabindex="-1" aria-hidden="true" required="">

                                                    <option value="">Select</option>

                                                      <?php foreach($race as $r) { ?>

                                                     <option value="<?php echo $r['id']  ?>" <?php echo (@$prospects_data[0]['race'] ==  $r['id']) ? 'selected' : '' ?>><?php echo $r['name']  ?></option>

                                                     

                                                     <?php  } ?>

                                                   </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Religion</strong></label>

                                                <select  name="religion" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                                                    <option value="">Select</option>

                                                     <?php foreach($religion as $religon) { ?>

                                                     <option value="<?php echo $religon['id']  ?>" <?php echo (@$prospects_data[0]['religion'] ==  $religon['id']) ? 'selected' : '' ?>><?php echo $religon['name']  ?></option>

                                                     

                                                     <?php  } ?>

                                                </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Marital</strong></label>

                                            <select name="marital" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required>

                                            <option value="">Select</option>

                                            <option value="2" <?php echo (@$prospects_data[0]['marital'] == '2') ? 'selected' : '' ?>>Married</option>

                                            <option value="1" <?php echo (@$prospects_data[0]['marital'] == '1') ? 'selected' : '' ?>>Single</option>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                          <label>Disability</label><br>

                                          <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                              <label class="btn btn-full active">

                                              <input type="radio" id="option2" name="disability" value="0" <?php echo (@$prospects_data[0]['disability'] == 'no') ? 'checked' : ''; ?>> No </label>

                                              <label class="btn btn-full ">

                                              <input name="disability" <?php echo (@$prospects_data[0]['disability'] == 'yes') ? 'checked' : ''; ?> value="1" type="radio" id="option3"> Yes</label>

                                          </div>

                                      </div>

                                    <div class="col-md-6 personal">

                                        <h6>Address</h6>

                                        <div class="row">

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label><strong>Address Line 1</strong></label>

                                            <input type="text" name="address_line1" class="form-control " value="<?php echo @$prospects_data[0]['address_line1'] ?>" placeholder="">

                                         </div>

                                    </div>

                                     <div class="col-md-6">

                                        <div class="form-group">

                                            <label><strong> Address Line 2</strong></label>

                                            <input type="text" value="<?php echo @$prospects_data[0]['address_line2'] ?>" name="address_line2" class="form-control "  placeholder="">

                                         </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label><strong>City</strong></label>

                                            <input type="text" value="<?php echo @$prospects_data[0]['city'] ?>" name="city" class="form-control "  placeholder="">

                                         </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-group">

                                          <label class="control-label"><strong>Country</strong></label>

                                            <select name="country" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                                              <option>Select</option>

                                                    <?php foreach($country as  $value) {  ?>

                                              <option <?php if ($value['id'] == $prospects_data[0]['country']){ ?> selected

                                                

                                              <?php } ?>  value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>

                                                      <?php  } ?>

                                            </select>

                                         </div>

                                    </div>

                                    

                                    <div class="col-md-12">

                                      <div class="form-group">

                                        <label class="control-label"><strong>State</strong></label>

                                        <select name="state" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                                          <option>Select</option>

                                                <?php foreach($states as  $state) {  ?>

                                          <option <?php if ($state['id'] == $prospects_data[0]['state']){ ?> selected

                                            

                                          <?php } ?>  value="<?php echo $state['id'] ?>" ><?php echo $state['name'] ?></option>

                                                  <?php  } ?>

                                        </select>

                                      </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label><strong> Postcode</strong></label>

                                            <input type="text" name="postcode" class="form-control " value="<?php echo @$prospects_data[0]['postcode'] ?>" placeholder="">

                                         </div>

                                    </div>

                                </div>

                                </div>

                                   <div class="col-md-6 personal">

                                        <h6>Secondary Address</h6>

                                        <div class="row">

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label><strong>Address Line 1</strong></label>

                                            <input type="text" name="s_address_line1" class="form-control " value="<?php echo @$prospects_data[0]['s_address_line1'] ?>" placeholder="">

                                         </div>

                                    </div>

                                     <div class="col-md-6">

                                        <div class="form-group">

                                            <label><strong> Address Line 2</strong></label>

                                            <input type="text" name="s_address_line2" class="form-control " value="<?php echo @$prospects_data[0]['s_address_line2'] ?>" placeholder="">

                                         </div>

                                    </div>

                  <div class="col-md-12">

                    <div class="form-group">

                      <label><strong>City</strong></label>

                      <input type="test" name="s_city" class="form-control "  placeholder="" value="<?php echo @$prospects_data[0]['s_city'] ?>">

                    </div>

                  </div>

                  <div class="col-md-12">

                    <div class="form-group">

                      <label class="control-label"><strong>Country</strong></label>

                      <select name="s_country" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                        <option>Select</option>

                        <option>Select</option>

                        <?php foreach($country as  $value) {  ?>

                        <option value="<?php echo $value['id'] ?>" ><?php echo $value['name'] ?></option>

                        <?php  } ?>

                      </select>

                    </div>

                  </div>

                    <div class="col-md-12">

                      <div class="form-group">

                        <label class="control-label"><strong>State</strong></label>

                        <select name="s_state" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                          <option>Select</option>

                          <?php foreach($states as  $state) {  ?>

                          <option value="<?php echo $state['id'] ?>" ><?php echo $state['name'] ?></option>

                                <?php  } ?>

                              </select>

                          </div>

                      </div>

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <label><strong> Postcode</strong></label>

                                            <input type="text" name="s_postcode" class="form-control " value="<?php echo @$prospects_data[0]['s_postcode'] ?>" placeholder="">

                                         </div>

                                    </div>

                                </div>

                                </div>

                               <div class="col-md-6 personal">

                                            <h6>Recruited by<!-- <span style="color: red;">*</span> --></h6>

                                            <div class="row">

                                      <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Marketing Source</strong></label>

                                            <select name="marketing_source" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                                              <option>Select</option>

                                                    <?php foreach($marketing_source as  $source) {  ?>

                                              <option <?php if ($source['id'] == $prospects_data[0]['marketing_source']){ ?> selected

                                                

                                              <?php } ?>  value="<?php echo $source['id'] ?>" ><?php echo $source['name'] ?></option>

                                                      <?php  } ?>

                                            </select>

                                        </div>

                                    </div>

                                  <!--  <div class="col-md-6">

                                        <div class="form-group">

                                            <label class="control-label"><strong>Marketing Staff</strong></label>

                                                <select name="marketing_staff" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                     <option value="1" <?php echo (@$prospects_data[0]['marketing_staff'] == '1') ? 'selected' : '' ?>>option1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['marketing_staff'] == '2') ? 'selected' : '' ?>>option2</option>

                                                </select>

                                        </div>

                                    </div>  -->

                                </div>

                                      <!--  <div class="form-group">

                                            <label class="control-label"><strong>Agent</strong></label>

                                                <select name="agent" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                      <option value="1" <?php echo (@$prospects_data[0]['agent'] == '1') ? 'selected' : '' ?>>agent1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['agent'] == '2') ? 'selected' : '' ?>>agent2</option>

                                                </select>

                                        </div>

                                         <div class="form-group">

                                            <label class="control-label"><strong>Student Get Student</strong></label>

                                                <select name="student_get_student" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                      <option value="1" <?php echo (@$prospects_data[0]['student_get_student'] == '1') ? 'selected' : '' ?>>option1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['student_get_student'] == '2') ? 'selected' : '' ?>>option2</option>

                                                </select>

                                        </div>  -->

                                        <!-- <div class="form-group">

                                            <label class="control-label"><strong>Staff Get Student</strong></label>

                                                <select name="staff_get_student" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                     <option value="1" <?php echo (@$prospects_data[0]['staff_get_student'] == '1') ? 'selected' : '' ?>>option1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['staff_get_student'] == '2') ? 'selected' : '' ?>>option2</option>

                                                </select>

                                        </div>

                                        <div class="form-group">

                                             <label class="control-label"><strong>Other</strong></label>

                                           <div><textarea  name="other" class="form-control" value="" placeholder="Other Recruiter information" rows="5"><?php echo $prospects_data[0]['other'] ?></textarea></div>

                                        </div>  -->

                                </div>



                               <div class="col-md-6 personal">

                                        <!--    <h4>Other</h4>

                                        <div class="form-group">

                                            <label class="control-label"><strong>Sponsor</strong></label>

                                                <select class="form-control select2 select2-hidden-accessible" name="sponsor" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                    <option value="1" <?php echo (@$prospects_data[0]['sponsor'] == '1') ? 'selected' : '' ?>>sponsor1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['sponsor'] == '2') ? 'selected' : '' ?>>sponsor2</option>

                                                </select>

                                        </div>

                                        <div class="form-group">

                                                    <label><strong>Hostal Required</strong></label><br>

                                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                                        <label class="btn btn-full active">

                                                        <input type="radio" name="hostal" id="option2"> No </label>

                                                        <label class="btn btn-full ">

                                                        <input type="radio" name="hostal" id="option3"> Yes</label>

                                                    </div>

                                                </div>

                                        <div class="form-group">

                                            <label class="control-label"><strong>Room Type</strong></label>

                                                <select class="form-control select2 select2-hidden-accessible" name="room_type" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                    <option value="1" <?php echo (@$prospects_data[0]['room_type'] == '1') ? 'selected' : '' ?>>room type 1</option>

                                                     <option value="2" <?php echo (@$prospects_data[0]['room_type'] == '2') ? 'selected' : '' ?>>room type 2</option>

                                                </select>

                                        </div>

                                        <div class="form-group">

                                            <label class="control-label"><strong>Information Source <small>(How Do You Know About Us?)</small></strong></label>

                                                <select name="information_source" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                                    <option>Select</option>

                                                     <option value="internet" <?php echo (@$prospects_data[0]['information_source'] == 'internet') ? 'selected' : '' ?>>internet</option>

                                                     <option value="news" <?php echo (@$prospects_data[0]['information_source'] == 'news') ? 'selected' : '' ?>>news</option>

                                                </select>

                                        </div>

                                        <div class="form-group">

                                            <label class="control-label"><strong>Remarks</strong></label>

                                           <div><textarea  name="remark" class="form-control" rows="5"><?php echo $prospects_data[0]['remark'] ?></textarea></div>

                                        </div>  -->

                                        

                                </div>

                                <div class="col-md-12 text-center">

                                            <button type="submit" id="personal-form" class="btn color_another btn-lg"> Save & Continue</button>

                                        </div>

                            </form>

                                    </strong>

                                </div>

                                            </div>



                <div class="tab-pane p-3 <?php if ($setup == 'step2') {echo 'active show';} else {echo '';}?>" id="profile1" role="tabpanel">

                  <h6>Past Education</h6>

                  <div class="col-md-4 ml-1" style="padding: 0px;" >

                    <div class="form-group">

                      <label class="control-label"><strong>Education Level <span style="color: red;">*</span></strong></label>

                      <select id="educatiion_level" onChange="past_education(this.value, this.text);" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                        <option value="">Select</option>

                        <option value="8">Degree</option>

                        <option value="7">Diploma</option>

                        <option value="9">Masters</option>

                      </select>

                    </div>

                  </div>

                  <div class="text-right pb-2">

                    <button type="button" id="add_eduction" disabled class="btn color_another waves-effect" data-toggle="modal" data-target="#contact_application1">Add New</button>

                  </div>

                <table class="table table-bordered">

                  <thead class="black white-text">

                    <tr>

                      <th scope="col">No</th>

                      <th scope="col">Institute</th>

                      <th scope="col">Qualification</th>

                      <th scope="col">Grade/CGPA</th>

                      <th scope="col">Year</th>

                      <!--<th scope="col">Action</th>-->

                    </tr>

                  </thead>

                <tbody>

              <?php

                $course = array('8' => 'Degree', '7' => 'Diploma', '9' => 'Masters');

                  foreach ($past_education as $education) {?>

                    <tr>

                    <th scope="row"><?php echo $education['id']; ?> </th>

                    <th scope="row"><?php echo $institute[$education['institute']]; ?> </th>

                    <td><?php echo @$course[$education['education_id']]; ?></td>

                    <td><?php echo $education['grade']; ?></td>

                    <td><?php echo $education['completion_year']; ?></td>

                    </tr>

                  <?php }?>

                </tbody>

              </table>

              <div class="float-left">

                <button type="button" class="btn btn-secondary waves-effect btn-lg btnPrevious" >Previous</button>

              </div>

              <div class="float-right">

                <button type="button" class="btn btn-lg waves-effect color_another" onclick="step1()">Save & Continue</button>

              </div>

              <div class="clearfix"></div>

            </div>

            <div class="tab-pane p-3 <?php if ($setup == 'step3') {echo 'active show';} else {echo '';}?>" id="messages1" role="tabpanel">

              <h6>Contact</h6>

              <div class="col-md-4 ml-1" style="padding: 0px;" >

              </div>

              <div class="text-right pb-2">

                <button type="button" class="btn color_another waves-effect" data-toggle="modal" data-target="#contact_application">Add New</button>

              </div>

              <table class="table table-bordered">

                <thead class="black white-text">

                <tr>

                  <th scope="col">No</th>

                  <th scope="col">Relationship</th>

                  <th scope="col">Name</th>

                  <th scope="col">Contact</th>

                </tr>

                </thead>

                <tbody>

                <?php foreach ($contact_data as $contacts) {?>

                <tr>

                  <th scope="row"> <?php echo $contacts['id']; ?></th>

                  <td> <?php echo @$relation_arr[$contacts['relationship']]; ?></td>

                  <td> <?php echo $contacts['name']; ?></td>

                  <td> <?php echo $contacts['phone']; ?></td>

                </tr>

                <?php }?>

                </tbody>

              </table>

              <div class="float-left">

                <button type="button" class="btn btn-secondary waves-effect btn-lg btnPrevious">Previous</button>

              </div>

              <div class="float-right">

                <button onclick="step2()" type="button" id="education-form" onClick="step2();" class="btn color_another btn-lg"> Save & Continue</button>

              </div>

            </div>

            <div class="tab-pane p-3 <?php if ($setup == 'step4') {echo 'active show';} else {echo '';}?>" id="settings1" role="tabpanel">

              <form action="<?php echo base_url('application/add_program/' . $id . '/step4/' . $action); ?>" id="program_submit_form" name="personal" method="post">

                <div class="row">

                  <div class="col-md-4 personal">

                    <h6>Program <span style="color: red;">*</span></h6>

                    <input type="hidden" value="" id="prospects_id1" value="<?php echo @$id; ?>" name="prospects_id">

                    <div class="form-group">

                      <label class="control-label"><strong>Intake<span style="color: red;">*</span></strong></label>

                      <select required name="intake" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">
                    

                        <option value=""> select </option>
                        
                         <?php foreach ($intakeP as $key => $value) { ?>
                          <option value="<?php echo $value['id']  ?>" <?php echo (@$prospects_data[0]['intake'] ==  $value['id']) ? 'selected' : '' ?>>
                          <?php echo $value['intake']  ?></option>
                          <?php } ?>

                      </select>

                    </div>

                  </div>

                <div class="col-md-8 personal">

                  <h6>Program 1 <span style="color: red;">*</span></h6>

                  <div class="form-group">

                    <label class="control-label"><strong>Campus</strong></label>

                    <select name="p1_campus" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                      <option>Select</option>

                      <?php foreach($campuses as  $campus_val) {  ?>

                      <option <?php if(@$prospects_data[0]['p1_campus'] == $campus_val['id']) { echo "selected"; } ?> value="<?php echo $campus_val['id'] ?>" ><?php echo $campus_val['name'] ?></option>

                      <?php  } ?>

                    </select>

                  </div>

                  <div class="form-group">

                    <label class="control-label"><strong>Program1</strong></label>

                    <select name="p1_program" required class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                      <option>Select</option>

                      <?php foreach($campus_program as  $program1) {  ?>

                      <option value="<?php echo $program1['id']  ?>" <?php echo (@$prospects_data[0]['p1_program'] ==  $program1['id']) ? 'selected' : '' ?>>
                      <?php echo $program1['name']  ?></option>

                      <?php  } ?>

                    </select>

                  </div>

                  <div class="form-group">

                    <label class="control-label"><strong>Program2</strong></label>

                    <select name="p2_program" required class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" required="">

                      <option>Select</option>

                      <?php foreach($campus_program as  $program2) {  ?>

                      <option value="<?php echo $program2['id']  ?>" <?php echo (@$prospects_data[0]['p2_program'] ==  $program2['id']) ? 'selected' : '' ?>>
                      <?php echo $program2['name']  ?></option>
                        <?php  } ?>

                    </select>

                  </div>

                 <!--  <div class="form-group">

                    <label class="control-label"><strong>Class</strong></label>

                    <select name="class_id" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                      <option>Select</option>

                      <?php foreach($classes as  $class) {  ?>

                      <option value="<?php echo $class['id']  ?>" <?php echo (@$prospects_data[0]['class_id'] ==  $class['id']) ? 'selected' : '' ?>>
                      <?php echo $class['class_name']  ?></option>
                        <?php  } ?>

                    </select>

                  </div>

                  <div class="form-group">

                    <label class="control-label"><strong>Subject</strong></label>

                    <select name="subject_id" class="form-control select2 " tabindex="-1" aria-hidden="true" id="country">

                      <option>Select</option>

                      <?php foreach($subjects as  $subject) {  ?>
<option value="<?php echo $subject['id']  ?>" <?php echo (@$prospects_data[0]['subject_id'] ==  $subject['id']) ? 'selected' : '' ?>>
                      <?php echo $subject['subject_name']  ?></option>
                        <?php  } ?>

                    </select>

                  </div> -->

                  <div class="form-group">

                    <label class="control-label"><strong>Study Mode</strong></label>

                    <select name="p1_study_mode" class="form-control select2 " tabindex="-1" aria-hidden="true">

                      <option>Select</option>

                      <?php foreach($study_mode as  $mode) {  ?>

                     <option value="<?php echo $mode['id']  ?>" <?php echo (@$prospects_data[0]['p1_study_mode'] ==  $mode['id']) ? 'selected' : '' ?>>
                      <?php echo $mode['name']  ?></option>
                        <?php  } ?>

                    </select>

                  </div>

                </div>

                <div class="col-md-12 pt-4">

                  <div class="float-left">

                    <button type="button" class="btn btn-secondary waves-effect btn-lg btnPrevious" >Previous</button>

                  </div>

                  <div class="float-right">

                    <button type="submit" class="btn btn-lg color_another" id="contact-form" >Save & Continue</button>

                  </div>

                </div>

              </div>

            </form>

          </div>



          <div class="tab-pane p-3 <?php if ($setup == 'step5') {echo 'active show';} else {echo '';}?>" id="settings2" role="tabpanel">
            <span id="image_name"></span><button id="upload_button" type="button" class="btn btn-dark waves-effect" data-toggle="modal" data-target="#document_application">Upload File</button>
            <div class="row">
              <div class="uploadrow">
              <h6>Documents </h6>
              
             <!--  <table class="table table-bordered">

                <thead class="black white-text">

                <tr>

                  <th scope="col">No</th>

                  <th scope="col">File Type</th>

                  <th scope="col">Name</th>
                  

                </tr>

                </thead>

                <tr role="row" class="odd">

                  <td>1</td>

                  <td class="sorting_1"><span class="text-danger">(Mandatory)</span> Gambar (1 copies)</td>

                  <td> <span id="image_name"></span><button type="button" class="btn btn-dark waves-effect" data-toggle="modal" data-target="#document_application">Upload File</button>
                  </td>
                </tr>
                   </table> -->
                </div>


                
             

                 <div class="document_row">
                 <table class="table table-bordered">
               
                
                <thead class="black white-text">

                <tr>

                  <th scope="col">No</th>
                  <th scope="col">Name</th>
                  <th scope="col">View | Download</th>

                </tr>
                 </thead>
               
                  <?php 
                  $i=1;
                  foreach ($documentUploadData as $key => $value) { ?>
                  <tr role="row" class="odd">
                  <td><?php echo $i; ?></td>
                  <td> 
                 <?php  print_r($value['name']);
                  ?></td>
                   <td class="image_classUpload">
                   <span id="targetLayer"></span>
      <a class="upload_image2" data-toggle="tooltip" data-placement="top" title="View" href=<?php echo "http://lmsmalaysia.com/uploads/".$value['name']; ?>>
        <i class="fa fa-eye" aria-hidden="true"></i>
        <!-- <img src="http://lmsmalaysia.com/public/images/download_icon.png"> -->
      </a>
      <a class="first_icon" download data-toggle="tooltip" data-placement="top" title="Download file" href="http://lmsmalaysia.com/uploads/<?php echo $value['name']; ?>"><i class="fas fa-download"></i></a>
                  </td>
                  </tr>
                <?php $i++; } ?>
                
               
                </table>
                </div>
               
               <div class="recently_uploaded">
                 <table class="table table-bordered">
               
                <h5>Recently Uploaded document</h5>
                <thead class="black white-text">

                <tr>

                  <th scope="col">No</th>
                  
                  <th scope="col">View</th>

                </tr>
                 </thead>
               
                  <tr role="row" class="odd">

                  <td>1</td>

                 

                 
                 
                  <td>
                   <span id="targetLayer"></span>
                   <a class="upload_image" data-toggle="tooltip" data-placement="top" title="view" href=""><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>



                </tr>
               
                </table>
                </div>
                 
              
              <div class="col-md-12">

                <div class="float-left">

                  <button type="button" class="btn btn-secondary waves-effect btn-lg btnPrevious" >Previous</button>

                </div>

                <div class="float-right">

                  <button type="button" class="btn btn-lg waves-effect color_another" onclick="step4()">Save & Continue</button>

                </div>

              </div>

              </div>

              </div>

              <div class="tab-pane p-3 <?php if ($setup == 'step6') {echo 'active show';} else {echo '';}?>" id="settings3" role="tabpanel">

              <form id="uploadForm1" action="upload.php" method="post">

              <div class="row">

              <h6>Upload Photo </h6>



              <div class="col-md-3 offset-md-2">

              <div class="form-group">

              <div class="text-center">

              <h6>Preview</h6>

              </div>

              <div class="main-img-preview"><?php  
                if (empty($prospects_data[0]['profile_pic'])) { ?>
                  <img class="thumbnail img-preview" src="http://lmsmalaysia.com/public/images/no-image.png" title="Preview Logo">
                <?php } 
                else{ ?>
                  <img class="thumbnail img-preview" src="<?php echo base_url('uploads/' .$prospects_data[0]['profile_pic']); ?>" title="Preview Logo">
               <?php }
              ?>

                

              </div>

              <div class="input-group">

                <div class="input-group-btn text-center" style="width: 100%;">

                  <div class="fileUpload btn btn-warning fake-shadow">

                    <span ><i class="glyphicon glyphicon-upload"></i> Upload Photo</span>

                    <input id="logo-id" name="logo" type="file" class="attachment_upload">

                  </div>

                </div>

              </div>

              </div>

            </div>

            <div class="col-md-12">

              <div class="float-left">

                <button type="button" class="btn btn-secondary waves-effect btn-lg btnPrevious" >Previous</button>

              </div>

              <div class="float-right">

                <button type="submit" class="btn btn-lg waves-effect color_another" onclick="step5()">Save & Continue</button>

              </div>

              </form>

            </div>

          </div>

        </div>

      <div class="tab-pane p-3 <?php if ($setup == 'step7') {echo 'active show';} else {echo '';}?>" id="settings4" role="tabpanel">

         

      <div class="col-12">

        <div class="card m-b-30">

          <div class="card-body">

            <div id="prospect"></div>

            <div style="clear:both"></div>

            <div class="hearding">

              <h4 class="mt-0 header-title left_side">Personal Information</h4>

            </div>

            <div class="clearfix"></div>

          <hr>

          <div class="row">

          <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>

                  <tr>

                    <th><strong>ID No</strong> </th>

                    <td><?php echo $prospects_data[0]['id_no'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>ID Type</strong></th>

                    <td><?php echo ($prospects_data[0]['idtype'] == 1) ? 'Passport' : 'Identity Card'; ?></td>

                  </tr>

                  <tr>

                    <th><strong>Name</strong></th>

                    <td><?php echo $prospects_data[0]['name'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Secondary Name</strong></th>

                    <td><?php echo $prospects_data[0]['secondary_name'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Salutation</strong></th>

                    <!-- <td><?php echo $prospects_data[0]['salutation'] ?></td> -->
                     <td><?php if ($prospects_data[0]['gender']=='m' || $prospects_data[0]['gender']=="M") {
                      echo "Mr";
                    }else{
                    	echo ($prospects_data[0]['marital'] == 1) ? 'Miss' : 'Mrs';
                    }?></td>

                  </tr>

                  <tr>

                    <th><strong>Gender</strong></th>

                    <td><?php if ($prospects_data[0]['gender']=='m' || $prospects_data[0]['gender']=="M") {
                      echo "Male";
                    }else{
                      echo "Female";
                    }?></td>
                    

                  </tr>

                </tbody>

              </table>

          </div>

          <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>



                  <tr>

                    <th><strong>Email</strong></th>

                    <td><?php echo $prospects_data[0]['email'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Secondary Email</strong></th>

                    <td><?php echo $prospects_data[0]['secondary_email'] ?> </td>

                  </tr>

                  <tr>

                    <th><strong>Phone</strong></th>

                    <td><?php echo $prospects_data[0]['phone'] ?></td>

                  </tr>

                   <tr>

                    <th><strong>Emergency Contact</strong></th>

                    <td><?php echo $prospects_data[0]['emergency_contact'] ?></td>

                  </tr>

                 

                  <tr>

                    <th><strong>International</strong></th>

                    <td><?php echo ($prospects_data[0]['international'] == 1 || $prospects_data[0]['international'] == y ) ? 'International' : 'Local'; ?></td>

                  </tr>

                  <tr>

                    <th><strong>Nationality</strong></th>

                    <td><?php echo $nation_id[0]['Nationality'] ?></td>

                  </tr>

                </tbody>

              </table>

          </div>

          <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>



                  <tr>

                    <th><strong>Race</strong></th>

                    <td><?php echo $race_id[0]['name'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Religion</strong></th>

                    <td><?php echo $religion_id[0]['name'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Place of Birth</strong></th>

                    <td><?php echo $prospects_data[0]['place_of_birth'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Date of Birth</strong></th>

                    <td><?php echo $prospects_data[0]['date_Of_birth'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Marital Status</strong></th>

                    <td><?php echo ($prospects_data[0]['marital'] == 1) ? 'Single' : 'Married'; ?></td>

                  </tr>

                  <tr>

                    <th><strong>Disability</strong></th>
                    <td><?php if ($prospects_data[0]['disability']==1) {
                     echo "Disabled";
                    }
                     else{
                        echo "None";
                     }  ?></td>

                    <!-- <td><?php echo ($prospects_data[0]['disability'] == 1) ? 'Yes' : 'None'; ?></td> -->

                  </tr>

                </tbody>

              </table>

          </div>

      </div>



  </div>

</div>

<div class="card m-b-30">

  <div class="card-body">

      <div id="prospect"></div>

      <div style="clear:both"></div>

      <div class="hearding">

              <h4 class="mt-0 header-title left_side">Course Preference</h4>

          </div>

          <div class="clearfix"></div>

          <hr>

          <div class="row">

             <div class="col-md-4">

              <!-- <span>Course Choice 1</span> -->

              <table class="table table-striped table-borderless">

                <tbody>

                  <tr>

                    <th><strong>Campus</strong></th>

                    <td> <?php echo @$campus[$prospects_data[0]['p1_campus']]; ?></td>

                  </tr>

                  <tr>

                    <th><strong>Intake</strong></th>
                    <td><?php foreach ($intakeP as $key => $value11) {
                                                        if ($value11['id']==$prospects_data[0]['intake']) {
                                                            print_r($value11['intake']);
                                                        }
                                                       
                                                    } ?></td>

                    <!-- <td> <?php  print_r($prospects_data[0]['intake']);  ?></td> -->

                  </tr>



                </tbody>

              </table>

          </div>

          <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>

                  <tr>
                   
                    <th><strong>Program 1</strong></th>
                        <td> 
                    <?php

                                                        $get_program1 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p1_program']);

                                                        $get_program2 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p2_program']);

                                                        $get_program3 = $this->CI->get_data_by_id('programs','id',$prospects_data[0]['p3_program']);

                                                        ?>
                    <?php if($prospects_data[0]['p1_program'] != '' ) { echo @$get_program1['name'];} else  { echo 'Not Availble'; } ?>
                  </td>
                  </tr>
                  <tr>
                    <th><strong>Program 2</strong></th>
                      <td>
                       <?php if($prospects_data[0]['p2_program'] != '' ) { echo @$get_program2['name'];} else  { echo 'Not Availble'; } ?>
                      </td>
                  </tr>

                </tbody>

              </table>

          </div>
              <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>
                  <tr>

                    <th><strong>Study Mode</strong></th><!-- <?php print_r($prospects_data); echo "string222"; ?> -->

                    <td><?php echo @$study[$prospects_data[0]['p1_study_mode']]; ?></td>

                  </tr>

                </tbody>

              </table>

          </div>

          </div>

      </div>

  </div>

  <div class="card m-b-30 ">

  <div class="card-body">

      <div id="prospect"></div>

      <div style="clear:both"></div>

      <div class="hearding">

              <h4 class="mt-0 header-title left_side">Address</h4>

          </div>

          <div class="clearfix"></div>

          <hr>

          <div class="row">

             <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>

                  <tr>

                    <th><strong>Address Line 1</strong></th>

                    <td><?php echo $prospects_data[0]['address_line1'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Address Line 2</strong></th>

                    <td><?php echo $prospects_data[0]['address_line2'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>Country</strong></th>

                    <td><?php echo $country_id[0]['name'] ?></td>

                  </tr>



                  </tr>

                </tbody>

              </table>

          </div>

          <div class="col-md-4">

              <table class="table table-striped table-borderless">

                <tbody>

                    <th><strong>State</strong></th>

                    <td><?php echo $state_id[0]['name'] ?></td>

                  </tr>

                  <tr>

                    <th><strong>City</strong></th>

                    <td><?php echo $prospects_data[0]['city'] ?> </td>

                  </tr>

                  <tr>

                    <th><strong>Postcode</strong></th>

                    <td><?php echo $prospects_data[0]['postcode'] ?> </td>

                  </tr>

                </tbody>

              </table>

          </div>

          </div>

      </div>

  <!-- </div> -->



<div class="card m-b-30">



</div>

</div>

<div class="text-center">

<button type="button" class=" btn color_another btn-lg waves-effect" data-toggle="modal" data-target="#myModal">Save &amp; Continue</button>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

<footer class="footer">Copyright© 2019 LMS<span class="d-none d-sm-inline-block"></span>.</footer>

</div>

</div>

<script src="<?php echo JS_PATH ?>js/jquery.min.js"></script>

<script src="<?php echo JS_PATH ?>js/bootstrap.bundle.min.js"></script>

<script src="<?php echo JS_PATH ?>js/metisMenu.min.js"></script>

<script src="<?php echo JS_PATH ?>js/jquery.slimscroll.js"></script>

<script src="<?php echo JS_PATH ?>js/waves.min.js"></script>

<script src="<?php echo PLUG_PATH ?>jquery-sparkline/jquery.sparkline.min.js"></script>

<!-- Plugins js -->

<script src="<?php echo PLUG_PATH ?>bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<script src="<?php echo PLUG_PATH ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script src="<?php echo PLUG_PATH ?>select2/js/select2.min.js"></script>

<script src="<?php echo PLUG_PATH ?>bootstrap-maxlength/bootstrap-maxlength.min.js"></script>

<script src="<?php echo PLUG_PATH ?>bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script>

<script src="<?php echo PLUG_PATH ?>bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

<!-- Plugins Init js -->

<script src="<?php echo CSS_PATH ?>pages/form-advanced.js"></script>

<!-- App js -->

<script src="<?php echo JS_PATH ?>js/app.js"></script>

<!-- Dropzone js -->

<script src="<?php echo PLUG_PATH ?>dropzone/dist/dropzone.js"></script>

<script type="text/javascript">

$(document).ready(function() {

var brand = document.getElementById('logo-id');

brand.className = 'attachment_upload';

brand.onchange = function() {

document.getElementById('fakeUploadLogo').value = this.value.substring(12);

};



// Source: http://stackoverflow.com/a/4459419/6396981

function readURL(input) {

if (input.files && input.files[0]) {

var reader = new FileReader();



reader.onload = function(e) {

$('.img-preview').attr('src', e.target.result);

};

reader.readAsDataURL(input.files[0]);

}

}

$("#logo-id").change(function() {

readURL(this);

});

});





</script>

<style type="text/css">



.avoid-clicks {

pointer-events: none;

}

.form-control, .thumbnail {

border-radius: 2px;

}

.btn-danger {

background-color: #B73333;

}



/* File Upload */

.fake-shadow {

box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);

}

.fileUpload {

position: relative;

overflow: hidden;

}

.fileUpload #logo-id {

position: absolute;

top: 0;

right: 0;

margin: 0;

padding: 0;

font-size: 33px;

cursor: pointer;

opacity: 0;

filter: alpha(opacity=0);

}

.img-preview {

max-width: 100%;

width: 100%;

}

.thumbnail {

display: block;

padding: 4px;

margin-bottom: 20px;

line-height: 1.428571429;

background-color: #fff;

border: 1px solid #ddd;

border-radius: 4px;

-webkit-transition: all .2s ease-in-out;

transition: all .2s ease-in-out;

}

.personal h6 {

background-color: #4856a4;

text-transform: uppercase;

border-top-right-radius: 5px;

border-top-left-radius: 5px;

}



</style>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>





function submit_form(){

var data_to_send = $('#personalformData').serialize();

var id =   '<?php echo $id; ?>';

if(id === undefined || id == null || id.length <= 0 || id =='') {

var url  =  '<?php echo base_url(); ?>application/add_app1';



} else  {



var url  =  '<?php echo base_url(); ?>application/add_app1/'+id;

}

$.ajax({

url: url,

type : 'POST',

data : data_to_send,

datatype : 'html',

success : function(data)

{   alert('Information submit successfully');

$('#prospects_id').val(data);

$('#prospects_id1').val(data);

$('#home1').removeClass('active');

$('#form_id').val(data);

$('#profile1').addClass('active');

$('#past_education').removeClass('avoid-clicks');

$('#past_education').addClass('active show');

$('#personal').removeClass('active show');



}

});

}





function program_submit_form(){

var data_to_send = $('#program_submit_form').serialize();



$.ajax({

url: "<?php echo base_url(); ?>application/add_program",

type : 'POST',

data : data_to_send,

datatype : 'html',

success : function(data)

{  var id = $('#prospects_id').val();

var id = '<?php echo $id; ?>';

window.location.href = "http://phpslick.com/lms/index.php/application/add_app/"+id+"/step5";





},

error: function (error) {

alert('error');

$('#home1').removeClass('active');

$('#profile1').addClass('active');

$('#past_education').removeClass('avoid-clicks');

$('#past_education').addClass('active show');

$('#personal').removeClass('active show');

}

});

}











function contact_submit_form(){

var data_to_send = $('#contact_formData').serialize();

var id= '<?php echo $id; ?>';

var action = '<?php echo $action; ?>';

alert(id);

alert(action);

$.ajax({

url: "<?php echo base_url(); ?>application/add_contact",

type : 'POST',

data : data_to_send,

datatype : 'html',

success : function(data)

{

var id = $('#prospects_id').val();

window.location.href = "http://phpslick.com/lms/index.php/application/add_app/"+id+"/step3/"+action;

}

});

}





/*function submit_eduction_form(){ alert('teste');

var data_to_send = $('#educationformData').serialize();



$.ajax({

url: "<?php echo base_url(); ?>application/add_app",

type : 'POST',

data : data_to_send,

datatype : 'html',

success : function(data)

{



}

});

}*/

function past_education(obj) {



if(obj == '8') {

var ed = 'Degree';

}

if( obj == '7') {

var ed = 'Diploma';

} if( obj == '9') {

var ed = 'Masters';



}

$('#add_eduction').prop('disabled', false);

$('#education_id').val(obj);

$('#post_job_qua').text(ed);

}

function step1() {

$('#profile1').removeClass('active');

$('#messages1').addClass('active');

$('#contact').removeClass('avoid-clicks');

$('#contact').addClass('active show');

$('#profile1').removeClass('active show');



}

function step2 () {

$('#messages1').removeClass('active');

$('#settings1').addClass('active');

$('#program').removeClass('avoid-clicks');

$('#program').addClass('active show');

$('#messages1').removeClass('active show');



}



function step4() {

$('#settings2').removeClass('active');

$('#settings3').addClass('active');

$('#upload_photos').removeClass('avoid-clicks');

$('#upload_photos').addClass('active show');

$('#documents').removeClass('active show');

//window.location.href = "http://stackoverflow.com";



}

function step3 () {

$('#messages1').removeClass('active');

$('#settings1').addClass('active');

$('#program').removeClass('avoid-clicks');

$('#program').addClass('active show');

$('#messages1').removeClass('active show');





}



function step5 () {

$('#settings3').removeClass('active');

$('#settings4').addClass('active');

$('#review').removeClass('avoid-clicks');

$('#review').addClass('active show');

$('#upload_photos').removeClass('active show');



}









$(document).ready(function (e){

 

var setup = '<?php echo $setup; ?>';

var id  =   '<?php echo $id; ?>';



if(id == '' && setup =='') {

$('#past_education').addClass('disabled');

$('#contact').addClass('disabled');

$('#program').addClass('disabled');

$('#documents').addClass('disabled');

$('#upload_photos').addClass('disabled');

$('#review').addClass('disabled');

}  else  {

$('#past_education').removeClass('disabled');

$('#contact').removeClass('disabled');

$('#program').removeClass('disabled');

$('#documents').removeClass('disabled');

$('#upload_photos').removeClass('disabled');

$('#review').removeClass('disabled');



}
$("#upload_button").click(function(){
    $("#contact-form1").removeAttr("disabled");
     $("#userImage").val();
});
$("#uploadForm").on('submit',(function(e){

alert('Please wait for a minute to upload a document');
$('#contact-form1').prop('disabled', true);

$('#cans').trigger('click');



e.preventDefault();

$.ajax({

url: "<?php echo base_url('application/add_document/' . $id . '/step6'); ?>",

type: "POST",

data:  new FormData(this),

contentType: false,

cache: false,

processData:false,

success: function(data){
alert("Document has been uploaded successfully");


$(".recently_uploaded").show();




$(".upload_image").attr("href", data);


},

error: function(){}

});

}));

});






$(document).ready(function (e){

$('.btnPrevious').click(function(){





$('.nav-tabs .nav-item > .active').parent().prev('.nav-item').find('a')[0].click();



});







$('#educatiion_level').on('change', function(){ alet('hkhkhk');

$('#education_id').val($(this).val());

$('#add_eduction').prop('disabled', false);

});

$("#uploadForm1").on('submit',(function(e){

var url =  '<?php echo base_url(); ?>';

e.preventDefault();

var id = '<?php echo $id; ?>';

$.ajax({

url: "<?php echo base_url('application/add_profile/' . $id . '/step5'); ?>",

type: "POST",

data:  new FormData(this),

contentType: false,

cache: false,

processData:false,

success: function(data){



},

error: function(){}

});

}));

});

</script>

<script>

function previous(n){

location.href="http://phpslick.com/lms/index.php/application/add_app/"+id+"/"+n;

}
$(document).ready(function() {



 $('#contactcountry').change(function() {

     var id  =  $(this).val();



      // alert(id);

     $.ajax({

      type: "POST",

      url: "<?php echo base_url('application/get_state') ?>",

      data: {'id' : id },

      success:  function(data){

      //  alert(data);

        $('#state').html(data);

        //window.location.reload();

        

      }

    });

   

  })

  

})


</script>

</body>

</html>

